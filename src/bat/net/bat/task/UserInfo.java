package net.bat.task;

import java.util.HashMap;

public class UserInfo {
    public static final String SESSION_KEY = "UserInfo";	
	private HashMap<String,Task> task_map=new HashMap<String,Task>();
	public HashMap<String, Task> getTask_map() {
		return task_map;
	}
	public void setTask_map(HashMap<String, Task> taskMap) {
		task_map = taskMap;
	}
	public void addTask(Task t){
		task_map.put(t.getId(), t);
	}
	public void removeTask(Task t){
		String tid = t.getId();
		if(task_map.containsKey(tid))
			task_map.remove(tid);
	}
}
