package net.bat.task;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import net.bat.service.IHandle;
import net.bat.util.Cfg;
import net.cs.um.UMService;
import net.sk.pt.IConsumer;


public class TaskHandle implements IHandle {
	public static final char METHOD_NEWTASK ='N';
	public static final char METHOD_GETTOTAL ='T';
	public static final char METHOD_GETCUR ='P';
	public static final char METHOD_CANCEL ='C';
	
	public String newTask(String sid){
		try {
			return TaskCenter.newTask(sid);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	static public int getTotal(HashMap<String,Object> req){
		String tid = (String)req.get("tid");
		return (int)TaskCenter.getTotal(tid);
	}
	static public String[] getCur(HashMap<String,Object> req){
		String[] rs = new String[3];
		String tid = (String)req.get("tid");
		rs[2]=""+TaskCenter.getTotal(tid);
		rs[1]=TaskCenter.getTaskInfo(tid);
		rs[0]=""+TaskCenter.getCur(tid);
		return rs;
	}
	static public void cancel(HashMap<String,Object> req){
		String tid = (String)req.get("tid");
		TaskCenter.cancel(tid);
	}
	@Override
	public Object handle(char method, HashMap<String, Object> rm,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		switch(method){
		case METHOD_NEWTASK:
			String sid = request.getSession().getId();
			return newTask(sid);
		case METHOD_GETTOTAL:
			return getTotal(rm);
		case METHOD_GETCUR:
			return getCur(rm);
		case METHOD_CANCEL:
			cancel(rm);
			break;
		}
		return null;
	}
	
	

}
