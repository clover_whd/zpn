package net.bat.task;

	import java.text.SimpleDateFormat;
import java.util.*;

import net.cs.um.UMService;
import net.sk.pt.IConsumer;


	/**
	 * 长周期任务管理
	 * @author chen4w
	 * @version 1.0
	 */
	public class TaskCenter {
		static final long MAX_ACCESS_SPAN=60000;
		static SimpleDateFormat fmt = new SimpleDateFormat("yyyy_MM_dd_");
		static final int TASKIDMAX=10000;
		/**
		 * 异常结束标志
		 */
		static long ERRCODE=-2;
		/**
		 * 递增任务标识 
		 */
		static private long taskId=1;
		/**
		 * 任务队列
		 */
		static public HashMap<String, Task> taskmap=new HashMap<String, Task>();
		
		public TaskCenter(){
			long l = 24*60*60*1000; 
			Date date = new Date(); 
			long ldt = date.getTime();
			long lid = ldt % l;
			taskId=lid;
		}
		private static void removeTask(Task t){
			taskmap.remove(t);
		}
	    
	    public static boolean isTimeout(String tid){
	    	if(tid==null)
	    		return false;
			Task tk = (Task)taskmap.get(tid);
			if(tk!=null){
				Date lastAccess = tk.getLastAccess();
				Date curdt = new Date();
				long span = curdt.getTime()-lastAccess.getTime();
				if(span<MAX_ACCESS_SPAN)
					return false;
			}
	    	return true;
	    }

		/**
		 * 获得任务标识名
		 * @param val 标识序号
		 * @return 标识名
		 */
		static private String strId(long val){
			return fmt.format(new Date())+val;
		}
		/**
		 * 请求并获得一个任务
		 * @return 任务对象
		 * @throws Exception 
		 */
		static public String newTask(String sid) throws Exception{
			taskId++;
			//if(taskId>TASKIDMAX)
			//	taskId=0;
			int uid = UMService.getUidBySid(sid);
			String tid=uid+"_"+strId(taskId);
			Task cur = new Task(tid,sid);
			taskmap.put(tid,cur);
			return tid;
		}
		/**
		 * 设置指定任务总长度
		 * @param tid 任务序号
		 * @param val 总长度
		 */
		static public void setTotal(String tid,long val){
			if(tid==null)
				return;
			Task tk = (Task)taskmap.get(tid);
			if(tk!=null)
				tk.setTotal(val);
			//Cfg.updateState(tk, null, State.ACTION_ADD);
		}
		/**
		 * 设置指定任务为出错状态
		 * @param tid 任务序号
		 */
		static public void setErr(String tid){
			Task tk = (Task)taskmap.get(tid);
			if(tk!=null){
				tk.setCur(ERRCODE);	
				//将来可以对taskmap中的长时间无响应task进行释放
				//tk.setLastUpdate();
				//Cfg.updateState(tk, null, State.ACTION_REMOVE);
			}
		}
		static public void setTaskInfo(String tid,String info){
			if(tid==null)
				return;
			Task tk = (Task)taskmap.get(tid);
			if(tk!=null){
				tk.setInfo(info);	
				//Cfg.updateState(tk, new String[]{"taskInfo"}, State.ACTION_UPDATE);
			}
		}
		static public Task getTask(String tid){
			return taskmap.get(tid);
		}
		static public String getTaskInfo(String tid){
			Task tk = (Task)taskmap.get(tid);
			if(tk!=null)
				return tk.getInfo();
			else
				return null;
			
		}
		/**
		 * 设置指定任务进度
		 * @param tid 任务序号
		 * @param cur 任务进度
		 */
		static public void setCur(String tid,long cur){
			if(tid==null)
				return;
			Task tk = (Task)taskmap.get(tid);
			if(tk!=null){
				tk.setCur(cur);	
				//Cfg.updateState(tk, new String[]{"cur"}, State.ACTION_UPDATE);
				//将来可以对taskmap中的长时间无响应task进行释放
				//tk.setLastUpdate();
				if(cur==tk.getTotal()){
					removeTask(tk);
					//Cfg.updateState(tk,null, State.ACTION_REMOVE);
				}
			}
		}
		/**
		 * 取消任务
		 * @param tid 任务序号
		 */
		static public void cancel(String tid){
			Task tk = (Task)taskmap.get(tid);
			if(tk!=null){
				removeTask(tk);			
				//Cfg.updateState(tk,null, State.ACTION_REMOVE);
			}
		}
		/**
		 * 获得指定任务总长度
		 * @param tid 任务序号
		 * @return 指定任务总长度
		 */
		static public long getTotal(String tid){
			Task tk = (Task)taskmap.get(tid);
			if(tk!=null)
				return tk.getTotal();
			return -1;
		}
		/**
		 * 获得指定任务进度
		 * @param tid 任务序号
		 * @return 指定任务进度
		 */
		 static public long getCur(String tid){
			Task tk = (Task)taskmap.get(tid);
			if(tk!=null){
				tk.setLastAccess(new Date());
				long cur= tk.getCur();
				//js端自行释放
				if(cur==tk.getTotal()||cur==ERRCODE)//-2表示异常结束
					removeTask(tk);
				return cur;
			}
			return -1;
		}
		/**
		 * 将指定任务状态置为已完成
		 * @param tid 任务序号
		 */
		static public void done(String tid){
			Task tk = (Task)taskmap.get(tid);
			if(tk!=null){
				removeTask(tk);
				//Cfg.updateState(tk, new String[]{"cur"}, State.ACTION_REMOVE);
			}
		}
		/**
		 * 指定任务是否已取消
		 * @param tid 任务序号
		 * @return 任务是否已取消
		 */
		static public boolean isCancel(String tid){
			if(tid==null)
				return false;
			Task tk = (Task)taskmap.get(tid);
			if(tk!=null)
				return  false;
			return true;
		}

}
