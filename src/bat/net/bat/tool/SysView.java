package net.bat.tool;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Vector;

import net.bat.db.BaseDAOImpl;

import org.hibernate.HibernateException;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.persister.entity.SingleTableEntityPersister;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 
 * @author Clover_whd
 * 简易视图输出工具
 */
public class SysView {

	public static LinkedHashMap map1=new LinkedHashMap();
	public static Scanner scan = new Scanner(System.in);
	
	public static void main(String[] args) throws HibernateException, ClassNotFoundException {
		
		    ApplicationContext applicationContext = new ClassPathXmlApplicationContext("/applicationContext-common.xml");	        
		    BaseDAOImpl dao = (BaseDAOImpl) applicationContext.getBean("BaseDAOImpl");
		    
			org.hibernate.SessionFactory sf =dao.getSessionFactory();
			Map map = sf.getAllClassMetadata();
			Iterator it = map.entrySet().iterator();  //所有类名
			Vector<String[]> rl = new Vector<String[]>();
			Map<String,String[]>  proMap=new HashMap<String,String[]>();
			while (it.hasNext()) {
				Map.Entry entry = (Map.Entry) it.next();
				String key_en = (String) entry.getKey();
				ClassMetadata classMetadata = sf.getClassMetadata(Class.forName(key_en));
				String[] propertyNames = classMetadata.getPropertyNames();
				
				SingleTableEntityPersister value=(SingleTableEntityPersister)entry.getValue();
				            
				
				SingleTableEntityPersister obj=(SingleTableEntityPersister) map.get(key_en);
				proMap.put(key_en, propertyNames);
			}
			String tabName="";
			String [] prop=null;
			System.out.println("请输入要生成视图的表名:");
			if (scan.hasNext()) {
				tabName = scan.next();
				if(tabName!=null){
					prop=proMap.get(tabName);
				}
			}
			
			String pssString="";
		    for(int i=0;i<prop.length;i++){
		    	String propName="";
		    	if(prop[i]!=null){
		    		pssString+="\""+prop[i]+"\",";
		    	}
		    	
		    }
			if(pssString!="")
				pssString=pssString.substring(0,pssString.length()-1);
			    
			PrintWriter pw;
			try {
				pw = new PrintWriter("E:/tool/View.dat");
		
			
				pw.printf("%s\n","\""+tabName+"\":{");				
			    pw.printf("%s\n","  req:{");
			    pw.printf("%s\n","    \"pss:["+pssString+"],");
			    pw.printf("%s\n","    \"cns\":[\""+tabName+"\"],");       //"cns" : ["dao.hb.MbMz"],
			    pw.printf("%s\n","    \"hql\":\"\"");
			    pw.printf("%s\n","  },");
			    
			    pw.printf("%s\n","  cfg_grid:{");
			    pw.printf("%s\n","     btns:[]");
			    pw.printf("%s\n","  },");
			    
			    pw.printf("%s\n","  cfg_prop:{");
		    
		    for(int i=0;i<prop.length;i++){
		    	String propName="";
		    	if(prop[i]!=null){
		    		propName=prop[i];
		    	}
		    	int size=i;
		    	pw.printf("%s\n","    \""+propName+"\":{");
		    	pw.printf("%s\n","        display:\"\",");
		    	pw.printf("%s\n","        editor:\"\"");
		    	pw.printf("%s","     }");
		    	if((size+1)<prop.length){
		    		pw.printf("%s\n",",");
		    	}
		    }
			    pw.println();
			    pw.printf("%s\n","  }");
			    
			    
			    
			    pw.printf("%s\n","}");
			    
	            pw.println();
	            pw.println();
	
				pw.flush();
				pw.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
	}
}
