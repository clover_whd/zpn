package net.bat.service.cb;

import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import net.bat.db.BaseDAOImpl;
import net.bat.pt.BObj;
import net.cs.um.UMService;
import service.ZpService;
import dao.hb.PinDataset;
import dao.hb.PinDatasetPerson;
import dao.hb.UUser;

public class CBPinDataset implements IHandleHook {
	@Autowired
	private BaseDAOImpl dao;

	@Override
	public void beforeHandle(char method, HashMap<String, Object> psMap,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		BObj[] objs = (BObj[])psMap.get("objs");
		String[] cns = (String[])psMap.get("cns");
		for(int i=0; i<objs.length; i++){
			BObj cur = objs[i];
			HashMap<String,Object>  its = cur.getItems();
			switch(cur.getAction()){
			case BObj.ACTION_REMOVE:
				Integer bm_id = (Integer)its.get("A.id");		
				dao.updateByQuery("delete from PinDatasetPerson where datasetId=?",
						new Object[]{bm_id});
				break;
			}
		}
	}

	@Override
	public void afterHandle(char method, HashMap<String, Object> psMap,
			HttpServletRequest request, Object result) throws Exception {
		// TODO Auto-generated method stub
		Object[][] rt = (Object[][] )result;
		UUser usr = UMService.getUserBySession(request);
		for(int i=0; i<rt.length; i++){
			PinDataset po = (PinDataset)rt[0][0];
			if(!po.getUsername().equals(usr.getUserId())){
				throw new Exception("不允许修改非本人建立的条目");
			}
		}
		
	}

}
