package net.bat.service.cb;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import net.bat.db.BaseDAOImpl;
import net.bat.pt.BObj;
import net.bat.util.Cfg;

public class CBUOrg implements IHandleHook{
	@Override
	public void beforeHandle(char method, HashMap<String, Object> psMap,
			HttpServletRequest request) throws Exception {

		BObj[] objs = (BObj[])psMap.get("objs");
		for(int i=0; i<objs.length; i++){
			BObj cur = objs[i];
			HashMap<String,Object>  its = cur.getItems();
			String actionStr=String.valueOf(objs[i].getAction());
			if(actionStr.equals("R")){
				Object idObj=its.get("id");
				if(idObj!=null){
					BaseDAOImpl baseDao = (BaseDAOImpl) Cfg.getBean("BaseDAOImpl");	
					String sql="update U_USER set OID=''  where OID='"+idObj+"'";	
					baseDao.executeSQL(sql);
				}				
				
			}
			
		}
	
		
	}
	@Override
	public void afterHandle(char method, HashMap<String, Object> psMap,
			HttpServletRequest request, Object result) throws Exception {}
}
