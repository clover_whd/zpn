package net.bat.service.cb;

import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import service.ZpService;
import dao.hb.PinDataset;
import dao.hb.PinDatasetPerson;
import net.bat.db.BaseDAOImpl;
import net.bat.pt.BObj;
import net.bat.util.Cfg;

public class CBPinDatasetPerson implements IHandleHook {
	@Autowired
	private BaseDAOImpl dao;

	@Override
	public void beforeHandle(char method, HashMap<String, Object> psMap,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		BObj[] objs = (BObj[])psMap.get("objs");
		String[] cns = (String[])psMap.get("cns");
		if(cns.length<2 || !cns[1].equals("dao.hb.PinPerson")){
			return;
		}
		for(int i=0; i<objs.length; i++){
			BObj cur = objs[i];
			HashMap<String,Object>  its = cur.getItems();
			switch(cur.getAction()){
			//不允许删除已归档的编目条目
			case BObj.ACTION_REMOVE:
				Integer bm_id = (Integer)its.get("B.id");		
				if(bm_id!=null){
					its.put("B.id", null);
				}
				break;
			}
		}
	}

	@Override
	public void afterHandle(char method, HashMap<String, Object> psMap,
			HttpServletRequest request, Object result) throws Exception {
		// TODO Auto-generated method stub
		Object[][] rt = (Object[][] )result;
		PinDatasetPerson po = (PinDatasetPerson)rt[0][0];
		int pid = po.getDatasetId();
		PinDataset pd = (PinDataset)dao.loadPO(ZpService.EN_DATASET, pid+"");
		if(pd!=null){
			pd.setTotal(dao.getCountByQuery(ZpService.HQL_DATASET_COUT, new Object[]{pid}));
			pd.setDtModify(new Date());
			dao.update(pd);
		}
	}

}
