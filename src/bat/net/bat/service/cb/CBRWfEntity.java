package net.bat.service.cb;

import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import dao.hb.UUser;
import net.bat.db.BaseDAOImpl;
import net.bat.pt.BObj;
import net.bat.service.HDDefault;
import net.bat.util.Cfg;
import net.cs.um.UMService;
import net.sk.pt.State;
import net.sk.pt.StateUtil;
import net.sk.web.StateCenter;

public class CBRWfEntity implements IHandleHook {
	
	
	private void pah(char method, HashMap<String, Object> psMap,
			HttpServletRequest request, Object result) throws Exception {
		// TODO Auto-generated method stub
		//向浏览器推送状态/无法处理多cn和非id主键
		BObj[] objs = (BObj[])psMap.get("objs");
		Object[][] rt = (Object[][] )result;
		BaseDAOImpl bdi = (BaseDAOImpl) Cfg.getBean("BaseDAOImpl");
		for(int i=0; i<objs.length; i++){
			BObj cur = objs[i];
			//获取保存之后的主键
			if(cur.getAction()==BObj.ACTION_ADD){
				Object rti1 = rt[i][1];
				Object rti0 = rt[i][0];
				BeanWrapper bw1 = new BeanWrapperImpl(rti1);
				BeanWrapper bw0 = new BeanWrapperImpl(rti0);
				bw0.setPropertyValue("entityId", bw1.getPropertyValue("id"));
				bdi.save(rti0);
			}
		}		
		
	}

	@Override
	public void beforeHandle(char method, HashMap<String, Object> psMap,
			HttpServletRequest request) throws Exception {
		
		UUser usr = UMService.getUserBySession(request);
		int userid = usr.getId();
		BaseDAOImpl bdi = (BaseDAOImpl) Cfg.getBean("BaseDAOImpl");
		BObj[] objs = (BObj[])psMap.get("objs");
		for(int i=0; i<objs.length; i++){
			BObj cur = objs[i];
			HashMap<String,Object>  its = cur.getItems();
			
			if(cur.getAction()==BObj.ACTION_UPDATE){
				
				
			}
		}
		
	}

	@Override
	public void afterHandle(char method, HashMap<String, Object> psMap,
			HttpServletRequest request, Object result) throws Exception {
		// TODO Auto-generated method stub
		switch(method){
		case HDDefault.METHOD_PUT:
			pah(method,psMap,request,result);
			break;
		}

	}

}
