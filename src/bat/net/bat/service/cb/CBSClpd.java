package net.bat.service.cb;

import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import dao.hb.UUser;

import net.bat.pt.BObj;
import net.cs.um.UMService;

public class CBSClpd implements IHandleHook {

	@Override
	public void beforeHandle(char method, HashMap<String, Object> psMap,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		Date dt = new Date();
		UUser usr = UMService.getUserBySession(request);
		BObj[] objs = (BObj[])psMap.get("objs");
		for(int i=0; i<objs.length; i++){
			BObj cur = objs[i];
			HashMap<String,Object>  its = cur.getItems();
			switch(cur.getAction()){
			case BObj.ACTION_UPDATE:
				if(its.containsKey("pdjl")){
					its.put("useridpd", usr.getId());
					its.put("fhrq", dt);
					its.put("status", 3);
				}else{
					its.put("dtModify", dt);
				}
				break;
			case BObj.ACTION_ADD:
				its.put("dtCreate",dt);
				its.put("sqrq",dt);
				its.put("userid", usr.getId());
				break;
			}
		}

	}

	@Override
	public void afterHandle(char method, HashMap<String, Object> psMap,
			HttpServletRequest request, Object result) throws Exception {
		// TODO Auto-generated method stub

	}

}
