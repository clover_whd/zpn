package net.bat.service.cb;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.EntityMode;

import net.bat.db.BaseDAOImpl;
import net.bat.pt.BObj;
import net.bat.util.Cfg;
import net.cs.um.UMService;
import dao.hb.CcSync;
import dao.hb.NBase;
import dao.hb.NImage;
import dao.hb.PinDataset;
import dao.hb.PinDatasetPerson;
import dao.hb.PinMajor;
import dao.hb.UUser;

public class CBPinMajor implements IHandleHook {

	final static String pn_review = "status";
	final static SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd:hh:mm:ss");
	
	@Override
	public void beforeHandle(char method, HashMap<String, Object> psMap,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("beforeHandle, ,method="+method);
		
	}

	@Override 
	public void afterHandle(char method, HashMap<String, Object> psMap,
			HttpServletRequest request, Object result) throws Exception {
//method, rm, request, result  [method:P][{rownum=15, pss=[[Ljava.lang.String;@123a47c, objs=[Lnet.bat.pt.BObj;@103ddfd, cns=[Ljava.lang.String;@e33fd4, start=0, hql=from PinMajor, method=G}]
		//写日志 
		BObj[] objs = (BObj[])psMap.get("objs");
		UUser usr = UMService.getUserBySession(request);
		int uid = usr.getId();
		BaseDAOImpl bdi = (BaseDAOImpl) Cfg.getBean("BaseDAOImpl");
		
		Object[][] rt = (Object[][] )result;
		for(int i=0; i<objs.length; i++){
			char action = objs[i].getAction();
			if ((action==BObj.ACTION_ADD) || (action==BObj.ACTION_UPDATE) || (action==BObj.ACTION_REMOVE)) {
				PinMajor rti = (PinMajor)rt[i][0];
				Integer majorId = rti.getId();
				//CcSync en=dao.hb.PinPerson  Keyval=majorId, oper=A,U,R,O  ,userid,ip,
				//System.out.println("afterHandle,"+i+"==majorId="+majorId+",method="+method);
				CcSync cs = new CcSync();
				String cn = "dao.hb.PinMajor";
				cs.setEn(cn);
				cs.setKeyval(majorId.toString());
				cs.setOper(action);
				cs.setUserid(String.valueOf(uid));
				cs.setTmCreate(new Date());
				bdi.save(cs);
			}
			

		//0==majorName=历史类（历史学、美术史、古代音乐史、古代戏剧史、工艺美术史）,method=P  修改
		//0==majorId=101,method=P  //增加
		
//	    SyncHlp.log("dao.hb.CeUser", obj.getItems().get("id").toString(),
//                Obj.ACTION_UPDATE, psMap.get("userId").toString());
	    
//		CcSync cs = (CcSync)cm_sync.instantiate(null,EntityMode.POJO);
//		cs.setEn(cn);
//		//cs.setEn(RPARTICLE_CN);//wjh 2012.7.18
//		cs.setKeyval(v1);
//		cs.setOper(Obj.ACTION_ADD);
//		cs.setUserid(null);
//		cs.setTmCreate(new Date());
//		s.save(cs);
//		
			//临时文件
//			if(uri.startsWith("_")){
//				moveFile(rti,uid,bdi);
//			}
		}		

	}

}
