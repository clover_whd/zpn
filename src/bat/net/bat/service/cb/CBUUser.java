package net.bat.service.cb;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import net.bat.db.BaseDAO;
import net.bat.db.BaseDAOImpl;
import net.bat.pt.BObj;
import net.bat.service.HDDefault;
import net.bat.upload.uploadServlet;
import net.bat.util.Cfg;
import net.bat.util.Encrypt;
import net.coobird.thumbnailator.Thumbnails;
import net.cs.um.UMService;
import net.sourceforge.pinyin4j.PinyinHelper;
import dao.hb.NImage;
import dao.hb.UUser;

public class CBUUser implements IHandleHook {
	final public static String PN_AVATAR="avatar";
	final public static String FMT_AVATAR = "jpg";
	final public static int SIZE_AVATAR = 50;
	@Autowired
	private BaseDAOImpl dao;
	public String getPath(String fname){
		return uploadServlet.getBindPath(fname);
	}
	private String setAvatar(String fname,String userid) throws IOException{
	  int pos2=fname.indexOf("]");
	  uploadServlet.makeDir(userid, uploadServlet.getPath(), false);
	  String op = userid+uploadServlet.PATH_SPLITER+PN_AVATAR+"."+FMT_AVATAR;
	  if( pos2!=-1) {
		  String fn =fname.substring(pos2+1);
	  }
	  File fin = new File(getPath(fname));
	  String path_out = getPath(op);
	  uploadServlet.makeDir(path_out,true);
	  File fout = new File(path_out);
	  Thumbnails.of(fin).size(SIZE_AVATAR, SIZE_AVATAR).outputFormat(FMT_AVATAR).toFile(fout);
	  return op;
	}
	private void pbh(char method, HashMap<String, Object> psMap,
			HttpServletRequest request) throws Exception {
		BObj[] objs = (BObj[])psMap.get("objs");
		for (BObj cur : objs) {
			HashMap<String,Object>  its = cur.getItems();
			char action=cur.getAction();
			String pwd = (String)its.get("pwd");
			String userId;
			if(action==BObj.ACTION_ADD){
				if(pwd==null)
					pwd="";
				userId=(String)its.get("userId");
			}else{
				UUser usr= (UUser)dao.get("from UUser where id=?", new Object[]{its.get("id")});
				userId = usr.getUserId();
			}
			//对新建的用户或修改密码的用户加密密码
			if(pwd!=null){
				its.put("pwd",Encrypt.calcUserEncrypt(userId, pwd));
			}
		}
	}

	@Override
	public void afterHandle(char method, HashMap<String, Object> psMap,
			HttpServletRequest request, Object result) throws Exception {
		// TODO Auto-generated method stub
		BObj[] objs = (BObj[])psMap.get("objs");
		UUser usr = UMService.getUserBySession(request);
		BaseDAOImpl bdi = (BaseDAOImpl) Cfg.getBean("BaseDAOImpl");
		Object[][] rt = (Object[][] )result;
		for(int i=0; i<objs.length; i++){
			UUser rti = (UUser)rt[i][0];
			//自动写入拼音简写
			rti.setUserid(usr.getId());
			String val_name =(String) objs[i].getItems().get("name");
			if(val_name!=null){
				rti.setNamePyh(getPinYinHeadChar(val_name));
				bdi.update(rti);
			}
		}	
	}

	@Override
	public void beforeHandle(char method, HashMap<String, Object> psMap,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		switch(method){
		case HDDefault.METHOD_PUT:
			pbh(method,psMap,request);
			return;
		}
	}
	
	public static String getPinYinHeadChar(String str) {

        String convert = "";
        for (int j = 0; j < str.length(); j++) {
            char word = str.charAt(j);
            String[] pinyinArray = PinyinHelper.toHanyuPinyinStringArray(word);
            if (pinyinArray != null) {
                convert += pinyinArray[0].charAt(0);
            } else {
                convert += word;
            }
        }
        return convert;
    }
}



