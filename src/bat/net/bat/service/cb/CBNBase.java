package net.bat.service.cb;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import dao.hb.NBase;
import dao.hb.UUser;

import net.bat.db.BaseDAOImpl;
import net.bat.pt.BObj;
import net.bat.upload.uploadServlet;
import net.bat.util.Cfg;
import net.bat.util.Encrypt;
import net.cs.um.UMService;

public class CBNBase implements IHandleHook {
	public static String PATH_WORK = uploadServlet.getWorkPath();

	@Override
	public void beforeHandle(char method, HashMap<String, Object> psMap,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		BObj[] objs = (BObj[])psMap.get("objs");
		for(int i=0; i<objs.length; i++){
			BObj cur = objs[i];
			switch(cur.getAction()){
			case BObj.ACTION_REMOVE:
				clearNode(cur);
				break;
			}
		}
	}
	private void clearNode(BObj bo){
		BaseDAOImpl bdi = (BaseDAOImpl) Cfg.getBean("BaseDAOImpl");
		HashMap<Integer,NBase> cmap = new HashMap<Integer,NBase>();
		NBase nb = (NBase)bdi.get("from NBase where id=?", new Object[]{bo.getItems().get("id")});
		clearNode(bdi,nb,0,cmap);
	}
	static public String getImagePath(NBase cur){
		return PATH_WORK+cur.getuserid()
				+File.separator+"nimage"
				+File.separator+cur.getId();
	}
	private void clearImage(NBase cur){
		File f = new File(getImagePath(cur));
		if(f.exists()){
			f.delete();
		}
	}
	private void clearNode(BaseDAOImpl dao,NBase cur, int level,HashMap<Integer,NBase> cmap ){
		//避免重复处理
		if(cmap.containsKey(cur.getId())){
			return;
		}
		cmap.put(cur.getId(), cur);
		System.out.println("delete Node id:"+cur.getId()+" itype:"+cur.getItype()+" lang:"+cur.getLanguage());
		List ls;
		//删除翻译节点 翻译节点不会成为其他节点的目标翻译节点
		if(cur.getTnid()==null){
			ls = dao.find("from NBase where tnid=?",new Object[]{cur.getId()});
			for(int i=0; i<ls.size(); i++){
				NBase tn = (NBase)ls.get(i);
				clearNode(dao,tn,level+1,cmap);
			}
		}
		switch(cur.getItype()){
		case 2:
			//删除历史版本
			dao.deleteByQuery("delete from NImage where nid=?", new Object[]{cur.getId()});
			//删除附件目录
			clearImage(cur);
			//删除自己
			if(level>0){
				dao.delete(cur);
			}
			break;
		case 1:
			//删除关联节点
			ls = dao.find("from NBase where pnid=?",new Object[]{cur.getId()});
			for(int i=0; i<ls.size(); i++){
				NBase pn = (NBase)ls.get(i);
				clearNode(dao,pn,level+1,cmap);
			}
			//删除历史版本
			dao.deleteByQuery("delete from NRestaurant where nid=?", new Object[]{cur.getId()});
			//删除自己
			if(level>0){
				dao.delete(cur);
			}
			//删除分类
			dao.deleteByQuery("delete from NSort where nid=?", new Object[]{cur.getId()});
			break;
		case 4:
			//删除关联节点
			ls = dao.find("from NBase where pnid=?",new Object[]{cur.getId()});
			for(int i=0; i<ls.size(); i++){
				NBase pn = (NBase)ls.get(i);
				clearNode(dao,pn,level+1,cmap);
			}
			//删除历史版本
			dao.deleteByQuery("delete from NMenu where nid=?", new Object[]{cur.getId()});
			//删除自己
			if(level>0){
				dao.delete(cur);
			}
			break;
		}
	}

	@Override
	public void afterHandle(char method, HashMap<String, Object> psMap,
			HttpServletRequest request, Object result) throws Exception {
		// TODO Auto-generated method stub
		//删除关联数据
	}

}
