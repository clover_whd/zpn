package net.bat.service.cb;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
/**
 * 挂接handle的处理,可以针对SN,SN+CN进行挂接
 * @author chen4w
 *
 */
public interface IHandleHook {
	/**
	 * handle前处理
	 * @param method 方法名
	 * @param psMap 参数map
	 * @throws Exception 抛出的异常
	 */
	public void beforeHandle(char method,HashMap<String, Object> psMap,HttpServletRequest request) throws Exception;
	/**
	 * handle后处理
	 * @param method 方法名
	 * @param psMap 参数map
	 * @param result handle结果
	 * @throws Exception 抛出异常
	 */
	public void afterHandle(char method,HashMap<String, Object> psMap,HttpServletRequest request,Object result) throws Exception;

}
