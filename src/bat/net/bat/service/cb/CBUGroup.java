package net.bat.service.cb;

import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import net.bat.pt.BObj;
import net.bat.util.Encrypt;

public class CBUGroup implements IHandleHook {

	@Override
	public void beforeHandle(char method, HashMap<String, Object> psMap,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		BObj[] objs = (BObj[])psMap.get("objs");
		for(int i=0; i<objs.length; i++){
			BObj cur = objs[i];
			HashMap<String,Object>  its = cur.getItems();
			if(cur.getAction()==BObj.ACTION_ADD)
				its.put("dtCreate",new Date());
			else
				its.put("dtModify", new Date());
		}
	}

	@Override
	public void afterHandle(char method, HashMap<String, Object> psMap,
			HttpServletRequest request, Object result) throws Exception {
		// TODO Auto-generated method stub

	}

}
