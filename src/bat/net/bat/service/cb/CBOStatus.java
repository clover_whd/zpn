package net.bat.service.cb;

import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import net.bat.pt.BObj;

public class CBOStatus implements IHandleHook {

	@Override
	public void afterHandle(char method, HashMap<String, Object> psMap,
			HttpServletRequest request, Object result) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeHandle(char method, HashMap<String, Object> psMap,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		BObj[] objs = (BObj[])psMap.get("objs");
		
		for(int i=0; i<objs.length; i++){
			BObj cur = objs[i];
			HashMap<String,Object>  its = cur.getItems();
			switch(cur.getAction()){
			case BObj.ACTION_ADD:
				its.put("tmCreate",new Date());
				break;
			case BObj.ACTION_UPDATE:				
			case BObj.ACTION_REMOVE:
				its.put("tmModify", new Date());				
				break;
			}
		}
	}

}
