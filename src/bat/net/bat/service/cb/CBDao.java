package net.bat.service.cb;

import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import dao.hb.UUser;

import net.bat.pt.BObj;
import net.bat.service.HDDefault;
import net.bat.util.Cfg;
import net.cs.um.UMService;
import net.sk.pt.State;
import net.sk.pt.StateUtil;
import net.sk.web.StateCenter;

public class CBDao implements IHandleHook {

	private void pbh(char method, HashMap<String, Object> psMap,
			HttpServletRequest request) throws Exception {
		//置时间戳,用户
		BObj[] objs = (BObj[])psMap.get("objs");
		Date dt = new Date();
		UUser usr = UMService.getUserBySession(request);
		for(int i=0; i<objs.length; i++){
			BObj cur = objs[i];
			HashMap<String,Object>  its = cur.getItems();
			//后台强制修改用户id,防止firebug攻击
			if(its.containsKey("userid") ){
				its.put("userid",usr.getId());
			}
			switch(cur.getAction()){
			case BObj.ACTION_ADD:
				if(its.containsKey("dtCreate") ){
					its.put("dtCreate",dt);
				}
				if(its.containsKey("dtModify")){
					its.put("dtModify", dt);
				}
				break;
			case BObj.ACTION_UPDATE:
				if(its.containsKey("dtModify")){
					its.put("dtModify", dt);
				}
				break;
			}
		}
		
	}
	private void gbh(char method, HashMap<String, Object> psMap,
			HttpServletRequest request) throws Exception {
		String hql =(String)psMap.get("hql");
		UUser usr = UMService.getUserBySession(request);
		int userid = usr.getId();
		//后台强制限制用户只能获取自己的数据,防止firebug攻击
		String appendstr;
		if(hql.indexOf("where")==-1){
			appendstr=" where userid="+userid;
		}else{
			appendstr=" and userid="+userid;
		}
		int pos_orderby = hql.indexOf("order");
		if(pos_orderby==-1){
			hql+=appendstr;
		}else{
			hql = hql.substring(0, pos_orderby-1)
					+appendstr+" "
					+hql.substring(pos_orderby);
		}
		psMap.put("hql",hql);		
	}
	private void pah(char method, HashMap<String, Object> psMap,
			HttpServletRequest request, Object result) throws Exception {
		// TODO Auto-generated method stub
		//向浏览器推送状态/无法处理多cn和非id主键
		BObj[] objs = (BObj[])psMap.get("objs");
		Object[][] rt = (Object[][] )result;
		for(int i=0; i<objs.length; i++){
			BObj cur = objs[i];
			HashMap<String,Object>  its = cur.getItems();
			//获取保存之后的主键
			if(cur.getAction()==BObj.ACTION_ADD){
				Object rti = rt[i][0];
				if(its.containsKey("id") ){
					BeanWrapper bw = new BeanWrapperImpl(rti);
					its.put("id", bw.getPropertyValue("id"));
				}
			}
			State s = new State();
			s.setCn(cur.getCns()[0]);
			s.setAction(cur.getAction());
			s.setItems(its);
			s.setKey(StateUtil.getKey(s));
			Cfg.getSC().provideState(StateCenter.PR_PR_DAO, s);
		}		
		
	}
	@Override
	public void beforeHandle(char method, HashMap<String, Object> psMap,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		switch(method){
		case HDDefault.METHOD_PUT:
			pbh(method,psMap,request);
			return;
		default:
			gbh(method,psMap,request);
			break;
		}
		
	}

	@Override
	public void afterHandle(char method, HashMap<String, Object> psMap,
			HttpServletRequest request, Object result) throws Exception {
		switch(method){
		case HDDefault.METHOD_PUT:
			pah(method,psMap,request,result);
			break;
		}
	}

}
