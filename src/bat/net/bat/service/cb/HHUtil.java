package net.bat.service.cb;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import net.bat.util.Cfg;
import net.sk.pt.IConsumer;
import net.sk.pt.IFilter;

/**
 * 管理handle挂接的工具类
 * @author chen4w
 *
 */
public class HHUtil {
	//spring 管理的map
	private HashMap<String,String> suMap;
	private HashMap<String,String> hhMap;
	private HashMap<String,Integer> transactionMap;

	public HashMap<String, String> getSuMap() {
		return suMap;
	}

	public void setSuMap(HashMap<String, String> suMap) {
		this.suMap = suMap;
	}
	public HashMap<String, Integer> getTransactionMap() {
		return transactionMap;
	}

	public void setTransactionMap(HashMap<String, Integer> transactionMap) {
		this.transactionMap = transactionMap;
	}

	public HashMap<String, String> getHhMap() {
		return hhMap;
	}

	public void setHhMap(HashMap<String, String> hhMap) {
		this.hhMap = hhMap;
	}
	public Integer getTransactionMode(String sn,char p){
		return transactionMap.get(sn+p);
	}
/**
 * 获得挂接对象
 * @param sn 服务名
 * @param method 方法名
 * @param cn 数据对象名
 * @return 挂接对象
 */
	public IHandleHook getHandleHook(String sn,char method,String cn){
		String kstr = sn+method+(cn==null?"":cn);
		String beanId =  hhMap.get(kstr);
		if(beanId==null)
			return null;
		return (IHandleHook)Cfg.getBean(beanId);
	}
	/**
	 * 获得多匹配挂接
	 */
	public IHandleHook getMatchHook(String sn,char method,String cn){
		String kstr = sn+method+(cn==null?"":cn);
		
		Iterator<Entry<String,String>> iter = suMap.entrySet().iterator(); 
		while (iter.hasNext()) { 
			Entry<String,String> entry = iter.next(); 
			String key = entry.getKey();
			String val = entry.getValue(); 
			if(kstr.indexOf(key)!=-1){
				return  (IHandleHook)Cfg.getBean(val);
			}
		} 	
		return null;
	}

}
