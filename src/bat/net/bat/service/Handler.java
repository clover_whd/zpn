package net.bat.service;

import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.transaction.support.TransactionTemplate;

import net.bat.service.cb.HHUtil;
import net.bat.service.cb.IHandleHook;
import net.bat.util.Cfg;
import net.bat.util.Info;

/**
 * 与jsonRPC对于的后台处理,调度的总入口
 * @author chen4w
 *
 */
public class Handler {
	//请求中不指定SN时,默认的处理
	static final String DEFAULTHANDLER = "HDDefault"; 
	//登录方法名
	final static char METHOD_LOGIN='L';
	//请求中服务名
	final static String NAME_SN="SN";
	//获取handle挂接的工具对象
	public static HHUtil hhutil;	


	public HHUtil getHhutil() {
		return hhutil;
	}

	public void setHhutil(HHUtil hhutil) {
		Handler.hhutil = hhutil;
	}

	/**
	 * 获取spring管理的bean
	 * @param sn bean id
	 * @return  bean
	 */
	static private IHandle getHandleImpl(String sn) {
		return (IHandle)Cfg.getBean(sn);
	}

	static public Object handle(char method,HashMap<String, Object> rm,String sn,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		if(sn==null || sn.trim().equals(""))
			sn = DEFAULTHANDLER;

		//session检查 为空且非登录请求
		/*if(method!=METHOD_LOGIN){
			throw new ServletException(Info.ERR_INVALID_SESSION);	
		}*/
		//如果存在before回调
		String cn = "";
		String[] cns= (String[])rm.get("cns");
		if(cns!=null){
			cn = cns[0];
		}
		//根据HHUtil中对sn+method的事务要求,采用编程决定是否启动事务管理
		Integer transanctionMode = hhutil.getTransactionMode(sn,method);
		PlatformTransactionManager transactionManager=null;
		TransactionStatus transactionStatus=null;
		if(transanctionMode!=null){
			transactionManager=(PlatformTransactionManager)Cfg.getBean("transactionManager");  
			TransactionDefinition transactionDefinition=
				new DefaultTransactionDefinition(transanctionMode);		
			transactionStatus=transactionManager.getTransaction(transactionDefinition);
		}
		try{
			IHandleHook hcb = hhutil.getHandleHook(sn, method, cn);
			if(hcb!=null)
				hcb.beforeHandle(method,rm,request);
			IHandleHook mcb = hhutil.getMatchHook(sn, method, cn);
			if(mcb!=null)
				mcb.beforeHandle(method,rm,request);
			Object result= getHandleImpl(sn).handle(method,rm,request);
			//如果存在after回调
			if(hcb!=null)
				hcb.afterHandle(method, rm, request, result);
			if(mcb!=null)
				mcb.afterHandle(method,rm,request,result);
			if(transactionManager!=null)
				transactionManager.commit(transactionStatus);
			return result;
		}catch(Exception e){
			e.printStackTrace();
			if(transactionManager!=null){
				try{
					transactionManager.rollback(transactionStatus);
				}catch(Exception et){
					et.printStackTrace();
				}
			}
			throw e;
		}
	}

}
