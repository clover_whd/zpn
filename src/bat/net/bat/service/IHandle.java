package net.bat.service;

import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;

/**
 * 对前台请求进行处理的接口定义,由于参数可以是hashmap,可以适应任何方法参数
 * @author chen4w
 *
 */
/**
* @param method 调用bean的方法
* @param args 前台json请求
* @param request 前台请求附带的http请求
* @return  可以是任意object对象
* 
 */
public interface IHandle {
	public Object handle(char method,HashMap<String,Object> args,HttpServletRequest request)throws Exception;
}
