package net.bat.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Timer;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import net.bat.db.BaseDAOImpl;
import net.bat.util.Cfg;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jabsorb.JSONRPCBridge;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;


public class InitServlet implements javax.servlet.ServletContextListener {
	private Properties prop_th;
	public static final String PROPS_TH = "/th.properties";
	public static final String PROP_SHUTDOWN = "th.lastShutdown";
	private static String PROP_LOCATION=null;
	private static final String NAME_WORK_PATH="work_path";
	private static Log logger = LogFactory.getLog(InitServlet.class);
	private static final long serialVersionUID = 1L;
	public static Date lastShutdown = null;
	
	private static final String CONTEXT_UM_HOME="avens.UmHome";
	private static final String CONTEXT_CAS_HOME="avens.CasHome";
	private static final String CONTEXT_LOCAL_HOME="avens.LocalHome";
	private static final String CONTEXT_LOGIN_BASEDN="avens.BaseDN";
	private static final String CONTEXT_LOGIN_URL_GETPROPERTY="avens.URLgetproperty";
	private static final String CONTEXT_ATTACHMENTSERVER="attachmentServer";
	private static final String CONTEXT_REALATTACHMENTSERVER="realAttachmentServer";
	private static final String REALATTACHMENTSERVERLOCAL="realAttachmentServerLocal";
	
	static public String getWorkPath(){
		if(File.separatorChar=='/'){
			return Cfg.getInitParameter(NAME_WORK_PATH+"_mac");
		}else{
			return Cfg.getInitParameter(NAME_WORK_PATH+"_win");
		}
	}
	static public String getPropLocation(){
		if(PROP_LOCATION==null){
			PROP_LOCATION=getWorkPath()+PROPS_TH;
		}
		return PROP_LOCATION;
	}
	public void init() throws ServletException {
    }
    public void destroy() {
    }
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		try {
			saveShutdownTM();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.info("container shut down");
	}
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		// TODO Auto-generated method stub
		ServletContext stx=sce.getServletContext();
		
		Cfg.init(stx);
		Cfg.UmHome = Cfg.getInitParameter(CONTEXT_UM_HOME);
		Cfg.CasHome = Cfg.getInitParameter(CONTEXT_CAS_HOME);
		Cfg.LocalHome = Cfg.getInitParameter(CONTEXT_LOCAL_HOME);	
		Cfg.BaseDN = Cfg.getInitParameter(CONTEXT_LOGIN_BASEDN);
		Cfg.getpropertyURL = Cfg.UmHome+Cfg.getInitParameter(CONTEXT_LOGIN_URL_GETPROPERTY);
		Cfg.attachmentServer = Cfg.getInitParameter(CONTEXT_ATTACHMENTSERVER);
		Cfg.realAttachmentServer = Cfg.getInitParameter(CONTEXT_REALATTACHMENTSERVER);
		Cfg.realAttachmentServerLocal = Cfg.getInitParameter(REALATTACHMENTSERVERLOCAL);
		
    	WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(stx);
    	Cfg.setContext(ctx);
    	Cfg.init(stx);
    	//start timer
    	try{
    		JSONRPCBridge.getGlobalBridge().registerClass("BH", net.bat.service.Handler.class);
    		logger.info("json-rpc ready.");
    		//load properties file and get last shutdown time
    		prop_th = new Properties();
    		File fp = new File(getPropLocation());
    		if(fp.exists()){
	    		FileInputStream fpIn=new FileInputStream(fp);
	    		prop_th.load(fpIn);
	    		fpIn.close();
	    		String sdStr = prop_th.getProperty(PROP_SHUTDOWN);
	    		if(sdStr!=null && !sdStr.trim().equals("") ){
	    			lastShutdown=new Date();
	    			lastShutdown.setTime(Long.parseLong(sdStr));
	    		}
	    		logger.info("last shutdown time ok.");
    		}
    	}catch(Exception e){
    		e.printStackTrace();
    	}
	}
	public void saveShutdownTM() throws FileNotFoundException, IOException{
		prop_th.setProperty(PROP_SHUTDOWN, new Date().getTime()+"");
		prop_th.store(new FileOutputStream(getPropLocation()), "th config");   
	}

}
