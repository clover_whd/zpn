package net.bat.servlet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExcelMapping {

	
    private List list=new ArrayList<Map<String,String>>();

	public List getList() {
		return list;
	}

	public void setList(List list) {
		this.list = list;
	}
      
      
      
	
	public List<Map<String,Map<String,String>>>  addTableInfo(){
		
		List<Map<String,Map<String,String>>> list=new ArrayList<Map<String,Map<String,String>>>();
		
		Map<String,String> map=new HashMap<String,String>();

		  map.put("id", "编号");
		  map.put("postName", "所报岗位");
		  map.put("code", "岗位代码"); 
		  map.put("identifier", "唯一标识符"); 
		  map.put("year", "年代"); 
		  map.put("username", "姓名"); 
		  map.put("attachmentFilename", "简历(附件)url");
		  map.put("photoStand", "证件照(附件)url");
		  map.put("identityNumber", "身份证号码"); 
		  map.put("birthday", "出生日期");
		  map.put("gender", "性别"); 
		  map.put("nation", "民族"); 
		  map.put("politicsStatus", "政治面貌"); 
		  map.put("partyTime", "入党（团）时间"); 
		  map.put("nativePlace", "籍贯"); 
		  map.put("phone", "联系电话（手机）"); 
		  map.put("graduateSchool", "毕业院校"); 
		  map.put("graduatedLocation", "毕业院校所在地");
		  map.put("educational", "学历"); 
		  map.put("major", "岗位专业要求"); 
		  map.put("major2", "其他专业"); 
		  map.put("major3", "所学专业"); 
		  map.put("englishFour", "英语四级成绩");
		  map.put("englishSix", "英语六级成绩");
		  map.put("otherlang", "其他语种掌握情况");
		  map.put("sourcePlace", "生源地（高中所在地）");
		  map.put("highSchool", "高中毕业院校");
		  map.put("selfdescription", "自我描述");
		  map.put("awarding", "获奖情况");
		  map.put("otherDescription", "其他说明");
		  map.put("email", "邮件");
		  map.put("dtCreate", "提交日期");
		  map.put("homeAddress", "家庭地址");
		  map.put("permanentResidence", "户口所在地");
		  map.put("familyInfo", "家庭成员信息");
		  map.put("learningExperience", "学习经历");
		  map.put("dtCheck", "审查时间");
		  map.put("regnum","注册次数");
		  map.put("isreceive","收到邮件标志");
		  map.put("majorReg","所报专业");
		  map.put("majorCode","专业代码");
		  map.put("graduateDate","毕业时间");
		  map.put("remark", "备注");
		  map.put("reviewResults","初审结果");
		  map.put("status","状态");
		  map.put("dtSubmit","提交时间");
		  map.put("userCheck","审查人");
		  map.put("statusFilter","初审结果");
		  map.put("issend","发邮件标志");
		  map.put("workExperience","工作经历");
	      Map<String,Map<String,String>> mapt=new HashMap<String,Map<String,String>>();
	      mapt.put("dao.hb.PinPerson", map);
	      list.add(mapt);
		  
		return list;
		
	}
}
