package net.bat.util;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.Locale;

public class Encrypt {

	private final static String[] hexDigits = {
		"0", "1", "2", "3", "4", "5", "6", "7",
		"8", "9", "a", "b", "c", "d", "e", "f"};

	private static String byteToHexString(byte b) {
		int n = b;
		if (n < 0)
			n = 256 + n;
		int d1 = n / 16;
		int d2 = n % 16;
		return hexDigits[d1] + hexDigits[d2];
	}
	
	private static String byteArrayToHexString(byte[] b) {
		StringBuffer resultSb = new StringBuffer();
		for (int i = 0; i < b.length; i++) {
			resultSb.append(byteToHexString(b[i]));
		}
		return resultSb.toString();
	}

	public static String MD5Encode(String origin) {
		String resultString = null;
		try {
			resultString=new String(origin);
			MessageDigest md = MessageDigest.getInstance("MD5");
			resultString=byteArrayToHexString(md.digest(resultString.getBytes()));
		} catch (Exception ex) {
		}
		return resultString;
	}

	public static String calcUserEncrypt(String uid, String soundCode) {
		if( soundCode == null ) 
			return null;
		return MD5.md5(uid+soundCode);
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String ms = Encrypt.MD5Encode("123456");
		System.out.println(calcUserEncrypt("sys","sysrm"));
		System.out.println("Default Charset=" + Charset.defaultCharset());  
		System.out.println("file.encoding=" + System.getProperty("file.encoding"));  
		
	}
}


class MD5
{

    public MD5()
    {
    }

    public static final String md5(String s)
    {
        char hexdigits[] = {
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
            'a', 'b', 'c', 'd', 'e', 'f'
        };
        try
        {
            byte strtemp[] = s.getBytes();
            MessageDigest mdtemp = MessageDigest.getInstance("MD5");
            mdtemp.update(strtemp);
            byte md[] = mdtemp.digest();
            int j = md.length;
            char str[] = new char[j * 2];
            int k = 0;
            for(int i = 0; i < j; i++)
            {
                byte byte0 = md[i];
                str[k++] = hexdigits[byte0 >>> 4 & 0xf];
                str[k++] = hexdigits[byte0 & 0xf];
            }

            return new String(str);
        }
        catch(Exception e)
        {
            return null;
        }
    }
}