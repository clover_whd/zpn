package net.bat.util;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;

import javax.servlet.ServletContext;

import net.sk.pt.State;
import net.sk.web.StateCenter;

import org.springframework.web.context.WebApplicationContext;
/**
 * 与全局配置文件对应的工具类,负责获取全局配置信息
 * @author chen4w
 *
 */
public class Cfg  {
	final public static String SESSION_USER="USER_INFO";
	//本地语言
	public static Locale local = Locale.CHINA;
	//web.xml上下文参数
	private static HashMap<String, String> pMap= new HashMap<String, String>();
	//spring的web上下文
	private static WebApplicationContext ctx ;
	//本地路径
	private static String realRoot;
	//本地路径名
	private final static String REAL_ROOTNAME="REAL_ROOT";
	//状态维护
    private static StateCenter sc =null;
    
	public static String  UmHome="";
	public static String  CasHome="";
	public static String  LocalHome="";	
	public static String  BaseDN="";
	public static String  getpropertyURL="";
	public static String  attachmentServer="";
	public static String  realAttachmentServer="";
	public static String  realAttachmentServerLocal="";
	
	public static void provideState(Object key,State s){
		getSC().provideState(key,s);
	}
	public static void distributeState(Object key,State s){
		getSC().distributeState(key,s);
	}
	public static StateCenter getSC() {
		return sc;
	}
	public static void setSc(StateCenter sc) {
		Cfg.sc = sc;
	}
	/**
	 * 获取本地路径
	 * @return
	 */
	public static String getRealRoot(){
		if(realRoot==null){
			realRoot = getInitParameter(REAL_ROOTNAME);
			if(realRoot==null) {
				realRoot = ctx.getServletContext().getRealPath("/");
			}
		}
		return realRoot;
	}
	/**
	 * 获得spring管理的bean
	 * @param beanName bean id
	 * @return bean
	 */
	public static Object getBean(String beanName){
		return ctx.getBean(beanName);
	}
	/**
	 * 设置spring web上下文
	 * @param beanFactory spring web上下文
	 */
	public static void setContext(WebApplicationContext beanFactory){
		ctx =beanFactory ;
	}
	/**
	 * 获得spring web上下文
	 * @return spring web上下文
	 */
	public static WebApplicationContext getContext(){
		return ctx;
	}
	/**
	 * 初始化并缓存web参数
	 * @param context web上下文
	 */
	@SuppressWarnings("unchecked")
	public static void init(ServletContext context){
		Enumeration  pns= context.getInitParameterNames();
		while(pns.hasMoreElements()){
			String pn = (String)pns.nextElement();
			pMap.put(pn, context.getInitParameter(pn));
		}
		//start state server
		sc = new StateCenter();
		sc.init();
	}
	/**
	 * 获得web参数
	 * @param pn web参数名
	 * @return web参数值
	 */
	public static String getInitParameter(String pn){
		return pMap.get(pn);
	}
	
	public static String info(String label){
		return info(label,null);
	}
	public static String info(String label,Object[] objs){
		String s= ctx.getMessage(label, objs, local);
		System.out.println(s);
		return s;
	}

}
