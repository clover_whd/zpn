package net.bat.db;

import java.util.HashMap;

import org.hibernate.SessionFactory;
import org.hibernate.metadata.ClassMetadata;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class SpringApp {
	static HashMap<SessionFactory,HashMap<String,ClassMetadata>> sfcmp = new HashMap<SessionFactory,HashMap<String,ClassMetadata>>();
	static private ApplicationContext  ctx = null;	
	static public ApplicationContext getCtx(){
		if(ctx==null){
			String[] locations = {"WebRoot/WEB-INF/classes/applicationContext-common.xml", "WebRoot/WEB-INF/classes/applicationContext-beans.xml"};
			ctx = new FileSystemXmlApplicationContext(locations ); ;		
		}
		return ctx;
	}
	static public BaseDAO getDAO(String target){
		return (BaseDAO)getCtx().getBean(target);		
	}
}
