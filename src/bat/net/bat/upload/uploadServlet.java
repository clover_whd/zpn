package net.bat.upload;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import javax.imageio.ImageIO;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.bat.db.BaseDAOImpl;
import net.bat.pt.BObj;
import net.bat.service.HDUtil;

import net.bat.util.Cfg;
import net.coobird.thumbnailator.Thumbnails;
import net.cs.um.UMService;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.hibernate.EntityMode;
import org.hibernate.metadata.ClassMetadata;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import dao.hb.LSync;
import dao.hb.NImage;
import dao.hb.OAttachment;
import dao.hb.UUser;

/**
 * 处理文件上传/管理请求
 * @author chen4w
 * @version 1.0
 */
public class uploadServlet extends HttpServlet {
	public static final String SPLIT_TASK_FN = "-";
	public static final String EN_IMG = "dao.hb.NImage";
	public static final String EN_ATTACH = "dao.hb.OAttachment";

	public static final String EN_BM = "dao.hb.Bm";
	public static final String SPLIT_INC = "\t";
	public static final String INDEX_ENCODE = "utf-8";
	public static final String INDEX_FN = "oper.inc";
	//附件最大尺寸2M
	public static final int MAX_LEN = 2048*1024;
	public static final String PATH_SPLITER = "\\";

	private static final String NAME_WORK_PATH="work_path";
	/**
	 * 存放文件的路径
	 */
	static private String path_upload=null;
	static public String getWorkPath(){
		if(File.separatorChar=='/'){
			return Cfg.getInitParameter(NAME_WORK_PATH+"_mac");
		}else{
			return Cfg.getInitParameter(NAME_WORK_PATH+"_win");
		}
	}
	
	static public String getPath(){
		if(path_upload==null){
			path_upload = getWorkPath();
		}
		return path_upload;
	}
	static public void setPath(String path){
		path_upload = path;
	}
  /**
	 * 
	 */
	private static final String REQ_ENCODE="utf-8";
	private static final String ATTACH_ROOT= "/upload";
	private static final long serialVersionUID = 1L;
	/**
	 * http响应类型
	 */
	 static final String CONTENT_TYPE = "text/html; charset=UTF-8";
	static public String splitChar = ",";
	
	//static public int num;
  /**
   * 文件后缀—类型映射表
   */
  static java.util.Hashtable map = new java.util.Hashtable();
  /**
   * 设置所有后缀—类型映射
   */
  static {
      fillMap();
  }
  /**
   * 设置后缀_类型映射
   * @param k 后缀
   * @param v 类型
   */
  static void setSuffix(String k, String v) {
      map.put(k, v);
  }
  /**
   * 设置所有后缀—类型映射
   *
   */
  static void fillMap() {
      setSuffix("", "content/unknown");
      setSuffix(".uu", "application/octet-stream");
      setSuffix(".exe", "application/octet-stream");
      setSuffix(".ps", "application/postscript");
      setSuffix(".zip", "application/zip");
      setSuffix(".sh", "application/x-shar");
      setSuffix(".tar", "application/x-tar");
      setSuffix(".snd", "audio/basic");
      setSuffix(".au", "audio/basic");
      setSuffix(".wav", "audio/x-wav");
      setSuffix(".gif", "image/gif");
      setSuffix(".png", "image/png");
      setSuffix(".jpg", "image/jpeg");
      setSuffix(".jpeg", "image/jpeg");
      setSuffix(".htm", "text/html");
      setSuffix(".html", "text/html");
      setSuffix(".text", "text/plain");
      setSuffix(".c", "text/plain");
      setSuffix(".cc", "text/plain");
      setSuffix(".c++", "text/plain");
      setSuffix(".h", "text/plain");
      setSuffix(".pl", "text/plain");
      setSuffix(".txt", "text/plain");
      setSuffix(".java", "text/plain");
      setSuffix(".svg", "image/svg+xml");
      setSuffix(".doc", "application/msword");
      setSuffix(".docx", "application/msword");
      //setSuffix(".doc", "application/java");
      //setSuffix(".doc", "aapplication/octet-stream");
      setSuffix(".xls", "application/vnd.ms-excel");
      setSuffix(".xlsx", "application/vnd.ms-excel");
      setSuffix(".ppt", "application/mspowerpoint");
      setSuffix(".pptx", "application/mspowerpoint");
      setSuffix(".pdf", "application/pdf");
  }

  private static String bindPath(String fpath,String pathsub){
	  if(pathsub.startsWith(File.separator)){
		  return (fpath+pathsub.substring(1)).trim();
	  } else {
		return (fpath+pathsub).trim();
	}
  }

  public static void makeDir(String fpath,boolean includef){
	  String fb="";
	  String[] fp = fpath.split(":");
	  if(fp.length>1){
		  fb=fp[0]+":";
	  }
	  makeDir(fp[fp.length-1],fb,includef);	    	 	  
  }
  public static void makeDir(String fileDir,String context,boolean includef){
//	 根据目录参数,创建无限层的目录结构
	//如果路径包含文件名,不要将文件名作为目录创建
	if(includef){
		  int pos=fileDir.lastIndexOf(File.separatorChar);
		  fileDir=fileDir.substring(0, pos);
	}
	String fpath = context+fileDir;
	File tf=new File(fpath);
	if(tf.isDirectory() && tf.exists()) {
		return;
	}
	StringTokenizer stringTokenizer = new StringTokenizer(fileDir, "/");
	String strTemp = "";
	while (stringTokenizer.hasMoreTokens()) {
		String str = stringTokenizer.nextToken();
		if("".equals(strTemp)){
			strTemp = str;
		}
		else{
			strTemp = strTemp + "/" + str;
		}
		File dir = new File(context + strTemp);
		if (!dir.isDirectory()) {
			dir.mkdirs();
		}
	}
  } 
  public static String getFileRN(String fnstr){
	  if(fnstr==null) {
		return null;
	}
	  String[] fns = fnstr.split(splitChar);
	  StringBuffer sbuf = new StringBuffer();
	  for(int i=0; i<fns.length; i++){
		  String fn = fns[i];
		  if(i>0) {
			sbuf.append(splitChar);
		}
		  if(fn.startsWith("_")) {
			sbuf.append(fn.substring(1));
		} else {
			sbuf.append(fn);
		}
	  }	  
	  return sbuf.toString();
  }
  public static long getFileSize(String fnstr){
	  if(fnstr==null) {
		return 0;
	}
	  long total =0;
	  String[] fns = fnstr.split(splitChar);
	  for (String fn : fns) {
		  String fpath =  bindPath(getPath(),getFPath(fn));
			File fileIn = new File(fpath);
			if(!fileIn.exists()) {
				continue;
			}
			total += fileIn.length();
	  }
	  return total;
  }
  public static void delFile(String fpath){
	  if(fpath==null)
		  return;
			File fileIn = new File(fpath);
			if(!fileIn.exists())
			  return;
			fileIn.delete();
	
  }
  public static void delFile(String fnstr,String suffix,String spec){
	  if(fnstr==null) {
		return;
	}
	  String[] fns = fnstr.split(splitChar);
	  for (String fn : fns) {
		  String fpath =  bindPath(getPath(),getFPath(fn));
			File fileIn = new File(fpath);
			if(!fileIn.exists()) {
				return;
			}
			fileIn.delete();
	  }
  }
/**
 * 删除文件及抽点文件
 * @param fn
 */

 public static String getFName(String fn){
	 String fp = getFPath(fn);
	 int pos = fp.lastIndexOf(File.separator);
	 if(pos!=-1)
		 return fp.substring(pos+1);
	 else
		 return fp;
 }
  public static  String getFPath(String fname){
	  //自定义路径
	  if(fname.indexOf(uploadServlet.PATH_SPLITER)!=-1){
		  return fname.replace(uploadServlet.PATH_SPLITER, File.separator);
	  }
	  String dtstr=fname;
	  int pos1=fname.indexOf("[");
	  int pos2=fname.indexOf("]");
	  if(pos1!=-1 && pos2!=-1) {
		  dtstr=fname.substring(pos1+1, pos2);
		  fname=fname.substring(pos2+1);
	  }
	  String ds[]=dtstr.split("_");
	  String rstr="";
	  for (String element : ds) {
		  rstr+=element+File.separatorChar;
	  }
	  return rstr+fname;
  }
 /**
   * servlet初始化,读入配置文件中的参数
   */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}

 static public String getURIPath(String uri){
	return bindPath(getPath(),getFPath(uri));
 }
 static public String getBindPath(String fname){
	 return bindPath(getPath(),getFPath(fname));
 }
  static public boolean openfile(HttpServletRequest request, HttpServletResponse response) throws IOException{
	  	String path = getPath();
	  	//request.setCharacterEncoding(REQ_ENCODE);
		String fname = request.getParameter("f");
		String nh = request.getParameter("nh");
		//if(fname!=null)
		//	fname = new String(fname.getBytes("ISO-8859-1"),REQ_ENCODE);
		int ipos = fname.indexOf("?");
		if (ipos>0){
			fname = fname.substring(0,ipos);
		}
		String fp = request.getParameter("fp");
		if(fp!=null) {
			fp = new String(fp.getBytes("ISO-8859-1"),REQ_ENCODE);
		} else {
			fp = bindPath(path,getFPath(fname));
		}
		
		
		File fileIn = new File(fp);
		if(!fileIn.exists()){
			response.setContentType(CONTENT_TYPE);
			response.setCharacterEncoding(REQ_ENCODE);
			String err_out = "["+fname+"] not found";
			response.sendError(response.SC_NOT_FOUND,err_out);
		  return false;
		}
		
		String s = request.getParameter("size");
		if(s!=null){
			int ds = Integer.parseInt(s);
			int pos = fp.lastIndexOf(".");
			String suffix = fp.substring(pos+1).toLowerCase();
			String fp1 = fp.substring(0,pos)+"_"+s+"."+suffix;
			File fileIn1 = new File(fp1);
			//抽点文件不存在，抽之
			if(!fileIn1.exists() && (suffix.equals("jpg")||suffix.equals("png"))){
				Thumbnails.of(fileIn).size(ds, ds).outputFormat(suffix).toFile(fileIn1);
			}
			fileIn=fileIn1;
		}

		
		try{
	    String ct = null;
	    int ind = fp.lastIndexOf('.');
	    if (ind > 0) {
	     ct = (String) map.get(fp.substring(ind).toLowerCase());
	    }
	    if (ct == null) {
	      ct = "application/octet-stream";
	     }
		  response.setContentType(ct);
		  if(nh==null)
			  response.addHeader("Content-Disposition", "attachment;filename=" + fname);		  
		  InputStream is = new FileInputStream(fileIn);
		  OutputStream os = response.getOutputStream();//得到输出流
		  byte data[]=new byte[4096];//缓冲字节数
		  int size=is.read(data);
		  while(size!=-1){
		   os.write(data,0,size);
		   size = is.read(data);
		  }
		  is.close();
		  os.flush();
		  os.close();
		  
		  return true;
		}catch(Exception e){
			return false;
		}
	}
  static void dblog(BaseDAOImpl dao,int uid,String entity,String entityId,int oper,String snap ){
	  HDUtil.dblog(dao, uid, entity, entityId, oper, snap);
	}


  static public String regularFN(String fp){
	  String fpath = fp ;
		int pos = fpath.lastIndexOf(File.separatorChar);
		if(pos!=-1){
		   fpath=fpath.substring(pos+1);
		}
        fpath = fpath.replace(";", "");
        int pr_pos = fpath.lastIndexOf(".");
        if(pr_pos!=-1){
     	   String pr = (fpath.substring(pr_pos)).toLowerCase();
     	   if(pr.equals(".jpeg")){
     		   pr = ".jpg";
     	   }
     	  fpath = fpath.substring(0,pr_pos)+pr;
        }
	 return fpath;
  }
 	/**
	 * 处理上传文件请求
	 */
  @Override
public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	  try {
		
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		PrintWriter out = response.getWriter();
		out.println(e);
	}
  }
  @Override
public void destroy() {
  }
  
  @Override
public void doGet( HttpServletRequest request , HttpServletResponse response )  throws ServletException , IOException{
	  openfile(request,response);
  }
 

}
