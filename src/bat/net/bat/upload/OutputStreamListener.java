package net.bat.upload;

import java.io.IOException;

public interface OutputStreamListener {
    public void start();
    public void bytesRead(int bytesRead)throws IOException;
    public void error(String message);
    public void done();
}
