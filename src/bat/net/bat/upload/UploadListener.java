package net.bat.upload;
import javax.servlet.http.HttpServletRequest;

import java.io.IOException;

public class UploadListener implements OutputStreamListener {
    private HttpServletRequest request;
    private long delay;
    private long total;
    private long cur;
    private String taskId;
    
    public UploadListener(HttpServletRequest request, long debugDelay) {
    	//todo get task id from request
        this.request = request;
        this.delay = debugDelay;
        total = request.getContentLength();
        taskId=request.getParameter("tid");
  }
    public String getTaskId(){
    	return taskId;
    }
	public void start() {
		// TODO Auto-generated method stub		
	}

	public void bytesRead(int bytesRead)throws IOException {
		// TODO Auto-generated method stub
        cur = cur + bytesRead;
        if(delay>0){
	        try{
	            Thread.sleep(delay);
	        }
	        catch (InterruptedException e)
	        {
	            e.printStackTrace();
	        }
        }
	}

	public void error(String message) {
		// TODO Auto-generated method stub

	}

	public void done() {
		// TODO Auto-generated method stub
	}

}
