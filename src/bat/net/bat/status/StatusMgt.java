package net.bat.status;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class StatusMgt {
	private HashMap<String,IStautas> keyMethod = new HashMap<String,IStautas>();
	private HashMap<String,HashMap<String,HashMap<String,Object>>> gInfo = 
		new HashMap<String,HashMap<String,HashMap<String,Object>>>();
	//数据库操作通用对象
	public void register(String cn,IStautas cbkey){
		keyMethod.put(cn,cbkey);
	}
	public HashMap<String,HashMap<String,Object>> getSMap(String cn){
		HashMap<String,HashMap<String,Object>> sm = gInfo.get(cn);
		if(sm!=null)
			return sm;
		else{
			sm = new HashMap<String,HashMap<String,Object>>();
			gInfo.put(cn, sm);
			return sm;
		}
	}
	//新增状态
	public void statusAdd(String cn,HashMap<String,Object> its){
		HashMap<String,HashMap<String,Object>>  sm = getSMap(cn);
		IStautas cbkey = keyMethod.get(cn);
		sm.put(cbkey.getKeyValue(its), its);
	}
	//移除状态
	public void statusRemove(String cn,HashMap<String,Object> its){
		HashMap<String,HashMap<String,Object>>  sm = getSMap(cn);
		IStautas cbkey = keyMethod.get(cn);
		String kval = cbkey.getKeyValue(its);
		sm.remove(sm.get(kval));
	}
	//更新状态
	@SuppressWarnings("unchecked")
	public void statusUpdate(String cn,HashMap<String,Object> its){
		HashMap<String,HashMap<String,Object>>  sm = getSMap(cn);
		IStautas cbkey = keyMethod.get(cn);
		String kval = cbkey.getKeyValue(its);
		HashMap<String,Object> sval = sm.get(kval);
		//its只包含了计算keyvalue需要的属性,和发生值改变的属性
		Iterator iter = its.entrySet().iterator();
		while (iter.hasNext()) {
		    Map.Entry entry = (Map.Entry) iter.next();
		    String key = (String)entry.getKey();
		    Object val = entry.getValue();
		    Object oldval = sval.get(key);
		    //上报的状态改变未必可靠,可能状态并未改变,防止将这种无用的信息通知browser
		    if(val!=null && val.equals(oldval)){
		    	its.remove(val);
		    }
		    sval.put(key, val);
		} 
	}

}
