package net.bat.pt;

import java.util.HashMap;
/**
 * object部分属性的map形式
 * @author chen4w
 *
 */
public class BObj {
	//从数据库获得的原始状态
	public static final char ACTION_ORIG = 'O';
	//新增对象
	public static final char ACTION_ADD = 'A';
	//已设置为删除状态
	public static final char ACTION_REMOVE='R';
	//经过了修改
	public static final char ACTION_UPDATE='U';
	//用于新增的默认对象
	public static final char ACTION_DEFAULT='D';
	//多表按顺序的类名
	private String[] cns;
	/**
	 * 属性数组
	 */
	private HashMap<String,Object> items;
	/**
	 * 动作标识
	 */
	private char action;
	public char getAction(){
		return action;
	}
	public void setAction(char action){
		this.action=action;
	}
	public String[] getCns(){
		return cns;
	}
	public void setCns(String[] cname){
		this.cns=cname;
	}
	public HashMap<String,Object>  getItems(){
		return items;
	}
	public void setItems(HashMap<String,Object>  fmap){
		this.items=fmap;
	}

}
