package cm.util;

import java.beans.PropertyDescriptor;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import net.bat.db.BaseDAOImpl;
import net.bat.util.Encrypt;

import org.hibernate.EntityMode;
import org.hibernate.metadata.ClassMetadata;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import dao.hb.PinMajor;
import dao.hb.PinPerson;
import dao.hb.UGroup;
import dao.hb.UUser;


public class DataTool {
	final static String PWD_DEFAULT = "6yhn";
	final static String EN_GROUP = "dao.hb.UGroup";
	final static String EN_USER = "dao.hb.UUser";
	final static String USER_SYS = "sys";
	public static final String EN_PIN_PERSON = "dao.hb.PinPerson";
	public DataTool(){
		initMap();
	}
	private UUser addUser(BaseDAOImpl dao,String name,String userId, int gid){
		UUser po;
		po =(UUser) dao.get("from UUser where userId=?", userId);
		if(po==null){
			ClassMetadata cm=dao.getClassMeta(EN_USER);	
			po = (UUser)cm.instantiate(null,EntityMode.POJO);
			po.setDtCreate(new Date());
			po.setName(name);
			po.setUserId(userId);
			po.setPwd(PWD_DEFAULT);
			String pwd_ept = Encrypt.calcUserEncrypt(po.getUserId(), po.getPwd());
			po.setGid(gid);
			po.setPwd(pwd_ept);
			//sys 保留给系统使用
			if(userId.equals(USER_SYS))
				po.setStatus(0);
			else
				po.setStatus(1);
			dao.save(po);
		}else{
			po.setDtModify(new Date());
			po.setName(name);
			po.setUserId(userId);
			po.setPwd(PWD_DEFAULT);
			String pwd_ept = Encrypt.calcUserEncrypt(po.getUserId(), po.getPwd());
			po.setGid(gid);
			po.setPwd(pwd_ept);
			if(userId.equals(USER_SYS))
				po.setStatus(0);
			else
				po.setStatus(1);
			dao.update(po);
		}
		return po;
	}
	private UGroup addGroup(BaseDAOImpl dao,String name,String grant){
		UGroup po;
		po = (UGroup)dao.get("from UGroup where name=?", name);
		if(po==null){
			ClassMetadata cm=dao.getClassMeta(EN_GROUP);	
			po = (UGroup)cm.instantiate(null,EntityMode.POJO);
			po.setDtCreate(new Date());
			po.setDtModify(new Date());
			po.setName(name);
			po.setGrantMenu(grant);
			dao.save(po);
		}else{
			po.setDtModify(new Date());
			po.setName(name);
			po.setGrantMenu(grant);
			dao.update(po);
		}
		return po;
	}
	
	private void addSysUsers(BaseDAOImpl dao){
		UGroup po = addGroup(dao,"系统管理",
				 "招聘管理;招聘管理#人员信息;招聘管理#数据集;"
				+ "系统管理;系统管理#用户;系统管理#用户组;系统管理#招聘专业;系统管理#活动日志");
		int gid = po.getId();
		addUser(dao,"System","sys",gid);
		addUser(dao,"系统管理员","admin",gid);
		List ls = dao.find("from UUser where name like '%郭珽%'");
		for(int i=0; i<ls.size(); i++){
			UUser cur = (UUser)ls.get(i);
			cur.setGid(gid);
			dao.update(cur);
		}
	}
	private void addHRUsers(BaseDAOImpl dao){
		UGroup po = addGroup(dao,"人事处",
				 "招聘管理;招聘管理#人员信息;招聘管理#数据集");
		int gid = po.getId();
		List ls = dao.find("from UUser where name like '%人事%'");
		for(int i=0; i<ls.size(); i++){
			UUser cur = (UUser)ls.get(i);
			cur.setGid(gid);
			dao.update(cur);
		}
	}

	private void addHeadUsers(BaseDAOImpl dao){
		String[] headers=new String[]{"主任","处长","馆长" ,"所长"};
		UGroup po = addGroup(dao,"用人单位",
				 "我的数据集;我的数据集#查看数据集");
		int gid = po.getId();
		for(int a=0; a<headers.length; a++){
			List ls = dao.find("from UUser where duty =?",new Object[]{headers[a]});
			for(int i=0; i<ls.size(); i++){
				UUser cur = (UUser)ls.get(i);
				cur.setGid(gid);
				dao.update(cur);
			}
		}
	}

	private HashMap<String,String[]> pm = new HashMap<String,String[]>();
	private void initMap(){
		pm.put("major", new String[]{"社会学","文艺学","公共管理","计算机体系结构","会计学（注册会计师）","美术","环境与材料科学"});
		pm.put("gender", new String[]{"男","女"});
		pm.put("politicsStatus", new String[]{"团员","中共党员"});
		pm.put("nation", new String[]{"汉族","维吾尔族","藏族"});
		pm.put("username", new String[]{"张三","李四","赵五","蓝胖子"});
		pm.put("photoStand", new String[]{
				"",
				"http://localhost:8080/zpn/demo/1.jpg",
				"http://localhost:8080/zpn/demo/2.jpg",
				"http://localhost:8080/zpn/demo/3.jpg",
				"http://localhost:8080/zpn/demo/4.jpg"});
		pm.put("attachmentFilename", new String[]{
				"",
				"http://localhost:8080/zpn/demo/1.jpg",
				"http://localhost:8080/zpn/demo/2.jpg",
				"http://localhost:8080/zpn/demo/3.jpg",
				"http://localhost:8080/zpn/demo/4.jpg"});
		
	}
	private void putMajorReg(BaseDAOImpl dao){
		List ls =dao.find("from PinMajor");
		int len = ls.size();
		String[] majorName= new String[len];
		String[] majorCode = new String[len];
		for(int i=0; i<len; i++){
			PinMajor pm = (PinMajor)ls.get(i);
			majorName[i]=pm.getMajorName();
			majorCode[i]=pm.getMajorCode();
		}
		pm.put("majorReg", majorName);
		pm.put("majorCode", majorCode);
	}
	private void addPersons(BaseDAOImpl dao, int step_from, int step_to){
		ClassMetadata cm=dao.getClassMeta(EN_PIN_PERSON);		

		Date dt_start = new Date();

        for(int i=step_from;i<=step_to;i++){
    		PinPerson  po=(PinPerson)cm.instantiate(null,EntityMode.POJO);;	
    		BeanWrapper bw = new BeanWrapperImpl(po);
    		PropertyDescriptor[]  ps = bw.getPropertyDescriptors();
    		for(int j=0; j<ps.length;j++){
        		PropertyDescriptor p = ps[j];
        		if(p.getWriteMethod()==null)
        			continue;
        		String pn = p.getDisplayName();
        		String pv;
        		String[] pa = pm.get(pn);
        		if(pa!=null && pa.length>0){
        			int pos = i % pa.length;
        			pv = pa[pos];
        			if(pn.equals("username"))
        				pv+=i;
        		}else{
        			pv = pn+i;
        		}
        		String pt  = p.getPropertyType().getName();
        		if(pn.equals("userCheck") || pn.equals("identityNumber")){
        			bw.setPropertyValue(pn,"1");
        			continue;
        		}
        		if(pt.equals("java.lang.String"))
        			bw.setPropertyValue(p.getName(), pv);
        		else if(pt.equals("java.util.Date"))
        			bw.setPropertyValue(p.getName(), new Date());
        		else
        			bw.setPropertyValue(p.getName(),null);
        	}
        	dao.save(po);
        }
        Date dt_end = new Date();
        long span = dt_end.getTime()- dt_start.getTime();
        System.out.println("Add " +(step_to - step_from)+" items, time span:"+span);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	    ApplicationContext applicationContext = new ClassPathXmlApplicationContext("/applicationContext-common.xml");	        
	    BaseDAOImpl dao = (BaseDAOImpl) applicationContext.getBean("BaseDAOImpl");
	    DataTool dt = new DataTool();
	    dt.putMajorReg(dao);
	    //dt.addPersons(dao,23,22000);
	    
	    dt.addSysUsers(dao);
	    dt.addHRUsers(dao);
	    dt.addHeadUsers(dao);
	}

}
