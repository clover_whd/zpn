package service;
import java.io.File;
import java.io.FilenameFilter;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import net.bat.db.BaseDAO;
import net.bat.pt.BObj;
import net.bat.service.HDUtil;
import net.bat.service.IHandle;
import net.bat.upload.uploadServlet;
import net.cs.um.UMService;

class FileAccept implements FilenameFilter {
	   String fn = null;
	   public FileAccept(String s) {
	    fn = s.substring(0,s.length()-4);
	   }
	   /* TODO 匹配抽点影像有问题*/
	@Override
	public boolean accept(File arg0, String arg1) {
		// TODO Auto-generated method stub
		if(arg1.startsWith(this.fn))
			return true;
		else
			return false;
	}
}
public class FileOperService implements IHandle {
	@Autowired
	private HDUtil util;

    public static final char METHOD_RNAME_FLIE = 'N';          //重命名附件
    public static final char METHOD_REMOVE_FILE='R';          //删除附件
    public static final char METHOD_ADD_FILE='A';          //上传附件
    public static final String SPLIT_FNS = ";";

    
    private void addFiles(HashMap<String, Object> req, HttpServletRequest request) throws Exception {
		BObj obj = (BObj)req.get("obj");
		String fld_name = (String)req.get("fld_name");
		if(obj!=null && obj.getAction()== BObj.ACTION_UPDATE){
			util.putItemValue(obj, obj.getCns(), new String[][]{{fld_name}}, UMService.getUserBySession(request));
		}
    }
    private int  removeFiles(HashMap<String, Object> req, HttpServletRequest request) throws Exception {
    	String fns =(String)req.get("fns");   	
    	String[] fna = fns.split(SPLIT_FNS);
    	int cout=0;
    	for(int i=0; i<fna.length; i++){
    		cout+=removeFile(fna[i]);
    	}
    	
		BObj obj = (BObj)req.get("obj");
		String fld_name = (String)req.get("fld_name");
		if(obj!=null && obj.getAction()== BObj.ACTION_UPDATE){
			util.putItemValue(obj, obj.getCns(), new String[][]{{fld_name}}, UMService.getUserBySession(request));
		}
    	return cout;
    }
    private int removeFile(String fn){
    	//return CBNImage.deleteFile(fn);
    	return 1;
    }
    private int renameFile(String fno,String fnn){
    	File f = new File(uploadServlet.getBindPath(fno));
    	if(!f.exists() || f.isDirectory())
    		return 0;
    	String fpath = f.getPath();
    	int pos = fpath.lastIndexOf(File.separator);
    	fpath = fpath.substring(0,pos);
    	File fp = new File(fpath);
     	File[] fs = fp.listFiles(new FileAccept(f.getName()));
    	for(int i=0; i<fs.length; i++){
    		File fc = fs[i];
    		String fn = fc.getPath().replaceAll(
    				uploadServlet.getFName(fno),
    				uploadServlet.getFName(fnn));
    		fc.renameTo(new File(fn));
    	}
    	return fs.length;
    }
    private String rnameFiles(HashMap<String, Object> req, HttpServletRequest request) throws Exception {
    	String fno =(String)req.get("fno");
    	String fnn = (String)req.get("fnn");
    	renameFile(fno,fnn);    	
		BObj obj = (BObj)req.get("obj");
		String fld_name = (String)req.get("fld_name");
		if(obj!=null && obj.getAction()== BObj.ACTION_UPDATE){
			util.putItemValue(obj, obj.getCns(), new String[][]{{fld_name}}, UMService.getUserBySession(request));
		}
    	return fnn;
    }
	@Override
	public Object handle(char method, HashMap<String, Object> rm,
			HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		switch(method){
		case METHOD_REMOVE_FILE:
			return removeFiles(rm,request);
		case METHOD_RNAME_FLIE:
			return rnameFiles(rm,request);
		case METHOD_ADD_FILE:
			 addFiles(rm,request);
		}
		return null;
	}

}
