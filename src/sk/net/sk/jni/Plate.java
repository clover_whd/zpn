package net.sk.jni;

public class Plate extends BaseClass  {
	public Plate(String arg){  
        super(arg);  
    }  	
	public native void InitPlate();
	public native String GetPlateResult(String prompt); 

}
