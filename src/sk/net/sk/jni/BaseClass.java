package net.sk.jni;

public class BaseClass {

    public BaseClass(String arg) {
        loadLibrary(arg);
    }

    private static void loadLibrary(String arg) {
        System.loadLibrary(arg);
    }
}