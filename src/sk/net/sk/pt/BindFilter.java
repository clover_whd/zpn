package net.sk.pt;

import java.util.HashMap;

public class BindFilter implements IFilter {
	String cn=null;
	HashMap<Object,Boolean> ids=null;
	public BindFilter(String cn,Object[] fids){
		if(cn==null || cn.trim().equals("")){
			return;
		}
		this.cn=cn;
		//订阅无id无意义,因此理解为订阅所有
		if(fids==null || fids.length==0){
			return;
		}
		this.ids = new HashMap<Object,Boolean>();
		for(int i=0; i<fids.length; i++){
			if(fids[i].equals("")){
				this.ids=null;
				return;
			}
			ids.put(fids[i],true);
		}
	}
	@Override
	public boolean bSend(State s) {
		// TODO Auto-generated method stub
		if(cn==null){
			return true;
		}else{
			if(!s.getCn().equals(cn)){
				return false;
			}else{
				if(ids==null){
					return true;
				}else{
					if(ids.containsKey(s.getKey())){
						return true;
					}else return false;
				}
			}			
		}
	}

}
