package net.sk.pt;

public interface IConsumer {
	Object getKey();
	boolean send(IProvider pr,State s);	
	/**
	 * 消费者能记住每个状态来自哪个发布者,从而当发布者发生异常,能够清除与之相关的状态
	 * @param pr
	 */
	void clearStateByPr(IProvider pr);
	void init(Object key);
	void destory();
}
