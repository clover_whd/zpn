package net.sk.pt;

import java.io.Serializable;
import java.util.HashMap;

public class State implements Serializable{
	//新增对象
	public static final char ACTION_ADD = 'A';
	//路过对象
	public static final char ACTION_PUT = 'P';
	//已设置为删除状态
	public static final char ACTION_REMOVE='R';
	//经过了修改
	public static final char ACTION_UPDATE='U';
	//类名
	private String cn;
	private Object pr;
	public Object getPr() {
		return pr;
	}
	public void setPr(Object pr) {
		this.pr = pr;
	}
	private Object key;
	public Object getKey() {
		return key;
	}
	public void setKey(Object key) {
		this.key = key;
	}
	/**
	 * 属性数组
	 */
	private HashMap<String,Object> items;
	/**
	 * 动作标识
	 */
	private char action;
	public char getAction(){
		return action;
	}
	public void setAction(char action){
		this.action=action;
	}
	public String getCn(){
		return cn;
	}
	public void setCn(String cname){
		this.cn=cname;
	}
	public HashMap<String,Object>  getItems(){
		return items;
	}
	public void setItems(HashMap<String,Object>  fmap){
		this.items=fmap;
	}
	public String toString() {
		return " ["+cn+"-"+key+"]."+action+items;
	}
}