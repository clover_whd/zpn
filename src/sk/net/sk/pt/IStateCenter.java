package net.sk.pt;

public interface IStateCenter {
	void init();
	void destory();
	void addConsumer(IConsumer cs);
	void removeConsumer(IConsumer cs);
	void addProvider(IProvider pr);
	void removeProvider(IProvider pr);
	void addDistributor(IDistributor db);
	void removeDistributor(IDistributor db);
	public IConsumer getConsumer(Object key);
}
