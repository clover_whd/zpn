package net.sk.pt;

import java.util.HashMap;
import java.util.List;

public interface IKeeper {
	void init();
	void destory();
	List<State> getStateByFilter(IFilter fl);
	/**
	 * 获得状态订阅者
	 * 
	 */
	/**
	 * 根据状态类名获得所有状态对象
	 * @param cn 状态类名
	 * @return  状态对象map
	 */
	HashMap<Object,State> getStatesByCn(String cn);
	/**
	 * 根据状态类名和唯一标示获得状态对象
	 * @param cn
	 * @param key
	 * @return
	 */
	State getState(String cn,Object key);
	/**
	 * 更新单个状态
	 * @param sta
	 */
	boolean update(State sta);
	/**
	 * 更新多个状态
	 * @param stas
	 */
}
