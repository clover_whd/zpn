package net.sk.pt;

import java.util.HashMap;

public interface IProvider {
	Object getKey();
	void send(State s);
	void destory();
	void init(IKeeper kp,Object key);
	void removeConsumer(IConsumer cs);
	void addConsumer(IConsumer cs,IFilter fl);
	HashMap<Object,IConsumer> getConsumers();
	IKeeper getKeeper();
}
