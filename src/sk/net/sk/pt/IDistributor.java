package net.sk.pt;

public interface IDistributor {
	void init(IProvider pr,Object key);
	void destory();
	void subscribe(IProvider pr,IFilter fl);
	void unsubscribe(IProvider pr);
	void publish(IConsumer cs,IFilter fl);
	void unpublish(IConsumer cs);
	Object getKey();
	void send(State s);
}
