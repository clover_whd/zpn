package net.sk.pt;


public interface ISender {
	/**
	 * 发送状态,根据订阅情况,当状态改变,向连接端的一个子集发送状态
	 * @param sta
	 */
	void send(State sta);	

}
