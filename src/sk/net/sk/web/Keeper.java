package net.sk.web;

import java.util.*;
import java.util.Iterator;
import java.util.Map.Entry;

import net.sk.pt.IFilter;
import net.sk.pt.IKeeper;
import net.sk.pt.State;

public class Keeper implements IKeeper {

	//构造双重结构以便从cn获取或者按顺序获取
	private HashMap<String,HashMap<Object,State>> states;
	private List<State> states_list;
	public Keeper(){
		init();
	}
	//TODO 先保存,先出
	@Override
	public List<State> getStateByFilter(IFilter fl) {
		// TODO Auto-generated method stub
		List<State> result = new ArrayList<State>();
		Iterator<Entry<String, HashMap<Object, State>>> iter = states.entrySet().iterator(); 
		while (iter.hasNext()) { 
			Entry<String, HashMap<Object, State>> entry = iter.next(); 
			HashMap<Object, State> cur = entry.getValue(); 
			Iterator<Entry<Object, State>> cur_iter = cur.entrySet().iterator(); 
			while(cur_iter.hasNext()){
				Entry<Object, State> cur_entry = cur_iter.next(); 
				State s = cur_entry.getValue();
				if(fl==null || fl.bSend(s)){
					result.add(s);
				}					
			}
		} 	
		return result;
	}
	
	@Override
	public HashMap<Object, State> getStatesByCn(String cn) {
		// TODO Auto-generated method stub
		return states.get(cn);
	}

	@Override
	public State getState(String cn, Object key) {
		// TODO Auto-generated method stub
		HashMap<Object, State> cur = states.get(cn);
		if(cur!=null){
			return cur.get(key);
		}
		return null;
	}

	@Override
	public boolean update(State sta) {
		// TODO Auto-generated method stub
		if(sta.getAction()==State.ACTION_PUT)
			return false;
		boolean sendFlag =true;
		String cn = sta.getCn();
		HashMap<Object, State> states_cn = states.get(cn);
		//首次使用初始化
		if(states_cn==null){
			states_cn = new HashMap<Object, State>();
			states.put(cn, states_cn);
		}
		Object key = sta.getKey();
		switch (sta.getAction()) {
		case State.ACTION_ADD:
			if (!states_cn.containsKey(key)) {
				states_cn.put(key, sta);
			} else {
				sendFlag = false;
			}
			break;
		case State.ACTION_REMOVE:
			if (states_cn.containsKey(key)) {
				states_cn.remove(key);
			} else {
				sendFlag = false;
			}
			break;
		case State.ACTION_UPDATE:
			if (states_cn.containsKey(key)) {
				State sta_target = states_cn.get(key);
				HashMap<String, Object> its_target = sta_target.getItems();
				HashMap<String, Object> its = sta.getItems();
				java.util.Iterator<Entry<String, Object>> iter = its.entrySet().iterator();
				while (iter.hasNext()) {
					Entry<String, Object> entry = iter.next();
					its_target.put(entry.getKey(), entry.getValue());
				}
			} else {
				sendFlag = false;
			}
			break;

		}
		return sendFlag;
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		states = new HashMap<String,HashMap<Object,State>>();
	}

	@Override
	public void destory() {
		// TODO Auto-generated method stub
		Iterator<Entry<String, HashMap<Object, State>>> iter = states.entrySet().iterator(); 
		while (iter.hasNext()) { 
			Entry<String, HashMap<Object, State>> entry = iter.next(); 
			HashMap<Object, State> cur = entry.getValue(); 
			cur.clear();
		} 	
		states.clear();
	}

}
