package net.sk.web;

import java.util.Date;

import net.sk.pt.State;

public class HeartThread implements Runnable {
	private final int loop_span = 60*1000;
	private boolean bStop = false;
	private Provider pr;
	private State s;
	
	
	HeartThread(Provider pr){
		this.pr =pr;
		this.s = new State();
		s.setAction(State.ACTION_PUT);
		s.setCn(StateCenter.PR_SYS_HEART);
	
	}
	void sendHeart(){
		s.setKey(new Date().getTime());
		pr.send(s);
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		while(!bStop){
			try {
				Thread.sleep(loop_span);
				sendHeart();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
