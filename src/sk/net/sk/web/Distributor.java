package net.sk.web;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import net.sk.pt.IConsumer;
import net.sk.pt.IDistributor;
import net.sk.pt.IFilter;
import net.sk.pt.IProvider;
import net.sk.pt.State;

public class Distributor implements IConsumer, IDistributor {

	private HashMap<Object,HashMap<Object,State>> pr_states;
	private IProvider pr;
	private Object key;
	@Override
	public void init( IProvider pr, Object key) {
		// TODO Auto-generated method stub
		this.pr = pr;
		this.key = key;
		pr_states = new HashMap<Object,HashMap<Object,State>>();
	}
	private void setPrState(IProvider pr,State s){
		if(s.getAction()==State.ACTION_UPDATE)
			return;
		Object pr_key = pr.getKey();
		HashMap<Object,State> prs =  pr_states.get(pr_key);
		if(prs==null){
			prs = new HashMap<Object,State>();
			pr_states.put(pr_key, prs);
		}
		switch(s.getAction()){
		case State.ACTION_ADD:
			prs.put(s.getKey(), s);
			break;
		case State.ACTION_REMOVE:
			prs.remove(s.getKey());
			break;
		}
	}
	private HashMap<Object,State> getPrStates(IProvider pr){
		return pr_states.get(pr.getKey());
	}
	/**
	 * 向目标provider订阅
	 */
	@Override
	public void subscribe(IProvider pr_target, IFilter fl) {
		// TODO Auto-generated method stub
		pr_target.addConsumer(this, fl);
	}

	@Override
	public void unsubscribe(IProvider pr_target) {
		// TODO Auto-generated method stub
		pr_target.removeConsumer(this);
	}
/**
 * 发布给订阅者
 */
	@Override
	public void publish(IConsumer cs, IFilter fl) {
		// TODO Auto-generated method stub
		pr.addConsumer(cs, fl);
	}

	@Override
	public void unpublish(IConsumer cs) {
		// TODO Auto-generated method stub
		pr.removeConsumer(cs);
	}

	@Override
	public Object getKey() {
		// TODO Auto-generated method stub
		return key;
	}

	@Override
	public boolean send(IProvider from,State s) {
		// TODO Auto-generated method stub
		//记住状态来自哪里
		setPrState(from,s);
		//仅仅发给订阅者就可以了
		pr.send(s);
		return true;
	}

	@Override
	public void init(Object key) {
		// TODO Auto-generated method stub
		this.key = key;
	}

	@Override
	public void destory() {
		// TODO Auto-generated method stub
		Iterator<Entry<Object, HashMap<Object, State>>> iter = pr_states.entrySet().iterator(); 
		while (iter.hasNext()) { 
			Entry<Object, HashMap<Object, State>> entry = iter.next(); 
			HashMap<Object, State> cur = entry.getValue(); 
			cur.clear();
		} 	
		pr_states.clear();
		pr.destory();
	}
	@Override
	public void clearStateByPr(IProvider pr_target) {
		// TODO Auto-generated method stub
		//向订阅者发布与该发布相关的所有状态清除
		HashMap<Object,State> prs = getPrStates(pr_target);
		if(prs==null){
			return;
		}
		Iterator<Entry<Object, State>> iter = prs.entrySet().iterator(); 
		while (iter.hasNext()) { 
			Entry<Object, State> entry = iter.next(); 
			State cur = entry.getValue(); 
			cur.setAction(State.ACTION_REMOVE);
			pr.send(cur);
		} 	
		prs.clear();
		pr_states.remove(pr_target.getKey());
	}
	@Override
	public void send(State s) {
		// TODO Auto-generated method stub
		pr.send(s);
	}

}
