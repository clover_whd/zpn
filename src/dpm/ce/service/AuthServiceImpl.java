package ce.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.annotation.Resource;

import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Service;

import net.bat.db.BaseDAOImpl;
//import net.bat.db.DBHelper;
import net.bat.util.Cfg;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

//import dao.hb.Test;


@Service("AuthServiceImpl")
public class AuthServiceImpl<T> {

	private BaseDAOImpl dao ;
	
	//private BaseDAOImpl2<T> dao2 ;
	
	private Class<T> clazz ;
	



	public BaseDAOImpl getDao() {
		return dao;
	}

	public void setDao(BaseDAOImpl dao) {
		this.dao = dao;
	}

//	public BaseDAOImpl2<T> getDao2() {
//		return dao2;
//	}
//
//	public void setDao2(BaseDAOImpl2<T> dao2) {
//		this.dao2 = dao2;
//	}

	public void saveEntity(T t) {
		dao.save(t);
	}

//	public void saveEntity2(T t) {
//		dao.saveEntity2(t);
//	}
	
	//hibernateTemplate
	/*
	public void  testTransaction_hiber1() throws Exception{
		//try{
			dao.hb.Test test1 = new dao.hb.Test();
			test1.setName1("htestTransaction_hiber1_1");
			saveEntity((T)test1);
			
			// int a = 1 / 0; 
			
			dao.hb.Test test2 = new dao.hb.Test();
			test2.setName1("testTransaction_hiber1_2");
			saveEntity((T)test2);
			
		//}catch(Exception ex){
//			ex.printStackTrace();
//			dao.hb.Test test3 = new dao.hb.Test();
//			test3.setName1("qqq");
//			saveEntity2((T)test3);
//			
//			// int a = 1 / 0; 
//			
//			dao.hb.Test test4 = new dao.hb.Test();
//			test4.setName1("wwww");
//			saveEntity2((T)test4);
		//}
	}
	
	public int  testRandom(){
		int max = 3;
		int min = 1;
		Random random = new Random();
		int s = random.nextInt(max) % (max-min+1) + min;
		return s;
	}
	
	public void  testCreate() throws Exception{
		
		for (int i=10021;i<=11020;i++){
			System.out.println("*********************"+i);

			
			dao.hb.PinPerson PinPerson = new dao.hb.PinPerson();
			
			String Major="英语";
			int r7= testRandom();
			if (r7==1){
				Major="博物馆学";
			}else if (r7==2){
				Major="建筑学";
			}
			String MajorReg = Major +"所报专业";
			
			PinPerson.setMajorReg(MajorReg);
			PinPerson.setMajorCode("zhuanyedaima12");
			PinPerson.setUsername("张三"+i);
			PinPerson.setIdentityNumber("1101011996060"+i); //18
			
			java.sql.Date dBirthday=java.sql.Date.valueOf("1996-06-01");  			
			PinPerson.setBirthday(dBirthday);
			
			int r1 = testRandom();
			String Gender="男";
			if (r1==1){
				Gender="男";
			}else if (r1==2){
				Gender="女";
			}
			
			PinPerson.setGender("男");
			PinPerson.setNation("汉");
			
			int r2= testRandom();
			String ps="群众";
			if (r2==1){
				ps="党员";
			}else if (r2==2){
				ps="团员";
			}
			PinPerson.setPoliticsStatus(ps);
			PinPerson.setPartyTime("2012年7月");
			
			int r6 = testRandom();
			String np="北京市";
			if (r6==1){
				np="河北省";
			}else if (r6==2){
				np="山西省";
			}
			PinPerson.setNativePlace(np);
			PinPerson.setPermanentResidence("北京市");
			PinPerson.setHomeAddress("家庭地址");
			PinPerson.setPhone("1390123"+i);
			
			int r3= testRandom();
			String gs="清华大学";
			if (r3==1){
				gs="北京理工大学";
			}else if (r3==2){
				gs="北京工业大学";
			}
			PinPerson.setGraduateSchool(gs);
			PinPerson.setGraduateDate("2013年7月");
			PinPerson.setGraduatedLocation("北京市");
			
			int r4= testRandom();
			String xl="大学";
			if (r4==1){
				xl="研究生";
			}else if (r4==2){
				xl="博士";
			}
			PinPerson.setEducational(xl);
			
//			int r5= testRandom();
//			String zy="英语";
//			if (r5==1){
//				zy="博物馆学";
//			}else if (r5==2){
//				zy="建筑";
//			}
			PinPerson.setMajor(Major); //专业
			PinPerson.setEnglishFour(84);//英语四级成绩
			PinPerson.setEnglishSix(86);//英语六级成绩
			PinPerson.setSourcePlace("河北");
			PinPerson.setHighSchool("河北省石 家庄市高级中学");
			PinPerson.setSelfdescription("自我描述,我是一个好学生。,我是一个好学生。,我是一个好学生。,我是一个好学生。,我是一个好学生。,我是一个好学生。,我是一个好学生。");
			PinPerson.setAwarding("获奖情况,优秀学生，优秀学生，优秀学生，优秀学生，优秀学生，优秀学生，优秀学生，优秀学生，优秀学生，优秀学生，优秀学生。");
			PinPerson.setOtherDescription("其他说明,其他说明其他说明其他说明其他说明其他说明其他说明其他说明,其他说明其他说明其他说明,,其他说明。");
			PinPerson.setFamilyInfo("家庭成员信息,家庭成员信息,家庭成员信息,家庭成员信息,家庭成员信息,家庭成员信息,家庭成员信息");
			PinPerson.setLearningExperience("学习经历,学习经历学习经历学习经历学习经历学习经历学习经历,学习经历学习经历学习经历,,,学习经历。");
			
			PinPerson.setEmail("aaa"+i+"@qq.com");
			PinPerson.setYear("2014");
			PinPerson.setIdentifier("121312432543"+i);
			PinPerson.setOtherlang("其他语种掌握说明,法语");
			PinPerson.setRemark("备注");
			PinPerson.setPhotoStand("http://www.dpm.org.cn/zhangsan"+i+".jpg");
			PinPerson.setAttachmentFilename("http://www.dpm.org.cn/zhangsan"+i+".doc");
			PinPerson.setReviewResults("合格");
			PinPerson.setDtCreate(new Date());
			PinPerson.setDtSubmit(new Date());
			PinPerson.setDtCheck(null);
			PinPerson.setStatus(1);
			PinPerson.setRegnum(1);
			PinPerson.setIssend(0);
			PinPerson.setIsreceive(0);

			
			saveEntity((T)PinPerson);
		}
	}
	
	//hibernateTemplate
	/*
	public void  testTransaction_hiber2() {
		
			dao.hb.Test test1 = new dao.hb.Test();
			test1.setName1("testTransaction_hiber2_1");
			saveEntity((T)test1);
			
			 //int a = 1 / 0; 
			
			dao.hb.Test test2 = new dao.hb.Test();
			test2.setName1("testTransaction_hiber2_2");
			saveEntity((T)test2);
	}
	
	//spring session
	public void  testTransaction(){
		
		dao.hb.Test test1 = new dao.hb.Test();
		test1.setName1("eee");
		saveEntity((T)test1);
		
		//int aa=1/0; 
		
		dao.hb.Test test2 = new dao.hb.Test();
		test2.setName1("ffff");
		saveEntity((T)test2);
		
		
//		Session session = sessionFactory.getCurrentSession();
//		System.out.println("**********session="+session.hashCode());
//		
//		dao.hb.Test test1 = new dao.hb.Test();
//		test1.setName1("aaaaaaaa");
//		dao.hb.Test test2 = new dao.hb.Test();
//		test2.setName1("bbb");
//		
//		session.save(test1);
//		
//		//int aa=1/0; 
//		
//		Session session2 = sessionFactory.getCurrentSession();
//		System.out.println("**********session="+session2.hashCode());
//		
//		session2.save(test2);
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
