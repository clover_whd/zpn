package ce.service;

import java.util.Calendar;
import java.util.Date;
import java.util.TimerTask;
import org.apache.log4j.Logger;
//import ce.dpm.log.ModuleLog;

//同步
public class SyncLdapTask extends TimerTask {
	
	private int mode;
	private  String ldapsynctime;
	
	
	
	//static final String VARNAME_LDAP = "CC_SYNC_LDAP_TIME";
	static final String VARNAME_SHTML = "RP_SHTML_UPDATE_TIME";
	static Logger log = Logger.getLogger(SyncLdapTask.class);
	int athour;
	static boolean bOK_ldap;
	// static boolean bOK_shtml;
	int day;
	int lastday = -1;

	public String getLdapsynctime() {
		return ldapsynctime;
	}

	public void setLdapsynctime(String ldapsynctime) {
		this.ldapsynctime = ldapsynctime;
	}

	public int getAthour() {
		return athour;
	}

	public void setAthour(int athour) {
		this.athour = athour;
	}



	public int getMode() {
		return mode;
	}

	public void setMode(int mode) { 
		this.mode = mode;
	}

	@Override
	public void run() {
System.out.println("");		
System.out.println("===========SyncLdapTask run=========");
System.out.println("");
		String out = "";
		Calendar cal = Calendar.getInstance();
		day = cal.get(Calendar.DAY_OF_MONTH);//  day : 10
		if (day != lastday) {           //lastday  -1
			bOK_ldap = false;
			// bOK_shtml=false;
		}
		int hour = cal.get(Calendar.HOUR_OF_DAY);  //12
//		if (getMode() == 0
//				&& hour == Integer.parseInt(SysVarHlp.getSysVar(VARNAME_LDAP,
//						null, null)) && !bOK_ldap) {    
		if (getMode() == 0
				&& hour == Integer.parseInt(ldapsynctime) && !bOK_ldap) {			
			Date begintime = new Date();
			out = "";
			try {
				log.info("同步用户开始");
				//String orgstr = SinosoftUM.getAllDeptfromLdap();      //机构
				String userstr = SinosoftUM.getAllpersonfromLdap();   //用户
				String orgstr="";
				out = "[同步组织机构人员]" + SinosoftUM.savetodb(userstr,orgstr);
				// out+="[自动生成JS文件]"+CreateJSBean.createAll();
				//ModuleLog.log(ModuleLog.MN_SYNC_ORG, null, null, out,
						//begintime, 1);
				log.info("同步用户结束");
			} catch (Exception e) {
				e.printStackTrace();
				//ModuleLog.log(ModuleLog.MN_SYNC_ORG, null, null, out,
						//begintime, 2);
				//ModuleLog.setModuleStatus(ModuleLog.MN_MAIL_SEND, 2);
			}
			bOK_ldap = true;
		}
		/*
		 * if(DefaultHandle.getMODE()==1 &&
		 * hour==Integer.parseInt(SysVarHlp.getSysVar(VARNAME_SHTML,null,null))
		 * && !bOK_shtml){ Date begintime = new Date(); out = ""; try {
		 * ChinaDate.TODAY=null; log.info("更新农历日期"); SysVarHlp.load();
		 * log.info("更新系统变量"); DoCreateHTML task = new DoCreateHTML(null,null);
		 * //log.info("自动更新页面开始"); log.info("自动更新页面开始,fm:"+
		 * Runtime.getRuntime().freeMemory()+",tm:"
		 * +Runtime.getRuntime().totalMemory()+",mm:"
		 * +Runtime.getRuntime().maxMemory()); out = "[自动生成静态网页]"+task.call();
		 * out+="[自动生成JS文件]"+CreateJSBean.createAll();
		 * ModuleLog.log(ModuleLog.MN_CREATE_HTML, null, null, out,
		 * begintime,1); //log.info("自动更新页面结束"); log.info("自动更新页面结束,fm:"+
		 * Runtime.getRuntime().freeMemory()+",tm:"
		 * +Runtime.getRuntime().totalMemory()+",mm:"
		 * +Runtime.getRuntime().maxMemory()); } catch (Exception e) { // TODO
		 * Auto-generated catch block log.info("自动更新页面及JS异常");
		 * ModuleLog.log(ModuleLog.MN_CREATE_HTML, null, null, out,
		 * begintime,2); ModuleLog.setModuleStatus(ModuleLog.MN_CREATE_HTML,2);
		 * e.printStackTrace(); } bOK_shtml=true; }
		 */
		lastday = day;
	}
}
