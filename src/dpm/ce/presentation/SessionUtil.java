package ce.presentation;

public class SessionUtil {
	
    private static final ThreadLocal session = new ThreadLocal();

    public static Session currentSession()  {
        Session s = (Session) session.get();
        if (s == null) {
            s = new Session();
            session.set(s);
        }
        return s;
    }

}
