package ce.presentation;

import  java.util.*;

public class Session {

	private String currentUser ="test";
	private Date   currentTime;
	private String currentActivityKey;
	private String currentActivityDefId;
	private String currentActivityName;
	private String mainProcessId;
	private boolean isOccurError;
	//给外部应用预留的一个参数，通过此参数可以使得 web 与 toolagent 之间传递工作流相关数据之外的额外数据
	private Object sysParam ; //此对象一般为做项目实施时，为整个系统定义的系统级别的参数
	private Object appParam ; //此对象一般为每个app拥有100%控制权的一个对象
	
	public String getCurrentActivityDefId() {
		return currentActivityDefId;
	}
	public void setCurrentActivityDefId(String currentActivityDefId) {
		this.currentActivityDefId = currentActivityDefId;
	}
	public String getCurrentActivityKey() {
		return currentActivityKey;
	}
	public void setCurrentActivityKey(String currentActivityKey) {
		this.currentActivityKey = currentActivityKey;
	}
	public String getCurrentActivityName() {
		return currentActivityName;
	}
	public void setCurrentActivityName(String currentActivityName) {
		this.currentActivityName = currentActivityName;
	}
	public Date getCurrentTime() {
		return currentTime;
	}
	public void setCurrentTime(Date currentTime) {
		this.currentTime = currentTime;
	}
	public String getCurrentUser() {
		return currentUser;
	}
	public void setCurrentUser(String currentUser) {
		this.currentUser = currentUser;
	}
	public String getMainProcessId() {
		return mainProcessId;
	}
	public void setMainProcessId(String mainProcessId) {
		this.mainProcessId = mainProcessId;
	}
	public Object getSysParam() {
		return sysParam;
	}
	public void setSysParam(Object param) {
		this.sysParam = param;
	}
	
	public Object getAppParam() {
		return appParam;
	}
	public void setAppParam(Object param) {
		this.appParam = param;
	}
	public boolean isOccurError() {
		return isOccurError;
	}
	public void setOccurError(boolean isOccurError) {
		this.isOccurError = isOccurError;
	}
	
	
}
