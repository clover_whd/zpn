package ce.presentation;

////import WorkflowService.*;
//import avens.omgapi.WfBase.BaseException;
//import avens.omgapi.WorkflowService.*;
//
//import javax.servlet.*;
//import avens.servlet.*;
/**
 * Object that will be held in session data.  This should
 * be the only object held there.  Methods should be called
 * on this object to set and get data.
 *
 * @author 
 * @version 1.0
 */
public class SharkTCSessionData {

    /**
     * Hash key to save session data for the SharkEC app in the Session
     */
    public static final String SESSION_KEY = "SharkTCSessionData";
    //private WorkflowClientService myService = null;
//    private Worklist myWorklist = null;

    private String userMessage = null;

    private String userError = null;

    private int authLevel=0;
    
	private String userName = null; //add by linhan
	private String userId = null; //add by linhan
	private String orgId = null; //add by linhan
	private String orgName = null; //add by linhan

//	private ServletContext context ;
    /**
     * Sets the WorkflowClientService object
     *
     * @param theWorkflowClientService the WorkflowClientService object
     */
//    public synchronized void setService(WorkflowClientService theWorkflowClientService) {
//        this.myService = theWorkflowClientService;
//    }

    /**
     * Gets the WorkflowClientService object
     *
     * @return WorkflowClientService
     */
////    public synchronized WorkflowClientService getService() {
////        return this.myService;
////    }
//
//    /**
//     * Sets the Worklist object
//     *
//     * @param theWorklist the Worklist object
//     */
//    public synchronized void setWorklist(Worklist theWorklist) {
//        this.myWorklist = theWorklist;
//    }
//
//    /**
//     * Gets the Worklist object
//     *
//     * @return Worklist
//     */
//    public synchronized Worklist getWorklist() {
//        return this.myWorklist;
//    }
//
//    /**
//     * Method to remove the current user from the session
//     */
//    public synchronized void removeUser() {
//		
//		try {
//			if(this.myService!=null)
//			  this.myService.disconnect();
//		} catch (BaseException e) {
//			// TODO �Զ���� catch ��
//			e.printStackTrace();
//		} catch (NotConnected e) {
//			// TODO �Զ���� catch ��
//			e.printStackTrace();
//		}
//        this.myService = null;
//        this.myWorklist = null;
//        //**add by linhan
//        this.userName = null;
//        this.userId=null;
//        this.orgId=null;
//        this.orgName=null;
//    }
//
//    /**
//     * Sets the message
//     *
//     * @param msg the message to be set
//     */
//    public synchronized void setUserMessage(String msg) {
//        this.userMessage = msg;
//    }
//
//    /**
//     * Sets the error
//     *
//     * @param msg the message to be set
//     */
//    public synchronized void setUserError(String msg) {
//        this.userError = msg;
//    }
//
//    public synchronized void setAuthLevel (int authLevel) {
//        this.authLevel=authLevel;
//    }
//
//    public int getAuthLevel() {
//        return authLevel;
//    }
//
//    /**
//     * Retrieve the most recent user message and then clear it so no
//     * other app tries to use it.
//     */
//    public synchronized String getAndClearUserMessage() {
//        String msg = this.userMessage;
//        this.userMessage = null;
//        return msg;
//    }
//
//    /**
//     * Retrieve the most recent user error and then clear it so no
//     * other app tries to use it.
//     */
//    public synchronized String getAndClearUserError() {
//        String msg = this.userError;
//        this.userError = null;
//        return msg;
//    }
//    
//	//**add by linhan
//	/**
//	 * Sets the userName
//	 *
//	 * @param usn the userName to be set
//	 */
//	public synchronized void setUserName(String usn) {
//		this.userName = usn;
//	}
//    
//	/**
//	 * Retrieve the user name 
//	 * 
//	 */
//	public synchronized String getUserName() {
//		return this.userName;
//	}
//
//	/**
//	 * Sets the userId
//	 *
//	 * @param usn the userName to be set
//	 */
//	public synchronized void setUserId(String uid) {
//		this.userId = uid;
//	}
//    
//	/**
//	 * Retrieve the org Id 
//	 * 
//	 */
//	public synchronized String getUserId() {
//		return this.userId;
//	}
//
//	/**
//	 * Sets the orgName
//	 *
//	 */	
//	public synchronized void setorgName(String orgn) {
//		this.orgName = orgn;
//	}
//    
//	/**
//	 * Retrieve the org name 
//	 * 
//	 */
//	public synchronized String getorgName() {
//		return this.orgName;
//	}
//
//	/**
//	 * Sets the org ID
//	 *
//	 */
//	public synchronized void setorgId(String orgid) {
//		this.orgId = orgid;
//	}
//    
//	/**
//	 * Retrieve the org ID 
//	 * 
//	 */
//	public synchronized String getorgId() {
//		return this.orgId;
//	}
//	//**add by linhan
//
//	public void setServletContext(ServletContext context) {
//		this.context =context;
//	}
//
//	public Worklist getAdminWorklist() {
//		Worklist worklist = (Worklist)this.context.getAttribute(WebConstants.AVENS_ADMIN_WORKLIST);
//		worklist.refresh();
//		return worklist;
//	}


}
