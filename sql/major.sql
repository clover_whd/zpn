insert into PIN_MAJOR (ID, MAJOR_NAME, MAJOR_CODE, DESCRIPTION, DEMAND, STATUS, YEAR, DT_CREATE, ORD)
values (41, '法语', 'fy', null, null, 1, '2014',null, null);
insert into PIN_MAJOR (ID, MAJOR_NAME, MAJOR_CODE, DESCRIPTION, DEMAND, STATUS, YEAR, DT_CREATE, ORD)
values (22, '社会工作、行政管理、工商管理专业', 'shgz', 'a', 'a', 1, '2014', null, 1);
insert into PIN_MAJOR (ID, MAJOR_NAME, MAJOR_CODE, DESCRIPTION, DEMAND, STATUS, YEAR, DT_CREATE, ORD)
values (23, '会计专业', 'kjzy', 'a', 'a', 1, '2014', null, 2);
insert into PIN_MAJOR (ID, MAJOR_NAME, MAJOR_CODE, DESCRIPTION, DEMAND, STATUS, YEAR, DT_CREATE, ORD)
values (24, '法律专业', 'flzy', 'a', 'a', 1, '2014', null, 3);
insert into PIN_MAJOR (ID, MAJOR_NAME, MAJOR_CODE, DESCRIPTION, DEMAND, STATUS, YEAR, DT_CREATE, ORD)
values (28, '英语（专业8级），第二外语以德语和日语为佳', 'yy8', 'a', 'a', 1, '2014', null, 4);
insert into PIN_MAJOR (ID, MAJOR_NAME, MAJOR_CODE, DESCRIPTION, DEMAND, STATUS, YEAR, DT_CREATE, ORD)
values (29, '非物质文化遗产保护、翻译专业', 'fwz', 'a', 'a', 1, '2014', null, 5);
insert into PIN_MAJOR (ID, MAJOR_NAME, MAJOR_CODE, DESCRIPTION, DEMAND, STATUS, YEAR, DT_CREATE, ORD)
values (30, '考古或艺术史、古文字或考古专业、美术专业', 'kg', 'a', 'a', 1, '2014', null, 6);
insert into PIN_MAJOR (ID, MAJOR_NAME, MAJOR_CODE, DESCRIPTION, DEMAND, STATUS, YEAR, DT_CREATE, ORD)
values (31, '博物馆学、历史、纺织丝绸史专业', 'bwgx', 'a', 'a', 1, '2014', null, 7);
insert into PIN_MAJOR (ID, MAJOR_NAME, MAJOR_CODE, DESCRIPTION, DEMAND, STATUS, YEAR, DT_CREATE, ORD)
values (32, '雕塑专业、服装专业、染织专业、漆艺专业、金属加工、机械制造、文物保护与修复专业', 'dszy', 'a', 'a', 1, '2014', null, 8);
insert into PIN_MAJOR (ID, MAJOR_NAME, MAJOR_CODE, DESCRIPTION, DEMAND, STATUS, YEAR, DT_CREATE, ORD)
values (33, '照明设计、物流管理、环境检测专业', 'zxsj', 'a', 'a', 1, '2014', null, 9);
insert into PIN_MAJOR (ID, MAJOR_NAME, MAJOR_CODE, DESCRIPTION, DEMAND, STATUS, YEAR, DT_CREATE, ORD)
values (34, '计算机相关专业、计算机软件编程、摄影', 'jsj', 'a', 'a', 1, '2014', null, 10);
insert into PIN_MAJOR (ID, MAJOR_NAME, MAJOR_CODE, DESCRIPTION, DEMAND, STATUS, YEAR, DT_CREATE, ORD)
values (35, '古籍修复、图书馆或古文献专业', 'gjxf', 'a', 'a', 1, '2014',null, 11);
insert into PIN_MAJOR (ID, MAJOR_NAME, MAJOR_CODE, DESCRIPTION, DEMAND, STATUS, YEAR, DT_CREATE, ORD)
values (36, '材料科学、化学、环境科学等复合型人才，硕士以上；建筑历史、遗产保护等专业', 'clkx', 'a', 'a', 1, '2014',null, 12);
insert into PIN_MAJOR (ID, MAJOR_NAME, MAJOR_CODE, DESCRIPTION, DEMAND, STATUS, YEAR, DT_CREATE, ORD)
values (37, '建筑工程及中文类相关专业', 'jzgc', 'a', 'a', 1, '2014',null, 13);
commit;