/*
Navicat MySQL Data Transfer

Source Server         : rm
Source Server Version : 50162
Source Host           : localhost:3306
Source Database       : zp

Target Server Type    : MYSQL
Target Server Version : 50162
File Encoding         : 65001

Date: 2014-09-25 09:04:10
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `l_sync`
-- ----------------------------
DROP TABLE IF EXISTS `l_sync`;
CREATE TABLE `l_sync` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entity` varchar(255) DEFAULT NULL,
  `entity_id` varchar(255) DEFAULT NULL,
  `oper` int(11) DEFAULT NULL,
  `dt_create` datetime DEFAULT NULL,
  `snap_json` varchar(4096) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of l_sync
-- ----------------------------

-- ----------------------------
-- Table structure for `m_inbox`
-- ----------------------------
DROP TABLE IF EXISTS `m_inbox`;
CREATE TABLE `m_inbox` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `dt_check` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_inbox
-- ----------------------------

-- ----------------------------
-- Table structure for `m_message`
-- ----------------------------
DROP TABLE IF EXISTS `m_message`;
CREATE TABLE `m_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `send_to` varchar(255) DEFAULT NULL,
  `uname` varchar(255) DEFAULT NULL,
  `on_line` int(11) DEFAULT NULL,
  `stype` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `dt_create` datetime DEFAULT NULL,
  `dt_modify` datetime DEFAULT NULL,
  `dt_reply` datetime DEFAULT NULL,
  `rname` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_message
-- ----------------------------

-- ----------------------------
-- Table structure for `n_base`
-- ----------------------------
DROP TABLE IF EXISTS `n_base`;
CREATE TABLE `n_base` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vid` int(11) DEFAULT NULL,
  `itype` int(11) DEFAULT NULL,
  `language` varchar(10) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `status_publish` int(11) DEFAULT NULL,
  `status_comment` int(11) DEFAULT NULL,
  `dt_create` datetime DEFAULT NULL,
  `dt_modify` datetime DEFAULT NULL,
  `tnid` int(11) DEFAULT NULL,
  `pnid` int(11) DEFAULT NULL,
  `ord` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of n_base
-- ----------------------------

-- ----------------------------
-- Table structure for `n_image`
-- ----------------------------
DROP TABLE IF EXISTS `n_image`;
CREATE TABLE `n_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nid` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `entity_name` varchar(255) DEFAULT NULL,
  `entity_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `detail` varchar(4096) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `fsize` int(11) DEFAULT NULL,
  `uri` varchar(255) DEFAULT NULL,
  `dt_create` datetime DEFAULT NULL,
  `dt_modify` datetime DEFAULT NULL,
  `did` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of n_image
-- ----------------------------

-- ----------------------------
-- Table structure for `n_sort`
-- ----------------------------
DROP TABLE IF EXISTS `n_sort`;
CREATE TABLE `n_sort` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nid` int(11) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `itype` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `ord` int(11) DEFAULT NULL,
  `dt_create` datetime DEFAULT NULL,
  `dt_modify` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of n_sort
-- ----------------------------

-- ----------------------------
-- Table structure for `o_attachment`
-- ----------------------------
DROP TABLE IF EXISTS `o_attachment`;
CREATE TABLE `o_attachment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_name` varchar(255) DEFAULT NULL,
  `entity_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL COMMENT 'Ãû³Æ',
  `stype` varchar(255) DEFAULT NULL COMMENT 'ÀàÐÍ',
  `suffix` varchar(255) DEFAULT NULL,
  `spec` varchar(255) DEFAULT NULL,
  `att_osize` int(11) DEFAULT NULL COMMENT 'ÎÄ¼þ´óÐ¡',
  `path` varchar(2048) DEFAULT NULL COMMENT 'Â·¾¶',
  `remark` varchar(255) DEFAULT NULL COMMENT '±¸×¢',
  `status` int(11) DEFAULT NULL COMMENT '×´Ì¬',
  `dt_create` datetime DEFAULT NULL,
  `dt_modify` datetime DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `did` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of o_attachment
-- ----------------------------

-- ----------------------------
-- Table structure for `pin_dataset`
-- ----------------------------
DROP TABLE IF EXISTS `pin_dataset`;
CREATE TABLE `pin_dataset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `USERNAME` varchar(255) NOT NULL,
  `DATASETNAME` varchar(100) NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `REMARK` varchar(255) DEFAULT NULL,
  `ORD` int(11) DEFAULT NULL,
  `DT_CREATE` datetime DEFAULT NULL,
  `STATUS` varchar(1000) DEFAULT NULL,
  `TOTAL` int(11) DEFAULT NULL,
  `DIRECTORS` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pin_dataset
-- ----------------------------

-- ----------------------------
-- Table structure for `pin_dataset_person`
-- ----------------------------
DROP TABLE IF EXISTS `pin_dataset_person`;
CREATE TABLE `pin_dataset_person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `DATASET_ID` int(11) NOT NULL,
  `PERSON_ID` int(11) DEFAULT NULL,
  `ORD` int(11) DEFAULT NULL,
  `REVIEW_RESULTS2` varchar(1000) DEFAULT NULL,
  `DT_CREATE` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pin_dataset_person
-- ----------------------------

-- ----------------------------
-- Table structure for `pin_funmapping`
-- ----------------------------
DROP TABLE IF EXISTS `pin_funmapping`;
CREATE TABLE `pin_funmapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `VERSION` int(11) NOT NULL,
  `USERNAME` varchar(255) NOT NULL,
  `FUN_NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pin_funmapping
-- ----------------------------

-- ----------------------------
-- Table structure for `pin_major`
-- ----------------------------
DROP TABLE IF EXISTS `pin_major`;
CREATE TABLE `pin_major` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `MAJOR_NAME` varchar(100) NOT NULL,
  `MAJOR_CODE` varchar(20) NOT NULL,
  `DESCRIPTION` varchar(1000) DEFAULT NULL,
  `DEMAND` varchar(1000) DEFAULT NULL,
  `STATUS` int(11) NOT NULL,
  `YEAR` varchar(20) NOT NULL,
  `DT_CREATE` datetime DEFAULT NULL,
  `ORD` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pin_major
-- ----------------------------

-- ----------------------------
-- Table structure for `pin_objmapping`
-- ----------------------------
DROP TABLE IF EXISTS `pin_objmapping`;
CREATE TABLE `pin_objmapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `USERNAME` varchar(255) DEFAULT NULL,
  `DATASET_ID` int(11) DEFAULT NULL,
  `DT_CREATE` datetime DEFAULT NULL,
  `DT_END` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pin_objmapping
-- ----------------------------

-- ----------------------------
-- Table structure for `pin_person`
-- ----------------------------
DROP TABLE IF EXISTS `pin_person`;
CREATE TABLE `pin_person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `MAJOR_REG` varchar(100) DEFAULT NULL,
  `MAJOR_CODE` varchar(20) DEFAULT NULL,
  `USERNAME` varchar(20) DEFAULT NULL,
  `IDENTITY_NUMBER` varchar(18) DEFAULT NULL,
  `BIRTHDAY` date DEFAULT NULL,
  `GENDER` varchar(20) DEFAULT NULL,
  `NATION` varchar(20) DEFAULT NULL,
  `POLITICS_STATUS` varchar(20) DEFAULT NULL,
  `PARTY_TIME` varchar(20) DEFAULT NULL,
  `NATIVE_PLACE` varchar(50) DEFAULT NULL,
  `PERMANENT_RESIDENCE` varchar(255) DEFAULT NULL,
  `HOME_ADDRESS` varchar(255) DEFAULT NULL,
  `PHONE` varchar(20) DEFAULT NULL,
  `GRADUATE_SCHOOL` varchar(50) DEFAULT NULL,
  `GRADUATE_DATE` varchar(50) DEFAULT NULL,
  `GRADUATED_LOCATION` varchar(100) DEFAULT NULL,
  `EDUCATIONAL` varchar(20) DEFAULT NULL,
  `MAJOR` varchar(100) DEFAULT NULL,
  `MAJOR2` varchar(100) DEFAULT NULL,
  `MAJOR3` varchar(100) DEFAULT NULL,
  `ENGLISH_FOUR` int(11) DEFAULT NULL,
  `ENGLISH_SIX` int(11) DEFAULT NULL,
  `SOURCE_PLACE` varchar(100) DEFAULT NULL,
  `HIGH_SCHOOL` varchar(100) DEFAULT NULL,
  `SELFDESCRIPTION` varchar(1000) DEFAULT NULL,
  `AWARDING` varchar(1000) DEFAULT NULL,
  `OTHER_DESCRIPTION` varchar(1000) DEFAULT NULL,
  `FAMILY_INFO` varchar(1000) DEFAULT NULL,
  `LEARNING_EXPERIENCE` varchar(1000) DEFAULT NULL,
  `EMAIL` varchar(50) NOT NULL,
  `YEAR` varchar(20) DEFAULT NULL,
  `IDENTIFIER` varchar(50) DEFAULT NULL,
  `OTHERLANG` varchar(100) DEFAULT NULL,
  `PHOTO_STAND` varchar(100) DEFAULT NULL,
  `ATTACHMENT_FILENAME` varchar(100) DEFAULT NULL,
  `REVIEW_RESULTS` varchar(20) DEFAULT NULL,
  `REMARK` varchar(255) DEFAULT NULL,
  `DT_CREATE` datetime DEFAULT NULL,
  `DT_SUBMIT` datetime DEFAULT NULL,
  `DT_CHECK` datetime DEFAULT NULL,
  `USER_CHECK` varchar(10) DEFAULT NULL,
  `STATUS` int(11) DEFAULT NULL,
  `REGNUM` int(11) DEFAULT NULL,
  `ISSEND` int(11) DEFAULT NULL,
  `ISRECEIVE` int(11) DEFAULT NULL,
  `WORK_EXPERIENCE` varchar(1000) DEFAULT NULL,
  `STATUS_FILTER` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pin_person
-- ----------------------------

-- ----------------------------
-- Table structure for `u_grant`
-- ----------------------------
DROP TABLE IF EXISTS `u_grant`;
CREATE TABLE `u_grant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gid` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `dt_create` datetime DEFAULT NULL,
  `dt_modify` datetime DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `pm_menu` varchar(255) DEFAULT NULL,
  `pm_entity` varchar(255) DEFAULT NULL,
  `pm_oper` int(11) DEFAULT NULL,
  `pm_includes` varchar(255) DEFAULT NULL,
  `pm_excludes` varchar(255) DEFAULT NULL,
  `did` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of u_grant
-- ----------------------------

-- ----------------------------
-- Table structure for `u_group`
-- ----------------------------
DROP TABLE IF EXISTS `u_group`;
CREATE TABLE `u_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `dt_create` datetime DEFAULT NULL,
  `dt_modify` datetime DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `did` int(11) DEFAULT NULL,
  `grant_menu` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of u_group
-- ----------------------------
INSERT INTO u_group VALUES ('1', null, null, null, null, null, '配置权限', null, '系统管理;系统管理#用户;系统管理#用户组;系统管理#招聘专业');

-- ----------------------------
-- Table structure for `u_org`
-- ----------------------------
DROP TABLE IF EXISTS `u_org`;
CREATE TABLE `u_org` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oid` varchar(255) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `pid` varchar(255) DEFAULT NULL,
  `ord` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `dt_create` datetime DEFAULT NULL,
  `dt_modify` datetime DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `did` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of u_org
-- ----------------------------

-- ----------------------------
-- Table structure for `u_user`
-- ----------------------------
DROP TABLE IF EXISTS `u_user`;
CREATE TABLE `u_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gid` int(11) NOT NULL,
  `userid` int(11) DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `lang` varchar(255) DEFAULT NULL,
  `ipaddr` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `tel` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `weibo` varchar(255) DEFAULT NULL,
  `dpt` varchar(255) DEFAULT NULL,
  `qq` varchar(255) DEFAULT NULL,
  `valid_email` bit(1) DEFAULT NULL,
  `valid_tel` bit(1) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `user_id` varchar(255) NOT NULL,
  `pwd` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `name_pyh` varchar(255) DEFAULT NULL,
  `dt_login` datetime DEFAULT NULL,
  `dt_logout` datetime DEFAULT NULL,
  `on_line` int(11) DEFAULT NULL,
  `handle` varchar(255) DEFAULT NULL,
  `dt_create` datetime DEFAULT NULL,
  `dt_modify` datetime DEFAULT NULL,
  `dt_sync` datetime DEFAULT NULL,
  `sync_id` int(11) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `oid` int(11) DEFAULT NULL,
  `did` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of u_user
-- ----------------------------
INSERT INTO u_user VALUES ('1', '1', '22', null, null, '127.0.0.1', null, null, null, null, null, null, null, null, null, '1', 'Admin', '2e33a9b0b06aa0a01ede70995674ee23', '初始化用户', 'cshyh', '2014-09-25 09:03:32', null, '1', '54a109bf-aceb-404e-97fa-b30d8f3ce2e0', null, null, null, null, null, null, null);
