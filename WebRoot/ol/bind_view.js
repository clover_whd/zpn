var map, wfs;
var btns;
var layers={};
var sel;
var gphy,snap;

OpenLayers.ProxyHost = "proxy.cgi?url=";
var attr_pref = "ATTR_";
var cn_device = "dao.hb.ODevice";
var cn_device_type = "dao.hb.ODeviceType";
var cn_split="@";
var attr_names={"bind":"input","mode":"input"};
//feature bind device map
var bfs={};
//mtype properties mapping,should get by subscribe
//通过型号表label字段定义决定显示哪些字段
var label_attr={"1":"类型1: ${ATTR_bind}\n\dt: ${dt}","2":"类型2: ${ATTR_bind}\n\ndt: ${dt} \ntmpr: ${tmpr} \nhumi: ${humi}"};

var LocString=String(window.document.location.href);
function getLabelRules(label_attrs){
	var rules=[];
	for(var x in label_attrs){
		rules[rules.length]=new OpenLayers.Rule({
            symbolizer: {
            	labelAlign:"left",
            	fill: true,
                label: label_attrs[x]
            },
            filter: new OpenLayers.Filter.Comparison({
                type: "==",
                property:"mid",
                value: x
            })
        });
	}	
	return rules;
}
function getQueryStr(str){
    var rs = new RegExp("(^|)"+str+"=([^\&]*)(\&|$)","gi").exec(LocString), tmp;

    if(tmp=rs){
        return tmp[2];
    }
    // parameter cannot be found
    return false;
}

var DeleteFeature = OpenLayers.Class(OpenLayers.Control, {
    initialize: function(layer, options) {
        OpenLayers.Control.prototype.initialize.apply(this, [options]);
        this.layer = layer;
        this.handler = new OpenLayers.Handler.Feature(
            this, layer, {click: this.clickFeature}
        );
    },
    clickFeature: function(feature) {
        // if feature doesn't have a fid, destroy it
        if(feature.fid == undefined) {
            this.layer.destroyFeatures([feature]);
        } else {
            feature.state = OpenLayers.State.DELETE;
            this.layer.events.triggerEvent("afterfeaturemodified", 
                                           {feature: feature});
            feature.renderIntent = "select";
            this.layer.drawFeature(feature);
        }
    },
    setMap: function(map) {
        this.handler.setMap(map);
        OpenLayers.Control.prototype.setMap.apply(this, arguments);
    },
    CLASS_NAME: "OpenLayers.Control.DeleteFeature"
});
function unselectAll(){
	for(var i=0; i<btns.length; i++){
		var btn = btns[i];
		if(btn.selectControl){
			btn.selectControl.unselectAll();
		}
	}
}
function addlayer(layer_name,rules){
    var feas = layers[layer_name].feas;
    var renderer = OpenLayers.Util.getParameters(window.location.href).renderer;
    renderer = (renderer) ? [renderer] : OpenLayers.Layer.Vector.prototype.renderers;
    var sm =  new OpenLayers.StyleMap({
    	'default': new OpenLayers.Style(null, {
        rules: rules})});
    wfs = new OpenLayers.Layer.Vector(layer_name, {
        styleMap:sm,
        renderers: renderer
    });    
    map.addLayers([wfs]);
	layers[layer_name]=wfs;
	var opt=document.createElement("option");	
	opt.text = layer_name;
	sel.add(opt);
	
     wfs.events.on({
        "featureselected": onKMLSelect,
        "featureunselected": onKMLUnselect
    });
	if(btns){
    	updateEditable(layer_name);
		return;
	}else{
		map.layer_edit = wfs;
	}
   var select = new OpenLayers.Control.SelectFeature(wfs/*, {
        selectStyle: OpenLayers.Util.extend(
            {fill: false, stroke: true},
            OpenLayers.Feature.Vector.style["select"])
    }*/);
    map.addControl(select);
    select.activate(); 
    if(feas){
    	for(var i=0; i<feas.length;i++){
    		feas[i].wfs=wfs;
    	}
    	wfs.addFeatures(feas);
    }
    
}


function updateEditable(name) {
	var layer = layers[name];
	wfs = layer;
	snap.setLayer(layer);
	for(var i=0; i<btns.length; i++){
		var btn = btns[i];
		var modActive = btn.active;
	    if(modActive) {
	        btn.deactivate();
	    }
		btn.layer = layer;
		if(btn.selectControl){
			btn.selectControl.unselectAll();
			btn.selectControl.layer = layer;
			btn.selectControl.handlers.feature.layer = layer;
		}
		if(btn.dragControl){
		    btn.dragControl.layer = layer;
		    btn.dragControl.handlers.drag.layer = layer;
		    btn.dragControl.handlers.feature.layer = layer;
		}
	    if(modActive) {
	        btn.activate();
	    }
	}
}
var imgLoad = function (url, callback) {
	var img = new Image();

	img.src = url;
	if (img.complete) {
		callback(url,img.width, img.height);
	} else {
		img.onload = function () {
			callback(url,img.width, img.height);
			img.onload = null;
		};
	};
};
function init(){
	var purl = getQueryStr("purl");
	var pid = getQueryStr("pid");
	if(pid && !purl){
	 jsonrpc = new JSONRpcClient(CFG.app_ctx+"JSON-RPC");  
	  var req={"map":{hql:'from OAttachment where id='+pid,cns:['dao.hb.OAttachment'],pss:[["id","path"]]},"javaClass":"java.util.HashMap"};
	  jsonrpc.BH.handle(function(data,ex){
	  	//alert(data);
	  	purl=data[1].items.map.path;
	  	imgLoad("../servlet/fu?f="+purl,initMap);
	  },'G',req,'HDDefault'); 		
	}else if(purl){
		imgLoad("../servlet/fu?f="+purl,initMap);
		//loadImg("../servlet/fu?f="+purl,img_width,img_height);
	}	
}
function initMap(url,img_width,img_height) {
    sel = document.getElementById("editable");
    sel.value = "poly";
    sel.onchange = function() {
        updateEditable(sel.value);
    };
    tb_attr =  document.getElementById("ul_attr");
    for(var x in attr_names){
		var td1=document.createElement('li');
		td1.innerHTML=x;
		var td2=document.createElement('li');
		var td2_ipt = document.createElement(attr_names[x]);
		td2_ipt.size=6;
		td2_ipt.onchange=function(){
		 var attr_fid = document.getElementById("attr_fid");
		 var feature=attr_fid.feature;
			feature.attributes[this.id]=this.value;
		}
		td2_ipt.id = attr_pref+x;
		td2.appendChild(td2_ipt);
		tb_attr.appendChild(td1);//行增加单元格
		tb_attr.appendChild(td2);//行增加单元格
    }
    map = new OpenLayers.Map('map');
	//addlayer("layer1");
	map.addControl(new OpenLayers.Control.LayerSwitcher());
	map.addControl(new OpenLayers.Control.MousePosition()); 

	loadImg(url,img_width,img_height);

    var in_options = {
        'internalProjection': map.baseLayer.projection,
        'externalProjection': new OpenLayers.Projection("EPSG:4326")
    };   
    var out_options = {
        'internalProjection': map.baseLayer.projection,
        'externalProjection': new OpenLayers.Projection("EPSG:4326")
    };
	fmt_in = new OpenLayers.Format.GeoJSON(in_options);
	fmt_out = new OpenLayers.Format.GeoJSON(out_options);
	//load feature
	var jurl = getQueryStr("jurl");
	if(jurl){
		$("#output").load("../servlet/fu?f="+jurl,function(responseText,textStatus,XMLHttpRequest){ 
			inputfmt();
		}); 
	}
	OpenLayers.Event.observe(document, "keydown", function(evt) {
		var draw;
		for(var i=0; i<btns.length; i++){
			var btn = btns[i];
			if(btn.active&& btn.undo){
				draw=btn;
				break;
			}
	    }
	    if(!draw){
	    	return;
	    }
	    var handled = false;
	    switch (evt.keyCode) {
	        case 90: // z
	            if (evt.metaKey || evt.ctrlKey) {
	                draw.undo();
	                handled = true;
	            }
	            break;
	        case 89: // y
	            if (evt.metaKey || evt.ctrlKey) {
	                draw.redo();
	                handled = true;
	            }
	            break;
	        case 27: // esc
	            draw.cancel();
	            handled = true;
	            break;
	    }
	    if (handled) {
	        OpenLayers.Event.stop(evt);
	    }
	});	
	
	init_modal();
}

function onKMLSelect(event) {
	 var feature = event.feature;
	 var attr_fid = document.getElementById("attr_fid");
	 attr_fid.feature=feature;
	 attr_fid.value = feature.id;
	 for(var x in attr_names){
	 	var ipt_id = attr_pref+x;
	 	var ipt = document.getElementById(ipt_id);
	 	if(ipt){
	 		ipt.value = feature.attributes[ipt_id]||"";
	 	}
	 }
}
function onKMLUnselect(event) {
    var feature = event.feature;
}
function output_layer(layer){
	document.getElementById("output").innerHTML=fmt_out.write(layer.features,true);
}
function output(){
 	unselectAll();
 	var out = "layers={};\n";
	for(var x in layers){
		out+="\nlayers['"+x+"']="+fmt_out.write(layers[x].features,true);
	}
	return out;
}
function inputfmt(){
    var element = document.getElementById('output');
    var instr = element.value;

	sel.innerHTML="";
	bfs={};
	for(var x in layers){
		map.removeLayer(layers[x]);
	}
	eval(instr);
	for(var x in layers){
		//addlayer(x);
		var features =fmt_in.read(layers[x]);
		buildbfs(features);
		layers[x].feas = features;
	}
    //element.value= len_added+" feature added."
    buildwsc();
}
function buildbfs(features){
    var bounds;
    if(features.constructor != Array) {
        features = [features];
    }
    //bind feature
    for(var i=0; i<features.length; i++){
    	var fea = features[i];
    	//build device-feature mapping
    	var bid = fea.attributes["ATTR_bind"];
    	if(bid){
    		bfs[cn_device+cn_split+bid]=fea;
    	}
    }
    //wfs.addFeatures(features);
}

function input_layer(features){
    var bounds;
    if(features.constructor != Array) {
        features = [features];
    }
    //bind feature
    for(var i=0; i<features.length; i++){
    	var fea = features[i];
    	//build device-feature mapping
    	var bid = fea.attributes["ATTR_bind"];
    	if(bid){
    		bfs[bid]=fea;
    		//fea.attributes["stype"]="2";
    	}
    }
    //wfs.addFeatures(features);
}

function loadImg(img_path,img_width,img_height){
	if(gphy){
		map.removeLayer(gphy);
		gphy.destroy();
	}		

    var out = document.getElementById('output');
    if(!img_path){
		return;
    }
    
   // img_size = QFord_getImageSize(img_path); 
	var ch = document.documentElement.clientHeight -out.clientHeight;
	var cw =document.documentElement.clientWidth ;
	var sh = img_height/ch;
	var sw = img_width/cw;
	var sm = Math.max(sh,sw);
	var div_size={};
 	if(sm==sh){		
		div_size.width =Math.round(img_width/sm);
		div_size.height=ch;
	}else{
		div_size.height =Math.round(img_height/sm);		
		div_size.width=cw;
	}
	var p;
	for(var p = 0; p<20; p++){
		if(Math.pow(2,p)>=sm){
			break;
		}
	}
    var extent = new OpenLayers.Bounds(
        0, 0, img_width, img_height
    );

	var options = {numZoomLevels: p+1};
    gphy = new OpenLayers.Layer.Image( 
        '底图',
        img_path,
        extent,
        new OpenLayers.Size(div_size.width, div_size.height),
        options
    );

   var map_div = document.getElementById('map');
	map_div.style.width = div_size.width+"px";
	map_div.style.height = div_size.height+"px";
	map.updateSize();
	map.addLayer(gphy);
	map.zoomToExtent(extent, true);
	document.getElementById('bg_img').value=img_path;
}


function init_modal() {
	
	jQuery.noConflict();
	
	// Position modal box in the center of the page
	jQuery.fn.center = function () {
		this.css("position","absolute");
		this.css("top", ( jQuery(window).height() - this.height() ) / 2+jQuery(window).scrollTop() + "px");
		this.css("left", ( jQuery(window).width() - this.width() ) / 2+jQuery(window).scrollLeft() + "px");
		return this;
	  }
	
	jQuery(".modal-profile").center();
	
	// Set height of light out div	
	jQuery('.modal-lightsout').css("height", jQuery(document).height());	

	// Fade in modal box once link is clicked
	jQuery('a[rel="modal-output"]').click(function() {
		document.getElementById("output").value =output();
		jQuery('.modal-profile').fadeIn("slow");
		jQuery('.modal-lightsout').fadeTo("slow", .5);
	});
	jQuery('a[rel="modal-input"]').click(function() {
		jQuery('.modal-profile').fadeIn("slow");
		jQuery('.modal-lightsout').fadeTo("slow", .5);
	});
	jQuery('a[rel="modal-bg"]').click(function() {
		var img_url = prompt("请输入底图url");
		if(img_url==null){
			return;
		}
		//loadImg(img_url,{width:width,height:height}); 
		imgLoad(img_url,loadImg);
	});
	jQuery('a[rel="bh_bind"]').click(function() {
		var ps={};
		ps.ct = output();
		var pid =getQueryStr("pid")
		if(pid){
			ps.pid = pid;
		}
		var jid =getQueryStr("jid")
		if(jid){
			ps.jid = jid;
		}
		bind(ps);
	});
	
	// closes modal box once close link is clicked, or if the lights out divis clicked
	jQuery('a.modal-close-profile, .modal-lightsout').click(function() {
		jQuery('.modal-profile').fadeOut("slow");
		jQuery('.modal-lightsout').fadeOut("slow");
	});
}

function bind(ps){
 jsonrpc = new JSONRpcClient(CFG.app_ctx+"JSON-RPC");  
  var req={"map":ps,"javaClass":"java.util.HashMap"};
  jsonrpc.BH.handle(function(data,ex){
  	alert(data);
  },'B',req,'UMService'); 
}

function updateInfo(s){
	var key = s.key;
	var fea = bfs[key];
	if(!fea){
		return;
	}
	var its = s.items.map;
	for(var x in its){
		fea.attributes[x]=its[x];
	}
	fea.wfs.drawFeature(fea);
}
function buildwsc(){
	var ids=[];
	for(var x in bfs){
		ids[ids.length]=x;
	}
	//凡序列化无法处理null及空数组[]
 	wsc = New(WSClient,[getWSURL(),10000,["PR_SYS_HEART","PR_DAO","PR_ROBOT","PR_DAO"],["",cn_device,"ROBOT_DATA",cn_device_type],[[""],ids,ids,[]]]);
	wsc.setevt("onclose",function(){
		alert("onclose");
	});
	wsc.setevt("onopen",function(){
		//alert("onopen");
	});
	
	wsc.setfunc("ROBOT_DATA",function(s){
		//收到设备动态属性增加/更新
		console.log(s);
		switch(s.action){
		case 'A':
			break;
		case 'R':
			break;
		case 'U':
			updateInfo(s);
			break;
		}
	});
  	wsc.setfunc("PR_SYS_END",function(s){
  		if(s.pr!="PR_DAO"){
  			return;
  		}
  		//所有dao.hb配置信息加载完成,todo 绑定设备
  		var dcs = wsc.getStateByCn(cn_device);
  		var dts = wsc.getStateByCn(cn_device_type);
  		//处理类型对应的label定义
  		if(!dts || !dcs){
  			return;
  		}
  		var labels={};
  		//set mid to feature
  		for(var x in dcs){
  			var mid = dcs[x].items.map.mid;
  			var fea = bfs[x];
  			fea.attributes["mid"]=mid;
  			if(!labels[mid]){
  				labels[mid]=dts[cn_device_type+cn_split+mid].items.map.attrLabel.replace(/\\n/g,"\n");
  			}
  		}
  		var rules = getLabelRules(labels);
  		//change layer stylemap
  		for(var x in layers){
			addlayer(x,rules);
  		}
  		//console.log(labels);
  	});
  	wsc.connect();	
}
