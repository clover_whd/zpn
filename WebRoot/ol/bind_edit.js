var map, wfs;
var btns;
var layers={};
var sel;
var gphy,snap;

OpenLayers.ProxyHost = "proxy.cgi?url=";
var attr_pref = "ATTR_";
var attr_names={"bind":"input","mode":"input"};


var LocString=String(window.document.location.href);

function getQueryStr(str){
    var rs = new RegExp("(^|)"+str+"=([^\&]*)(\&|$)","gi").exec(LocString), tmp;

    if(tmp=rs){
        return tmp[2];
    }
    // parameter cannot be found
    return false;
}

var DeleteFeature = OpenLayers.Class(OpenLayers.Control, {
    initialize: function(layer, options) {
        OpenLayers.Control.prototype.initialize.apply(this, [options]);
        this.layer = layer;
        this.handler = new OpenLayers.Handler.Feature(
            this, layer, {click: this.clickFeature}
        );
    },
    clickFeature: function(feature) {
        // if feature doesn't have a fid, destroy it
        if(feature.fid == undefined) {
            this.layer.destroyFeatures([feature]);
        } else {
            feature.state = OpenLayers.State.DELETE;
            this.layer.events.triggerEvent("afterfeaturemodified", 
                                           {feature: feature});
            feature.renderIntent = "select";
            this.layer.drawFeature(feature);
        }
    },
    setMap: function(map) {
        this.handler.setMap(map);
        OpenLayers.Control.prototype.setMap.apply(this, arguments);
    },
    CLASS_NAME: "OpenLayers.Control.DeleteFeature"
});
function unselectAll(){
	for(var i=0; i<btns.length; i++){
		var btn = btns[i];
		if(btn.selectControl){
			btn.selectControl.unselectAll();
		}
	}
}
function addlayer(layer_name,feas){
    wfs = new OpenLayers.Layer.Vector(layer_name);    
    map.addLayers([wfs]);
	layers[layer_name]=wfs;
	var opt=document.createElement("option");	
	opt.text = layer_name;
	sel.add(opt);
     wfs.events.on({
        "featureselected": onKMLSelect,
        "featureunselected": onKMLUnselect
    });
    if(feas){
		wfs.addFeatures(feas);
	}
	if(btns){
    	updateEditable(layer_name);
		return wfs;
	}else{
		map.layer_edit = wfs;
	}
	 snap = new OpenLayers.Control.Snapping({
        layer: wfs,
        targets: [wfs],
        greedy: false
    });
    snap.activate();
    
    var draw = new OpenLayers.Control.DrawFeature(
        wfs, OpenLayers.Handler.Polygon,
        {
            title: "画多边形",
            displayClass: "olControlDrawFeaturePolygon",
            multi: true
        }
    );
    var draw_rect = new OpenLayers.Control.DrawFeature(
        wfs, OpenLayers.Handler.RegularPolygon,
        {
            title: "画方框",
            displayClass: "olControlDrawFeaturePolygon",
            handlerOptions: {sides: 4,irregular:true}
        }
    );
    var draw_point = new OpenLayers.Control.DrawFeature(
        wfs, OpenLayers.Handler.Point,
        {
            title: "画点",
            displayClass: "olControlDrawFeaturePoint",
            multi: true
        }
    );
   
    var edit = new OpenLayers.Control.ModifyFeature(wfs, {
        title: "整体修改",
        displayClass: "olControlModifyFeature"
    });
	edit.mode |= OpenLayers.Control.ModifyFeature.ROTATE;
	edit.mode |= OpenLayers.Control.ModifyFeature.DRAG;
	edit.mode &= ~OpenLayers.Control.ModifyFeature.RESHAPE;

    var edit2 = new OpenLayers.Control.ModifyFeature(wfs, {
        title: "局部修改",
        displayClass: "olControlModifyFeature"
    });
	
	var del = new DeleteFeature(wfs, {title: "点击删除"});
   
    var btn_save = new OpenLayers.Control.Button({
        title: "输出当前层",
        trigger: function() {
        	 unselectAll();
            output_layer(wfs);
			jQuery('.modal-profile').fadeIn("slow");
			jQuery('.modal-lightsout').fadeTo("slow", .5);
        },
        displayClass: "olControlSaveFeatures"
    });
    btns=[btn_save, del, edit, edit2, draw,draw_rect,draw_point];
    
    var container = document.getElementById("panel_toolbar");
     var panel = new OpenLayers.Control.Panel({
        displayClass: 'customEditingToolbar',
        allowDepress: true,
        div:container
    });
    panel.addControls(btns);
    map.addControl(panel);
    return wfs;
}


function updateEditable(name) {
	var layer = layers[name];
	wfs = layer;
	snap.setLayer(layer);
	for(var i=0; i<btns.length; i++){
		var btn = btns[i];
		var modActive = btn.active;
	    if(modActive) {
	        btn.deactivate();
	    }
		btn.layer = layer;
		if(btn.selectControl){
			btn.selectControl.unselectAll();
			btn.selectControl.layer = layer;
			btn.selectControl.handlers.feature.layer = layer;
		}
		if(btn.dragControl){
		    btn.dragControl.layer = layer;
		    btn.dragControl.handlers.drag.layer = layer;
		    btn.dragControl.handlers.feature.layer = layer;
		}
	    if(modActive) {
	        btn.activate();
	    }
	}
}
var imgLoad = function (url, callback) {
	var img = new Image();

	img.src = url;
	if (img.complete) {
		callback(url,img.width, img.height);
	} else {
		img.onload = function () {
			callback(url,img.width, img.height);
			img.onload = null;
		};
	};
};
function init(){
	var purl = getQueryStr("purl");
	var pid = getQueryStr("pid");
	if(pid && !purl){
	 jsonrpc = new JSONRpcClient(CFG.app_ctx+"JSON-RPC");  
	  var req={"map":{hql:'from OAttachment where id='+pid,cns:['dao.hb.OAttachment'],pss:[["id","path"]]},"javaClass":"java.util.HashMap"};
	  jsonrpc.BH.handle(function(data,ex){
	  	//alert(data);
	  	purl=data[1].items.map.path;
	  	imgLoad("../servlet/fu?f="+purl,initMap);
	  },'G',req,'HDDefault'); 		
	}else if(purl){
		imgLoad("../servlet/fu?f="+purl,initMap);
		//loadImg("../servlet/fu?f="+purl,img_width,img_height);
	}	
}
function initMap(url,img_width,img_height) {
    sel = document.getElementById("editable");
    sel.value = "poly";
    sel.onchange = function() {
        updateEditable(sel.value);
    };
    tb_attr =  document.getElementById("ul_attr");
    for(x in attr_names){
		var td1=document.createElement('li');
		td1.innerHTML=x;
		var td2=document.createElement('li');
		var td2_ipt = document.createElement(attr_names[x]);
		td2_ipt.size=6;
		td2_ipt.onchange=function(){
		 var attr_fid = document.getElementById("attr_fid");
		 var feature=attr_fid.feature;
			feature.attributes[this.id]=this.value;
		}
		td2_ipt.id = attr_pref+x;
		td2.appendChild(td2_ipt);
		tb_attr.appendChild(td1);//行增加单元格
		tb_attr.appendChild(td2);//行增加单元格
    }
    map = new OpenLayers.Map('map');
	map.addControl(new OpenLayers.Control.LayerSwitcher());
	map.addControl(new OpenLayers.Control.MousePosition()); 

	loadImg(url,img_width,img_height);

    var in_options = {
        'internalProjection': map.baseLayer.projection,
        'externalProjection': new OpenLayers.Projection("EPSG:4326")
    };   
    var out_options = {
        'internalProjection': map.baseLayer.projection,
        'externalProjection': new OpenLayers.Projection("EPSG:4326")
    };
	fmt_in = new OpenLayers.Format.GeoJSON(in_options);
	fmt_out = new OpenLayers.Format.GeoJSON(out_options);
	//load feature
	var jurl = getQueryStr("jurl");
	if(jurl){
		$("#output").load("../servlet/fu?f="+jurl,function(responseText,textStatus,XMLHttpRequest){ 
			inputfmt();
		}); 
	}else{
		addlayer("layer1");
	}
	
	OpenLayers.Event.observe(document, "keydown", function(evt) {
		var draw;
		for(var i=0; i<btns.length; i++){
			var btn = btns[i];
			if(btn.active&& btn.undo){
				draw=btn;
				break;
			}
	    }
	    if(!draw){
	    	return;
	    }
	    var handled = false;
	    switch (evt.keyCode) {
	        case 90: // z
	            if (evt.metaKey || evt.ctrlKey) {
	                draw.undo();
	                handled = true;
	            }
	            break;
	        case 89: // y
	            if (evt.metaKey || evt.ctrlKey) {
	                draw.redo();
	                handled = true;
	            }
	            break;
	        case 27: // esc
	            draw.cancel();
	            handled = true;
	            break;
	    }
	    if (handled) {
	        OpenLayers.Event.stop(evt);
	    }
	});	
	
	init_modal();
}

function onKMLSelect(event) {
	 var feature = event.feature;
	 var attr_fid = document.getElementById("attr_fid");
	 attr_fid.feature=feature;
	 attr_fid.value = feature.id;
	 for(x in attr_names){
	 	var ipt_id = attr_pref+x;
	 	var ipt = document.getElementById(ipt_id);
	 	if(ipt){
	 		ipt.value = feature.attributes[ipt_id]||"";
	 	}
	 }
}
function onKMLUnselect(event) {
    var feature = event.feature;
}
function output_layer(layer){
	document.getElementById("output").innerHTML=fmt_out.write(layer.features,true);
}
function output(){
 	unselectAll();
 	var out = "layers={};\n";
	for(x in layers){
		out+="\nlayers['"+x+"']="+fmt_out.write(layers[x].features,true);
	}
	return out;
}
function inputfmt(){
    var element = document.getElementById('output');
    var instr = element.value;
    //var len_added=0;
    if(instr.indexOf("{")<5){
    	var features = fmt_in.read(instr);
		len_added+=input_layer(features);
    }else{
    	sel.innerHTML="";
    	for(x in layers){
    		map.removeLayer(layers[x]);
    	}
    	eval(instr);
    	for(x in layers){
    		
    		var features =fmt_in.read(layers[x]);
    		addlayer(x,features);
    		//layers[x]=x;
			//len_added+=input_layer(features);
    	}
    }
    //element.value= len_added+" feature added."
}
function input_layer(features){
    var bounds;
    if(features) {
        if(features.constructor != Array) {
            features = [features];
        }
        for(var i=0; i<features.length; ++i) {
            if (!bounds) {
                bounds = features[i].geometry.getBounds();
            } else {
                bounds.extend(features[i].geometry.getBounds());
            }

        }
        wfs.addFeatures(features);
        //map.zoomToExtent(bounds);
        var plural = (features.length > 1) ? 's' : '';
        return features.length;
    } else {
        return 0;
    }	
}
function QFord_getImageSize(FilePath) {  
    var imgSize={  
    width:0,  
    height:0  
    };  
     image=new Image();  
    image.src=FilePath;  
    imgSize.width =image .width;  
    imgSize .height=image .height;  
    return imgSize;  
}  

function loadImg(img_path,img_width,img_height){
	if(gphy){
		map.removeLayer(gphy);
		gphy.destroy();
	}		

    var out = document.getElementById('output');
    if(!img_path){
		return;
    }
    
   // img_size = QFord_getImageSize(img_path); 
	var ch = document.documentElement.clientHeight -out.clientHeight;
	var cw =document.documentElement.clientWidth ;
	var sh = img_height/ch;
	var sw = img_width/cw;
	var sm = Math.max(sh,sw);
	var div_size={};
 	if(sm==sh){		
		div_size.width =Math.round(img_width/sm);
		div_size.height=ch;
	}else{
		div_size.height =Math.round(img_height/sm);		
		div_size.width=cw;
	}
	var p;
	for(var p = 0; p<20; p++){
		if(Math.pow(2,p)>=sm){
			break;
		}
	}
    var extent = new OpenLayers.Bounds(
        0, 0, img_width, img_height
    );

	var options = {numZoomLevels: p+1};
    gphy = new OpenLayers.Layer.Image( 
        '底图',
        img_path,
        extent,
        new OpenLayers.Size(div_size.width, div_size.height),
        options
    );

   var map_div = document.getElementById('map');
	map_div.style.width = div_size.width+"px";
	map_div.style.height = div_size.height+"px";
	map.updateSize();
	map.addLayer(gphy);
	map.zoomToExtent(extent, true);
	document.getElementById('bg_img').value=img_path;
}


function init_modal() {
	
	jQuery.noConflict();
	
	// Position modal box in the center of the page
	jQuery.fn.center = function () {
		this.css("position","absolute");
		this.css("top", ( jQuery(window).height() - this.height() ) / 2+jQuery(window).scrollTop() + "px");
		this.css("left", ( jQuery(window).width() - this.width() ) / 2+jQuery(window).scrollLeft() + "px");
		return this;
	  }
	
	jQuery(".modal-profile").center();
	
	// Set height of light out div	
	jQuery('.modal-lightsout').css("height", jQuery(document).height());	

	jQuery('a[rel="modal-new"]').click(function() {
       unselectAll();
       var layer_name = prompt("给新的层取个名字吧");
       addlayer(layer_name);
	});
	// Fade in modal box once link is clicked
	jQuery('a[rel="modal-output"]').click(function() {
		document.getElementById("output").value =output();
		jQuery('.modal-profile').fadeIn("slow");
		jQuery('.modal-lightsout').fadeTo("slow", .5);
	});
	jQuery('a[rel="modal-input"]').click(function() {
		jQuery('.modal-profile').fadeIn("slow");
		jQuery('.modal-lightsout').fadeTo("slow", .5);
	});
	jQuery('a[rel="modal-bg"]').click(function() {
		var img_url = prompt("请输入底图url");
		if(img_url==null){
			return;
		}
		//loadImg(img_url,{width:width,height:height}); 
		imgLoad(img_url,loadImg);
	});
	jQuery('a[rel="bh_bind"]').click(function() {
		var ps={};
		ps.ct = output();
		var pid =getQueryStr("pid")
		if(pid){
			ps.pid = pid;
		}
		var jid =getQueryStr("jid")
		if(jid){
			ps.jid = jid;
		}
		bind(ps);
	});
	
	// closes modal box once close link is clicked, or if the lights out divis clicked
	jQuery('a.modal-close-profile, .modal-lightsout').click(function() {
		jQuery('.modal-profile').fadeOut("slow");
		jQuery('.modal-lightsout').fadeOut("slow");
	});
}

function bind(ps){
 jsonrpc = new JSONRpcClient(CFG.app_ctx+"JSON-RPC");  
  var req={"map":ps,"javaClass":"java.util.HashMap"};
  jsonrpc.BH.handle(function(data,ex){
  	alert(data);
  },'B',req,'UMService'); 
}