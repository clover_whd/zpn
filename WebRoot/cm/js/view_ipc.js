//10-24
var localUrl="";
function getRealUrlAtt(value){
	if (!localUrl){
		var str=window.location.href; 
		var ipos1 = str.indexOf("/zpn/");
		if (ipos1 > 0){
			localUrl = str.substring(0,ipos1); 
			var ipos4=localUrl.lastIndexOf(":");
			if (ipos4>10){
				localUrl = localUrl.substring(0,ipos4);
			}
		}
	}
	var realValue = value;
	if (value){
		var ipos2 = value.indexOf("/zp/");   
		if (ipos2 > 0 ){
			realValue = localUrl + value.substring(ipos2);
		}
	}
	//document.title=value +"--"+ realValue;
	return realValue;
}


var gname = '用人单位';
var encodegname =encodeURI('用人单位');
var word_timer;
function outputWord(hql,path_tp,page_break,pss_set){
  if(!Ext.isIE){
  	Bat.outputEx({msg:'目前word导出只支持ie,并要求本机已安装word'},'运行环境错误');
    return; 
  }
  var req,word;
  var bstop=false;
  var pagenum=3;
  var pss,pref;
  var info={
  	cout_succ:0,
  	cout_fail:0,
  	err_info:"",
  	hql:"",
  	pos:0,
  	total:0
  };
   var f_step = function(info){
	clearTimeout(word_timer);
	if(bstop || info.pos>=info.total){
		Ext.MessageBox.updateProgress(1, info.pos+'/'+info.total);
		var tip="共导出【"+info.cout_succ+"】条";
		var icon = Ext.MessageBox.INFO;
		if(info.cout_fail>0){
			tip+=",出错【"+info.cout_fail+"】条,错误日志如下：<br>"+info.err_info;
			icon = Ext.MessageBox.ERROR
		}
		Ext.MessageBox.show({
			   title: '导出结束',
			   msg: tip,
			   buttons: Ext.MessageBox.OK,
			   icon: icon
		  });
		return;
	}
   	req.map.start=info.pos;
   	req.map.rownum=1;
	jsonrpc.BH.handle(function(data, ex) {
		if (Bat.outputEx(ex, '请求数据异常'))
			return;
		var cwh = 120;
		var len = data.length;
		for(var i=0; i<len; i++){
			var obj = data[i];
			if(obj.action!='O')
				continue;
			info.pos++;
			var itmap=obj.items.map;
			//test permanentResidence
			var pn="";
			var pv="";
			
			try{
				var range = word.paste();
				for(var j=0;j<pss.length;j++){
					//var pn = pss[j];
					pn = pss[j];
					if(pn=='photoStand')
						continue;
					//var pv=itmap[pn];
					pv=itmap[pref+pn];
					if(pv && typeof pv=='object'){
						pv= Ext.Date.format(pv,'Y-m-d');						
					}
					if (!pv){
						pv="";
					}
					word.replaceText(range,pn,pv);
				}
				//for photo
				pn=pref+'photoStand';
				pv = itmap[pn];
				var new_pv=getRealUrlAtt(pv);
				var img = new Image(); 
				img.range=range;
				img.src = new_pv;
				img.pn = 'photoStand';
				if(img.complete){
			    	if(img.width>img.height){
			    		w = cwh;
			    		h = Math.floor((cwh * img.height)/img.width);
			    	}else{
			    		h = cwh;
			    		w =Math.floor( (cwh * img.width)/img.height);		    		
			    	}							
					var res = word.replacePic(range,pn,new_pv,w,h);
				}else img.onload = function (){ 
					//alert(this);
					var img =this;
			    	if(img.width>img.height){
			    		w = cwh;
			    		h = Math.floor((cwh * img.height)/img.width);
			    	}else{
			    		h = cwh;
			    		w = Math.floor((cwh * img.width)/img.height);		    		
			    	}							
					var res = word.replacePic(img.range,img.pn,img.src,w,h);
				}
				info.cout_succ++;
			}catch(e){
				info.cout_fail++;
				info.err_info+=itmap[pref+'username']+"["+itmap[pref+'id']+"] "+pn+":"+pv+"<br>";
				continue;
			}
			//是否插入分页符
			if(page_break && info.pos%page_break==0){
				word.appendPageBreak();
			}
		}
   		//fill doc template with data
	}, 'G', req, '');  

	var ps = info.pos/info.total;
	Ext.MessageBox.updateProgress(ps, info.pos+'/'+info.total);
	word_timer=setTimeout(function(){
		f_step(info);
	},1000);
   }
   var f_show=function(){
   dlg_export = Ext.MessageBox.show({
       title: '导出word文件,导出过程中请勿关闭word',
       msg: '正在请求数据并导出...',
       progressText: '',
       width:300,
       progress:true,
       buttons :Ext.MessageBox.CANCEL,
       fn:function(){
	      Ext.MessageBox.confirm("确认","是否终止当前导出？", function(btn){
	    	  if(btn=="yes")
		 		bstop=true;
		 	  else
		 	  	f_show();
	       });
	       return false;
	   	},
       closable:false
   });
   }	
   f_show();
   //请求总数
   if(hql.indexOf('PinDatasetPerson')!=-1){
   	req={		
		"map" : {
			"rownum" : pagenum,
			"pss" : [pss_set,['id']],
			"cns" : ['dao.hb.PinPerson','dao.hb.PinDatasetPerson'],
			"hql" : hql,
			"start" : 0
		},
		"javaClass" : "java.util.HashMap"
	}; 
	pref="A.";
   }
	else{
	req={		
		"map" : {
			"rownum" : pagenum,
			"pss" : [pss_set],
			"cns" : ['dao.hb.PinPerson'],
			"hql" : hql,
			"start" : 0
		},
		"javaClass" : "java.util.HashMap"
	}; 
	pref="";
	}
	pss=pss_set||req.map.pss[0];
	jsonrpc.BH.handle(function(data, ex) {
		if (Bat.outputEx(ex, '请求记录数异常'))
			return;
		info.total=data;
		info.hql=hql;
   		Ext.MessageBox.updateProgress(0, '0/'+info.total);
   		word_timer=setTimeout(function(){
			word = new WordApp(path_tp); 
			word.wordObj.visible=true; 
			word.copyAll();
   			f_step(info);
   		},100);   		
	}, 'C', req, '');  
}

var fea_tile0 = Ext.create('Ext.ux.grid.feature.Tileview', {
	id:Ext.id(),
    tableTpl: [
        '{%',
            // Add the row/column line classes to the table element.
            'var view=values.view,tableCls=["' + Ext.baseCSSPrefix + '" + view.id + "-table ' + Ext.baseCSSPrefix + 'grid-table"];',
            'if (view.columnLines) tableCls[tableCls.length]=view.ownerCt.colLinesCls;',
            'if (view.rowLines) tableCls[tableCls.length]=view.ownerCt.rowLinesCls;',
        '%}',
        '<table role="presentation" id="{view.id}-table" class="{[tableCls.join(" ")]}" border="0" cellspacing="0" cellpadding="0" style="{tableStyle}" tabIndex="-1">',
            '{[view.renderTHead(values, out)]}',
            '{[view.renderTFoot(values, out)]}',
            '<tbody id="{view.id}-body">',
            '{%',
                'view.renderRows(values.rows, values.viewStartIndex, out);',
            '%}',
            '</tbody>',
        '</table>',
        {
            priority: 0
        }
    ],
    viewMode: 'tiles',
    viewTpls:
    {
        medium: [
            '<tpl if="view.tileViewFeature.viewMode==\'medium\'">',
            '{%',
                'console.log(values.view.tileViewFeature.viewMode, "medium");',
                'var dataRowCls = values.recordIndex === -1 ? "" : " ' + Ext.baseCSSPrefix + 'grid-data-row";',
                'var columnValues = values.view.tileViewFeature.getColumnValues(values.columns, values.record);',
            '%}',
            '<tr role="row" {[values.rowId ? ("id=\\"" + values.rowId + "\\"") : ""]} ',
                'data-boundView="{view.id}" ',
                'data-recordId="{record.internalId}" ',
                'data-recordIndex="{recordIndex}" ',
                'class="tileview {[values.itemClasses.join(" ")]} {[values.rowClasses.join(" ")]}{[dataRowCls]}" ',
                '{rowAttr:attributes} tabIndex="-1">',
                '<td role="gridcell" class="{tdCls} x-grid-tdx ux-explorerview-medium-icon-row" {tdAttr} id="{[Ext.id()]}">',
                    '<table class="x-grid-row-table">',
                        '<tbody>',
                            '<tr>',
                                '<td class="x-grid-col x-grid-cell ux-explorerview-icon" style="background: url(&quot;/bat/servlet/fu?f={[this.getTN(columnValues.uri,128)]}&quot;) no-repeat scroll 50% 100% transparent;">',
                                '</td>',
                            '</tr>',
                            '<tr>',
                                '<td class="x-grid-col x-grid-cell">',
                                    '<div {unselectableAttr} class="x-grid-cell-inner">{[columnValues.title]}</div>',
                                '</td>',
                            '</tr>',
                        '</tbody>',
                    '</table>',
                '</td>',
            '</tr>',
            '<tpl else>',
                '{% console.log(values.view.tileViewFeature.viewMode, "NOT medium"); %}',
                '{%this.nextTpl.applyOut(values, out, parent);%}',
            '</tpl>',
            {
                priority: 0,
	             getTN: function(uri,attSize){
		           return uri+'&size='+attSize+'&mr='+Math.random();
		        }
            }
        ],

        tiles: [
            '<tpl if="view.tileViewFeature.viewMode==\'tiles\'">',
            '{%',
                'console.log(values.view.tileViewFeature.viewMode, "tiles");',
                'var dataRowCls = values.recordIndex === -1 ? "" : " ' + Ext.baseCSSPrefix + 'grid-data-row";',
                'var columnValues = values.view.tileViewFeature.getColumnValues(values.columns, values.record);',
            '%}',
            '<tr role="row" {[values.rowId ? ("id=\\"" + values.rowId + "\\"") : ""]} ',
                'data-boundView="{view.id}" ',
                'data-recordId="{record.internalId}" ',
                'data-recordIndex="{recordIndex}" ',
                'class="tileview {[values.itemClasses.join(" ")]} {[values.rowClasses.join(" ")]}{[dataRowCls]}" ',
                '{rowAttr:attributes} tabIndex="-1">',
                '<td role="gridcell" class="{tdCls} x-grid-tdx ux-explorerview-detailed-icon-row" {tdAttr} id="{[Ext.id()]}">',
                    '<table class="x-grid-row-table">',
                        '<tbody>',
                            '<tr>',
                                '<td class="x-grid-col x-grid-cell ux-explorerview-icon" style="background: url(&quot;/bat/servlet/fu?f={[this.getTN(columnValues.uri,128)]}&quot;) no-repeat scroll 50% 50% transparent;">',
                                '</td>',
                                '<td class="x-grid-col x-grid-cell">',
                                    '<div class="x-grid-cell-inner" unselectable="on">{[columnValues.title]}<br><span>{[columnValues.detail]}</span></div>',
                                '</td>',
                            '</tr>',
                        '</tbody>',
                    '</table>',
                '</td>',
            '</tr>',
            '<tpl else>',
                '{% console.log(values.view.tileViewFeature.viewMode, "NOT tiles"); %}',
                '{%this.nextTpl.applyOut(values, out, parent);%}',
            '</tpl>',
            {
                priority: 0,
	             getTN: function(uri,attSize){
		           return uri+'&size='+attSize+'&mr='+Math.random();
		        }
            }
        ]
    }
});

function buildRel(btn,entity2,fn_id,fns){
	var grid = btn.up('bat_grid');
	var ui=grid.ui;
	 var ui1=ui.ui1;
	 if(!ui1)
	 	return;
	 var sels=grid.getSelectionModel().getSelection();
	if(!sels) return;
	 var tobjs = ui1.objs;
	 var robj = ui1.robj;
	 var entity1,kv1;
	
	 entity1 = robj.cns[0];	

	 var kn = 'id';
	//考虑到中英文表的情况
	 kv1 = robj.items.map[kn]||robj.items.map['A.'+kn];
	 var dobj = tobjs[0];
	 var appendObjs=[];
	for(var i=0; i<sels.length; i++){
		var tobj = Bat.clone(dobj);
		tobj.action='A';
		var o = sels[i].obj;
		o.action = "R";
		tobj.items.map['A.entity1']=entity1 ;
		tobj.items.map['A.id1']=kv1;
		tobj.items.map['A.entity2']=entity2;
		tobj.items.map['A.id2']=o.items.map[fn_id];
		tobj.items.map['A.ord']=tobjs.length-1;
		for(var j=0; j<fns.length; j++){
			var fn = fns[j];
			var pos = fn.indexOf('.');
			var fno = (pos==-1)?fn:fn.substring(pos+1);
			//由于关联视图保存时,从对象没有id,不保存,增加一些属性不会出问题
			tobj.items.map['B.'+fno]=o.items.map[fn];
		}
		tobjs[tobjs.length]=tobj;
		appendObjs[appendObjs.length]=tobj;
	}
	
	ui.reload = false;
	ui.ds.load();
	if(ui1.grid.renderfunc){
		ui1.grid.renderfunc(ui1.grid.body,ui1,appendObjs);
	}else{
		ui1.reload = false;
		ui1.ds.load();
		//ui1.grid.getSelectionModel().selectLastRow();
	}
}

String.prototype.byteLen = function(){

	var len = 0;

	i = this.length;

	while(i--){

	len += (this.charCodeAt(i)>255?2:1);

	}

	return len;

	}

BView.btns={
	"json_cls":	{text:'json',
		cls: 'x-btn-text-icon',
		menu:{items:[{
	   		text: '导出',
	        cls: 'x-btn-text-icon',
	        icon: 'icons/disk.png',
			handler : function(btn){
		var robj = btn.ui.robj;
		   Ext.MessageBox.wait({
	           title: 'Please wait',
	           msg: 'Loading items...'
	       });
	       var im = robj.items.map;
		  var req={"map":{nid:im.id},"javaClass":"java.util.HashMap"};
		  jsonrpc.BH.handle(function(data,ex){
	   		if(Bat.outputEx(ex,'生成JSon异常')){
	   			return;
	   		}
			Ext.Msg.show({
			   title:'JSon output complete',
			   msg:data,
			   buttons: Ext.Msg.OK,
			   icon: Ext.MessageBox.INFO
			});	   		
	   		//Ext.MessageBox.hide();
	   		//var fn = im.id  +"_cls_"+im.language+".json";
	   		//var jwin = window.open("../servlet/fu?f="+im.userid+"\\json\\"+fn);
		  },'S',req,'RMService');            	
 		
	}},{
	text: '导入',
    cls: 'x-btn-text-icon',
    icon: 'icons/add.png',
	handler : function(btn){
		//TODO
	}				
	}]}},
	"json":	{text:'json',
		cls: 'x-btn-text-icon',
		menu:{items:[{
	   		text: '导出内容',
	        cls: 'x-btn-text-icon',
	        icon: 'icons/disk.png',
			handler : function(btn){
			  var filter;
			  var sels=btn.ui.grid.getSelectionModel().getSelections();
			  if(!sels || sels.length==0){
		        Ext.MessageBox.show({
		           title: '系统提示',
		           msg: '请先选中一条记录',
		           buttons: Ext.MessageBox.OK,
		           icon: Ext.MessageBox.INFO
		       });
			  	return;
			  }
			var selmap = sels[0].obj.items.map;
			filter = "from NBase where id="+selmap.id;
		   Ext.MessageBox.show({
	           title: 'Please wait...',
	           msg: 'Loading items...',
	           progressText: 'Initializing...',
	           width:300,
	           progress:true,
	           closable:true
	           //animEl: 'mb6'
	       });
	  var req={"map":layout.usr_info,"javaClass":"java.util.HashMap"};
	  jsonrpc.BH.handle(function(data,ex){
   		if(Bat.outputEx(ex,'建立任务异常'))
   			return;
   		
		  var req={"map":{tid:data,filter:filter,bcover:false},"javaClass":"java.util.HashMap"};
		  jsonrpc.BH.handle(function(data,ex){
	   		if(Bat.outputEx(ex,'生成JSon异常')){
	   			//Ext.MessageBox.hide();
	   			return;
	   		}
			Ext.Msg.show({
			   title:'JSon output complete',
			   msg:data,
			   buttons: Ext.Msg.OK,
			   icon: Ext.MessageBox.INFO
			});	   		
		  },'J',req,'RMService');            	

   			
   		},'N',req,'TaskHandle');            	
		
	}},{
	text: '导出封面',
    cls: 'x-btn-text-icon',
    icon: 'icons/add.png',
	handler : function(btn){
	  var req={"map":layout.usr_info,"javaClass":"java.util.HashMap"};
	  jsonrpc.BH.handle(function(data,ex){
   		if(Bat.outputEx(ex,'建立任务异常'))
   			return;
   		
		  var req={"map":{tid:data,bcover:true},"javaClass":"java.util.HashMap"};
		  jsonrpc.BH.handle(function(data,ex){
	   		if(Bat.outputEx(ex,'生成JSon异常')){
	   			//Ext.MessageBox.hide();
	   			return;
	   		}
			Ext.Msg.show({
			   title:'JSon output complete',
			   msg:data,
			   buttons: Ext.Msg.OK,
			   icon: Ext.MessageBox.INFO
			});	   		
		  },'J',req,'RMService');            	

   			
   		},'N',req,'TaskHandle');            	
	}				
	}]}},
	
 "copyto":{
	text: Bat.info.mnu_copyto_title,
    cls: 'x-btn-text-icon',
    icon: 'icons/disk_multiple.png',
	handler : function(btn){		
      var ui=btn.ui;
	  var sels=btn.ui.grid.getSelectionModel().getSelections();
	  var bsel = sels && sels.length>0;
	  if(!bsel){
	  	return;
	  }
	  var title = (bsel?Bat.info.mnu_copyto_sel:Bat.info.mnu_copyto_flt)+Bat.info.mnu_copyto_title;
		Ext.Msg.prompt(title,Bat.info.mnu_copyto_rtid, function(btn, text){
		   if (btn == 'ok'){
		        // process text value and close...
		   	var ids=[];
		   	var rtid=text;
	  		for(var i=0; i<sels.length; i++){
	  			var im = sels[i].obj.items.map;
	  			ids[ids.length]=im.id;
	  		}

		   Ext.MessageBox.show({
	           title: 'Please wait',
	           msg: 'Loading items...',
	           progressText: 'Initializing...',
	           width:300,
	           progress:true,
	           closable:true
	           //animEl: 'mb6'
	       });
	  var req={"map":layout.usr_info,"javaClass":"java.util.HashMap"};
	  jsonrpc.BH.handle(function(data,ex){
   		if(Bat.outputEx(ex,'建立任务异常'))
   			return;
   		
		  var req={"map":{tid:data,ids:ids,rtid:rtid},"javaClass":"java.util.HashMap"};
		  jsonrpc.BH.handle(function(data,ex){
	   		if(Bat.outputEx(ex,'复制菜单异常')){
	   			//Ext.MessageBox.hide();
	   			return;
	   		}
	   		Ext.MessageBox.hide();
		  },'C',req,'RMService');            	
   		},'N',req,'TaskHandle');            		  			  		
		   }
		});
}}
}

BView.viewMap={
"dao.hb.UOrg":{
	handles:{'before_setobj_detail':function(ui,editors,obj){
		 
 	},
	'before_setdata':function(ui,pobjs,flg){
		
		var po = pobjs[0];
		if(po.action=='D'){
			po.items.map.status=1;
		}			
	}
	}, 
	  req:{
		  "pss":[["id","name","oid","pid",/*"userid",*//*"ord",*//*"status",*/"dtModify","dtCreate"]],
		  "cns":["dao.hb.UOrg"],
		  "hql":"from UOrg"
	  },
	  cfg_prop:{
		  "id":{
			  display:"编号",
			  editor:"DisplayField"
		  },
		  "oid":{
			  display:"机构编号",
			  hidden:true
		  },
		  "name":{
			  display:"名称",
			  width:150,
			  cfg:{
					  allowBlank:false,
					  regex:/^.{0,100}$/,
				      validationEvent:"click",
				      regexText:"只能输入0-100汉字或字符"
				 }
		  },
		  "pid":{
			   display:"上级机构",
				editor:"TreeCombo",
				 dcfg:{
					   editable:false
					},
				cfg:{
					singleSelect:true,
					width:385,
			        lazyInit:false,
					store: new Ext.data.TreeStore({
		                proxy: {
		                    type: 'ajax',
		                    url:Bat.WC +'servlet/JSonBaseServlet?oid=1'
		                },
		                sorters: [{
		                    property: 'leaf',
		                    direction: 'ASC'
		                }, {
		                    property: 'text',
		                    direction: 'ASC'
		                }]
		            })
				}
		   },
		  /*"A.ord":{
			  display:"排序",
			  hidden:true
		  },*/
		 /* "status":{
			  display:"状态",
			  editor:'ComboBox',
		        display:"使用状态",
		        width:100,
		        cfg:{
			        displayField : 'txt',
					valueField : 'val',
					typeAhead : true,
					queryMode : 'local',
					triggerAction : 'all',				
					selectOnFocus : true,
			    	listConfig: {
					        getInnerTpl: function() {
					            return '<div data-qtip="{txt}">{style} {txt}</div>';
					        }
					},
					store : new Ext.data.ArrayStore({
						fields :['txt', 'val'],
						data :[['使用',1],['停用',0]]
					})
		        },
		        renderer : function(val) {	
		           	  var dtxt = {						
						'1':'<font style="color:green;font-size:13px">使用</font>',	
						'0':'<font style="color:red;font-size:13px">停用</font>'};
						return dtxt[val];
		       }
		  },*/
		  "dtCreate":{
			  display:'建立时间',
			  editor:"DateField",
		      cfg:{readOnly:true,readOnlyCls:'form_date_readonly'}
		  },
		  "dtModify":{
			  display:'修改时间',
			  editor:"DateField",
		      cfg:{readOnly:true,readOnlyCls:'form_date_readonly'}
		  }/*,
		  "userid":{
			  display:"创建人",
			  editor:"DisplayField"
		  }*/
	  }
	  ,cfg_detail:{
			btns:["保存"],
			tabs:[{
				title : "基本信息",
				fset : [{
					legend : "",
					props :["id",/*"userid",*/"name","pid",/*"A.ord",*//*"status",*/"dtModify","dtCreate"]
				}]
				}]},
			cfg_grid:{
				btns:['录入','提示删除','检索','刷新','排序']
			}
  },	
"rel_user":{
	req:{
		"pss" : [["id","entity1", "id1","entity2","id2","ord","dtCreate"],['name','avatar']],
		"cns" : ["dao.hb.RRel","dao.hb.UUser"],
		"hql" : "from RRel A, UUser B where A.entity2='dao.hb.UUser' and A.id2=B.id order by A.ord"
	},
	cfg_detail:{
		btns:["保存"]
	},	
	cfg_grid:{
		btns:['删除','保存','检索','刷新','排序']
	},
	cfg_prop:{
	"A.id":{
	  display:'序号',
	  cfg:{
	  	readOnly:true
	  }
	},
	"A.id1":{
	 	display:"id1",
	 	hidden:true
	},
	"A.id2":{
	 	display:"id2",
	 	hidden:true
	},
	"A.ord":{
	 	display:"顺序"
	},
	"A.entity1":{
	 	display:"entity1",
	 	hidden:true
	},
	"A.entity2":{
	 	display:"entity2",
	 	hidden:true
	},
	"B.name":{
	 display:"用户名"
	},	
	"B.avatar":{
	  display:'用户头像',
	  	cfg:{readOnly:true},
	  	editor:'DisplayField',
	  	renderer : function(itxt,r,col,l) {	
	  		var src=itxt?"/ipcm/servlet/fu?f="+itxt:'icons/avatar.png';
	  		var its = r.record?r.record.data:r.item||{};
	  		return "<img style='cursor:hand;' src='"+src+"' title='"+ its['B.name']
	  		+"' onclick='layout.renderDetailByKey(\"dao.hb.UUser\",false,"+its['A.id2']+");return false;'/>";
		}
	 },
	 "pid":{
	 display:"父节点"
	},"A.dtCreate":{
	  editor:"DateField",
	  display:'建立时间',
	  cfg:{
		readOnly:true
	 	}  
	 }
	}
 },
 "dao.hb.NSort":{
	req:{
		"pss" : [["id","nid", "userid","title","pid","dtCreate"]],
		"cns" : ["dao.hb.NSort"],
		"hql" : "from NSort"
	},
	cfg_detail:{
		btns:["保存"]
	},	
	cfg_prop:{
	"id":{
	  display:'序号',
	  cfg:{
	  	readOnly:true
	  }
	},
	"nid":{
	 display:"主节点"
	},
	"title":{
	 display:"题名"
	},	
	"userid":{
	 display:"编辑者"
	},	
	"pid":{
	 display:"父节点"
	},
	"dtCreate":{
	  editor:"DateField",
	  display:'建立时间',
	  cfg:{
		readOnly:true
	 	}  
	 }
	}
 },

"dao.hb.NImage":{
	cn:'影像',
	req:{
		"pss" : [["id", "userid","title","detail","width","height","fsize","uri","entityName","entityId","dtCreate"]],
		"cns" : ["dao.hb.NImage"],
		"hql" : "from NImage"
	},
	cfg_grid:{
	cfg:{	   
	   func_init:function(grid){
		grid.on('afterrender', function(grid) {  
		var view = grid.getView();
		console.log("viewId:"+view.id);
		var tip = Ext.create('Ext.tip.ToolTip', {
			width:128,
			height:128,
			showDelay:50,
		    // The overall target element.
		    target: view.el,
		    // Each grid row causes its own separate show and hide.
		    delegate: view.itemSelector,
		    // Moving within the row should not hide the tip.
		    trackMouse: true,
		    // Render immediately so that tip.body can be referenced prior to the first show.
		    renderTo: Ext.getBody(),
		    listeners: {
		        // Change content dynamically depending on which element triggered the show.
		        beforeshow: function updateTipBody(tip) {
		        	if(view.viewMode != 'default')
		        		return false;
		            //tip.update('Over company "' + view.getRecord(tip.triggerElement).get('id') + '"');
		        	var uri = view.getRecord(tip.triggerElement).get('uri'); 
		        	var img_src = '/bat/servlet/fu?f='+uri+'&size=128&mr='+Math.random();
		        	tip.body.dom.innerHTML = '<img src= "'+ img_src + '">';  
		        }
		    }
		});	   	
		});
	  	}
	  },
		hide: ["detail","userid","width","height","fsize","entityName","entityId"],
        features: [fea_tile0],
		
		btns:['录入','提示删除',{
	   		text: '设置代表影像',
	        cls: 'x-btn-text-icon',
	        icon: '../cm/icons/fam/image_add.png',
			handler : function(btn){
				    var sels=BView.getSels(btn);
		            var ui=BView.getUi(btn);
		            if (!sels || sels.length == 0){
					  Ext.MessageBox.show({
				           title: '系统提示',
				           msg: '请选择影像图片',
				           buttons: Ext.MessageBox.OK,
				           icon: Ext.MessageBox.INFO
			          });
						return;
					}else if (sels.length >1){
					  Ext.MessageBox.show({
				           title: '系统提示',
				           msg: '请只选择一张图片作为代表影像',
				           buttons: Ext.MessageBox.OK,
				           icon: Ext.MessageBox.INFO
			          });
						return;
					}
					
				    var its = sels[0].obj.items.map;
				    var uri=its.uri;
				    var entityId=its.entityId;
		            var req={"map":{'uri':uri,"entityId":entityId},"javaClass":"java.util.HashMap"};
		            jsonrpc.BH.handle(function(data,ex){
		  		   		if(Bat.outputEx(ex,'增加数据集异常')){
		  		   			return;
		  		   		}
		  		   		
		  		   		     Ext.MessageBox.show({
					           title: '系统提示',
					           msg: '代表影像设置成功！',
					           buttons: Ext.MessageBox.OK,
					           icon: Ext.MessageBox.INFO
				             });
					           
				            /*var value="[1_image]78.jpg";
				            var ed=ui.pui.editors['imgUri'][0];
				            ed.setValue();**/
				            
		  			  },'S',req,'BmService');  
			}
		}, {
			toggleGroup: 'tile_img',
			icon: 'icons/tile_default.png',
            cls: 'x-btn-icon',
            tooltip: '<b>列表方式</b><br/>按列表方式排列',
            listeners: {
                click: function(btn, item)
                {
                	fea_tile0.setView('default',btn);
                },
                scope: this
            }
        }, {
        	toggleGroup: 'tile_img',
        	pressed:true,
			icon: 'icons/tile_tile.png',
            cls: 'x-btn-icon',
            tooltip: '<b>图标文字</b><br/>按图标加文字方式排列',
            listeners: {
                click: function(btn, item)
                {
                	fea_tile0.setView('tiles',btn);
                },
                scope: this
            }
        }, {
        	toggleGroup: 'tile_img',
 			icon: 'icons/tile_icon.png',
 			tooltip: '<b>图标方式</b><br/>按大图标加题名排列',
            cls: 'x-btn-icon',
            listeners: {
                click: function(btn, item)
                {
                	fea_tile0.setView('medium',btn);
                },
                scope: this
            }
        },new Ext.form.FileField({
        	readOnly:false,
			multiple:true,
			entityFile:'dao.hb.NImage',
			entityName:'dao.hb.Bm',
			entityId:'id',
			height:30,
			accept:'image/*'
		})]
	},
	cfg_detail:{
		btns:["保存","游标"],
		tabs:[{
			title:'基本信息',
			xtype: 'form',
	        items:[{
            	xtype: 'container',
				layout: 'hbox',
		        items:[{
	                xtype: 'container',
	                flex: 1,
	                border:false,
	                layout: 'anchor',
		            props :["id","fsize"]

		        },{
	                xtype: 'container',
	                flex: 1,
	                border:false,
	                layout: 'anchor',
		            props :["dtCreate","width"]

		        },{
	                xtype: 'container',
	                flex: 1,
	                border:false,
	                layout: 'anchor',
		            props :["userid","height"]

		        }]	            
			},{
			   xtype: 'container',props :["title","detail","uri"]
			}]
		}]
			
		},	
	cfg_prop:{
	"id":{
	  cfg:{readOnly:true,readOnlyCls:'form_editor_readonly'},	
	  display:'序号'
	},
	"nid":{
	 	display:"主节点"
	},
	"entityName":{
	  hidden:true,
	 display:'数据库表',
	 cfg:{
	  	readOnly:true
	 },
	 editor : 'ComboBox',
	 cfg :BView.func_cfg_combo_hql("LIST_ENTITYS","id")
	},
	"entityId":{
	 hidden:true,
	 display:"记录编号"
	},
	"title":{
	  display:"题名",
	  width:300,
	  dcfg:{width:600}
	},	
	"userid":{
	 display:"编辑者"
	},	
	"detail":{
	 display:"描述",
	 editor:'TextArea',
	 dcfg:{width:600,height:100}
	},
	"width":{
	hidden:true,
	 display:"宽度",
	 cfg:{readOnly:true,readOnlyCls:'form_editor_readonly'}
	},
	"height":{
	  cfg:{readOnly:true,readOnlyCls:'form_editor_readonly'},	
	 hidden:true,
	 display:"高度"
	},
	"fsize":{
	  cfg:{readOnly:true,readOnlyCls:'form_editor_readonly'},	
	 hidden:true,
	 display:"文件尺寸"
	},
	"uri":{
	 width:150,
	 display:"路径",
	 editor:"FileField",
	 cfg:{
		accept:'image/*',
		size:500,
		height:520
	 }
	},
	"dtCreate":{
	  width:450,
	  editor:"DateField",
	  display:'建立时间',
	  cfg:{readOnly:true,readOnlyCls:'form_date_readonly'}
	 }
	}
 },
"dao.hb.NBase":{
	req:{
		"pss" : [["id","vid", "itype","language","title","userid","statusPublish","statusComment","dtCreate","dtModify","tnid","pnid","ord"]],
		"cns" : ["dao.hb.NBase"],
		"hql" : "from NBase"
	},
	cfg_grid:{
		btns:['增加','删除','打开','保存','检索','刷新','排序'],
		hide: ["remark"]
	},
	cfg_detail:{
		btns:["保存"]
	},	
	cfg_prop:{
	"id":{
	  display:'序号',
	  cfg:{
	  	readOnly:true
	  }
	},
	"vid":{
	 display:"当前版本"
	},
	"itype":{
	 display:"类型",
		editor : 'ComboBox',
		cfg :BView.func_cfg_combo([['餐馆', 1],['图片',2],['分类',3],['菜品',4]])
	},
	"language":{
	 display:"语种",
	 editor : 'ComboBox',
	 cfg :BView.func_cfg_combo_hql("LIST_LANG_","id")
	},	
	"title":{
	 display:"题名"
	},	
	"userid":{
	 display:"编辑者"
	},	
	"statusPublish":{
	 display:"发布状态",
		editor : 'ComboBox',
		cfg :BView.func_cfg_combo([['隐藏', 0],['超稿',1],['发布',2]])
	},
	"statusComment":{
	 display:"评论状态",
		editor : 'ComboBox',
		cfg :BView.func_cfg_combo([['禁止', 0],['允许',1]])
	},
	"dtCreate":{
	  editor:"DateField",
	  display:'建立时间',
	  dcfg:{
		readOnly:true
	 	}  
	 },
	"dtModify":{
	  editor:"DateField",
	  display:'最后修改',
	  dcfg:{
		readOnly:true
	 	}
	},
	"tnid":{
	  display:'翻译节点'
	},
	"pnid":{
	  display:'父节点'
	},
	"ord":{
	  display:'顺序'
	}
	},
	child_views : [{
  	title:"翻译",
  	vn:"dao.hb.NBase",
	func_filter: function(robj, ui) {
		var hql = " from NBase where  tnid=" + (robj.items.map['id'] || 0)+" order by id";		
		ui.setHQL(hql);
	},
	func_setdefault:function(robj,objs){
		objs[0].items.map["tnid"]=robj.items.map['id'];
	}
  },{
  	title:"菜品分类",
  	vn:"dao.hb.NSort",
  	func_limit:function(robj){
  		return robj.items.map['itype']==1;
  	},
  	func_view:function(vw,robj){
  		vw.cfg_grid.btns=['增加','删除','打开','保存','检索','刷新','排序',BView.btns["copyto"],BView.btns["json_cls"]];
  		vw.robj = robj;
  	},
	func_filter: function(robj, ui) {
		var hql = " from NSort where  nid=" + (robj.items.map['id'] || 0)+" order by id";		
		ui.setHQL(hql);
	},
	func_setdefault:function(robj,objs){
		objs[0].items.map["nid"]=robj.items.map['id'];
	}
  },{
  	title:"相关图片",
  	vn:"dao.hb.NBase",
  	func_limit:function(robj){
  		return robj.items.map['itype']==1 || robj.items.map['itype']==4;
  	},
	func_filter: function(robj, ui) {
		var hql = " from NBase where itype=2 and pnid=" + (robj.items.map['id'] || 0)+" order by id";		
		ui.setHQL(hql);
	},
	func_setdefault:function(robj,objs){
		objs[0].items.map["pnid"]=robj.items.map['id'];
		objs[0].items.map["itype"]=2;
	}
  },{
  	title:"相关菜品",
  	vn:"dao.hb.NBase",
  	func_limit:function(robj){
  		return robj.items.map['itype']==1;
  	},
  	func_view:function(vw,robj){
  		vw.cfg_grid.btns=['增加','删除','打开','保存','检索','刷新','排序',BView.btns["copyto"]];
  	},
	func_filter: function(robj, ui) {
		var hql = " from NBase where itype=4 and pnid=" + (robj.items.map['id'] || 0)+" order by id";		
		ui.setHQL(hql);
	},
	func_setdefault:function(robj,objs){
		objs[0].items.map["pnid"]=robj.items.map['id'];
		objs[0].items.map["itype"]=4;
	}
  },{
  	title:"餐馆历史版本",
  	vn:"dao.hb.NRestaurant",
  	func_limit:function(robj){
  		return robj.items.map['itype']==1;
  	},
	func_filter: function(robj, ui) {
		var hql = " from NRestaurant where nid=" + (robj.items.map['id'] || 0)+" order by id";		
		ui.setHQL(hql);
	},
	func_setdefault:function(robj,objs){
		objs[0].items.map["nid"]=robj.items.map['id'];
	}
  },{
  	title:"菜品历史版本",
  	vn:"dao.hb.NMenu",
  	func_limit:function(robj){
  		return robj.items.map['itype']==4;
  	},
  	func_view:function(vw,robj){
   		vw.cfg_prop["stype"].cfg .loader={
            	autoLoad: true,
               // url:'class_mnu.json',
              	url:Bat.WC +'servlet/JSonBaseServlet?nid='+robj.items.map['pnid']
            };
  	},
	func_filter: function(robj, ui) {
		var hql = " from NMenu where nid=" + (robj.items.map['id'] || 0)+" order by id";		
		ui.setHQL(hql);
	},
	func_setdefault:function(robj,objs){
		objs[0].items.map["nid"]=robj.items.map['id'];
	}
  },{
  	title:"图片历史版本",
  	vn:"dao.hb.NImage",
  	func_limit:function(robj){
  		return robj.items.map['itype']==2;
  	},  	
	func_filter: function(robj, ui) {
		var hql = " from NImage where nid=" + (robj.items.map['id'] || 0)+" order by id";		
		ui.setHQL(hql);
	},
	func_setdefault:function(robj,objs){
		objs[0].items.map["nid"]=robj.items.map['id'];
	}
  }]
 },

 "dao.hb.OAttachment":{
 	cn:'附件',
 	func_setdefault:function(robj,objs){
		objs[0].items.map["entityName"]="dao.hb.OStation";
	},
	req:{
		"pss" : [["id","entityName", "entityId","name","stype", "path","attSize","dtCreate","dtModify","remark"]],
		"cns" : ["dao.hb.OAttachment"],
		"hql" : "from OAttachment"
	},
	cfg_detail:{
	btns:["保存"],
	tabs:[{
		title : "基本信息",
		fset : [{
			legend : "",
			props :["id","name","stype", "path","remark","attSize","dtCreate","dtModify"]
		}]
		}]},
	cfg_grid:{
		btns:['录入','提示删除','检索','刷新','排序'],
		hide: ["dtCreate","dtModify","remark"]
	},
	cfg_prop:{
	"id":{
	  display:'序号',
	  editor:"DisplayField",
	  cfg:{
	  	readOnly:true
	  }
	},
	"status":{
	 display:"状态",
		editor : 'ComboBox',
		cfg :BView.func_cfg_combo([['启用', 1], ['禁用', 0]])
	},
	"entityName":{
	 display:'数据库表',
	 	  cfg:{
	  	readOnly:true
	 },
	 editor : 'ComboBox',
	 cfg :BView.func_cfg_combo_hql("LIST_ENTITYS","id")
	},
	"entityId":{
	 display:"记录编号"
	},
	"name":{
	 display:"附件名",
	 width:200,
	 cfg:{allowBlank:false}
	},
	"stype":{
	 display:"附件分类"
	},
	"attSize":{
	   display:"文件长度",
	   editor:"DisplayField"
	},
	"path":{
		display:'附件路径',
		editor:"FileField",
		cfg:{
			readOnly:false,
			multiple:true
		}
	},
	"dtModify":{
	  editor:"DateField",
	  display:'更新时间',
	  cfg:{readOnly:true,readOnlyCls:'form_date_readonly'}
	},	
	"dtCreate":{
	  editor:"DateField",
	  display:'建立时间',
	  cfg:{readOnly:true,readOnlyCls:'form_date_readonly'}
	},"remark":{
	  display:'备注',
	  editor:"TextArea"
	}
  }
 },

  "dao.hb.LSync":{
	req:{
		"pss" : [["id","userid","dtCreate", "snapJson","entity","entityId","oper","remark"],["name","avatar"]],
		"cns" : ["dao.hb.LSync","dao.hb.UUser"],
		"hql" : "from LSync A,UUser B where A.userid = B.id order by A.id desc"
	},
	cfg_detail:{	
	tabs:[{
		title:'基本信息',
		props:["A.id","B.name","A.oper","A.entity","A.entityId","A.dtCreate","A.snapJson"]
 	}]},
	//cfg_prop_all:{cfg:{readOnly:true}},
	cfg_grid:{
		propNames:["A.id","A.dtCreate","A.oper","A.entity","A.entityId","B.name"],
		btns:['检索','刷新','排序','图表']
	},
	cfg_prop:{
    "A.remark":{
       display:"备注",
       editor:"DisplayField"
    },
	"A.id":{
	   display:'序号',
	   editor:"DisplayField"
	},
	"A.entity":{
	   display:'数据类',
	   width:150,
	   editor:'ComboBox',
	   dcfg:{readOnly:true,readOnlyCls:'form_date_readonly'},
	   cfg:BView.func_cfg_combo()
	},
	"A.entityId":{
	    display:"数据标识",
	  	editor:'DisplayField'
	},
	"A.oper":{
	  display:"动作",
	  dcfg:{readOnly:true,readOnlyCls:'form_date_readonly'},
	  editor : 'ComboBox',
	  cfg :BView.func_cfg_combo([['增加', 65], ['删除', 82],['修改',85]])
	},
	"A.dtCreate":{
	  editor:"DateField",
	  display:'时间',
	  cfg:{readOnly:true,readOnlyCls:'form_date_readonly'}
	},

	"A.snapJson":{
	  display:'内容',
	  hiden:true,
	  width:250,
	  cfg:{hideLabel:true},
	  	editor:'DisplayField',
	  	renderer : function(itxt,r,col,l) {	
	  		if(col){
	  			return itxt;
	  		}
	  		var its = r.record?r.record.data:r.item||{};
	  		return BView.renderSync(its['A.snapJson'],its['A.entity']);
		}
	 },
	 
	 "B.name":{
	  editor:"DisplayField",
	  display:'操作人',
	  hidden:false
	 },
	 "B.avatar":{
	  display:'用户',
	  	width:80,
	  	cfg:{readOnly:true},
	  	hidden:true,
	  	dcfg:{width:100},
	  	editor:'DisplayField',
	  	renderer : function(itxt,r,col,l) {	
	  		var src=itxt?"/bat/servlet/fu?f="+itxt:'icons/avatar.png';
	  		var its = r.record?r.record.data:r.item||{};
	  		return "<img style='border-radius: 10px;cursor:hand;' src='"+src+"' title='"+ its['B.name']
	  		+"' onclick='layout.renderDetailByKey(\"dao.hb.UUser\",false,"+its['A.userid']+");return false;'/>";
		}
	 }
  }
 },

  "dao.hb.UUser":{
  	cn:'用户',
	 handles:{'before_setobj_detail':function(ui,editors,obj){
	 		if(obj.action=='A'){
  				editors['userId'][0].enable();
	 		}
  			else{
  				editors['userId'][0].disable();
  			}
	 	},
		'before_setdata':function(ui,pobjs,flg){			
			var po = pobjs[0];
			if(po.action=='D'){
				po.items.map.status=1;
			}			
 	}
	}, 
 	cn:'用户',
	req:{
		"pss" : [["id", "userId","duty","gid","dpt","userid","pwd","name","email","lang","tel","phone","dpt","qq","status","onLine","dtLogin","dtLogout","dtCreate","dtModify","remark","handle"]],
		"cns" : ["dao.hb.UUser"],
		"hql" : "from UUser"
		//"hql" : "from UUser"
	},
	cfg_detail:{
		btns:["保存"],
		tabs:[{
			title:'基本信息',
	        items:[{
            	xtype: 'form',
				layout: 'hbox',
		        items:[{
	                xtype: 'container',
	                flex: 1,
	                border:false,
	                layout: 'anchor',
		            props :["userId","name","gid","status","dpt","dtCreate","dtModify"]
		        }]	            
			}]
		}]},
	cfg_grid:{
		selType:'rowmodel',
		btns:['录入','检索','刷新','排序'],
		propNames:  ["userId","name","dpt","duty","gid"]
	},
	cfg_prop:{
	"id":{
	 display:'序号',
	 editor:"DisplayField",
	 hidden:true,
	 cfg:{
		readOnly:true
		}
	},	
	"duty":{
	   display:"职务"
	},
	"gid":{
	 display:'用户组',
	 width:150,
	 editor : 'ComboBox',
	 dcfg:{
		 editable:false,
		 width:300
	 },
	 cfg :BView.func_cfg_combo_hql("select id,name from UGroup order by id","id")
	},
	"avatar":{
	  display:'头像',
	  editor:"FileField",
	  width:150,
	  isShow:false,
	  renderer:function(v,meta,rec){
	  	var src=v?"/bat/servlet/fu?f="+v:'icons/avatar.png';
	  	if(meta){
	     	meta.tdAttr="data-qtip=\"<img width=60px height=60px src='"+src+"'>\"";
	  	}
	    return "<a href='"+src+"'>"+src+"</a>";
	  }
	},	
	"userId":{
	  display:'登陆名',
	  width:150,
	  cfg:{
	  	  width:350,
	  	  msgTarget :'side',
	      validator : function(val) {  
	      	if(val.length<3 || val.length>10)
	      		return "只能输入3-10汉字或字符";
            var req={"map":{hql:"from dao.hb.UUser where userId='"+val+"'"},"javaClass":"java.util.HashMap"};
            var data = jsonrpc.BH.handle('C',req,'HDDefault');  
            if(data==0)
            	return true;
            else
            	return "存在重复的用户名";
	      }
	 }
	},	
	"userid":{
		display:"操作人编号",
		hidden:true
	},
	"email":{
		hidden:true
	},
	"phone":{
		display:"手机",
		hidden:true
	},
	"qq":{
		display:"QQ",
		hidden:true
	},
	"dpt":{
		display:'部门',
		editor:"DisplayField",
		width:200
		//editor:"ComboBox",
		//dcfg:{width:300},
		//cfg:BView.func_cfg_combo_hql("select name,name from UOrg ")

	},	
	"pwd":{
	    display:'用户密码',
	    hidden:true,
	    cfg:{
	  	  inputType:'password',
	  	  regex:/^[\s\S]{1,40}$/,
		  width:300,
		  regexText:"只能输入1-40英文或字符"
	    }
	},	
	"name":{
	  display:'真实姓名',
	  width:150,
	  cfg:{
	  	  width:400,
	  	  msgTarget :'side',
		  allowBlank:false,
		  regex:/^[\s\S]{0,80}$/,	      
	      regexText:"只能输入0-80汉字或字符"
	 }
	},
	"lang":{
	  display:"语种",
	  editor : 'ComboBox',
	  hidden:true,
	  cfg :BView.func_cfg_combo_hql("LIST_LANG_","id")
	},
	"duty":{
	  display:'职务',
	  width:100
	},	
	"tel":{
	  display:'电话',
	  hidden:true
	},	
	"onLine":{
	  display:'在线吗',
	  editor:"DisplayField",
	  hidden:true,
	  cfg:{
	  	readOnly:true
	  },
	  renderer : function(itxt,r,col,l) {	
	  	if(!col)
	  		return itxt;
	  	if(itxt==1)
	  		return "<img src=\"icons/status_onLine.png\"/>";
	  	else
	  		return "<img src=\"icons/status_offline.png\"/>";
	  }
	},	
	 "status":{
	        editor:'ComboBox',
	        display:"使用状态",
	        width:100,
		    dcfg:{
				   editable:false,
				   width:300
				},
	        cfg:{
		        displayField : 'txt',
				valueField : 'val',
				typeAhead : true,
				queryMode : 'local',
				triggerAction : 'all',				
				selectOnFocus : true,
		    	listConfig: {
				        getInnerTpl: function() {
				            return '<div data-qtip="{txt}">{style} {txt}</div>';
				        }
				},
				store : new Ext.data.ArrayStore({
					fields :['txt', 'val'],
					data :[['使用',1],['停用',0]]
				})
	        },
			renderer : function(val) {	
	           	  var dtxt = {						
					'1':'<font style="color:green;font-size:13px">使用</font>',	
					'0':'<font style="color:red;font-size:13px">停用</font>'};
					return dtxt[val];
	       }
        
     },
	"remark":{
	  display:'备注',
	  editor:"TextArea",
	  cfg:{ 
	  	  msgTarget :'side',
		  height:150,
	      width:500,
	      regex:/^[\s\S]{0,255}$/,
	      regexText:"只能输入0-255汉字或字符"
	    },
	hidden:true
	},	
	"handle":{
	  display:'登录令牌',
	  editor:"DisplayField",
	  hidden:true
	},	
	"dtModify":{
	  editor:"DateField",
	  display:'更新时间',
	  cfg:{readOnly:true,readOnlyCls:'form_date_readonly'}
	},	
	"dtCreate":{
	  editor:"DateField",
	  display:'建立时间',
	  cfg:{readOnly:true,readOnlyCls:'form_date_readonly'}
	},	
	"dtLogin":{
	  editor:"DateField",
	  hidden:true,
	  display:'最近登入',
	  cfg:{readOnly:true,readOnlyCls:'form_date_readonly'}
	},
	"dtLogout":{
	  editor:"DateField",
	  display:'最近退出',
	  hidden:true,
	  cfg:{readOnly:true,readOnlyCls:'form_date_readonly'}
	}			
  }
 },
 "dao.hb.UGroup":{
 	cn:'用户组',
	req:{
		"pss" : [["id", "name","remark","grantMenu","dtCreate","dtModify"]],
		"cns" : ["dao.hb.UGroup"],
		"hql" : "from UGroup"
	},
	cfg_detail:{
		 btns:["保存"],
		 tabs:[{
			title : "基本信息",
			props :["id", "name","grantMenu","remark","dtCreate","dtModify"]
		}]
	},
	cfg_grid:{
		propNames:["name","dtCreate","dtModify"],
		btns:['录入','提示删除','检索','刷新','排序'],
		hide: ["remark"]
	},
	child_views : [{
	  	title:"用户组人员",
	  	vn:"dao.hb.UUser",
		func_filter: function(robj, ui) {
			var hql = " from UUser where  gid=" + (robj.items.map['id'] || 0)+" order by id";		
			ui.setHQL(hql);
		},
		func_view:function(vw,robj){
	            vw.cfg_grid.btns=["检索","刷新","排序"];
	           
	        }
	  }],
	cfg_prop:{
	"id":{
	  display:'序号',
	  editor:"DisplayField"
	},
	"name":{
	     display:'名称',
	     width:150,
	     cfg:{
			  regex:/^.{0,100}$/,
		      validationEvent:"click",
		      regexText:"只能输入0-100汉子或字符"
		 }
	},
   "grantMenu":{
	   display:"功能菜单",
		editor:"TreeCombo",
		 dcfg:{
			   editable:false
			},
		cfg:{
			singleSelect:false,
			sepperator:';',
			width:400,
	        lazyInit:false,
			store: new Ext.data.TreeStore({
               proxy: {
                   type: 'ajax',
                   async : false,
                   url: 'json/mb_xgqx.json'
               }
           })
		}
   },
	"dtModify":{
	  editor:"DateField",
	  display:'更新时间',
	  cfg:{readOnly:true,readOnlyCls:'form_date_readonly'}
	},	
	"dtCreate":{
	  editor:"DateField",
	  display:'建立时间',
	  cfg:{readOnly:true,readOnlyCls:'form_date_readonly'}
	},
	"remark":{
	  display:'备注',
	  hidden:true,
	  width:200,
	  height:200,
	  editor:"TextArea",
	  cfg:{
		  allowBlank:true,
		  regex:/^.{0,100}$/,
	      validationEvent:"click",
	      regexText:"只能输入0-100汉字或字符"
	 }
	}

  }
 },

/* "dao.hb.UGrant":{
	req:{
		"pss" : [["id", "gid","pmMenu","pmEntity","pmOper","dtCreate","dtModify","remark"]],
		"cns" : ["dao.hb.UGrant"],
		"hql" : "from UGrant"
	},
	cfg_detail:{
	btns:["保存"],
	tabs:[{
		title : "基本信息",
		fset : [{
			legend : "",
			props :["id", "gid","pmMenu","pmEntity","pmOper","dtModify","dtCreate"]
		}]
		},{
		title : "备注",
		fset : [{
			legend : "",
			props :["remark"]
		}]
	}]},
	cfg_grid:{
		hide: ["dtCreate","dtModify","remark"]
	},
	cfg_prop:{
	"id":{
	  display:'序号',
	  editor:"DisplayField"
	},
	"gid":{
	 display:'用户组',
	 editor : 'ComboBox',
	 cfg :BView.func_cfg_combo_hql("select id,name from UGroup order by id","id")
	},
	"pmMenu":{
	 display:'功能菜单',
	 editor : 'ComboBox'//,
	 //cfg :BView.func_cfg_combo("LIST_ENTITYS","id")
	},
	"pmEntity":{
	 display:'数据库表',
	 editor : 'ComboBox',
	 cfg :BView.func_cfg_combo_hql("LIST_ENTITYS","id")
	},
	"pmOper":{
	 	display:'操作',
		editor : 'ComboBox',
		renderer : function(val) {				
		  var dtxt = {						
			'1':'查看',	
			'2':'修改',
			'3':'增删'
			};
			return dtxt[val];
		},
		cfg :BView.func_cfg_combo([["查看", 1], [ "修改",2], ["增删",3 ]])
	},
	"dtModify":{
	  editor:"DateField",
	  display:'更新时间',
	  cfg:{readOnly:true,readOnlyCls:'form_date_readonly'}
	},	
	"dtCreate":{
	  editor:"DateField",
	  display:'建立时间',
	  cfg:{readOnly:true,readOnlyCls:'form_date_readonly'}
	},
	"remark":{
	  display:'备注',
	  editor:"TextArea"
	}
	
  }
 },*/


 "dao.hb.MMessage":{
	req:{
		"pss" : [["id", "userid","mid","onLine","stype","sendTo","title","content","dtCreate","dtModify","dtReply","rname","status"]],
		"cns" : ["dao.hb.MMessage"],
		"hql" : "from MMessage order by id desc"
	},
	cfg_prop_all:{cfg:{readOnly:true}},
	cfg_detail:{tabs:[{
			title:'留言信息',
			bodyPadding: '5 5 0',
			xtype: 'form',
	        items:[{
            	xtype: 'container',
            	layout:'hbox',
		        items:[{
	                xtype: 'container',
	                flex: 1,
	                border:false,
	                layout: 'anchor',
		            props :["id"]
		        },{
	                xtype: 'container',
	                flex: 1,
	                border:false,
	                layout: 'anchor',
		            props :["dtCreate"]
		        }]	            
			},{
			   xtype: 'container',bodyPadding: '5 5 0 5',props :["title","content"]
			}]
		}]},
	cfg_grid:{
		btns:[{
	   		text: '用户组留言',
	        cls: 'x-btn-text-icon',
	        icon: 'icons/comments.png',
			handler : function(btn){
			  BView.sendMessage("SendToGroup","发送留言到用户组",{sendToGroup:"",title:"",content:""},{offline:"on"});
			}
        } ,'检索','刷新','排序'],
        btn_revise: function(b){ if(b.text=='编辑') b.text='查看' },
		hide: ["content","userid","dtCreate","dtModify"]
	},
	cfg_prop:{
	"id":{
	  display:'序号',
	  cfg:{
	  	readOnly:true
	  }
	},
	"userid":{
	 hidden:true,
	 display:'用户'
	},
	"mid":{
	  hidden:true,
	 display:'主留言id'
	},
	"title":{
	 display:'留言题名',
	 dcfg:{width:500}
	},
	"content":{
	 display:'留言内容',
	 editor:"TextArea",
	 dcfg:{width:500}
	},
	"pmMenu":{
	 display:'功能菜单',
	 editor : 'ComboBox'//,
	 //cfg :BView.func_cfg_combo("LIST_ENTITYS","id")
	},
	"onLine":{
	  hidden:true,
	 display:'在线消息'
	},
	"sendTo":{
	 display:'目标用户(组)'
	},
	"stype":{
	   hidden:true,
	 	display:'类型',
		editor : 'ComboBox',
		renderer : function(val) {				
		  var dtxt = {						
			'1':'系统通知',	
			'2':'业务沟通',
			'3':'问候'};
			return dtxt[val];
		},
		cfg :BView.func_cfg_combo([["系统通知", 1], [ "业务沟通",2], ["问候",3 ]])
	},
	"dtModify":{
	   hidden:true,
	  editor:"DateField",
	  display:'更新时间'
	},	
	"dtCreate":{
	  editor:"DateField",
	  display:'建立时间'
	},"dtReply":{
		editor:"DateField",
	  	display:'回复时间'
	},"rname":{
	  display:'回复人'
	},"status":{
	 hidden:true,
	  display:'状态'
	}
	
  },child_views : [{
  	title:"相关回复",
  	vn:"dao.hb.MMessage",
	func_filter: function(robj, ui) {
		var hql = " from MMessage where mid=" + (robj.items.map['id'] || 0);		
		ui.setHQL(hql);
	} }]
 },
  "dao.hb.MInbox":{
  	handles:{'before_save':function(ui){
  		//将如果查看时间为空,设置为已查看
    		for(var i=0,len=ui.objs.length;i<len;i++){
    			var cur = ui.objs[i];
    			if(cur.action=='O'&& cur.items.map["A.dtCheck"]==null){
    				cur.action='U';
    			}
    		}
    	}
    },
	req:{
		"pss" : [["id", "mid","userid","dtCheck"],["id","uname","stype","title","dtCreate","content"]],
		"cns" : ["dao.hb.MInbox","dao.hb.MMessage"],
		"hql" : "from MInbox A, MMessage B where A.mid=B.id order by A.id desc"
	},
	cfg_detail:{
	 btns:[{
	   		text: '回复',
	        cls: 'x-btn-text-icon',
	        icon: 'icons/email_go.png',
			handler : function(btn){
			 var im = this.obj_detail.items.map;
			 var useridetail=this;
			 BView.sendMessage("Re","回复留言["+im["A.mid"]+"]",{title:"Re:"+im["B.title"],content:""},{mid:im["A.mid"],offline:"on",to:[im["B.id"]]},
			 	function(){
			 		var ap = layout.tabPanel.getActiveTab();
		 			layout.tabPanel.remove(ap,false);
			 	});
			}},{
	   		text: '转发',
	        cls: 'x-btn-text-icon',
	        icon: 'icons/email_link.png',
			handler : function(btn){
			 var im = this.obj_detail.items.map;
			 BView.sendMessage("forward","转发留言["+im["A.mid"]+"]",{sendToGroup:"",title:"Fw:"+im["B.title"],content:im["B.content"]},{offline:"on"},
			 function(){
			 		var ap = layout.tabPanel.getActiveTab();
		 			layout.tabPanel.remove(ap,false);
			 });
			}}],
	 tabs:[{
			title:'留言信息',
			bodyPadding: '5 5 0',
			xtype: 'form',
	        items:[{
            	xtype: 'container',
            	layout:'hbox',
		        items:[{
	                xtype: 'container',
	                flex: 1,
	                border:false,
	                layout: 'anchor',
		            props :["A.id"]

		        },{
	                xtype: 'container',
	                flex: 1,
	                border:false,
	                layout: 'anchor',
		            props :["B.dtCreate"]

		        }]	            
			},{
			   xtype: 'container',bodyPadding: '5 5 0 5',props :["B.title","B.content"]
			}]
		}]},
	cfg_prop_all:{cfg:{readOnly:true}},
	cfg_grid:{
		btns:[{
	   		text: '回复',
	        cls: 'x-btn-text-icon',
	        icon: 'icons/email_go.png',
			handler : function(btn){
			  var sel0=BView.getSelObj(btn);
			  if(!sel0){
			  	return false;
			  }
			  var im = sel0.items.map;
			  BView.sendMessage("Re","回复留言["+im["A.mid"]+"]",{title:"Re:"+im["B.title"],content:""},{mid:im["A.mid"],offline:"on",to:[im["B.id"]]});
			}},{
	   		text: '转发',
	        cls: 'x-btn-text-icon',
	        icon: 'icons/email_link.png',
			handler : function(btn){
			  var sel0=BView.getSelObj(btn);
			  if(!sel0){
			  	return false;
			  }
			  var im = sel0.items.map;
			  BView.sendMessage("forward","转发留言["+im["A.mid"]+"]",{sendToGroup:"",title:"Fw:"+im["B.title"],content:im["B.content"]},{offline:"on"});
			}},'检索','刷新','排序'],
		btn_revise: function(b){ if(b.text=='编辑') b.text='查看' },
		hide: ["A.userid","A.mid","B.content"]
	},
	cfg_prop:{
	"A.id":{
	  display:'序号'
	},
	"A.mid":{
	  hidden:true,
	  display:'留言序号'
	},
	"A.userid":{
	  hidden:true,
	  display:'目标用户'
	},
	"A.dtCheck":{
	  hidden:true,
	  display:'查看时间',
	  editor:"DateField"
	},
	"B.id":{
	  hidden:true,
	  display:'留言序号'
	},
	"B.uname":{
	  display:'留言者'
	},
	"B.stype":{
	  hidden:true,
	  display:'留言类型'
	},
	"B.title":{
	  display:'留言题名'
	},
	"B.content":{
	  display:'留言内容',
	  editor:"TextArea"
	},
	"B.dtCreate":{
	  display:'留言时间',
	  editor:"DateField"
	}
  },child_views : [{
  	title:"相关回复",
  	vn:"dao.hb.MMessage",
	func_filter: function(robj, ui) {
		var cn = robj.cns[0];
		var hql = " from MMessage where mid=" + (robj.items.map['B.id'] || 0);		
		ui.setHQL(hql);
	},
	func_setdefault:function(robj,objs){
		//objs[0].items.map["mid"]=robj.items.map['id'];
	}
  }]
 },
 
"dao.hb.PinPerson":{
	req:{
		"pss" : [["id","attachmentFilename","dtSubmit","photoStand","statusFilter", "majorCode","username","majorReg","graduateSchool","major","educational","gender","graduatedLocation","permanentResidence","nativePlace","nation","politicsStatus","birthday","identityNumber",
		          "jth","partyTime","homeAddress","phone","graduateDate", "englishFour","englishSix","sourcePlace","highSchool","selfdescription","awarding","otherDescription","familyInfo","learningExperience","workExperience","email","year","identifier","status","regnum","issend","isreceive"]],
		"cns" : ["dao.hb.PinPerson"],
		"hql" : "from PinPerson where status=2 "
	},
	cfg_grid:{
		propNames:["id","username","majorReg","graduateSchool","graduateDate","major","educational","gender","permanentResidence","jth","nation","politicsStatus","identityNumber","dtSubmit","phone"],
	/**cfg:{	   
		   func_init:function(grid){
			grid.on('afterrender', function(grid) {  
			var view = grid.getView();
			var tip = Ext.create('Ext.tip.ToolTip', {
				width:200,
				height:200,
				showDelay:50,
				padding:0,
				style:"background-color:transparent;border:none",
				bodyStyle:"padding:0px;background-color:transparent",
			    // The overall target element.
			    target: view.el,
			    // Each grid row causes its own separate show and hide.
			    delegate: view.itemSelector,
			    // Moving within the row should not hide the tip.
			    trackMouse: true,
			    // Render immediately so that tip.body can be referenced prior to the first show.
			    renderTo: Ext.getBody(),
			    listeners: {
			        // Change content dynamically depending on which element triggered the show.
			        beforeshow: function updateTipBody(tip) {
			            //tip.update('Over company "' + view.getRecord(tip.triggerElement).get('id') + '"');
			        	var uri = view.getRecord(tip.triggerElement).get('photoStand'); 
			        	if(uri==""||uri==null){
			        	    tip.body.dom.innerHTML = '<img style="border:1px solid" src= "../cm/icons/no-photo-available.jpg">';  
			        	}else{
				        	var img_src = uri;
			        	    tip.body.dom.innerHTML = '<img style="border:1px solid"  src= "'+ img_src + '">';  
			        	}
			        	
			        }
			    }
			});	   	
			});
		  	}
		  }, 
		  **/
		btns:['检索','刷新','排序',{
	   		text: '按选中添加',
	   		iconCls: 'plugin_add',
	   		icon : 'icons/add.png',
			handler : function(btn){
				 BView.showDataSet('append_dataset','添加选中条目到数据集',btn,true);
		    }},{
		   		text: '按检索添加',
		   		iconCls: 'plugin_add',
		   		icon : 'icons/fam/user_add.png',
				handler : function(btn){
		             BView.showDataSet('append_dataset','添加检索结果到数据集',btn);
			    }}]
	},
	cfg_detail:{
    btns:["游标",{
		   		text: '添加到',
		   		iconCls: 'plugin_add',
		   		icon : 'icons/fam/user_add.png',
				handler : function(btn){
		             BView.showDataSet('append_dataset','添加选中条目到数据集',btn);
			    }},{
	      text:  '导出',
	      cls: 'x-btn-text-icon',
	      icon : 'icons/door_out.png',
	      menu : new Ext.menu.Menu({
          items : [{
            text : "导出Word简表",
            scope : this,
            handler : function(btn,item){
            	var tp = layout.tabPanel;
    		 	var ap = tp.getActiveTab();
    		 	var ui = ap.uiDetail;
    		 	var objs = ui.objs;
    		 
    		 	var objcur=ui.obj_detail;
              	var objId = objcur.items.map.id
                var hql = "from PinPerson where id = "+objId;   
              	outputWord(hql,"doc_template/template_jian.doc",3,['id','username','graduateSchool','major','educational','gender','graduatedLocation','permanentResidence',
           'nativePlace','politicsStatus','nation','identityNumber','birthday','phone','photoStand'
           //,'partyTime','homeAddress','graduateDate','englishFour','englishSix','sourcePlace','highSchool','selfdescription','awarding','otherDescription','familyInfo','learningExperience','email','otherlang','dtSubmit'
           ]); 
            }
          },
          {
            text : "导出Word详细表",
            scope : this,
            handler : function(btn,item){
            
            	var tp = layout.tabPanel;
    		 	var ap = tp.getActiveTab();
    		 	var ui = ap.uiDetail;
    		 	var objs = ui.objs;
    		 
    		 	var objcur=ui.obj_detail;
              	var objId = objcur.items.map.id
              	var hql = "from PinPerson where id  = "+objId;  
              	outputWord(hql,"doc_template/template_detail.doc",1,['id','username','graduateSchool','major','educational','gender','graduatedLocation','permanentResidence',
              	                                            'nativePlace','politicsStatus','nation','identityNumber','birthday','phone','photoStand',
              	                                            'partyTime','homeAddress','graduateDate','englishFour','englishSix','sourcePlace','highSchool','selfdescription','awarding','otherDescription','familyInfo','learningExperience','email','dtSubmit'
              	                                            ]);  
            }
          },
          {
            text : "导出Excel",
            scope : this,
            handler : function(btn,item){
           		var tp = layout.tabPanel;
    		 	var ap = tp.getActiveTab();
    		 	var ui = ap.uiDetail;
    		 	var objs = ui.objs;
    		 
    		 	//2016.2.24 
    		 	var objcur=ui.obj_detail;
    		 	var id= objcur.items.map.id;
    		 	//var id= objs[1].items.map.id;    		 	
              Ext.MessageBox.confirm("确认","是否导出当前人员到Excel", function(btn){
            	  if(btn=="no")
    		 			return;
       		       var baseURL =CFG.app_ctx+"servlet/DefaultExportExcelControl?obj=dao.hb.PinPerson"; 
    			   var url = baseURL+"&type=F&filter="
    			   +encodeURIComponent("from PinPerson where id="+id);
    			   window.open(url,'_blank');
               });
            }
          }]
          
	      })
	    }],
	    //tab
	    tabs:[{
				title:'基本信息',	
		        items:[{
	            	xtype: 'form',
					layout: 'hbox',
			        items:[
			        {
			        	flex: 1,
			        	xtype: 'container',
			        	layout: 'vbox',
			        	items:[
			        	{
		                xtype: 'container',
		                border:false,
		                layout: 'anchor',
			            props :["username","major","majorReg","graduateSchool","educational","gender","graduatedLocation","permanentResidence","jth","nativePlace","nation","politicsStatus","birthday",
		                        "partyTime","graduateDate", "englishFour","englishSix","sourcePlace","highSchool","learningExperience","workExperience","awarding"]
			        	}
			        	]
			        },{
			        	 flex: 1,
			        	xtype: 'container',
			        	layout: 'vbox',
			        	items:[
			        	{
		                xtype: 'container',
		                border:false,
		                layout: 'anchor',
			            props :["photoStand","attachmentFilename","identityNumber","phone","email","year","homeAddress","selfdescription","familyInfo","otherDescription"]
			        	}
			        	]
			        }]	            
				}
			 ]}]
	    },	
	cfg_prop:{
	"id":{
	  display:'编号'
	},
	"jth":{
	   display:"集体户口",
	   editor:"ComboBox",
	   cfg :BView.func_cfg_combo([['是', '是'], ['否', '否']]),
	   dcfg:{readOnly:true,readOnlyCls:'form_date_readonly'}
	},
	"majorReg":{
	   display:"所报专业",
	   editor : 'ComboBox',
       cfg :BView.func_cfg_combo_hql("select majorName,majorName from dao.hb.PinMajor order by ord","name"),
       dcfg:{width:385,readOnly:true,readOnlyCls:'form_date_readonly'}
	},
	"majorCode":{
	   display:"专业代码",
	   editor : 'ComboBox',
	   renderer:BView.render_display,
	   cfg :BView.func_cfg_combo_hql("select id,majorCode from dao.hb.PinMajor order by ord","name")
	},
	"username":{
	   display:"姓名",
	   width:100,
	   editor:"DisplayField",
	   cfg:{width:385},
	   renderer:function(v,r,rec){
		  	var its = r.record?r.record.data:r.item||{};
        	var uri = getRealUrlAtt(its[ 'photoStand']); 
        	var img_src;
        	if(uri==""||uri==null){
        	    img_src="../cm/icons/no-photo-available.jpg";  
        	}else{
        		img_src=uri;
        	}
		  	if(r){
		     	r.tdAttr="data-qtip=\"<img style='max-width:200px;max-height:200px;'  src='"+img_src+"'>\"";
		  	}
		    return v;
	    }
	},	
	"identityNumber":{
	   display:"身份证号码",
	   editor:"DisplayField",
	   width:200
	},	
	"birthday":{
	   editor:"DateField",
	   display:"出生日期",
	   cfg:{
        	   format:'Y-m-d',
        	   readOnly:true,readOnlyCls:'form_date_readonly'
       }
	},	
	"gender":{
	   display:"性别",
    	editor : 'ComboBox',
    	cfg :BView.func_cfg_combo([['男', '男'], ['女', '女']]),
	   dcfg:{width:385,readOnly:true,readOnlyCls:'form_date_readonly'}
	},
	"nation":{
	   display:"民族",
	   width:100,
	   editor:"DisplayField"
	},
	"politicsStatus":{
	    display:'政治面貌',
	    editor:"DisplayField"
	 },
	"partyTime":{
	    display:'入党（团）时间',
	    editor:"DisplayField"
	},
	"nativePlace":{
	    display:'籍贯', 
	    editor:"DisplayField"
	},
	"permanentResidence":{
	    display:'户口所在地',
        editor:"DisplayField"
	},
	"homeAddress":{
	    display:'家庭住址',
	    editor:"DisplayField",
		 cfg:{
				width:385,
				height:150,
			  	readOnly:true,
			  	autoScroll:'none',
			  	readOnlyCls:'form_date_readonly'
		 }
	},
	"phone":{                                    
	    display:'手机号码',
	    editor:"DisplayField"
	},
	"graduateSchool":{
		display:'毕业院校',
		editor:"DisplayField"
	},
	"graduateDate":{
		editor:"DisplayField",
		display:'毕业时间'
	},
	"graduatedLocation":{
		display:'院校所在地',
		editor:"DisplayField"
	},
	"educational":{
		display:'学历',
		editor:"DisplayField"
	},
	"major":{
		display:'具体专业',
		editor:"DisplayField"
	},
	"englishFour":{
		display:'英语四级成绩',
		editor:"DisplayField"
	},
	"englishSix":{
		display:'英语六级成绩',
		editor:"DisplayField"
	},
	"sourcePlace":{
		display:'生源所在地）',
		editor:"DisplayField"
	},
	"highSchool":{
		display:'高中毕业院校',
		editor:"DisplayField",
		 cfg:{
			  	readOnly:true,
			  	width:300,
				height:20
			  }
	},
	"selfdescription":{               
		display:'自我描述',
		editor:"DisplayField",
		 cfg:{
				width:385,
				height:300,
			  	readOnly:true,
			  	readOnlyCls:'form_date_readonly'
			  }
	},
	"awarding":{
		display:'获奖情况',
		editor:"DisplayField",
		 cfg:{
				width:385,
				height:300,
			  	readOnly:true,
			  	readOnlyCls:'form_date_readonly'
			  }
	},
    "otherDescription":{
		display:'其他说明',
		editor:"DisplayField",
		 cfg:{
				width:385,
				height:300,
			  	readOnly:true,
			  	readOnlyCls:'form_date_readonly'
			  }
	},
    "familyInfo":{
		display:'家庭成员信息',
		editor:"DisplayField",
		 cfg:{
			  	readOnly:true,
				width:385,
				height:300,
				readOnlyCls:'form_date_readonly'
			  }
	},
    "learningExperience":{
		display:'学习经历',
		editor:"DisplayField",
		 cfg:{
			  	readOnly:true,
				width:385,
				height:300,
				readOnlyCls:'form_date_readonly'
			  }
	},
    "workExperience":{
		display:'工作经历',
		editor:"DisplayField",
		 cfg:{
			  	readOnly:true,
				width:385,
				height:300,
				readOnlyCls:'form_date_readonly'
			  }
	},
    "email":{
		display:'邮件',
		editor:"DisplayField"
	},
    "year":{
		display:'年代',
		editor:"DisplayField"
	},
    "identifier":{
		display:'唯一标识符',
		editor:"DisplayField"
	},
    "otherlang":{
		display:'其他语种掌握说明',
		editor:"DisplayField",
		 cfg:{
			  	readOnly:true,
			  	width:300,
				height:20,
				readOnlyCls:'form_date_readonly'
			  }
	},
    "remark":{
		display:'备注'
	},
   "photoStand":{
		display:'证件照' ,
	    cfg:{
	    	fieldLabel:false
	    },
	    editor:'DisplayField',
	    hidden:true,
		renderer : function(itxt,r,col,l) {	
	  	    if(itxt==""){
              return "<div style='width:200px;height:200px;overflow:hidden;' ><img style='max-width:200px;max-height:200px;' src= '../cm/icons/no-photo-detail.png'/></div>";
	  	    }
	  		return "<div style='width:200px;height:200px;overflow:hidden;' >" +
	  				"<img style='max-width:200px;max-height:200px;' src='"
	  				+getRealUrlAtt(itxt)+"'/></div>";
		}
	},


    "attachmentFilename":{
		display:'附件文件名',
		editor:"DisplayField",
		renderer:function(val,r,col,l) {
			var url = getRealUrlAtt(val);
			return "<a href=\""+url+"\" target=\"_blank\">"+url+"</a>"			
	    }
	},
    "dtCreate":{
      editor:"DateField",
		display:'注册时间',
		dcfg:{
		  	readOnly:true
		  }
	},
	"dtSubmit":{
		editor:"DateField",
		display:'提交时间'   ,
        dcfg:{
			readOnly:true
		 	}  
	},
	"dtCheck":{
		 editor:"DateField",
		display:'审查时间',
		renderer:BView.render_date,
    dcfg:{
      readOnly:true
    }  
	}, 
	"userCheck":{
	  display:'审查人',
	  cfg:{
   		  	readOnly:true
   		  }
	},
	"status":{
	  display:'状态',
    editor : 'ComboBox',
    cfg :BView.func_cfg_combo([['提交', 2], ['未提交', 0]])
    
	}, 
	"regnum":{
		display:'注册次数',
		 cfg:{
			  	readOnly:true
			  }
	}, 
	"issend":{
		display:'发邮件标志'
			, cfg:{
			  	readOnly:true
			  }
	}, 
	"isreceive":{
		display:'收到邮件标志',
		 cfg:{
			  	readOnly:true
			  }
	},
	"statusFilter":{
	  display:'初审结果',
	  editor : 'ComboBox',
	  dcfg:{
		  width:385
	  },
    cfg :BView.func_cfg_combo([['合格', 0],['基本合格', 1],['不合格', 2]])
	  
	}
	
	}	
	
},
"dao.hb.PinDataset":{
	cn:'数据集',
	req:{
		"pss" : [["id","username","datasetname","description","remark","total","directors","status","dtModify","dtCreate"],["userId","name"]],
		"cns" : ["dao.hb.PinDataset","dao.hb.UUser"],
        "hql":"from PinDataset A, UUser B where A.username=B.userId "
	},
	cfg_detail:{
	    btns:["保存"],
	    tabs:[{
			title : "基本信息",
			props :["B.name","A.total","A.dtModify","A.dtCreate","A.datasetname","A.description","A.directors"]
		}]
	},
	cfg_grid:{
		propNames:["B.name","A.datasetname","A.total","A.dtModify","A.dtCreate"],
		btns:[{
			text:'删除',
			icon : '../bat/icons/delete.png',
			scope:this,
			handler:function(btn,item){
				var ui=BView.getUi(btn);
				var objs = ui.objs;
				var sels = ui.grid.getSelectionModel().getSelection();
				if (!sels || sels.length == 0){
				  Ext.MessageBox.show({
			           title: '系统提示',
			           msg: '请先选中要删除的记录',
			           buttons: Ext.MessageBox.OK,
			           icon: Ext.MessageBox.INFO
		          });
				  return;
				}
				var me=ui;
				for (var i = 0; i < sels.length; i++) {
					var o = sels[i].obj;
					if(o.items.map['A.username']!=layout.usr_info.user){
						Ext.MessageBox.show({
					           title: '系统提示',
					           msg: '只允许选择删除本人建立的条目，请重新选择',
					           buttons: Ext.MessageBox.OK,
					           icon: Ext.MessageBox.INFO
				          });
						  return;
					}
				}
				Ext.MessageBox.confirm("确认","是否确认删除选中的["+sels.length+"]条数据",function(btn){
				     if(btn=="no"){
				        return;
				     }
				 	for (var i = 0; i < sels.length; i++) {
						var o = sels[i].obj;
						if (o.action == "A") {
							var pos = null;
							for (var k = 0, klen = objs.length; k < klen; k++) {
								if (objs[k] == o) {
									pos = k;
									break;
								}
							}
							if (pos) {
								for (var k = pos, klen = objs.length; k < klen; k++)
									objs[k] = objs[k + 1];
								objs.length--;
							}
						} else{
							o.action = "R";
						}
				  }
				me.save();
    		 		 
				});
			}
		},'检索','刷新','排序',{
		      text:  '导出',
		      cls: 'x-btn-text-icon',
		      icon : 'icons/door_out.png',
		      menu : new Ext.menu.Menu({
            items : [{
                text : "导出Word简表",
                scope : this,
                handler : function(btn,item){
                	var ui=BView.getUi(btn);
                	var sels = BView.getSels(btn);
                  if (!sels || sels.length == 0){
                      Ext.MessageBox.alert("操作提示","请先选择要导出的数据集!");
                      return;             
                    }
                  if (sels.length > 1){
                    Ext.MessageBox.alert("操作提示","为避免系统响应缓慢，请一次导出一个数据集");
                    return;             
                  }
                  var value = sels[0].data;  
                         // process text value and close...
                  var hql = "from PinPerson A,PinDatasetPerson B where  B.datasetId = "+value['A.id']+" and B.personId=A.id ) order by A.id ";                 
                  outputWord(hql,"doc_template/template_jian.doc",3,['id','username','graduateSchool','major','educational','gender','graduatedLocation','permanentResidence',
            'nativePlace','politicsStatus','nation','identityNumber','birthday','phone','photoStand'
            //,'partyTime','homeAddress','graduateDate','englishFour','englishSix','sourcePlace','highSchool','selfdescription','awarding','otherDescription','familyInfo','learningExperience','email','otherlang','dtSubmit'
            ]);  
                }
              },{
                text : "导出Word详表",
                scope : this,
                handler : function(btn,item){
                	var ui=BView.getUi(btn);
                	var sels = BView.getSels(btn);
                  if (!sels || sels.length == 0){
                      Ext.MessageBox.alert("操作提示","请先选择要导出的数据集!");
                      return;             
                    }
                  if (sels.length > 1){
                    Ext.MessageBox.alert("操作提示","为避免系统响应缓慢，请一次导出一个数据集");
                    return;             
                  }
                  var value = sels[0].data;  
                         // process text value and close...
                  var hql = "from PinPerson A,PinDatasetPerson B where  B.datasetId = "+value['A.id']+" and B.personId=A.id ) order by A.id ";                 
                  outputWord(hql,"doc_template/template_detail.doc",1,['id','username','graduateSchool','major','educational','gender','graduatedLocation','permanentResidence',
            'nativePlace','politicsStatus','nation','identityNumber','birthday','phone','photoStand',
            'partyTime','homeAddress','graduateDate','englishFour','englishSix','sourcePlace','highSchool','selfdescription','awarding','otherDescription','familyInfo','learningExperience','email','dtSubmit'
            ]);  
                }
              },
              {
                text : "导出Excel",
                scope : this,
                handler : function(btn,item){
                	var ui=BView.getUi(btn);
                	var sels = BView.getSels(btn);
                  if (!sels || sels.length == 0){
                      Ext.MessageBox.alert("操作提示","请先选择要导出的数据集!");
                      return;             
                    }
                  var oim = sels[0].obj.items.map;
                  Ext.MessageBox.confirm("确认","是否导出【"+oim['A.datasetname']+"】中的全部人员到Excel", function(btn){
                	  if(btn=="no")
	    		 			return;
	       		       var ui=btn.ui;
	       		       var baseURL =CFG.app_ctx+"servlet/DefaultExportExcelControl?obj=dao.hb.PinPerson"; 
	    			   var url = baseURL+"&type=F&filter="
	    			   +encodeURIComponent("from PinPerson A, PinDatasetPerson B where A.id=B.personId and B.datasetId="+oim['A.id']);
	    			   window.open(url,'_blank');
                   });
                }
              }]
              
		      })

		    }
		    ]
	},
	child_views : [{
	  	title:"数据集应聘人员",
	  	vn:"dao.hb.PinDatasetPerson",
		func_filter: function(robj, ui) {
			var tbar = ui.grid.down('toolbar');
			if(robj.items.map['A.username']==layout.usr_info.user){
				tbar.items.items[7].show();
				tbar.items.items[8].show();
			}else{
				tbar.items.items[7].hide();
				tbar.items.items[8].hide();
			}
			var cn = robj.cns[0];			
			var hql = " from PinDatasetPerson A, PinPerson B where A.personId=B.id and A.datasetId=" + (robj.items.map['A.id'] || 0);		
			ui.setHQL(hql);
		},
		func_setdefault:function(robj,objs){
			objs[0].items.map["A.datasetId"]=robj.items.map['A.id'];
		}
	  }],
	
	cfg_prop:{
	  "B.name":{
	     display:"创建人",
	     width:150,
		editor:"DisplayField"
		},
	  "A.id":{
		  display:'编号',		 
		  cfg:{
			  	readOnly:true
			  }
	  },
	  "A.directors":{
		display:'发送给',
		editor:"TreeCombo",
		dcfg:{
			   editable:false
		},
		cfg:{
			//singleSelect:false,
			sepperator:';',
	        width: 400,
	        listWidth:320,
	        lazyInit:false,
	        store: new Ext.data.TreeStore({
	        	proxy:{
	        	    type: 'ajax',
			           url: CFG.app_ctx+"servlet/JSonBaseServlet?status=tree&hql=from UUser  where gid=(select id from UGroup where name like '"+encodegname+"')"
		        
				        }
				        
       })
       }
       
	  },
	  "A.username":{
		  display:'创建人标识',
		  editor:"DisplayField"
	  },
	  "A.datasetname":{
		  display:'名称',
		  renderer:BView.render_display,
		  width:150,
		  cfg:{
			  width:385
		  }
	    
	  },
	  "A.description":{
		  display:'描述',
		  editor:"TextArea",
		  width:385,
		  height:150
	  },
	  "A.remark":{
		  display:'备注'
	  },
	  "A.ord":{
		  display:'排序'
	  },
	  "A.dtCreate":{
	       display:"建立时间",
		  editor:"DateField",
		  dcfg:{
			readOnly:true,
			readOnlyCls:'form_date_readonly'
		 	}  
	  },
	  "A.dtModify":{
	       display:"修改时间",
		  editor:"DateField",
		  dcfg:{
			readOnly:true,
			readOnlyCls:'form_date_readonly'
		 	}  
	  },
	  "A.total":{
	  	display:"总人数",
	  	editor:"DisplayField"
	  },
	  "A.status":{

		  
	  	display:"评审状态",
	  	width:500,
	  	editor : 'ComboBox',
        dcfg:{
        	readOnly:true
        },
    	renderer:function(val){
    		if(val)
    			return val.replace(/\n/g, "<br>");
    		else
    			return "";
    	}
	  }

	}
},
"dao.hb.PinDatasetRead":{
	cn:'数据集',
	req:{
		"pss" : [["id","username","datasetname","description","remark","total","directors","status","dtModify","dtCreate"],["userId","name"]],
		"cns" : ["dao.hb.PinDataset","dao.hb.UUser"],
        "hql":"from PinDataset A, UUser B where A.username=B.userId "
	},
	cfg_detail:{
	    btns:[],
	    tabs:[{
			title : "基本信息",
			props :["B.name","A.total","A.dtModify","A.dtCreate","A.datasetname","A.description","A.directors"]
		}]
	},
	cfg_grid:{
		propNames:["B.name","A.datasetname","A.total","A.dtModify","A.dtCreate"],
		btns:['检索','刷新','排序']
	},
	child_views : [{
	  	title:"数据集应聘人员",
	  	vn:"dao.hb.PinDatasetPersonRead",
		func_filter: function(robj, ui) {
			
			var cn = robj.cns[0];			
			var hql = " from PinDatasetPerson A, PinPerson B where A.personId=B.id and A.datasetId=" + (robj.items.map['A.id'] || 0);		
			ui.setHQL(hql);
		},
		func_setdefault:function(robj,objs){
			objs[0].items.map["A.datasetId"]=robj.items.map['A.id'];
		}
	  }],
	
	cfg_prop:{
	  "A.id":{
		  display:'编号',		 
		  cfg:{
			  	readOnly:true
			  }
	  },
	  "B.name":{
	      display:"创建人",
	      width:150,
	       editor:"DisplayField"
	  },
	  "A.directors":{
		display:'发送给',
		editor:"TreeCombo",
		dcfg:{
			   editable:false
		},
		cfg:{
			//singleSelect:false,
			sepperator:';',
	        width: 400,
	        listWidth:320,
	        lazyInit:false,
	        store: new Ext.data.TreeStore({
	        	proxy:{
	        	    type: 'ajax',
			        url: CFG.app_ctx+"servlet/JSonBaseServlet?status=tree&hql= from UUser  where gid=(select id from UGroup where name like '用人单位')"
		        
				        }
				        
       })
       }
       
	  },
	  "A.username":{
		  display:'创建人标识',
		  editor:"DisplayField"
	  },
	  "A.datasetname":{
		  display:'名称',
		  renderer:BView.render_display,
		  width:150,
		  cfg:{
			  width:385
		  }
	    
	  },
	  "A.description":{
		  display:'描述',
		  editor:"TextArea",
		  width:385,
		  height:150
	  },
	  "A.remark":{
		  display:'备注'
	  },
	  "A.ord":{
		  display:'排序'
	  },
	  "A.dtCreate":{
	       display:"建立时间",
		  editor:"DateField",
		  dcfg:{
			readOnly:true,
			readOnlyCls:'form_date_readonly'
		 	}  
	  },
	  "A.dtModify":{
	       display:"修改时间",
		  editor:"DateField",
		  dcfg:{
			readOnly:true,
			readOnlyCls:'form_date_readonly'
		 	}  
	  },
	  "A.total":{
	  	display:"总人数",
	  	editor:"DisplayField"
	  },
	  "A.status":{

		  
	  	display:"评审状态",
	  	width:500,
	  	editor : 'ComboBox',
        dcfg:{
        	readOnly:true
        },
    	renderer:function(val){
    		if(val)
    			return val.replace(/\n/g, "<br>");
    		else
    			return "";
    	}
	  }

	}
},
"dao.hb.PinDatasetPerson":{
	handles:{
		'before_setobj_detail':function( ui, editors, obj){
			var tb = layout.tbar;
			var robj = ui.robj;
			var btns = tb.items.items;
			//find 删除按钮
			var btn_delete;
			for(var i=0; i<btns.length; i++){
				var btn = btns[i];
				if(btn.text=='删除'){
					btn_delete=btn;
					break;
				}				
			}
			if(!btn_delete)
				return;
			if(robj.items.map['A.username']==layout.usr_info.user){
				btn_delete.bhide=false;
				btn_delete.show();
			}
			else{
				btn_delete.bhide=true;
				btn_delete.hide();
			}
		},
		'after_setdata':function(ui){
			//详细页删除 打开下一条
    	     if(ui.index_detail){	           
    	     	var pos = ui.index_detail;
    	     	delete ui.index_detail;
    	     	var objs = ui.objs;
    	     	//无对象可打开,返回
    	     	if(objs.length<2){
    	     		var tb = layout.tabPanel.tabBar;
    	     		var btn_return = tb.queryById("btn_return");
    	     		btn_return.fireEvent('click', btn_return);
    	     		return;
    	     	}
    	     	var robj;
    	     	if(pos<objs.length-1)
    	     		robj = objs[pos]; 
    	     	else //后面没有了,取前面的
    	     		robj= objs[pos-1];
				ui.renderDetailPanel(robj);    	     	
    	     }
    	}
	},
	cn:'数据集人员',
	req:{
		"pss" : [["id","datasetId","personId","reviewResults2","ord","dtCreate"],["id","jth","attachmentFilename","statusFilter", "majorCode","username","majorReg","graduateSchool","major","educational","gender","graduatedLocation","permanentResidence","nativePlace","nation","politicsStatus","birthday","identityNumber",
		          "partyTime","homeAddress","phone","graduateDate",
		          "englishFour","englishSix","sourcePlace","highSchool",
		          "selfdescription","awarding","otherDescription","familyInfo","learningExperience","workExperience","email","year","identifier",
		          "remark","photoStand","attachmentFilename", "dtCreate","dtSubmit",
		          "status","regnum","issend","isreceive"]],
		"cns" : ["dao.hb.PinDatasetPerson","dao.hb.PinPerson"],
		"hql" : "from PinDatasetPerson A, PinPerson B where A.personId=B.id ",
		"rownum" : 100
	},
	  cfg_detail:{
		    btns:["游标",{
		   		text: '添加到',
		   		iconCls: 'plugin_add',
		   		icon : 'icons/fam/user_add.png',
				handler : function(btn){
		             BView.showDataSet('append_dataset','共享',btn);
			    }},{
		   		text: '删除',
		   		iconCls: 'plugin_add',
		   		//id:'mi_delete',
		   		icon : 'icons/delete.png',
				handler : function(btn){
					Ext.MessageBox.confirm("确认","是否删除本条数据",function(btn){
					     if(btn=="no"){
					        return;
					     }
					 	var tp = layout.tabPanel;
					 	var ap = tp.getActiveTab();
					 	var uidt = ap.uiDetail;
					 	var robj = uidt.obj_detail;
					 	var objs = uidt.objs;
					 	var pobj = uidt.pui.obj_detail;

					 	if(pobj.items.map['A.username']!=layout.usr_info.user){
							Ext.MessageBox.show({
						           title: '系统提示',
						           msg: '只允许选择删除本人建立的条目',
						           buttons: Ext.MessageBox.OK,
						           icon: Ext.MessageBox.INFO
					          });
							  return;
						}
					 	for(var i=0; i<objs.length; i++){
					 		if(objs[i]==robj){
					 			uidt.index_detail=i;
					 			break;
					 		}
					 	}
					 	
					 	robj.action = "R";
				 		uidt.save();
					});					
			    }}],
		     tabs:[{
				title:'基本信息',	
		        items:[{
	            	xtype: 'form',
					layout: 'hbox',
			        items:[
			        {
			        	flex: 1,
			        	xtype: 'container',
			        	layout: 'vbox',
			        	items:[
			        	{
		                xtype: 'container',
		                border:false,
		                layout: 'anchor',
			            props :["B.username","B.major","B.majorReg","B.graduateSchool","B.educational","B.gender","B.graduatedLocation","B.permanentResidence","B.jth","B.nativePlace","B.nation","B.politicsStatus","B.birthday",
		                        "B.partyTime","B.graduateDate", "B.englishFour","B.englishSix","B.sourcePlace","B.highSchool","B.learningExperience","B.workExperience","B.awarding"]
			        	}
			        	]
			        },{
			        	 flex: 1,
			        	xtype: 'container',
			        	layout: 'vbox',
			        	items:[
			        	{
		                xtype: 'container',
		                border:false,
		                layout: 'anchor',
			            props :["B.photoStand","B.attachmentFilename","B.identityNumber","B.phone","B.email","B.year","B.homeAddress","B.selfdescription","B.familyInfo","B.otherDescription"]
			        	}
			        	]
			        }]	            
				}
			 ]}]
		
		},
	cfg_grid:{
		propNames:["B.id","B.username","B.majorReg","B.graduateSchool","B.graduateDate","B.major","B.educational","B.gender","B.permanentResidence","B.jth","B.nation","B.politicsStatus","B.identityNumber"],
		btns:['检索','刷新','排序',{
	   		text: '按选中添加',
	   		iconCls: 'plugin_add',
	   		icon : 'icons/add.png',
			handler : function(btn){
				 BView.showDataSet('append_dataset','',btn,true);
		    }},{
		   		text: '按检索添加',
		   		iconCls: 'plugin_add',
		   		icon : 'icons/fam/user_add.png',
				handler : function(btn){
		             BView.showDataSet('append_dataset','共享',btn);
			    }},'提示删除',{
			text:'删除检索结果',
			iconCls:'plugin_del',
			icon : 'icons/fam/user_delete.png',
			handler:function(btn){
				var ui=BView.getUi(btn);
				Ext.MessageBox.confirm('提示','您是否确认删除检索结果总【'+ui.total+'】条', function(btn, text){
	  		 		if(btn=="no")
			 			return;
	              	var req={"map":{'hql':ui.getreq(),'ds_id':ui.robj.items.map['A.id']},"javaClass":"java.util.HashMap"};
   					jsonrpc.BH.handle(function(data,ex){
					if(Bat.outputEx(ex,'删除数据集异常')){
	  		   			return;
	  		   		}
					ui.refreshGrid();
   			       },'R',req,'ZpService'); 
		   				 
		   		});
	   			   
				}
			},{

        text:  '导出',
        cls: 'x-btn-text-icon',
        icon : 'icons/door_out.png',
        menu : new Ext.menu.Menu({
          items : [{
              text : "导出Word简表",
              scope : this,
              handler : function(btn,item){
                var ui=BView.getUi(btn);
                var sels = BView.getSels(btn);
                if (!sels || sels.length == 0){
                    Ext.MessageBox.alert("操作提示","请先选择要导出的条目!");
                    return;             
                  }
              var id = "";
              for (var i = 0 ; i < sels.length ; i++){
                var value = sels[i].data;  
                var personId = value["A.personId"];
                id = id.concat(personId,",");
              }
              id = id.substring(0, id.length-1);
              var hql = "from PinPerson where id in (" +id + ") order by id "; 
                outputWord(hql,"doc_template/template_jian.doc",3,['id','username','graduateSchool','major','educational','gender','graduatedLocation','permanentResidence',
          'nativePlace','politicsStatus','nation','identityNumber','birthday','phone','photoStand'
          //,'partyTime','homeAddress','graduateDate','englishFour','englishSix','sourcePlace','highSchool','selfdescription','awarding','otherDescription','familyInfo','learningExperience','email','otherlang','dtSubmit'
          ]);  
              }
            },{
              text : "导出Word详表",
              scope : this,
              handler : function(btn,item){
                var ui=BView.getUi(btn);
                var sels = BView.getSels(btn);
                if (!sels || sels.length == 0){
                    Ext.MessageBox.alert("操作提示","请先选择要导出的条目!");
                    return;             
                  }
              var id = "";
              for (var i = 0 ; i < sels.length ; i++){
                var value = sels[i].data;  
                var personId = value["A.personId"];
                id = id.concat(personId,",");
              }
              id = id.substring(0, id.length-1);
              var hql = "from PinPerson where id in (" +id + ") order by id "; 
                outputWord(hql,"doc_template/template_detail.doc",1,['id','username','graduateSchool','major','educational','gender','graduatedLocation','permanentResidence',
          'nativePlace','politicsStatus','nation','identityNumber','birthday','phone','photoStand',
          'partyTime','homeAddress','graduateDate','englishFour','englishSix','sourcePlace','highSchool','selfdescription','awarding','otherDescription','familyInfo','learningExperience','email','dtSubmit'
          ]);  
              }
            },
            {
            	//whd 3012.10.24
              text : "导出Excel",
              scope : this,
              handler : function(btn,item){
                var ui=BView.getUi(btn);
                var sels = BView.getSels(btn);
                if (!sels || sels.length == 0){
                    Ext.MessageBox.alert("操作提示","请先选择要导出的条目!");
                    return;             
                  }
                var id = "";
                for (var i = 0 ; i < sels.length ; i++){
                  var value = sels[i].data;  
                  var personId = value["A.personId"];
                  id = id.concat(personId,",");
                }
                id = id.substring(0, id.length-1);
                var oim = sels[0].obj.items.map;
       
                Ext.MessageBox.confirm("确认","是否导出选择的条目到Excel", function(btn){
                  if(btn=="no")
              return;
                 var ui=btn.ui;
                 var baseURL =CFG.app_ctx+"servlet/DefaultExportExcelControl?obj=dao.hb.PinPerson"; 
             var url = baseURL+"&type=F&filter="
             +encodeURIComponent("from PinPerson where id in ("+id+")");
             window.open(url,'_blank');
                 });
              }
            }]
            
        })

      
			}
			]
	},
    cfg_prop:{
      "B.id":{
      	display:'序号'
      },
      "A.id":{
    	  display:'数据集应聘人员表编号',
    	  cfg:{
    		  	readOnly:true
    		  }
      },
      "A.datasetId":{
    	  display:'数据集编号',
    	  cfg:{
  		  	readOnly:true
  		  }
      },
      "A.personId":{
    	  display:'人员编号'
      },
      "A.reviewResults2":{
    	display:'审查结果',
    	editor : 'ComboBox',
    	cfg :BView.func_cfg_combo([['符合', 0], ['不符合', 1]])
      },
      "A.ord":{
    	  display:'排序'
      },
      "A.dtCreate":{
    	  editor:"DateField",
    	  display:'生成时间',
		  dcfg:{
			readOnly:true
		 	}  
      },
      	"B.majorReg":{
      	   display:"所报专业",
		   editor : 'ComboBox',
	       cfg :BView.func_cfg_combo_hql("select majorName,majorName from dao.hb.PinMajor order by ord","name"),
	       dcfg:{width:385,readOnly:true,readOnlyCls:'form_date_readonly'}
      	},
      	"B.majorCode":{
      	   display:"专业代码",
      	   cfg:{
      		  	readOnly:true
      		  }
      	},
      	"B.username":{
      	   display:"姓名",
      	   width:100,
      	   cfg:{
  		  	readOnly:true
  		  },
		  renderer:function(v,r,rec){
		  	var its = r.record?r.record.data:r.item||{};

        	var uri = getRealUrlAtt(its[ 'B.photoStand']); 
        	var img_src;
        	if(uri==""||uri==null){
        	    img_src="../cm/icons/no-photo-available.jpg";  
        	}else{
        		img_src=uri;
        	}
		  	if(r){
		     	r.tdAttr="data-qtip=\"<img  style='max-width:200px;max-height:200px;' src='"+img_src+"'>\"";
		  	}
		    return v;
		  }
      	},	
      	"B.identityNumber":{
      	   display:"身份证号码",
      	   width:200,
      	   cfg:{
      		  	readOnly:true
      		  }
      	},	
      	"B.birthday":{
      	   editor:"DateField",
      	   display:"出生日期",
      	    cfg:{
        	   format:'Y-m-d',
        	   readOnly:true,readOnlyCls:'form_date_readonly'
       }
      	},	
      	"B.gender":{
      	   display:"性别",
	    	editor : 'ComboBox',
	    	cfg :BView.func_cfg_combo([['男', '男'], ['女', '女']]),
		   dcfg:{width:385,readOnly:true,readOnlyCls:'form_date_readonly'}
      	},
      	"B.nation":{
      	   display:"民族",
      	   cfg:{
      		  	readOnly:true
      		  }
      	},
      	"B.politicsStatus":{
      	    display:'政治面貌',
      	    cfg:{
      		  	readOnly:true
      		  }
      	 },
      	"B.partyTime":{
      		
      	    display:'入党（团）时间',
      	    cfg:{
      		  	readOnly:true
      		  }
      	},
      	"B.nativePlace":{
      	    display:'籍贯',
      	    cfg:{
      	    	  	readOnly:true
      	  	  }
      	},
      	"B.permanentResidence":{
      	    display:'户口所在地',
      	    cfg:{
      		  	readOnly:true
      		  }
      	},
      	"B.homeAddress":{
      	    display:'家庭地址',
      	    editor:"TextArea",
		 	cfg:{
				width:400,
				height:150,
			  	readOnly:true,
			  	autoScroll:'none'
			  }
      	},
      	"B.phone":{                                    
      	    display:'联系电话（手机）',
      	    cfg:{
      		  	readOnly:true
      		  }
      	}
      	,"B.graduateSchool":{
      		display:'毕业院校',
      		 cfg:{
      			  	readOnly:true
      			  }
      	},
      	"B.graduateDate":{
      		display:'毕业时间',
      		 cfg:{
      			  	readOnly:true
      			  }
      	},
      	"B.graduatedLocation":{
      		display:'毕业院校所在地'
      			, cfg:{
      			  	readOnly:true
      			  }
      	},
      	"B.educational":{
      		display:'学历',
      		 cfg:{
      			  	readOnly:true
      			  }
      	},
      	"B.major":{
      		display:'具体专业',
      		 cfg:{
      			  	readOnly:true
      			  }
      	},
      	"B.englishFour":{
      		display:'英语四级成绩',
      		 cfg:{
      			  	readOnly:true
      			  }
      	},
      	"B.englishSix":{
      		display:'英语六级成绩',
      		 cfg:{
      			  	readOnly:true
      			  }
      	},
      	"B.sourcePlace":{
      		display:'生源地（高中所在地）',
      		 cfg:{
      			  	readOnly:true
      			  }
      	},
      	"B.highSchool":{
      		display:'高中毕业院校',
      		 cfg:{
      			  	readOnly:true,
      			    width:300,
    				height:20
      			  }
      	},
      	"B.selfdescription":{               
      		display:'自我描述',
      		editor:"TextArea",
    		 cfg:{
    				width:400,
    				height:300,
    			  	readOnly:true
    			  }
      	},
      	"B.awarding":{
      		display:'获奖情况',
      		editor:"TextArea",
    		 cfg:{
    				width:400,
    				height:300,
    			  	readOnly:true
    			  }
      	},
          "B.otherDescription":{
      		display:'其他说明',
      		editor:"TextArea",
    		 cfg:{
    				width:400,
    				height:300,
    			  	readOnly:true
    			  }
      	},
          "B.familyInfo":{
      		display:'家庭成员信息',
      		editor:"TextArea",
    		 cfg:{
    			  	readOnly:true,
    				width:400,
    				height:300
    			  }
      	},
          "B.learningExperience":{
      		display:'学习经历',
      		editor:"TextArea",
    		 cfg:{
    			  	readOnly:true,
    				width:400,
    				height:300
    			  }
      	},
        "B.workExperience":{
      		display:'工作经历',
      		editor:"TextArea",
    		 cfg:{
    			  	readOnly:true,
    				width:400,
    				height:300
    			  }
      	},
          "B.email":{
      		display:'邮件',
      		 cfg:{
      			  	readOnly:true
      			  }
      	},
          "B.year":{
      		display:'年代',
      		 cfg:{
      			  	readOnly:true
      			  }
      	},
          "B.identifier":{
      		display:'唯一标识符',
      		 cfg:{
      			  	readOnly:true
      			  }
      	},
          "B.otherlang":{
      		display:'其他语种掌握说明',
      		 cfg:{
      			  	readOnly:true,
      			    width:300,
  				    height:20
      			  }
      	},
          "B.remark":{
      		display:'备注'
      	},
      	"B.jth":{
      	   display:"集体户口",
      	   editor:"ComboBox",
      	   cfg :BView.func_cfg_combo([['是', '是'], ['否', '否']]),
      	   dcfg:{readOnly:true,readOnlyCls:'form_date_readonly'}
      	},
      	 "B.photoStand":{
      	 	display:'证件照' ,
		    cfg:{fieldLabel:false},
		    editor:'DisplayField',
		    hidden:true,
			renderer : function(itxt,r,col,l) {	
		  	    if(itxt==""){
	              return "<div style='width:200px;height:200px;overflow:hidden;' ><img style='max-width:200px;max-height:200px;' src= '../cm/icons/no-photo-detail.png'/></div>";
		  	    }
		  		return "<div style='width:200px;height:200px;overflow:hidden;' >" +"<img style='max-width:200px;max-height:200px;' src='"
		  		+getRealUrlAtt(itxt)+"'/></div>";
			}
    	},
          "B.attachmentFilename":{
      		display:'附件文件名',
      		editor:"DisplayField",
    		renderer:function(val,r,col,l) { 
				var url = getRealUrlAtt(val);
				return "<a href=\""+url+"\" target=\"_blank\">"+url+"</a>"
	   		 }
      	},
          "B.dtCreate":{
          	 editor:"DateField",
          	 readOnly:true,
      		display:'注册时间',
      		 cfg:{
      			  	readOnly:true
      			  }
      	},
      	"B.dtSubmit":{
      	  readOnly:true,
      		display:'提交时间'   ,
    		  editor:"DateField",
    		  dcfg:{
    			readOnly:true
    		 	}  
      	},
      	"B.dtCheck":{
      		editor:"DateField",
      		display:'审查时间',
      		renderer:BView.render_date,
          dcfg:{
            readOnly:true
          }  
      	}, 
      	"B.userCheck":{
      	  display:'审查人',
      	  cfg:{
       		  	readOnly:true
       		  }
      	},
      	"B.status":{
      		display:'状态',
      		 cfg:{
      			  	readOnly:true
      			 },
      		renderer : function(val){
      		    if (val=="2")
      		    	return "提交";
      		    else
      		    	return "未提交";
    	  	   }
      	}, 
      	"B.regnum":{
      		display:'注册次数',
      		 cfg:{
      			  	readOnly:true
      			  }
      	}, 
      	"B.issend":{
      		display:'发邮件标志'
      			, cfg:{
      			  	readOnly:true
      			  }
      	}, 
      	"B.isreceive":{
      		display:'收到邮件标志',
      		 cfg:{
      			  	readOnly:true
      			  }
      	},
    	"B.statusFilter":{
    		  display:'审查结果',
    		  editor : 'ComboBox',
    		  dcfg:{
    			  width:150
    		  },
    	    cfg :BView.func_cfg_combo([['合格', 0],['基本合格', 1],['不合格', 2]])
    		  
    		}

    }
},
"dao.hb.PinDatasetPersonRead":{

	handles:{
		'after_setdata':function(ui){
			//详细页删除 打开下一条
    	     if(ui.index_detail){	           
    	     	var pos = ui.index_detail;
    	     	delete ui.index_detail;
    	     	var objs = ui.objs;
    	     	//无对象可打开,返回
    	     	if(objs.length<2){
    	     		var tb = layout.tabPanel.tabBar;
    	     		var btn_return = tb.queryById("btn_return");
    	     		btn_return.fireEvent('click', btn_return);
    	     		return;
    	     	}
    	     	var robj;
    	     	if(pos<objs.length-1)
    	     		robj = objs[pos]; 
    	     	else //后面没有了,取前面的
    	     		robj= objs[pos-1];
				ui.renderDetailPanel(robj);    	     	
    	     }
    	}
	},
	cn:'数据集人员',
	req:{
		"pss" : [["id","datasetId","personId","reviewResults2","ord","dtCreate"],["id","jth","attachmentFilename","statusFilter", "majorCode","username","majorReg","graduateSchool","major","educational","gender","graduatedLocation","permanentResidence","nativePlace","nation","politicsStatus","birthday","identityNumber",
		          "partyTime","homeAddress","phone","graduateDate",
		          "englishFour","englishSix","sourcePlace","highSchool",
		          "selfdescription","awarding","otherDescription","familyInfo","learningExperience","workExperience","email","year","identifier",
		          "remark","photoStand","attachmentFilename", "dtCreate","dtSubmit",
		          "status","regnum","issend","isreceive"]],
		"cns" : ["dao.hb.PinDatasetPerson","dao.hb.PinPerson"],
		"hql" : "from PinDatasetPerson A, PinPerson B where A.personId=B.id ",
		"rownum" : 100
	},
	  cfg_detail:{
		    btns:["游标"],
		     tabs:[{
				title:'基本信息',	
		        items:[{
	            	xtype: 'form',
					layout: 'hbox',
			        items:[
			        {
			        	flex: 1,
			        	xtype: 'container',
			        	layout: 'vbox',
			        	items:[
			        	{
		                xtype: 'container',
		                border:false,
		                layout: 'anchor',
			            props :["B.username","B.major","B.majorReg","B.graduateSchool","B.educational","B.gender","B.graduatedLocation","B.permanentResidence","B.jth","B.nativePlace","B.nation","B.politicsStatus","B.birthday",
		                        "B.partyTime","B.graduateDate", "B.englishFour","B.englishSix","B.sourcePlace","B.highSchool","B.learningExperience","B.workExperience","B.awarding"]
			        	}
			        	]
			        },{
			        	 flex: 1,
			        	xtype: 'container',
			        	layout: 'vbox',
			        	items:[
			        	{
		                xtype: 'container',
		                border:false,
		                layout: 'anchor',
			            props :["B.photoStand","B.attachmentFilename","B.identityNumber","B.phone","B.email","B.year","B.homeAddress","B.selfdescription","B.familyInfo","B.otherDescription"]
			        	}
			        	]
			        }]	            
				}
			 ]}]
		
		},
	cfg_grid:{
		propNames:["B.id","B.username","B.majorReg","B.graduateSchool","B.major","B.educational","B.gender","B.permanentResidence","B.jth","B.nation","B.politicsStatus","B.identityNumber"],
		btns:['检索','刷新','排序']
	},
    cfg_prop:{
      "B.id":{
      	display:'序号'
      },
      "A.id":{
    	  display:'数据集应聘人员表编号',
    	  cfg:{
    		  	readOnly:true
    		  }
      },
      "A.datasetId":{
    	  display:'数据集编号',
    	  cfg:{
  		  	readOnly:true
  		  }
      },
      "A.personId":{
    	  display:'人员编号'
      },
      "A.reviewResults2":{
    	display:'审查结果',
    	editor : 'ComboBox',
    	cfg :BView.func_cfg_combo([['符合', 0], ['不符合', 1]])
      },
      "A.ord":{
    	  display:'排序'
      },
      "A.dtCreate":{
    	  editor:"DateField",
    	  display:'生成时间',
		  dcfg:{
			readOnly:true
		 	}  
      },
      	"B.majorReg":{
      	   display:"所报专业",
		   editor : 'ComboBox',
	       cfg :BView.func_cfg_combo_hql("select majorName,majorName from dao.hb.PinMajor order by ord","name"),
	       dcfg:{width:385,readOnly:true,readOnlyCls:'form_date_readonly'}
      	},
      	"B.majorCode":{
      	   display:"专业代码",
      	   cfg:{
      		  	readOnly:true
      		  }
      	},
      	"B.jth":{
     	   display:"集体户口",
     	   editor:"ComboBox",
     	   cfg :BView.func_cfg_combo([['是', '是'], ['否', '否']]),
     	   dcfg:{readOnly:true,readOnlyCls:'form_date_readonly'}
     	},
      	"B.username":{
      	   display:"姓名",
      	   width:100,
      	   cfg:{
  		  	readOnly:true
  		  },
		  renderer:function(v,r,rec){
		  	var its = r.record?r.record.data:r.item||{};

        	var uri = getRealUrlAtt(its[ 'B.photoStand']); 
        	var img_src;
        	if(uri==""||uri==null){
        	    img_src="../cm/icons/no-photo-available.jpg";  
        	}else{
        		img_src=uri;
        	}
		  	if(r){
		     	r.tdAttr="data-qtip=\"<img style='max-width:200px;max-height:200px;' src='"+img_src+"'>\"";
		  	}
		    return v;
		  }
      	},	
      	"B.identityNumber":{
      	   display:"身份证号码",
      	   width:200,
      	   cfg:{
      		  	readOnly:true
      		  }
      	},	
      	"B.birthday":{
      	   editor:"DateField",
      	   display:"出生日期",
      	  cfg:{
        	   format:'Y-m-d',
        	   readOnly:true,readOnlyCls:'form_date_readonly'
       }
      	},	
      	"B.gender":{
      	   display:"性别",
	    	editor : 'ComboBox',
	    	cfg :BView.func_cfg_combo([['男', '男'], ['女', '女']]),
		   dcfg:{width:385,readOnly:true,readOnlyCls:'form_date_readonly'}
      	},
      	"B.nation":{
      	   display:"民族",
      	   cfg:{
      		  	readOnly:true
      		  }
      	},
      	"B.politicsStatus":{
      	    display:'政治面貌',
      	    cfg:{
      		  	readOnly:true
      		  }
      	 },
      	"B.partyTime":{
      		
      	    display:'入党（团）时间',
      	    cfg:{
      		  	readOnly:true
      		  }
      	},
      	"B.nativePlace":{
      	    display:'籍贯',
      	    cfg:{
      	    	  	readOnly:true
      	  	  }
      	},
      	"B.permanentResidence":{
      	    display:'户口所在地',
      	    cfg:{
      		  	readOnly:true
      		  }
      	},
      	"B.homeAddress":{
      	    display:'家庭地址',
      	    editor:"TextArea",
		 	cfg:{
				width:400,
				height:150,
			  	readOnly:true,
			  	autoScroll:'none'
			  }
      	},
      	"B.phone":{                                    
      	    display:'联系电话（手机）',
      	    cfg:{
      		  	readOnly:true
      		  }
      	}
      	,"B.graduateSchool":{
      		display:'毕业院校',
      		 cfg:{
      			  	readOnly:true
      			  }
      	},
      	"B.graduateDate":{
      		display:'毕业时间',
      		 cfg:{
      			  	readOnly:true
      			  }
      	},
      	"B.graduatedLocation":{
      		display:'毕业院校所在地'
      			, cfg:{
      			  	readOnly:true
      			  }
      	},
      	"B.educational":{
      		display:'学历',
      		 cfg:{
      			  	readOnly:true
      			  }
      	},
      	"B.major":{
      		display:'具体专业',
      		 cfg:{
      			  	readOnly:true
      			  }
      	},
      	"B.englishFour":{
      		display:'英语四级成绩',
      		 cfg:{
      			  	readOnly:true
      			  }
      	},
      	"B.englishSix":{
      		display:'英语六级成绩',
      		 cfg:{
      			  	readOnly:true
      			  }
      	},
      	"B.sourcePlace":{
      		display:'生源地（高中所在地）',
      		 cfg:{
      			  	readOnly:true
      			  }
      	},
      	"B.highSchool":{
      		display:'高中毕业院校',
      		 cfg:{
      			  	readOnly:true,
      			    width:300,
    				height:20
      			  }
      	},
      	"B.selfdescription":{               
      		display:'自我描述',
      		editor:"TextArea",
    		 cfg:{
    				width:400,
    				height:300,
    			  	readOnly:true
    			  }
      	},
      	"B.awarding":{
      		display:'获奖情况',
      		editor:"TextArea",
    		 cfg:{
    				width:400,
    				height:300,
    			  	readOnly:true
    			  }
      	},
          "B.otherDescription":{
      		display:'其他说明',
      		editor:"TextArea",
    		 cfg:{
    				width:400,
    				height:300,
    			  	readOnly:true
    			  }
      	},
          "B.familyInfo":{
      		display:'家庭成员信息',
      		editor:"TextArea",
    		 cfg:{
    			  	readOnly:true,
    				width:400,
    				height:300
    			  }
      	},
          "B.learningExperience":{
      		display:'学习经历',
      		editor:"TextArea",
    		 cfg:{
    			  	readOnly:true,
    				width:400,
    				height:300
    			  }
      	},
        "B.workExperience":{
      		display:'工作经历',
      		editor:"TextArea",
    		 cfg:{
    			  	readOnly:true,
    				width:400,
    				height:300
    			  }
      	},
          "B.email":{
      		display:'邮件',
      		 cfg:{
      			  	readOnly:true
      			  }
      	},
          "B.year":{
      		display:'年代',
      		 cfg:{
      			  	readOnly:true
      			  }
      	},
          "B.identifier":{
      		display:'唯一标识符',
      		 cfg:{
      			  	readOnly:true
      			  }
      	},
          "B.otherlang":{
      		display:'其他语种掌握说明',
      		 cfg:{
      			  	readOnly:true,
      			    width:300,
  				    height:20
      			  }
      	},
          "B.remark":{
      		display:'备注'
      	},
      	 "B.photoStand":{
      	 	display:'证件照' ,
		    cfg:{fieldLabel:false},
		    editor:'DisplayField',
		    hidden:true,
			renderer : function(itxt,r,col,l) {	
		  	    if(itxt==""){
	              return "<div style='width:200px;height:200px;overflow:hidden;' ><img style='max-width:200px;max-height:200px;' src= '../cm/icons/no-photo-detail.png'/></div>";
		  	    }
		  		return "<div style='width:200px;height:200px;overflow:hidden;' >" +"<img style='max-width:200px;max-height:200px;' src='"+
		  		getRealUrlAtt(itxt)+"'/></div>";
			}
    	},
          "B.attachmentFilename":{
      		display:'附件文件名',
      		editor:"DisplayField",
    		renderer:function(val,r,col,l) { 
				var url = getRealUrlAtt(val);
				return "<a href=\""+url+"\" target=\"_blank\">"+url+"</a>"
	   		 }
      	},
          "B.dtCreate":{
          	 editor:"DateField",
          	 readOnly:true,
      		display:'注册时间',
      		 cfg:{
      			  	readOnly:true
      			  }
      	},
      	"B.dtSubmit":{
      	  readOnly:true,
      		display:'提交时间'   ,
    		  editor:"DateField",
    		  dcfg:{
    			readOnly:true
    		 	}  
      	},
      	"B.dtCheck":{
      		editor:"DateField",
      		display:'审查时间',
      		renderer:BView.render_date,
          dcfg:{
            readOnly:true
          }  
      	}, 
      	"B.userCheck":{
      	  display:'审查人',
      	  cfg:{
       		  	readOnly:true
       		  }
      	},
      	"B.status":{
      		display:'状态',
      		 cfg:{
      			  	readOnly:true
      			 },
      		renderer : function(val){
      		    if (val=="2")
      		    	return "提交";
      		    else
      		    	return "未提交";
    	  	   }
      	}, 
      	"B.regnum":{
      		display:'注册次数',
      		 cfg:{
      			  	readOnly:true
      			  }
      	}, 
      	"B.issend":{
      		display:'发邮件标志'
      			, cfg:{
      			  	readOnly:true
      			  }
      	}, 
      	"B.isreceive":{
      		display:'收到邮件标志',
      		 cfg:{
      			  	readOnly:true
      			  }
      	},
    	"B.statusFilter":{
    		  display:'审查结果',
    		  editor : 'ComboBox',
    		  dcfg:{
    			  width:150
    		  },
    	    cfg :BView.func_cfg_combo([['合格', 0],['基本合格', 1],['不合格', 2]])
    		  
    		}

    }

},
"dao.hb.PinMajor":{
	cn:'招聘专业',
	req:{
		"pss" : [["id","majorName","majorCode","description","demand","status","year","dtCreate"]],
		"cns" : ["dao.hb.PinMajor"],
		"hql" : "from PinMajor"
	},
	cfg_detail:{
	    btns:["保存","游标"],
	    tabs:[{
			title : "基本信息",
			props :["id","majorName","majorCode","description","demand","status","year","dtCreate"]
		}]
	},
	cfg_grid:{
	    propNames:["majorName","status","year"]
	},
    cfg_prop:{
      "id":{
    	  display:'编号',
    	  cfg:{
    		  	readOnly:true
    		  }
      },
      "majorName":{
    	  display:'名称',
    	  renderer:BView.render_display,
    	  width:200,
    	  cfg:{
    		   allowBlank:false
    		  }

      },
      "majorCode":{
    	  display:'代码',
    	  cfg:{
    		   allowBlank:false
    		  }

      },
      "description":{
    	  display:'描述'
      },
      "demand":{
    	  display:'要求'
      },
      "status":{
    	  display:'状态',
    	  editor : 'ComboBox',
    	  dcfg:{
    		  allowBlank:false
    	  },
    	  cfg :
    		  BView.func_cfg_combo([['发布', 1], ['禁用', 0]])	
      },
      "year":{
    	  display:'年份',
    	  cfg:{
    		   allowBlank:false
    		  }

      },
      "dtCreate":{
    	  display:'生成时间',
		  editor:"DateField",
		  dcfg:{
			readOnly:true
		 	}  
      }
    }
}
	 }
//clone view and modify return bv?bv.cn||itxt:itxt;
var pvw = Bat.clone(BView.viewMap['dao.hb.LSync']);
var bv = BView.viewMap;
var arg=new Array();
var i=0;
for(var key in bv){
	var value=bv[key];
	var cn=value.cn||key;
	arg.push([cn,key]);
    
}

pvw.cfg_prop["A.entity"].cfg=BView.func_cfg_combo(arg);
pvw.cfg_grid.btns=[];
pvw.req.pss=[["id","userid","entity","entityId","snapJson","dtCreate","entity", "remark","oper"],["name","avatar"]];
pvw.cfg_grid.propNames=["A.id","A.dtCreate","A.oper","A.remark","A.entity","A.entityId"],
BView.viewMap['dao.hb.LSync1']=pvw;



