/*!
 * Ext JS Library 3.2.1
 * Copyright(c) 2006-2010 Ext JS, Inc.
 * licensing@extjs.com
 * http://www.extjs.com/license
 */
function setActiveStyleSheet(title) {
	title="neptune";       //2014.02.21    设置默认风格 为 “neptune”
    var i,
        a,
        links = document.getElementsByTagName("link"),
        len = links.length;
    for (i = 0; i < len; i++) {
        a = links[i];
        if (a.getAttribute("rel").indexOf("style") != -1 && a.getAttribute("title")) {
            a.disabled = true;
            if (a.getAttribute("title") == title) a.disabled = false;
        }
    }
    title=title;
}

function getActiveStyleSheet() {
    var i,
        a,
        links = document.getElementsByTagName("link"),
        len = links.length;
    for (i = 0; i < len; i++) {
        a = links[i];
        if (a.getAttribute("rel").indexOf("style") != -1 && a.getAttribute("title") && !a.disabled) {
            return a.getAttribute("title");
        }
    }
    return null;
}

function getPreferredStyleSheet(ck) {
    var i,
        a,
        links = document.getElementsByTagName("link"),
        len = links.length;
    var theme_default;
    for (i = 0; i < len; i++) {
        a = links[i];
        if (a.getAttribute("rel").indexOf("style") != -1 && a.getAttribute("rel").indexOf("alt") == -1 && a.getAttribute("title")) {
        	if(ck==a.getAttribute("title"))
            	return a.getAttribute("title");
            else{
            	theme_default=a.getAttribute("title")
            }
        }
    }
    return theme_default;
}

function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    } else {
        expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=",
        ca = document.cookie.split(';'),
        i,
        c,
        len = ca.length;
    for ( i = 0; i < len; i++) {
        c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1, c.length);
        }
        if (c.indexOf(nameEQ) == 0) {
            return c.substring(nameEQ.length, c.length);
        }
    }
    return null;
}
var gridPagePer;
function getPreferredPagePer(){
	return 15;
}
window.onload = function (e) {
    var cookie = readCookie("style");
    var title = getPreferredStyleSheet(cookie);
    setActiveStyleSheet(title);
    var cookie = readCookie("gridPagePer");
    if(cookie)
    	gridPagePer =parseInt(cookie);
    else
    	gridPagePer = getPreferredPagePer();
    //c4w 2015
    gridPagePer=200;
}

window.onunload = function (e) {
    var title = getActiveStyleSheet();
    createCookie("style", title, 365);
    createCookie("gridPagePer", gridPagePer, 365);
}
//setActiveStyleSheet(title);
