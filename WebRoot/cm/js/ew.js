/**  
 * <p> Title: 用word替换的方式将内容导出到word</p> 
 * <p> Description: **</p> 
 * <p> Copyright: Copyright (c) 2007-2010 </p> 
 * <p> Company: ** </p> 
 * @author c4w 
 * @version 1.0 
 */ 

var WordApp = function(wordTplPath){ 
 var wordObj = new ActiveXObject("Word.Application"); 
 if(wordObj==null){ 
  alert( "不能创建Word对象！"); 
 }    
 wordObj.visible=false; 
 wordObj.Options.CheckSpellingAsYouType=false;
 wordObj.Options.CheckGrammarAsYouType=false;
 wordObj.Options.CheckGrammarWithSpelling=false;

 this.wordObj = wordObj; 
//   /zp/    doc_template/template_jian.doc
 this.docObj = this.wordObj.Documents.Open(getRootPath() + wordTplPath); 
 wordObj.ActiveDocument.ShowGrammaticalErrors = false;
 wordObj.ActiveDocument.ShowSpellingErrors = false;

} 

WordApp.prototype.closeApp = function(){ 
 if (this.wordObj !=null){ 
   this.wordObj.Quit();  
 } 
} 
WordApp.prototype.copyAll = function(){
	return this.docObj.Range().Cut();	
}

WordApp.prototype.paste = function(){
	//this.wordObj.Selection.MoveDown();
	//this.wordObj.selection.InsertBreak(7);
	//光标移至结尾
	this.wordObj.Selection.EndKey(6);
	this.wordObj.Selection.Paste();
	var len = this.docObj.Tables.count;
	return this.docObj.Tables(len).Range;
}
WordApp.prototype.appendPageBreak = function(){
	this.wordObj.Selection.EndKey(6);
	this.wordObj.Selection.InsertBreak(7);
}
WordApp.prototype.replacePic = function( Range, strFld, strVal , w, h ) {
	try {
	    var Selection = Range;
	    Selection.Find.Replacement.Text = '';
	    Selection.Find.Forward = false;
	    Selection.Find.Wrap = 1;
	    Selection.Find.Format = false;
	    Selection.Find.MatchCase = false;
	    Selection.Find.MatchWholeWord = true;
	    Selection.Find.ClearFormatting();    
	    Selection.Find.Text = strFld;
	    
	    if(Selection.Find.Execute()){
	    	Selection.Text='';
	    	var pic = Selection.InlineShapes.AddPicture(strVal);
		    	pic.Height=h;
		    	pic.Width=w;
		} 
	    return true;
    } catch (e) {
    	return false;
    }
} 
WordApp.prototype.replaceText = function( Range, strFld, strVal ) {
    var Selection = Range;
    Selection.Find.Replacement.Text = '';
    Selection.Find.Forward = true;
    Selection.Find.Wrap = 1;
    Selection.Find.Format = false;
    Selection.Find.MatchCase = false;
    Selection.Find.MatchWholeWord = true;
    Selection.Find.ClearFormatting();    
    Selection.Find.Text = strFld;
    if(Selection.Find.Execute()){
    	Selection.Text=strVal;
    }    
} 

WordApp.prototype.replace = function( Range, strFld, strVal ) {
　　	Range.Find.Execute( strFld, true, false, false, false, false, true, false, false, strVal);
} 

WordApp.prototype.replaceBookmark = function(strName,content,type){ 
 if (this.wordObj.ActiveDocument.BookMarks.Exists(strName)) { 
  if (type != null && type == "pic") {//图片 
            var objDoc = this.wordObj.ActiveDocument.BookMarks(strName).Range.Select(); 
            var objSelection = this.wordObj.Selection; 
            objSelection.TypeParagraph(); 
   //alert(getRootPath()+content); 
            var objShape = objSelection.InlineShapes.AddPicture(getRootPath()+content); 
  } 
  else { 
   this.wordObj.ActiveDocument.BookMarks(strName).Range.Select(); 
   this.wordObj.Application.selection.Text = content; 
  } 
 }else{ 
  //alert("标签不存在"); 
 } 
} 

WordApp.prototype.replaceBookmarkUsevo = function(voObj){ 
 if(typeof voObj != "object"){ 
  alert("请输入正确的vo对象"); 
 }else{ 
  for(var i in voObj){ 
   this.replaceBookmark(i,voObj[i]); 
  } 
 } 
} 

WordApp.prototype.replaceBookmarkUsepicvo = function(voObj){ 
 if(typeof voObj !="object"){ 
  alert("请输入正确的vo对象"); 
 }else{ 
  for(var i in voObj){ 
   this.replaceBookmark(i,voObj[i],"pic"); 
  } 
 } 
} 

WordApp.prototype.replaceBookmarkUsevolist = function(strName,voListObj){ 
 if(typeof voListObj != "object"){ 
  alert("参数应为数组类型"); 
 }else{  
  var row = voListObj.volist.length; 
  var col = voListObj.cols.length; 
  var objDoc = this.wordObj.ActiveDocument.BookMarks(strName).Range; 
  var objTable = this.docObj.Tables.Add(objDoc,row,col) ;//插入表格 
  for (var i = 0; i < row; i++) { 
   for(var j=0; j<col; j++){ 
    //todo 列表里面如果有图片类型不支持，需要判断 
    objTable.Cell(i+1,j+1).Range.InsertAfter(voListObj.volist[i][voListObj.cols[j]]); 
    var width = voListObj.widths[j]; 
    if(width.indexOf("px")!=-1){ 
     objTable.Cell(i+1,j+1).Width = (width.substr(0,width.length-2)/100) * 28.35;//1厘米=28.35磅 
    }    
   } 
  } 
  //objTable.AutoFormat(16); 
  objTable.Borders.InsideLineStyle = 1 
        objTable.Borders.OutsideLineStyle = 0; 
 } 
} 


function objToString(obj){ 
 if(obj instanceof Array){ 
  var str=""; 
  for(var i=0;i<obj.length;i++){ 
   str+="["; 
   for(var j in obj[i]){ 
    str+=j+"="+obj[i][j]+" "; 
   } 
   str+="]\n";   
  } 
  return str; 
 }else if(obj instanceof Object){ 
  var str=""; 
  for(var i in obj){ 
   str+=i+"="+obj[i]+" "; 
  } 
  return str;   
 } 
} 

function getRootPath() 
{ 
 var location=document.location;  
 if ("file:" == location.protocol) { 
  var str = location.toString(); 
  return str.replace(str.split("/").reverse()[0], ""); 
 } 
 var pathName=location.pathname.split("/"); 
 return location.protocol+"//"+location.host+"/"+pathName[1]+"/"; 
}