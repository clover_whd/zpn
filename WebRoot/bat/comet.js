//语法甘露：
var object = // 定义小写的object基本类，用于实现最基础的方法等
{
	isA : function(aType) // 一个判断类与类之间以及对象与类之间关系的基础方法
	{
		var self = this;
		while (self) {
			if (self == aType)
				return true;
			self = self.Type;
		};
		return false;
	}
};

function Class(aBaseClass, aClassDefine) // 创建类的函数，用于声明类及继承关系
{
	function class_() // 创建类的临时函数壳
	{
		this.Type = aBaseClass; // 我们给每一个类约定一个Type属性，引用其继承的类
		for (var member in aClassDefine)
			this[member] = aClassDefine[member]; // 复制类的全部定义到当前创建的类
	};
	class_.prototype = aBaseClass;
	return new class_();
};

function New(aClass, aParams) // 创建对象的函数，用于任意类的对象创建
{
	function new_() // 创建对象的临时函数壳
	{
		this.Type = aClass; // 我们也给每一个对象约定一个Type属性，据此可以访问到对象所属的类
		if (aClass.Create)
			aClass.Create.apply(this, aParams); // 我们约定所有类的构造函数都叫Create，这和DELPHI比较相似
	};
	new_.prototype = aClass;
	return new new_();
};

var WSClient = Class(object, {
	Create:function(url){
		this.url = "ws://localhost:8080/zp/wst.do";
		this.data = {};
	},
	updateState:function(s){
		var s = JSON.parse(str);
		var smap = this.data[s.cn];
		if(!smap){
			smap = {};
			this.data[s.cn]=smap;
		}
		switch(s.action){
		case 'A':
			break;
		case 'R':
			break;
		case 'U':
			break;
		}
	},
	onmessage:function(evt){
		this.updateState(evt.data)
	},
	onclose:function(){
		this.data = {};
	},
	onopen:function(){
		
	},
	connect:function(){
		var WSocket = window.WebSocket||window.MozWebSocket;
		if (!WSocket) 
			return;
		// 创建WebSocket
		var ws = new WSocket(this.url);
		// 收到消息时在消息框内显示
		ws.onmessage = this.onmessage(evt);
		// 断开时会走这个方法
		ws.onclose = this.onclose();
		// 连接上时走这个方法
		ws.onopen = this.onopen();
	}
});

function startWebSocket() {
	var WSocket = window.WebSocket||window.MozWebSocket;
	if (!WSocket) alert("WebSocket not supported by this browser!");
	// 创建WebSocket
	ws = new WSocket("ws://localhost:8080/zp/wst.do");
	// 收到消息时在消息框内显示
	ws.onmessage = function(evt) {   
		console.log(evt.data);
	};
	// 断开时会走这个方法
	ws.onclose = function() { 
		console.log('close~~~~~~~');
	};
	// 连接上时走这个方法
	ws.onopen = function() {   
		console.log('open~~~~~~~~'); 
	};
}

//startWebSocket();