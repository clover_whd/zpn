
Bat.ExtObjReader = function(meta, recordType){
    this.meta = meta;
    var props=[];
    for(var i=0,len=recordType.length; i<len; i++)
      props[props.length]=recordType[i].name;
    this.props=props;
};
Ext.define('Bat.ExtObjReader',  {
	extend:'Ext.data.DataReader',
    constructor:function( recordType){
	    var props=[];
	    for(var i=0,len=recordType.length; i<len; i++)
	      props[props.length]=recordType[i].name;
	    this.props=props;
       Bat.ExtObjReader.superclass.constructor.call(this,recordType);
	},
    readRecords : function(o){
    	var props=this.props;
    	var records = [];
    	for(var i=0,len=o.length; i<len; i++){
    		if(o[i].action=='R' || o[i].action=='D'	)
    		  continue;
    		var imap = o[i].items.map;
    		var record = new Bat.ExtRecordObj({data:imap,id:i});
	        record.obj=o[i];
	        records[records.length] = record;
    	}
	    return {
	        records : records
	    };
    }
});

Ext.define('Bat.ExtRecordObj',  {
	extend:'Ext.data.Record',
	constructor:function(config){
    	Bat.ExtRecordObj.superclass.constructor.call(this,config);
		var id = config.id;
		this.id = (id || id === 0) ? id : ++Ext.data.Record.AUTO_ID;
    	this.data = config.data;
	},
	getField:function(name){
		return this.fields.get(name);
	},
    set : function(name, value){
    	Bat.ExtRecordObj.superclass.set.call(this,name,value);
    	this.obj.items.map[name]=value;
    	if(this.obj.action!='A')
    		this.obj.action='U';
  }});

Ext.define('Bat.ExtProxy', {
    extend: 'Ext.data.proxy.Proxy',
    alias: 'proxy.Bat',
    constructor:function(config){
       this.ui=config.ui;
       this.reader = config.reader;
       Bat.ExtProxy.superclass.constructor.call(this,config);
	},
    create: function (operation, callback, scope) {
             //Here goes your create logic
        },
        read: function (operation, callback, scope) {
        	var reader = this.reader; 
        	this.ui.load(operation,reader,callback,scope);
        },
        update: function (operation, callback, scope) {
             //Here goes your update logic
        },
        destroy: function (operation, callback, scope) {
             //Here goes your delete logic
        }
});