Ext.define('Ext.form.ChildView', {
    alias : 'child_view',
    xtype: 'child_view',
	extend :'Ext.Component', 
    getValue : function(){
        //return Ext.form.ChildView.superclass.getValue.call(this)|| "";
    	return null;
    },
    setValue : function(val){
    	if(!this.bodyEl)
    		return;
        Ext.form.ChildView.superclass.setValue.call(this, val);
        this.fireEvent("change", this, val);
    },
    // private
    initComponent: function(){
        //Ext.form.ChildView.superclass.initComponent.call(this);
    	this.pui.evt.on('after_setobj_detail', function(bui,editors,robj){
    		if(robj.action=='A'){
    			this.ui1.grid.disable();
    		}else{
    			this.ui1.grid.enable();
				this.ui1.robj = robj;
				if (this.func_filter)
					this.func_filter(robj, this.ui1);
				this.ui1.refreshGrid();
    		}
    	},this);
        this.addEvents(
            'uploaded',
            'fileselected'
        );
    },
    // private
    onRender : function(ct, position){
        var me = this,id = me.id;
        me.callParent(arguments);

        var eo = me.el;
        if(me.height)
        	me.height=300;
        eo.setWidth("100%");
        //eo.dom.innerHTML="<div>hahahaha</div>";
        var rw = me;
		var vw1 = New(BView, [BView.viewMap[rw.vn]]);
		if(rw.func_view){
			rw.func_view(vw1,robj);
		}
		var ui1 = New(BatUI, [vw1, layout.tabPanel,
				rw.title]);
		me.ui1 = ui1;
		ui1.gridHeight = me.height-50;
		if (rw.func_setdefault)
			ui1.func_setdefault = rw.func_setdefault;
        var grid = new Bat.Grid({
		    title: 'Fit Panel',
		    header:false,
		    border: false,
		    renderTo :eo,
		    ui:ui1,
		    height:me.height
		});
    }
        
});
/**
 * @class Ext.ux.grid.feature.Tileview
 * @extends Ext.grid.feature.Feature
 *
 * @author Harald Hanek (c) 2011-2012
 * @license http://harrydeluxe.mit-license.org
 */
Ext.define('Ext.ux.grid.feature.Tileview', {
    extend: 'Ext.grid.feature.Feature',
    alias: 'feature.tileview',
    metaTableTplOrig: null, // stores the original template
    viewMode: null,
    viewTpls: {},

    init: function(grid) {
        var me = this,
        view = me.view;
		//me.id = Ext.id();
		//me.grid = grid;
       // grid.view.tableTpl.html = grid.view.tableTpl.html.replace(/\{\[view.renderColumnSizer\(out\)\]\}/, '');
        //grid.view.tableTpl=this.tableTpl;
        me.metaTableTplOrig = me.view.tableTpl;
        me.view.addTableTpl(new Ext.XTemplate(me.tableTpl));
        view.tileViewFeature = me;

        Ext.Object.each(this.viewTpls, function(key, rowTpl) {
            view.addRowTpl(new Ext.XTemplate(rowTpl));
        })


        me.callParent(arguments);
    },

    getColumnValues: function(columns, record) {
        var columnValues = {};
        Ext.each(columns, function(column) {
            var key = column.dataIndex,
                value = record.data[column.dataIndex];

            columnValues[key] = value;
        });
        return columnValues;
    },

    getRowBody: function(values, viewMode)
    {
        if(this.viewTpls[viewMode])
        {
            return this.viewTpls[viewMode];
        }
    },

    setView: function(mode,btn)
    {
        var me = this;
		var view = btn.up('bat_grid').view;
        if(view.viewMode != mode)
        {
            if (mode!='default') {
                view.addTableTpl(new Ext.XTemplate(me.tableTpl));
            } else {
                view.addTableTpl(me.metaTableTplOrig);
            }
            me.viewMode = mode;
            view.viewMode = mode;
            view.refresh();
        }
    }
});

function uploadResponse(){
	var iframeId = document.getElementById("bat_iframehide");
	var content;
	 if (iframeId.contentDocument) {
        content = iframeId.contentDocument.body.innerHTML;
    } else if (iframeId.contentWindow) {
        content = iframeId.contentWindow.document.body.innerHTML;
    } else if (iframeId.document) {
        content = iframeId.document.body.innerHTML;
    }
 	if(content.indexOf('OK')==0){
 		Ext.MessageBox.hide();
 		if(content.indexOf('Refresh')>0){
 			layout.selectNode("系统管理","流程部署");
 		}else if(content.indexOf('OK-FILEUPLOAD')!=-1){
 			iframeId._fld_upload.addFiles(content.substring(14));
 			//alert(content);
 		}
 		
 	}else{
 		if(!content.trim()=="")
 			Ext.MessageBox.alert('上传文件失败',content);
 	}
}
Ext.util.Format.comboRenderer = function(combo){
    return function(value){
        var record = combo.findRecord(combo.valueField, value);
        return record ? record.get(combo.displayField) : combo.valueNotFoundText;
    }
}



/*------------------------------------------------------------*/
//Ext.ns('Ext.ux','Ext.ux.form');
Ext.ns('Ext.ux.grid');

Ext.define('Ext.ux.grid.CheckColumn', {
	extend:'Ext.grid.Column',

    /**
     * @private
     * Process and refire events routed from the GridView's processEvent method.
     */
    processEvent : function(name, e, grid, rowIndex, colIndex){
        if (name == 'mousedown') {
            var record = grid.store.getAt(rowIndex);
            record.set(this.dataIndex, !record.data[this.dataIndex]);
            return false; // Cancel row selection.
        } else {
            return Ext.grid.ActionColumn.superclass.processEvent.apply(this, arguments);
        }
    },

    renderer : function(v, p, record){
        p.css += ' x-grid3-check-col-td'; 
        return String.format('<div class="x-grid3-check-col{0}">&#160;</div>', v ? '-on' : '');
    },

    // Deprecate use as a plugin. Remove in 4.0
    init: Ext.emptyFn
});

Ext.define('Ext.form.TreeCombo', {
	extend:'Ext.form.TriggerField',
    triggerClass: 'x-form-tree-trigger',

    getPathText:function(node){
    	return node.getPath('text',this.pathSpliter).substring(6);
    },
    initComponent : function(){
    	//c4w
 		this.id=Ext.id();
        this.readOnly = false;
		this.isExpanded = false;
		//c4w
		if(!this.root)
			this.root={
				text:"Root", 
				allowDrag:false,
				allowDrop:false
			};
			//this.root = new Ext.tree.AsyncTreeNode({  });
		if (!this.sepperator) {
                this.sepperator=','
        }
		if (!this.pathSpliter) {
                this.pathSpliter='#'
        }
		
		if (!Ext.isDefined(this.singleCheck)) {
            this.singleCheck=false;
        } 
        
        Ext.form.TreeCombo.superclass.initComponent.call(this);
        this.on('show',function() {
			this.setRawValue('');
			this.getTree();
			
			if (this.treePanel.loader.isLoading()) {
				this.treePanel.loader.on('load',function(c,n) {
					//c4w
					//n.expandChildNodes(true);
					if (this.setValueToTree()) this.getValueFromTree();
				},this);
			} else {
				if (this.setValueToTree()) this.getValueFromTree();
			}
		});
    },
	
	onTriggerClick: function() {
		if (this.isExpanded) {
			this.collapse();
		} else {
			this.expand();
		}
    } ,
	
	// was called combobox was collapse
    collapse: function() {
		this.isExpanded=false;
		this.getTree().hide();
        if (this.resizer)this.resizer.resizeTo(this.treeWidth, this.treeHeight);
		this.getValueFromTree();
    },
	
	// was called combobox was expand
	expand: function () {
        this.isExpanded=true;
		this.getTree().show();
		var el = this.getTree().getEl();
        el.alignTo(this.getEl().wrap(), 'tl-bl?');

		this.setValueToTree();
	},
	setValue: function (v,n) {
		this.value=v;
		this.setValueToTree();
	},
	
    getValue: function() {
        if (!this.value) { 
			return '';
		} else {
			return this.value;
		}
    },
	setValueToTree: function () {
		//c4w
		this.setRawValue(this.value);
		if(this.singleSelect)
			return false;
		// check for tree ist exist
		if (!this.treePanel){
			//c4w
			this.getTree();
			//return false;
		}

		// split this.value to array with sepperate value-elements
		var arrVal=new Array();
		try {
			arrVal = this.value.split(this.sepperator);
		} catch (e) {};
		
		// find root-element of treepanel, and expand all childs
		var node=this.treePanel.getRootNode();
		//c4w
		node.expandChildren(true);
		
		// search all tree-children and check it, when value in this.value
		node.cascadeBy(function (n) {
			var nodeCompareVal='';
			var nodeCheckState=false;  // default the note will be unchecked
			
			if (n.value) {
				// in node-element a value-property was used
				nodeCompareVal=Ext.util.Format.trim(n.value);
			} else {
				// in node-element can't find a value-property, for compare with this.value will be use node-element.text
				//c4w 
				//nodeCompareVal=String.trim(n.attributes.text);
				nodeCompareVal=Ext.util.Format.trim(this.getPathText(n));
			}
			
			Ext.each(arrVal,function(arrVal_Item) {
				if (Ext.util.Format.trim(arrVal_Item) == nodeCompareVal) {
					// set variable "nodeCheckState" to check node
					nodeCheckState=true;
				}
			},this);			
			// when state (of node) is other as variable "nodeCheckState", then set new value to node!
			if(n.get('checked')!=nodeCheckState){
				n.set('checked',nodeCheckState);
				n.fireEvent('checkchange', n, nodeCheckState);  
			}
			//if (n.getUI().isChecked()!=nodeCheckState) n.getUI().toggleCheck(nodeCheckState);			
		},this);		
		return true;
	},
	
	
	
	getValueFromTree: function () {
		if(!this.singleSelect){		
			this.ArrVal= new Array();
			this.ArrDesc= new Array();
	
			Ext.each(this.treePanel.getChecked(),function(item) {
				if (!item.get('value')) {
					//c4w
					this.ArrVal.push(this.getPathText(item));
				} else {
					this.ArrVal.push(item.value);
				}
				this.ArrDesc.push(this.getPathText(item));
			},this);
	
	
			this.value=this.ArrVal.join(this.sepperator);
			this.valueText=this.ArrDesc.join(this.sepperator);
		}else{
			var sels = this.treePanel.getSelectionModel().getSelection();
			if(sels && sels.length>0){
				var val= sels[0].raw['value'];
				if(val){
					this.value=val;
				}
			}
		}
		this.setRawValue(this.valueText);
		this.setValue(this.value);
		this.fireEvent("change", this, this.value);
	},
	
	validateBlur : function(){
        return !this.treePanel || !this.treePanel.isVisible();
    },

	/*
	 * following functions are using by treePanel
	 */
	
    getTree: function() {
        if (!this.treePanel) {
            if (!this.treeWidth) {
                this.treeWidth = Math.max(200, this.width || 200);
            }
            if (!this.treeHeight) {
                this.treeHeight = 200;
            }
            this.treePanel = new Ext.tree.TreePanel({
                renderTo: Ext.getBody(),
                pathSeparator :'#',
                id:Ext.id(),
                store: this.store ,
                root: this.root,
                rootVisible: false,
                floating: true,
                autoScroll: true,
                minWidth: 200,
                minHeight: 200,
                width: this.treeWidth,
                height: this.treeHeight,
                listeners: {
                    hide: this.onTreeHide,
                    show: this.onTreeShow,
                    select: this.onTreeNodeClick,
					checkchange: this.onTreeCheckChange,
                    expand: this.onExpandOrCollapseNode,
                    collapse: this.onExpandOrCollapseNode,
                    resize: this.onTreeResize,
                    scope: this
                }
            });
            this.treePanel.show();
            this.treePanel.hide();
            //this.relayEvents(this.treePanel.loader, ['beforeload', 'load', 'loadexception']);
            if(this.resizable){
                this.resizer = new Ext.Resizable(this.treePanel.getEl(),  {
                   pinned:true, handles:'se'
                });
                this.mon(this.resizer, 'resize', function(r, w, h){
                    this.treePanel.setSize(w, h);
                }, this);
            }
        }
        return this.treePanel;
    },

    onExpandOrCollapseNode: function() {
        if (!this.maxHeight || this.resizable)
            return;  // -----------------------------> RETURN
        var treeEl = this.treePanel.getTreeEl();
        var heightPadding = treeEl.getHeight() - treeEl.dom.clientHeight;
        var ulEl = treeEl.child('ul');  // Get the underlying tree element
        var heightRequired = ulEl.getHeight() + heightPadding;
        if (heightRequired > this.maxHeight)
            heightRequired = this.maxHeight;
        this.treePanel.setHeight(heightRequired);
    },

    onTreeResize: function() {
        if (this.treePanel)
            this.treePanel.getEl().alignTo(this.getEl().wrap(), 'tl-bl?');
    },

    onTreeShow: function() {
        Ext.getDoc().on('mousewheel', this.collapseIf, this);
        Ext.getDoc().on('mousedown', this.collapseIf, this);
    },

    onTreeHide: function() {
        Ext.getDoc().un('mousewheel', this.collapseIf, this);
        Ext.getDoc().un('mousedown', this.collapseIf, this);
    },

    collapseIf : function(e){
        if(!e.within(this.wrap) && !e.within(this.getTree().getEl())){
            this.collapse();
        }
    },

    onTreeNodeClick: function(tree, node) {
    	if(this.singleSelect){
    		if(node.get('value')){
    			this.value=node.get('value');
    		}else{
    			this.value=this.getPathText(node);
    		}
	        this.valueText =this.getPathText(node);
	        this.collapse();
    	}
   },
	onTreeCheckChange:function (node,value,eOpts) {
		console.log(eOpts);
		if (this.singleCheck) {
			// temporary disable event-listeners on treePanel-object 
			this.treePanel.suspendEvents(false);

			// disable all tree-checkboxes, there checked at the moment			
			Ext.each(this.treePanel.getChecked(),function(arrVal) { 
				arrVal.getUI().toggleCheck(false);
			} );
			
			// re-check the selected node on treePanel-object
			node.getUI().toggleCheck(true);
			
			// activate event-listeners on treePanel-object
			this.treePanel.resumeEvents();
		}else{
			var flag=true;
			var parentNode=node.parentNode;
			var child=parentNode.childNodes;
			for(var i=0;i<child.length;i++){
				if(!child[i].data.checked){
					flag=false;
				}
			}
				parentNode.set("checked",flag);					
			if(!node.raw.leaf){				
				node.eachChild(function(child) {
					child.set("checked",value);	
			});	
				
		}
		}
	},
	
	getRendererFunction: function (value) {
		var out=new Array();
		
		//console.info(value);
		//console.debug(this.getEditor().field.loader);
		if (Ext.util.Format.trim(value)!="") {
			var TreePanel = this.getEditor().field.getTree();
			var Sepperator = this.getEditor().field.sepperator;
			
				
			// split this.value to array with sepperate value-elements
			var arrVal=new Array();
			try {
				arrVal = value.split(Sepperator);
			} catch (e) {};
				
			TreePanel.expandAll();			
			TreePanel.getRootNode().cascadeBy(function (n) {
				Ext.each(arrVal,function(arrVal_Item) {
					if (Ext.util.Format.trim(arrVal_Item) == n.attributes.value) {
						out.push(n.attributes.text);
					}
				},this);
			});
					
			//console.debug("OUT",out);		
			//console.debug("TreePanel",TreePanel);
		}
		
		if (out.length!=0) {
			return out.join(Sepperator + ' ');	
		} else {
			return value;
		}
		
	}
	
});


/**
 * Plugin for adding a close context menu to tabs. Note that the menu respects
 * the closable configuration on the tab. As such, commands like remove others
 * and remove all will not remove items that are not closable.
 */
Ext.define('Ext.ux.TabCloseMenu', {
    alias: 'plugin.tabclosemenu',

    mixins: {
        observable: 'Ext.util.Observable'
    },

    /**
     * @cfg {String} closeTabText
     * The text for closing the current tab.
     */
    closeTabText: '关闭本页',

    /**
     * @cfg {Boolean} showCloseOthers
     * Indicates whether to show the 'Close Others' option.
     */
    showCloseOthers: true,

    /**
     * @cfg {String} closeOthersTabsText
     * The text for closing all tabs except the current one.
     */
    closeOthersTabsText: '关闭其他页',

    /**
     * @cfg {Boolean} showCloseAll
     * Indicates whether to show the 'Close All' option.
     */
    showCloseAll: true,

    /**
     * @cfg {String} closeAllTabsText
     * The text for closing all tabs.
     */
    closeAllTabsText: '关闭全部',

    /**
     * @cfg {Array} extraItemsHead
     * An array of additional context menu items to add to the front of the context menu.
     */
    extraItemsHead: null,

    /**
     * @cfg {Array} extraItemsTail
     * An array of additional context menu items to add to the end of the context menu.
     */
    extraItemsTail: null,

    //public
    constructor: function (config) {
        this.addEvents(
            'aftermenu',
            'beforemenu');

        this.mixins.observable.constructor.call(this, config);
    },

    init : function(tabpanel){
        this.tabPanel = tabpanel;
        this.tabBar = tabpanel.down("tabbar");

        this.mon(this.tabPanel, {
            scope: this,
            afterlayout: this.onAfterLayout,
            single: true
        });
    },

    onAfterLayout: function() {
        this.mon(this.tabBar.el, {
            scope: this,
            contextmenu: this.onContextMenu,
            delegate: '.x-tab'
        });
    },

    onBeforeDestroy : function(){
        Ext.destroy(this.menu);
        this.callParent(arguments);
    },

    // private
    onContextMenu : function(event, target){
        var me = this,
            menu = me.createMenu(),
            disableAll = true,
            disableOthers = true,
            tab = me.tabBar.getChildByElement(target),
            index = me.tabBar.items.indexOf(tab);

        me.item = me.tabPanel.getComponent(index);
        menu.child('*[text="' + me.closeTabText + '"]').setDisabled(!me.item.closable);

        if (me.showCloseAll || me.showCloseOthers) {
            me.tabPanel.items.each(function(item) {
                if (item.closable) {
                    disableAll = false;
                    if (item != me.item) {
                        disableOthers = false;
                        return false;
                    }
                }
                return true;
            });

            if (me.showCloseAll) {
                menu.child('*[text="' + me.closeAllTabsText + '"]').setDisabled(disableAll);
            }

            if (me.showCloseOthers) {
                menu.child('*[text="' + me.closeOthersTabsText + '"]').setDisabled(disableOthers);
            }
        }

        event.preventDefault();
        me.fireEvent('beforemenu', menu, me.item, me);

        menu.showAt(event.getXY());
    },

    createMenu : function() {
        var me = this;

        if (!me.menu) {
            var items = [{
                text: me.closeTabText,
                scope: me,
                handler: me.onClose
            }];

            if (me.showCloseAll || me.showCloseOthers) {
                items.push('-');
            }

            if (me.showCloseOthers) {
                items.push({
                    text: me.closeOthersTabsText,
                    scope: me,
                    handler: me.onCloseOthers
                });
            }

            if (me.showCloseAll) {
                items.push({
                    text: me.closeAllTabsText,
                    scope: me,
                    handler: me.onCloseAll
                });
            }

            if (me.extraItemsHead) {
                items = me.extraItemsHead.concat(items);
            }

            if (me.extraItemsTail) {
                items = items.concat(me.extraItemsTail);
            }

            me.menu = Ext.create('Ext.menu.Menu', {
                items: items,
                listeners: {
                    hide: me.onHideMenu,
                    scope: me
                }
            });
        }

        return me.menu;
    },

    onHideMenu: function () {
        var me = this;

        //me.item = null;
        me.fireEvent('aftermenu', me.menu, me);
    },

    onClose : function(){
        this.tabPanel.remove(this.item);
    },

    onCloseOthers : function(){
        this.doClose(true);
    },

    onCloseAll : function(){
        this.doClose(false);
    },

    doClose : function(excludeActive){
        var items = [];

        this.tabPanel.items.each(function(item){
            if(item.closable){
                if(!excludeActive || item != this.item){
                    items.push(item);
                }
            }
        }, this);

        Ext.each(items, function(item){
            this.tabPanel.remove(item);
        }, this);
    }
});


Ext.define('Ext.form.FileField', {
	extend :'Ext.form.field.Base', 
    getValue : function(){
        return Ext.form.FileField.superclass.getValue.call(this)|| "";
    },
	setReadOnly:function( readOnly ){
		this.callParent(arguments);
		var eo = this.bodyEl.el;
		var tr=eo.query("tr",true)[0];
		if(readOnly){
			tr.cells[0].style.display="none";
			tr.cells[1].style.display="none";
		}else{
			tr.cells[0].style.display="";
			tr.cells[1].style.display="";
		}
	},
	addFiles:function(fns){
		var me = this;
		if(me.entityName){
			var grid = me.up('bat_grid');
			grid.ui.ds.load();
			return;
		}
		me.file.value="";
		var val_old = this.getValue();
		var val_new;
		if(this.multiple){
			if(!val_old || val_old==""){
				val_new=fns;
			}else{
				val_new = val_old+";"+fns;
			}
	    	var eo = this.bodyEl.el;
	    	var gvw = eo.query('div',true)[2];
			this.addItems(fns,gvw);
			this.setVal(val_new);
		}else{
			val_new=fns;
			this.setValue(val_new);
		}
 		var req={map:{fld_name:me.name,obj:me.obj},"javaClass":"java.util.HashMap"};
   	 	jsonrpc.BH.handle(function(data,ex){
   			if(Bat.outputEx(ex,'上传文件异常'))
   				return;		
   	 	 },'A',req,"FileOperService");  		
	},
	//不刷新界面删除文件
	removeItem:function(fn){
		var fns = this.getValue();
		var fnn;
		//最后一个文件删掉了
		if(fn==fns){
			fnn = '';
		}else{
			var pos = fns.indexOf(fn);
			if(pos==0){
				fnn = fns.substring(fn.length+1);
			}else{
				fnn = fns.substring(0,pos)+fns.substring(pos+fn.length+1);
			}
		}
		//删掉后只留下一个文件了,不需要分隔符号
		if(fnn.charAt(fnn.length-1)==';'){
			fnn = fnn.substring(0,fnn.length-1);
		}
		this.setVal(fnn);
	},
	//不刷新界面设置值
	setVal:function(val){
        Ext.form.FileField.superclass.setValue.call(this, val);
        this.fireEvent("change", this, val);		
	},
	addItems:function(val,gvw){
		if(!val || val.trim()=="")
			return;
     	var fns = val.split(";");
     	var me = this;
     	var size=me.size||100;

	   	for(var i=0; i<fns.length; i++){
    		var fn = fns[i].trim();
    		if(fn=="")
    		  continue;
    		var suffix = fn.substring(fn.length-3).toLowerCase();
    		var div_c = Ext.DomHelper.append(gvw,{tag:"div"},true);
    		div_c.addCls("item enabled");
    		div_c.addClsOnOver('item-hover');
    		if(me.ctx_mnu){
	    		div_c.on('contextmenu',function(e,t,opt){
	    			var position = e.getXY();
	                e.stopEvent();
	                this.ctx_item = t;
	                this.ctx_mnu.showAt(position);
	    		},this);
    		}
    		var surl = encodeURI("/bat/servlet/fu?f="+fn);
    		var furl = "";
    		//替换圆括号
    		for(k=0;k<surl.length;k++){
    			var ch = surl.charAt(k);
    			if(ch=='('){
    				furl += "%28";
    			}else if(ch==')'){
    				furl+="%29";
    			}else furl+=ch;
    		}
    		var cfg_a = {tag:"div",title:fn,furl:furl};
    		cfg_a.addCls("thumb-large global-icon-62 global-icon-62-"+suffix);
    		if(suffix=="png"||suffix=="jpg"){
    			cfg_a.style="background: url("+furl+"&size="+size+"&mr="
    			+Math.random()
    			+") 50% 50% no-repeat;";
    			cfg_a.suffix = suffix;
    		}
			if(me.size){
				cfg_a.style+="width:"+size+"px;height:"+size+"px;";
			}
    		var div_a = Ext.DomHelper.append(div_c,cfg_a,true);
    		div_a.dom.furl = furl;
    		div_a.on('click',function(e,t,opt){
    			BView.openTabpanel('tab_pic',t.title,t.furl+"&nh=true");
    		},this);
    		var div_fn =  Ext.DomHelper.append(div_c,{tag:"div"});
    		div_fn.addCls("file-name");
    		var div_fna =  Ext.DomHelper.append(div_fn,{tag:"a",target:"_blank",href:furl,title:fn});
    		var pos1 = this.getPrefPos(fn);
    		if(pos1!=-1){
    			fn = fn.substring(pos1+1);
    		}
    		div_fna.innerHTML =  fn;

    		var div_fn_edit =   Ext.DomHelper.append(div_fn,{tag:"div", style:"display: none;margin:0"});
    		div_fn_edit.addCls("edit-name");
            var ipt_fn = Ext.DomHelper.append(div_fn_edit,{tag:"input", type:"text",value:""});
            ipt_fn.addCls("box");
            var span_sure = Ext.DomHelper.append(div_fn_edit,{tag:"span"},true);
            span_sure.addCls("sure");
            span_sure.on('click',function(e,t,opt){
            	var el_span_cancel = Ext.get(t);
            	var item_name = el_span_cancel.up('div.edit-name');
            	var item_ipt = item_name.down('input');
            	item_name.setStyle('display','none');
            	var item_fn = item_name.up('div.file-name');
            	var item_a = item_fn.down('a');
            	var ipt_val = item_ipt.getValue();
            	item_a.setHTML(ipt_val);
            	item_a.setStyle('display','block');
            	var fno = item_a.getAttribute('title');
            	var pos1 = this.getPrefPos(fno);
            	if(pos1!=-1){
            		fn = fno.substring(0,pos1+1)+ipt_val;
            	}else{
            		fn = ipt_val;
            	}
				var furl = encodeURI("/bat/servlet/fu?f="+fn);
				item_a.set({title:fn,href:furl});

				var item = item_fn.up('div.item');
				var item_icon = item.down('div.thumb-large');
				item_icon.set({title:fn});
				item_icon.dom.furl = furl;
				this.setVal(this.getValue().replace(fno,fn));
				 var req={map:{fno:fno,fnn:fn,fld_name:me.name,obj:me.obj},"javaClass":"java.util.HashMap"};
		   	 	jsonrpc.BH.handle(function(data,ex){
		   			if(Bat.outputEx(ex,'上传文件异常'))
		   				return;		
		   	 	 },'N',req,"FileOperService");  		

            },this);
            var span_cancel = Ext.DomHelper.append(div_fn_edit,{tag:"span"},true);
            span_cancel.addCls("cancel");
            span_cancel.on('click',function(e,t,opt){
            	var el_span_cancel = Ext.get(t);
            	var item_name = el_span_cancel.up('div.edit-name');
            	item_name.setStyle('display','none');
            	var item_fn = item_name.up('div.file-name');
            	var item_a = item_fn.down('a');
            	item_a.setStyle('display','block');

            },this);
    	}
	},
	getPrefPos:function(fn){
		var pos = fn.indexOf('_');
		for(var i=0; i<5; i++){
			if(pos==-1)
				break;
			pos = fn.indexOf('_',pos+1);
		}
		return pos;
	},
    setValue : function(val){
    	if(!this.bodyEl)
    		return;
    	var eo = this.bodyEl.el;
    	var gvw = eo.query('div',true)[2];
    	while (gvw.hasChildNodes()) {
   		 gvw.removeChild(gvw.lastChild);
		}
		
    	this.addItems(val,gvw);
        Ext.form.FileField.superclass.setValue.call(this, val);
        this.fireEvent("change", this, val);
    },
    // private
    initComponent: function(){
        Ext.form.FileField.superclass.initComponent.call(this);
        
        this.addEvents(
            'uploaded',
            'fileselected'
        );
    },
    // private
    onRender : function(ct, position){
        var me = this,
            id = me.id,
            bodyEl;
        me.callParent(arguments);

        var eo = me.bodyEl.el;
        var servletUrl='fu';
        if(this.URL){
        	servletUrl=this.URL;
        }  
        var inputEl = me.bodyEl;
        var h = this.height|| 250;
        eo.dom.innerHTML="<div><table><tr><td ><form action='"
        +Bat.WC+"servlet/"+servletUrl+"' method='POST' enctype='multipart/form-data' target='bat_iframehide'>" +
       "</form></td><td style='vertical-align:top;'></td><td></td></tr>"
       +"</table><div node-type=\"module\" class=\"module-grid-view\" style=\"display: block;\"><div node-type=\"list\" class=\"wrapper global-clearfix\" style=\"height: "
       +h+"px; overflow: auto;\">  </div></div>";   
    	var dh = Ext.DomHelper;
        var tr=eo.query("tr",true)[0];
        var fm=eo.query("form",true)[0];
		fm.encoding="multipart/form-data";
       if(this.readOnly)
		 return;

	var mnu_items =  [
        { text: '重命名', handler: function() {
        	//找到item
        	var ctx_el=Ext.get(this.ctx_item);
        	var item;
        	if(ctx_el.hasCls('item')){
        		item = ctx_el;
        	}else{
        		item = ctx_el.up('div.item',3);
        	}
        	var item_fn = item.down('div.file-name');
        	var item_a = item_fn.down('a');
        	item_a.setStyle('display','none');
        	var item_name = item_fn.down('div.edit-name');
        	item_name.setStyle('display','block');
        	var item_name_ipt = item_name.down('input');
        	item_name_ipt.set({value:item_a.getHTML()});
        	item_name_ipt.focus();
        	
        },scope:this},
        { text: '删除', handler: function() {
        	//找到item
        	var ctx_el=Ext.get(this.ctx_item);
        	var item;
        	if(ctx_el.hasCls('item')){
        		item = ctx_el;
        	}else{
        		item = ctx_el.up('div.item',3);
        	}
        	var item_fn = item.down('div.file-name');
        	var item_a = item_fn.down('a');
        	var me = this;
         	Ext.MessageBox.confirm('确认删除','您要删除文件【'+item_a.getHTML()+'】',function(btn){
        		if(btn!='yes')
        			return;
        		var fn_remove = item_a.getAttribute('title');
	   			item.remove();
	   			me.removeItem(fn_remove);
		 		var req={map:{fns:fn_remove,fld_name:me.name,obj:me.obj},"javaClass":"java.util.HashMap"};
		   	 	jsonrpc.BH.handle(function(data,ex){
		   			if(Bat.outputEx(ex,'删除文件异常'))
		   				return;		
		   	 	 },'R',req,"FileOperService");  		
              });
        },scope:this
        }];
     if(this.multiple){
     	mnu_items= mnu_items.concat([{ text: '删除全部', handler: function() {
        	//找到item
        	var ctx_el=Ext.get(this.ctx_item);
        	var item;
        	if(ctx_el.hasCls('item')){
        		item = ctx_el;
        	}else{
        		item = ctx_el.up('div.item',3);
        	}
        	var item_list = item.up('div');
        	var me = this;
        	Ext.MessageBox.confirm('确认删除','您要删除全部【'
        			+item_list.dom.childNodes.length
        			+'】个文件',function(btn){
        		if(btn!='yes')
        			return;
	   			var gvw = item_list.dom;
		    	while (gvw.hasChildNodes()) {
		   		 gvw.removeChild(gvw.lastChild);
				}
				var fval = me.getValue();
	   			me.setVal('');
		 		var req={map:{fns:fval,fld_name:me.name,obj:me.obj},"javaClass":"java.util.HashMap"};
		   	 	jsonrpc.BH.handle(function(data,ex){
		   			if(Bat.outputEx(ex,'删除全部文件异常'))
		   				return;	
		   	 	 },'R',req,"FileOperService");  		
        	});
        },scope:this
        },{
        	text:'设为代表影像',
        	handler:function(){
        	var ctx_el=Ext.get(this.ctx_item);
        	var item;
        	if(ctx_el.hasCls('item')){
        		item = ctx_el;
        	}else{
        		item = ctx_el.up('div.item',3);
        	}
        	var item_fn = item.down('div.file-name');
        	var item_a = item_fn.down('a');
        	var me = this;
        	var item_list = item.up('div');
        	var item0=item_list.down('div.item');
        	if(item==item0){
        		return;
        	}
   			item.insertBefore(item0);
    		var fn_default = item_a.getAttribute('title');
   			var fns = me.getValue();
   			var pos = fns.indexOf(fn_default);
   			var fnn = fn_default+";"+fns.substring(0,pos-1)+fns.substring(pos+fn_default.length);
   			me.setVal(fnn);
	 		var req={map:{fns:fn_default,fld_name:me.name,obj:me.obj},"javaClass":"java.util.HashMap"};
	   	 	jsonrpc.BH.handle(function(data,ex){
	   			if(Bat.outputEx(ex,'设置代表影像文件异常'))
	   				return;		
	   	 	 },'D',req,"FileOperService");  		        		
        	},
        	scope:this
        }]);
    	this.ctx_mnu = new Ext.menu.Menu({items: mnu_items});
     }
       // Ext.form.FileField.superclass.onRender.call(this, ct, position);
        
		var cfg = {tag: "input",type: "file",name:"file", cls:"x-btn-button" };
		if(this.multiple)
			cfg.multiple="multiple";
		if(this.accept)
			cfg.accept = this.accept;
        var file=dh.append(fm,cfg);
        this.file = file;
        Ext.get(file).on('change', function(){
            var v = this.file.value;
           // this.setValue(v);
            this.fireEvent('fileselected', this, v);
        }, this);

	    var btntd=tr.cells[1];
        var elink=tr.cells[2];
        me.elink = elink;
        
       var gvw = eo.query('div',true)[1];
       // var me=this;
        var todayBtn = new Ext.Button({
        	renderTo :btntd, 
            text: "上传",
            //tooltip: "upload a file",
            handler:function(){
            	var total=240;
            	var cur=0;
            	var remoteTotal;
				var fpath=me.file.value.trim();
				if(!fpath || fpath==""){
					 return Ext.MessageBox.show({
			           title: '操作提示',
			           msg: '请首先选择上传文件',
			           buttons: Ext.MessageBox.OK,
			           icon: Ext.MessageBox.INFO
			       });
				}else{
					var pos=fpath.lastIndexOf("\\");
					me.fname=fpath.substr(pos+1);
					me.fn=me.fname;
				}

		 		var req={map:{},"javaClass":"java.util.HashMap"};
		   	 	jsonrpc.BH.handle(function(data,ex){
		   			if(Bat.outputEx(ex,'获取任务号异常'))
		   				return;		   	 		
		   	 		var tid = data;
		   	 		//c4wtodo 121005 用户上传目录分离
		   	 		var pos=tid.indexOf('_',1);
		   	 		var userid = tid.substring(0,pos);
        			me.fn="_["+userid+"]"+tid+"_"+me.fname;
        			//me.setValue(me.fn);
    				var oa=fm.action;
    				fm.action+="?tid="+tid;
					if(me.entityName){
						fm.action+="&entityName="+me.entityName;
						var grid=me.up('bat_grid');
						var obj0 = grid.ui.objs[0];
						fm.action+="&entityId="+ obj0.items.map['entityId'];
						fm.action+="&entityFile="+ me.entityFile;
						console.log(fm.action);
					}
    				var frm_upload = document.getElementById("bat_iframehide");
					frm_upload._fld_upload = me;
    				Ext.MessageBox.show({ msg:'正在上传，请稍候...',title:'系统提示',
    					modal:true,wait:true,closable:false});
    				fm.submit();
    				fm.action=oa;					
		   	 	 },'N',req,"TaskHandle");  		
        		//Bat.showProgress(null,true);	
            },
            scope: me
        });
    }
        
});

Ext.define('Ext.form.TA', {
   extend: 'Ext.form.TextArea',
   config: {
	enableKeyEvents:true,
  	listeners:{
	'blur':function(fd,e){
		var me=this;
		me.hideMenu();
		me.bStep=false;
		if(me.timer){
			clearTimeout(me.timer);
		}
	},
	'focus':function(fd,e){
		var me = this;
		me.bStep = true;
		me.stepCheck();
	},
  	 'specialkey': function(field, e){
  	 	this.onKey(e);
     }
	 }
	},
	stepCheck:function(){
		var me=this;
		var ta = me.inputEl.dom;  
		var pos = ta.selectionStart;
		if(pos!=me.pos_start){
			me.showMenu(true);
			me.pos_start=pos;
		}
		if(me.bStep){
			//killTimer(me.timer);
			me.timer = setTimeout(function(){
				me.stepCheck();
			},200);
		}
	},
	filterBy:function(str){
		var me = this;
		this.mnu.getStore().filterBy(function(rd){
			if(me.func_filterBy){
				return me.func_filterBy(rd,str);
			}else return rd.get('pyh').indexOf(str)==0;
		},this);
	},
	showMenu:function(bfocus){
		var ta = this;
		var pos = ta.inputEl.dom.selectionStart;
		var txt = ta.getRawValue( ).substring(0,pos);
		var p0 = ta.getXY();
		console.log(txt);
		
		var bHide=false;
		var key;
		var pos_at = txt.lastIndexOf('@');
		if(pos_at==-1){
			bHide=true;
		}else{
			key = txt.substring(pos_at+1);
			//不允许中间有空格
			if(key.indexOf(' ')!=-1)
				bHide=true;
		}
		if(bHide){
			ta.hideMenu();
			return;
		}
		var x = p0[0]+ta.getDivWidth(txt);
		var y = p0[1]+ta.getDivHeight(txt);
		//contactsDropDown.bindElement(ta.el);
		var mnu = this.mnu;
       	mnu.showAt([x,y]);
       	this.filterBy(key);
       	mnu.getSelectionModel().select(0,true,true);
       	if(bfocus)
       		this.focus();		
	},
	hideMenu:function(){
		this.mnu.hide();
	},
    constructor: function (config) {
    	config.width =config.width||400;
    	config.height =config.height||200;
        this.callParent(arguments); 
    },
	getDivHeight:function(text){
		this.heightDiv.setHTML(text.replace(/\n/g, '<br/>'));
		return this.heightDiv.getHeight() + 10;
	},
	getDivWidth:function(text){
       var h = this.getDivHeight('x');
       var olen = this.getDivHeight(text);
        if (h != olen) {
            for (var i = 0; i < text.length; ++i) {
                var s = text.substr(0, text.length - i);
                var len = this.getDivHeight(s);
                if (len != olen) {
                    text = text.substr(text.length - i);
                    break;
                }
            }
        }
        this.widthDiv.setHTML(text);
        return this.widthDiv.getWidth();
	},
	buildMenu:function(){
		var me=this;
		var hql="select id,name,namePyh from UUser";
		me.mnu=Ext.create('Ext.grid.Panel', {
			floating:true,
			//focusOnToFront:false,
			selModel: 'SINGLE',
		    store: me.ds,
		    hideHeaders :true,
		    columns: me.columns,
		    height: me.menuHeight||200,
		    width: me.menuWidth||400
		});	
		me.mnu.on("select",function(gd, record, item, index, e, eOpts ){
			me.onSelOk();
		});
	},
	insertText:function(val){  
		var value=val=='\r\n'?val:val+' ';
        var ta = this.inputEl.dom;  
        var oriValue = this.getValue();  
        var txt1=oriValue.substring(0,ta.selectionStart)
        var pos_start = txt1.lastIndexOf('@');
		this.setValue(oriValue.substring(0,pos_start+1) + value + oriValue.substring(ta.selectionEnd));  
        //ta.setValue(oriValue.toString() + value );  
        ta.selectionStart = pos_start+value.length+1;  
        ta.selectionEnd = ta.selectionStart;  
        //this.focus();
	}, 
	onSelOk:function(){
  	 	var sel = this.mnu.getSelectionModel();
  	 	var si = sel.getSelection();
  	 	if(!si || si.length==0)
  	 		return;
  	 	this.insertText(this.func_insert(si[0].data)||si[0].data['name']);
  	 	this.hideMenu();
	},
	
  	onKey: function(e){
  	 	if(!this.mnu.isVisible()){
  	 		if(e.getKey()==e.ENTER){
  	 			this.insertText('\r\n');
  	 		}
  	 		return;
  	 	}
        // e.HOME, e.END, e.PAGE_UP, e.PAGE_DOWN,
        // e.TAB, e.ESC, arrow keys: e.LEFT, e.RIGHT, e.UP, e.DOWN
  	 	var sel = this.mnu.getSelectionModel();
  	 	var si = sel.getSelection();
  	 	var pos = 0;
  	 	if(si && si.length>0){
  	 		pos = si[0].index;
  	 	}
 		var mnu = this.mnu;
 		var ds = mnu.getStore();
 		var selectedRecord = sel.getLastSelected();
		var recordIndex = ds.indexOf(selectedRecord);
		var nextRecord ;

 	 	switch(e.getKey()){
        case e.DOWN:
  	 		e.stopEvent();
			nextRecord = recordIndex + 1;
			if(nextRecord>=ds.getCount())
				nextRecord=0;
			sel.select(ds.getAt(nextRecord),true,true); 	 		
			break;
         case e.UP:
         	e.stopEvent();
			nextRecord = recordIndex - 1;
			if(nextRecord<0)
				nextRecord=ds.getCount()-1;
			sel.select(ds.getAt(nextRecord),true,true); 	 		
	       	break;
	     case e.ESC:
  	 		e.stopEvent();
	     	this.hideMenu();
	       	break;
	     case e.ENTER:
  	 		e.stopEvent();
	     	this.onSelOk();
	       	break;
       }
	   //this.focus();
    },
	
	
  buildKey:function(){
    this.keyNav = new Ext.KeyNav(this.inputEl, {
        "enter": function(e) {
        	console.log('enter');
        	this.onKey(e);
        },
        "esc": function(e) {
        	console.log('esc');
        	this.onKey(e);
        },
        "tab": function(e) {
        	console.log('tab');
        	this.onKey(e);
        },
        scope: this
    });
  },
    onRender: function () {
        this.callParent();
		this.heightDiv = Ext.DomHelper.append(document.body,{tag:'div',id:'height_div',style:'visibility:hidden;position:absolute;z-index:10000;top:0px;left:3000px;word-break:break-all;word-wrap:break-word'},true);
		this.heightDiv.setWidth(this.getWidth());
		this.heightDiv.setStyle('font-size',this.inputEl.getStyle('font-size'));
		this.widthDiv = Ext.DomHelper.append(document.body,{tag:'div',id:'width_div',style:'visibility:hidden;position:absolute;z-index:10000;top:0px;left:3000px;'},true);
		this.widthDiv.setHeight(this.getHeight());
		this.widthDiv.setStyle('font-size',this.inputEl.getStyle('font-size'));  	
		
		this.buildMenu();
		this.buildKey();
   }
});
