/**
 * @Description	Html编辑器 插入图片控件
 * @author
 */
/**
 * ie bug fix 兼容ff、chrome 
 * @author fucj
 */
Ext.override(Ext.form.HtmlEditor, {
    onEditorEvent: function() {
        if (Ext.isIE) {
            this.currentRange = this.getDoc().selection.createRange();
        }
        this.updateToolbar();
    },
    insertAtCursor: function(text) {
        if (Ext.isIE) {
            this.win.focus();
            var r = this.currentRange || this.doc.selection.createRange();
            if (r) {
                r.collapse(true);
                r.pasteHTML(text);
                this.syncValue();
                this.deferFocus();
            }
 
        } else if (Ext.isGecko || Ext.isOpera || Ext.isChrome) {
            this.win.focus();
            this.execCmd('InsertHTML', text);
            this.deferFocus();
        } else if (Ext.isSafari ) {
            this.execCmd('InsertText', text);
            this.deferFocus();
        }
    }
});

var alertFallback = false;
if (typeof console === "undefined" || typeof console.log === "undefined") {
	 console = {};
	 if (alertFallback) {
	     console.log = function(msg) {
	          alert(msg);
	     };
	 } else {
	     console.log = function() {};
	 }
}

//04.01.10     ie8 不支持 String trim 
if(typeof String.prototype.trim!=='function'){
	String.prototype.trim=function(){
		return this.replace(/^\s+|\s+$/g, '');
	};
}
// 语法甘露：c4w 2013-10-24 
var object = // 定义小写的object基本类，用于实现最基础的方法等
{
	isA : function(aType) // 一个判断类与类之间以及对象与类之间关系的基础方法
	{
		var self = this;
		while (self) {
			if (self == aType)
				return true;
			self = self.Type;
		};
		return false;
	}
};

function Class(aBaseClass, aClassDefine) // 创建类的函数，用于声明类及继承关系
{
	function class_() // 创建类的临时函数壳
	{
		this.Type = aBaseClass; // 我们给每一个类约定一个Type属性，引用其继承的类
		for (var member in aClassDefine)
			this[member] = aClassDefine[member]; // 复制类的全部定义到当前创建的类
	};
	class_.prototype = aBaseClass;
	return new class_();
};

function New(aClass, aParams) // 创建对象的函数，用于任意类的对象创建
{
	function new_() // 创建对象的临时函数壳
	{
		this.Type = aClass; // 我们也给每一个对象约定一个Type属性，据此可以访问到对象所属的类
		if (aClass.Create)
			aClass.Create.apply(this, aParams); // 我们约定所有类的构造函数都叫Create，这和DELPHI比较相似
	};
	new_.prototype = aClass;
	return new new_();
};
//===============================================================2014.01.14fucj 数组删除指定元素
Array.prototype.indexOf = function(val) {
  for (var i = 0; i < this.length; i++) {
      if (this[i] == val) return i;
  }
  return -1;
};
Array.prototype.remove = function(val) {
  var index = this.indexOf(val);
  if (index > -1) {
      this.splice(index, 1);
  }
};
Bat = {
	version : '11.01.24',
	cloned : [],
	ORDBYDIS : '排序',
	GRPBYDIS : 'Group by',
	msgCt : null,
	winProgress : null,
	winGroup : null,
	comet : null,
	dlgs : {},
	child_win : {}
};
Bat.info={
	usr_name:"user name",
	usr_pwd:"password"
};
Bat.err={
	
};
Bat.openwin = function(url, win_name, args) {
	var win = Bat.child_win[win_name];
	if (win && !win.closed) {
		try {
			win.focus();
			return;
		} catch (e) {
		}
	}
	Bat.child_win[win_name] = window.open(url, win_name, args);
};
Bat.close_win_all = function() {
	for (x in Bat.child_win) {
		try {
			Bat.child_win[x].close();
		} catch (e) {
		}
	}
}
Bat.getContext = function() {
	var pos = 0;
	var head = "";
	var durl = decodeURI(document.URL);
	var p = [];
	for (var i = 0; i < 4; i++) {
		pos = durl.indexOf('/', pos);
		p[i] = pos;
		pos++;
	}
	// web path
	Bat.WP = durl.substr(0, pos);
	// web context
	Bat.WC = durl.substring(p[2], p[3] + 1);
	var pp = durl.indexOf("://");
	if (pp > 1)
		pp = durl.indexOf(":", pp + 1);
	else
		pp = durl.indexOf(":", 0);
	var p2 = Bat.WP.lastIndexOf('/', Bat.WP.length - 2);
	Bat.HOSTPORT = Bat.WP.substring(0, p2);
	Bat.Host = durl.substring(0, pp);
	Bat.dirListURL = Bat.WC + "servlet/dirlist?";
}
Bat.getContext();

Bat.init = function(layout) {
	Bat.winGroup = new Ext.WindowGroup();
	Bat.layout = layout;
	if(!Ext.isIE)
		layout.getws(getWSURL(),-1,['PR_SYS_MSG','PR_SYS_HEART']); 
}

Ext.define('Bat.Grid', {
	extend : 'Ext.grid.Panel',
    alias : 'bat_grid',
    xtype: 'bat_grid',

    constructor:function(config){
       this.ui=config.ui;
       this.ui.grid=this;
       Bat.Grid.superclass.constructor.call(this,config);
	},
	initComponent:function() {
        var config = this.ui.getConfigCrid();
	  if(config.selType =='cellmodel' ) {   
			this.cellEditing = new Ext.grid.plugin.CellEditing({
	            clicksToEdit: 1
	        });  
	        var plugins = config.plugins||[];
	        plugins[plugins.length]=this.cellEditing;
		 	config.plugins=plugins;
        }else{
        	var lsn = config.listeners||{};
        	//双击处理
        	if(!lsn.itemdblclick){
	        	lsn.itemdblclick=function(grid, record, item, index, e, eOpts){
					var robj = record.obj;
					this.ui.renderDetailPanel(robj);
	        	}
        	}
        	 config.listeners=lsn;
        }
        // apply config
        Ext.apply(this, Ext.apply(this.initialConfig, config));
        // call parent
        Bat.Grid.superclass.initComponent.apply(this, arguments);
        //c4w 2013-10-10
        if(config.func_init){
          config.func_init(this);
        }
    } // eo function initComponent
    ,onRender:function() { 
        // call parent
        Bat.Grid.superclass.onRender.apply(this, arguments);
        //don't need load the store
       //  this.ui.ds.load();
 
    } // eo function onRender
 
});
//Ext.reg('bat_grid', Bat.Grid);
 
	

Bat.showProgress = function(p, bshow) {
	var wp = Bat.winProgress;

	if (!wp) {
		var its = [];
		for (var i = 0; i < 40; i++)
			its[its.length] = {
				tag : "div",
				id : "id" + i,
				html : i + "1111"
			};
		wp = new Ext.Window({
			id : "win_progress",
			title : "0个任务进行中...",
			width : 500,
			height : 300,
			bodyStyle : 'overflow-y:auto;overflow-x:hidden;',
			closeAction : 'hide',
			group : Bat.winGroup,
			html : "",
			buttons : [{
				text : '退出',
				handler : function() {
					wp.hide();
				}
			}]
		});
		wp.taskCount = 0;
		Bat.winProgress = wp;
	}
	if (bshow)
		wp.show();
	if (!p)
		return;
	var pbar;
	var pi = p.items.map;
	if (p.action == "A") {
		pbar = wp.add(new Ext.ProgressBar({
			text : 'Ready',
			id : pi.id,
			cls : 'left-align',
			width : 460
		}));
		pbar.total = pi.total;
		wp.taskCount++;
		wp.setTitle(wp.taskCount + "个任务进行中...");
		wp.doLayout();
	} else if (p.action == "U") {
		pbar = wp.findById(pi.id);
		if (pbar)
			pbar.updateProgress(pi.cur / pbar.total, pi.id
					+ "&nbsp;&nbsp;&nbsp;&nbsp;" + pi.cur + "/" + pbar.total);
	} else if (p.action == "R") {
		// TODO remove wp.
		pbar = wp.findById(pi.id);
		if (pbar) {
			wp.remove(pbar);
			wp.taskCount--;
			wp.setTitle(wp.taskCount + "个任务进行中...");
			wp.doLayout();
			if (wp.taskCount == 0)
				wp.hide();
		}
	}
}
Bat.outputEx = function(ex, title) {
	if (!ex)
		return false;
	var tstr = "[" + new Date().toLocaleString() + "]\n<br>";
	Ext.MessageBox.show({
		minWidth : 300,
		title : title || '服务异常',
		msg : tstr + ex.msg,
		buttons : Ext.MessageBox.OK,
		icon : Ext.MessageBox.ERROR
	});
	return true;
}
Bat.createBox = function(t, s) {
	return '<div class="msg"><h3>' + t + '</h3><p>' + s + '</p></div>';
}

Bat.msg = function(title, s) {
	if (!Bat.msgCt) {
		Bat.msgCt =  msgCt = Ext.DomHelper.insertFirst(document.body, {id:'msg-div'}, true);
	}
	var m = Ext.DomHelper.append(Bat.msgCt, {
		html : Bat.createBox(title||'系统通知', s)
	}, true);
	Bat.msgCt.alignTo(document, 'br-br');
	m.slideIn('b');
	m.on("click", function(e, o) {
		m.ghost("r", { remove: true});
	}, this);
	var clds = Bat.msgCt.dom.childNodes;
	if (clds.length > 3) {
		var fcld = Ext.get(clds[0]);
		fcld.ghost('b', {remove : true});
	}
		// .pause(10).ghost("t", {remove:true});
}
Bat.msgfmt = function(pm){
	var dt = new Date();
	dt.setTime(pm.time);
	Bat.msg(Ext.util.Format.htmlEncode(pm.title),
		"<div><img src=\"icons/user.png\"> <b>"+pm.uname
		+"</b> "+ dt.toLocaleString()+"说：</div><br/>"
		+Ext.util.Format.nl2br(Ext.util.Format.htmlEncode(pm.content))
	);
}
Bat.onItemClick = function(item) {
	Bat.showProgress();
		// Bat.msg('Menu Click', 'You clicked the "{0}" menu item.', item.text);

	
}

Bat.jsDateByDbType=function(fval,dbType){
	if(fval==null)
		return null;
	if(dbType=="oracle"){
	  return "to_date('" + fval.getFullYear() + "-" + (fval.getMonth() + 1) + "-"
			 + fval.getDate() + " " + fval.getHours() + ":" + fval.getMinutes()
			 + ":" + fval.getSeconds() + "','yyyy-mm-dd hh24:mi:ss')";
	}else{
	  return "DATE('" + fval.getFullYear() + "-" + (fval.getMonth() + 1) + "-"
			+ fval.getDate() + "')";
	}
}
Bat.apply = function(o, c, defaults) {

	if (defaults) {
		Bat.apply(o, defaults);
	}
	if (o && c && typeof c == 'object') {
		for (var p in c) {
			o[p] = c[p];
		}
	}
	return o;
};
Bat.resetIts=function(o){
	o.its=o.items;
}
//比较变化,只提交变化的属性和主键属性
Bat.getIts=function(o,ui){
	if(o.action == 'D' || o.action == 'O'){
		return null;
	}
	var items = o.items.map;
	var its = o.its;
	var items_save = {};
	var bsame;
	if(o.action=='R' || o.action=='A'){
		bsame = false;
	}else{
		bsame = true;
	}
	 
	//缓存ui的keymap
	var keymap=ui.keymap;
	if(!keymap){
		keymap={};
		var cns = o.cns;
		if(cns.length==1){
			var ov;
			if(Bat.gvs && Bat.gvs.ovs)
				ov=Bat.gvs.ovs[cns[0]];
			var key = ov?ov.key:'id';
			keymap[key]=true;
		}else{
			for(var i=0;i<cns.length; i++){
				var ov;
				if(Bat.gvs && Bat.gvs.ovs)
					ov=Bat.gvs.ovs[cns[0]];
				var key = ov?ov.key:'id';
				var pre=String.fromCharCode(65+i)+'.';
				keymap[pre+key]=true;
			}
		}
		ui.keymap=keymap;
	}
	for(var x in items){
		//if(x=="id" || x.substring(1)==".id" ){
		if(keymap[x]||x=="dtModify"){
			items_save[x]=items[x];
		}else if(o.action=='A' || !Bat.equalObj(items[x],its[x])){
			items_save[x]=items[x];
			//至少有一项被修改
			if(bsame){
				bsame=false;
			}
		}
	}
	if(bsame){
		return null;
	}else{
		return {action:o.action,cns:o.cns,javaClass:"net.bat.pt.BObj",items:{javaClass:"java.util.HashMap",map:items_save}};
	}
}


//建立初始克隆
Bat.setIts=function(o){
	//2013年12月20日,通过跟踪obj.sid,定位出了bug,grid如果ds.load之后不clearSelection,getSelection,返回的是load之前的数据。
	//o.sid = Ext.id();
	if(o.its){
		return;
	}
	o.its = Bat.cloneObj(o.items.map);
}

Bat.equalObj = function(o1,o2){
	if( typeof o1 != 'object'){
		return o1==o2;
	}else{
		if( typeof o2 != 'object'){
			return false;
		}
		//js date
		if(!o1){
			if(!o2)
				return true;
			else
				return false;
		}else{
			if(o1.getTime){
				if(!o2)
					return false;
				else
					return o2.time==o1.getTime();
			}
		}
		for(var x in o1){
			if(!Bat.equalObj(o1[x],o2[x])){
				return false;
			}
		}
		for(var x in o2){
			if(!Bat.equalObj(o2[x],o1[x])){
				return false;
			}
		}
		return true;
	}
}
// 能够处理双向引用的深度clone方法
Bat.cloneObj = function(o,func) {
	if (!o || 'object' !== typeof o) {
		return o;
	}
	var c = 'function' == typeof o.pop ? [] : {};

	for (var i = 0; i < Bat.cloned.length; i++) {
		if (Bat.cloned[i] == o) {
			return Bat.cloned[i];
		}
	}
	Bat.cloned[Bat.cloned.length] = o;

	var p, v;
	for (p in o) {
		if (o.hasOwnProperty(p)) {
			v = o[p];
			if (p == 'store') {
				c[p] = v;
				continue;
			}

			if (v && 'object' === typeof v) {
				c[p] = Bat.cloneObj(v);
				if(func){
					c[p]=func(p,c[p]);
				}
			} else {
				c[p] = v;
			}
		}
	}
	return c;
}

Bat.clone = function(o,func) {
	return Ext.clone(o);
	Bat.cloned = [];
	return Bat.cloneObj(o,func);
}

var BatUI = Class(object, {
	Create : function(view, tabPanel, gridTitle) {
		this.filter_map = {};
		this.tabDetails={};
		this.reload = true;
		this.evt = new Ext.util.Observable();
		this.view = view;
		//c4w 2.13
		if(view.func_setdefault)
		  this.func_setdefault = view.func_setdefault;
		this.tabPanel = tabPanel;
		this.gridTitle = gridTitle;
		var h = this.view.handles || {};
		// 保存视图定义的原生hql,因为检索过程中会被覆盖
		this.setHQL(view.req.map.hql);
		for (x in h)
			this.evt.on(x, h[x]);
	},
	setHQL:function(hql){
		var phql = {hql:hql};
		var lhql = hql.toLowerCase();
		var pos_where = lhql.indexOf('where') ;
		var pos_orderby = lhql.indexOf('order by');
		
		if(pos_orderby==-1){
			pos_orderby=lhql.length+1;
		}
		else{
			phql.orderby=hql.substring(pos_orderby+8);
		}
		if(pos_where==-1)
			pos_where=pos_orderby-1;	
		else
			phql.where=hql.substring(pos_where+5,pos_orderby-1);
		phql.from = hql.substring(0,pos_where);		
		this.phql = phql;
		//console.log(this.phql);
	},
	getwhere_desc:function(){
		if(!this.co_oper)
			return false;		
		var oper = this.co_oper.getValue();
		if(oper=='')
			return false;
		var cofld = this.cofld;
		//c4w10.26
		if(!cofld.getValue()){
			return '全部';
		}
		var fval;
		var lf = Ext.ComponentMgr.get(cofld.ipt_id);// wjh
		if (lf.items.length > 1) {
			var fld0 = lf.items.items[0];
			var fld1 = lf.items.items[1];
			var fval1;
			if (fld0.edr == "DateField") {
				fval0 = Bat.jsDateByDbType(fld0.getValue(),CFG.dbType);
				fval1 = Bat.jsDateByDbType(fld1.getValue(),CFG.dbType);
			} else {
				fval0 = fld0.getValue();
				fval1 = fld1.getValue();
			}
			fval = fval0 + " and " + fval1;
		} else {
			var fld = lf.items.items[0];
			if (fld.edr == "DateField")
				fval = Bat.jsDateByDbType(fld.getValue(),CFG.dbType);
			else {
				fval = fld.getValue();
				if (fld.edr == 'Checkbox')
					fval = fval ? 'Y' : 'N';
				if (oper == 'like'){
					if(fval.indexOf('*')!=-1){
						var sval="";
						for(var p=0; p<fval.length; p++){
							var ch=fval.charAt(p);
							if(ch=='*')
								sval+='%';
							else
								sval+=ch;
						}
						fval=sval;
					}						
					else
						fval = '%' + fval + '%';
				}
				if (oper == 'in')
					fval = '(' + fval + ')';
				else if (typeof(fval) == "string") {
					if (fval.indexOf("'") != 0)
						fval = fval.replace(/'/g, "''");
					fval = "'" + fval + "'";alert(fval);
				}
			}
		}
		if (oper == "is null" || oper == "is not null")
			fval = '';
		return '['+cofld.getRawValue() + ']' + this.co_oper.getRawValue() + ' ' + fval;
	},
	//logic default:and  or clearAll
	getwhere:function(lo){
		if(!this.co_oper)
			return false;		
		var oper = this.co_oper.getValue();
		//c4w10.26
		var cofld = this.cofld;
		if(oper==''|| !cofld.getValue())
			return false;
		var fval;
		var lf = Ext.ComponentMgr.get(cofld.ipt_id);// wjh
		var hql;
		var jd_val;
		var desc;
		var fn = cofld.getValue() ;
		//复合属性处理,检索主主属性
		var pos = fn.lastIndexOf('.variable.');
		if(pos!=-1)
			fn = fn.substring(0,pos+9);
		if (lf.items.length > 1) {
			var fld0 = lf.items.items[0];
			var fld1 = lf.items.items[1];
			var fval1;
			if (fld0.edr == "DateField") {
				fval0 = Bat.jsDateByDbType(fld0.getValue(),CFG.dbType);
				fval1 = Bat.jsDateByDbType(fld1.getValue(),CFG.dbType);
			} else {
				fval0 = fld0.getValue();
				fval1 = fld1.getValue();
			}
			if(fval0){
				if(fval1)
					hql = fn +' > '+ fval0 + ' and '+fn+' < '+fval1;
				else
					hql =  fn +' > '+ fval0;
			}else {
				if(fval1)
					hql = fn+' < '+fval1;
				else
					hql =  '';
			}
			jd_val = (fld0.getValue()?Ext.Date.format(fld0.getValue(),'Y-m-d'):'')
				+'~'+(fld1.getValue()?Ext.Date.format(fld1.getValue(),'Y-m-d'):'');
			desc = '['+cofld.getRawValue() + ']' +jd_val;
		} else {
			var fld = lf.items.items[0];
			if (fld.edr == "DateField")
				fval = Bat.jsDateByDbType(fld.getValue(),CFG.dbType);
			else {
				fval = fld.getValue();
				if (fld.edr == 'Checkbox')
					fval = fval ? 'Y' : 'N';
				if (oper == 'like')
					if(fval.indexOf('*')!=-1){
						var sval="";
						for(var p=0; p<fval.length; p++){
							var ch=fval.charAt(p);
							if(ch=='*')
								sval+='%';
							else
								sval+=ch;
						}
						fval=sval;
					}
					else						
						fval = '%' + fval + '%';
				if (oper == 'in')
					fval = '(' + fval + ')';
				else if (typeof(fval) == "string") {
					if (fval.indexOf("'") != 0)
						fval = fval.replace(/'/g, "''");
					fval = "'" + fval + "'";
				}
			}
			if (oper == "is null" || oper == "is not null")
				fval = '';
			hql= fn + ' ' + oper + ' ' + fval;
			jd_val = fld.getRawValue();
			desc = '['+cofld.getRawValue() + ']' + this.co_oper.getRawValue() + ' ' + fld.getRawValue();
		}

		var fm_cur = this.filter_map[fn]||{};
		if(lo && fm_cur.hql){
			hql = "(" + fm_cur.hql + ((lo=='or') ? " or ":" and ")+ hql+" )";
			jd_val = fm_cur.jd_val+"、"+ jd_val;
			desc= "(" + fm_cur.desc +  ((lo=='or') ? " 或 ":" 且 ") + desc+" )";
		}
		
		var fm ={
			hql: hql,
			desc:desc ,		
			jd_label:cofld.getRawValue(),
			jd_val:jd_val
		}
		var btn = fm_cur.btn;
		if(!btn){
			//build button
			var tbar_filter = this.grid.down('container[cls="jd_tip"]');
			btn = tbar_filter.add({
            	cls:'f-list',
                html: '<li><a>'+fm.jd_label+'：<strong>'+fm.jd_val+'</strong><b></b></a></li>'
            });
			var el = btn.getEl().down('a');
			el.on('click',this.handleFilterClick,this,{fn:fn,btn:btn});
            
		}else{
			//set button text
			btn.getEl().down('strong').setHTML(fm.jd_val);
		}
		fm.btn = btn;
		this.filter_map[fn]=fm;
		return hql
	},
	handleFilterClick:function(e,el,o){
		//console.log(o.fn);
		delete this.filter_map[o.fn];
		var tbar_filter = this.grid.down('container[cls="jd_tip"]');
		tbar_filter.remove(o.btn);
    	this.cofld.setValue('');
		var lf = Ext.ComponentMgr.get(this.cofld.ipt_id); // wjh
		lf.removeAll(true);
		this.refreshGrid();
	},
	getreq : function() {
		var phql = this.phql;
		//this.getwhere();
		
		var fs=false;
		for(var x in this.filter_map){
			var fln = this.filter_map[x];
			if(!fln)
				continue;
			if(!fs){
				fs = fln.hql;
			}
			else{
				fs+=" and "+ fln.hql;
			}
		}
		
		if(fs){
			if(phql.where)
				fs = phql.where + " and ("+fs+")";
		}else{
			fs = phql.where;
		}
		
	
		var rhql = phql.from+ (fs?" where "+fs:"");
		var ordbtn = this.ordbtn;
		var ordstr;
		if(ordbtn && ordbtn.getText() != Bat.ORDBYDIS){
			ordstr =ordbtn.orderby;
		}
		else
			ordstr = phql.orderby;
		rhql+= ordstr?" order by "+ordstr:"";
		this.view.req.map.hql = rhql;
		return rhql;
	},
	getreq_desc:function() {
		var fdesc=false;
		for(var x in this.filter_map){
			var fln = this.filter_map[x];
			if(!fln)
				continue;
			if(!fdesc){
				fdesc=fln.desc;
			}
			else{
				fdesc+=" 并且 "+ fln.desc;
			}
		}
		return fdesc;
	},
	getDetailKey:function(robj){
		var vw = this.view;
		var key = "tabs_";
		if(vw.cfg_detail && vw.cfg_detail.tabs){
			for(var i=0; i<vw.cfg_detail.tabs.length; i++){
				key+=i+"_";
			}
		}
		key+="clds_";
		if(vw.child_views){
			for(var i=0; i<vw.child_views.length; i++){
				var cw = vw.child_views[i];
				if(cw.func_limit && !cw.func_limit(robj))
					continue;
				key+=i+"_";
			}
		}
		return key;
	},
	clearFilter:function(){
		var tbar_filter = this.grid.down('container[cls="jd_tip"]');
		for(var x in this.filter_map){
			var fl = this.filter_map[x];
			if(!fl)
				continue;
			tbar_filter.remove(fl.btn);
		}
		delete this.filter_map;
		this.filter_map={};
    	this.cofld.setValue('');
		var lf = Ext.ComponentMgr.get(this.cofld.ipt_id); // wjh
		lf.removeAll(true);
		this.refreshGrid();
	},
	query:function(){
	   var query_form = this.grid.down('form[cls="query_form"]');
	   if(!query_form){
	   		var query_tab = this.grid.down('tabpanel[cls="query_tab"]');
	   		if(query_tab)
	   			query_form = query_tab.getActiveTab();
	   }
	   if(!query_form)
	   	return;
		var rs = query_form.getValues(false,true);
		console.log(rs);
		this.filter_map={};
		
		var pd = this.view.cfg_prop;
		for(var x in rs){
			var pdf = pd[x]||{};
			this.filter_map[x]={
				hql: x +" like '%"+rs[x]+"%'",
				desc:'['+(pdf.display||x) +']包含:' + rs[x]
			};
		}
	},
	search :function(logic_oper){
		//var t = this.grid.getBottomToolbar();
		//t.changePage(1);
		this.getwhere(logic_oper);
		this.refreshGrid();
	},
	refreshGrid:function(){
		this.grid.getSelectionModel().clearSelections();
		var hql_desc = this.getreq_desc();
		var hql = this.getreq();
		var bbar = this.grid.getDockedItems('toolbar[dock=bottom]');
		bbar[0].getComponent(11).setTooltip(hql);
		var btn = bbar[0].getComponent(12);
		btn.setText(hql_desc);
		btn.setTooltip(hql_desc);
		this.ds.loadPage(1);

	},
	toolbarItem : {
		"录入" : {
			text : '录入',
			cls : 'x-btn-text-icon',
			icon : '../bat/icons/add.png',
			handler : function(b, e) {
				//this.grid.stopEditing();
				var objs = this.objs;
				var o = Bat.clone(objs[0]);
				o.action = "A";
				objs[objs.length] = o;
				this.reload = false;
				this.ds.load();
				this.renderDetailPanel(o);
			}
		},
		"增加" : {
			text : '新增',
			cls : 'x-btn-text-icon',
			icon : '../bat/icons/add.png',
			handler : function(b, e) {
				//this.grid.stopEditing();
				var objs = this.objs;
				var o = Bat.clone(objs[0]);
				o.action = "A";
				objs[objs.length] = o;
				this.reload = false;
				this.ds.load();
				//this.grid.getSelectionModel().selectLastRow();
				//var row = this.grid.store.getCount() - 1;
				//this.grid.startEditing(row,1);	
			}
		},
		"删除" : {
			text : '删除',
			cls : 'x-btn-text-icon',
			icon : '../bat/icons/delete.png',
			scope : this,
			handler : function(b, e) {
				var objs = this.objs;
				var sels = this.grid.getSelectionModel().getSelection();
				if (!sels || sels.length == 0){
					return;
				}
			
	    		       
			 	for (var i = 0; i < sels.length; i++) {
				var o = sels[i].obj;
				if (o.action == "A") {
					var pos = null;
					for (var k = 0, klen = objs.length; k < klen; k++) {
						if (objs[k] == o) {
							pos = k;
							break;
						}
					}
					if (pos) {
						for (var k = pos, klen = objs.length; k < klen; k++)
							objs[k] = objs[k + 1];
						    objs.length--;
					}
				} else
					o.action = "R";
				}
				this.reload = false;
				this.ds.load();
				
			}
		},
		"提示删除" : {
			text : '删除',
			cls : 'x-btn-text-icon',
			icon : '../bat/icons/delete.png',
			handler : function(b, e) {
				var objs = this.objs;
				var sels = this.grid.getSelectionModel().getSelection();
				if (!sels || sels.length == 0){
				  Ext.MessageBox.show({
			           title: '系统提示',
			           msg: '请先选中要删除的记录',
			           buttons: Ext.MessageBox.OK,
			           icon: Ext.MessageBox.INFO
		          });
					return;
				}
				var me=this;
					Ext.MessageBox.confirm("确认","是否确认删除选中的["+sels.length+"]条数据",function(btn){
					     if(btn=="no"){
					        return;
					     }
					 	for (var i = 0; i < sels.length; i++) {
							var o = sels[i].obj;
							if (o.action == "A") {
								var pos = null;
								for (var k = 0, klen = objs.length; k < klen; k++) {
									if (objs[k] == o) {
										pos = k;
										break;
									}
								}
								if (pos) {
									for (var k = pos, klen = objs.length; k < klen; k++)
										objs[k] = objs[k + 1];
									objs.length--;
								}
							} else{
								o.action = "R";
							}
					  }
					me.save();
	    		 		 
					});
			}
		},
		"打开" : {
			text : "详细",
			cls : 'x-btn-text-icon',
			icon : '../bat/icons/form.png',
			handler : function(b, e) {
				var sels = this.grid.getSelectionModel().getSelection();
				if (!sels || sels.length == 0){
					 Ext.MessageBox.alert("提示","请选择要查看记录");
					return;
				}
				
				//this.grid.stopEditing();
				var objs = this.ds.proxy.data;
				var sels = this.grid.getSelectionModel().getSelection();
				if (!sels || sels.length == 0)
					return;
				var robj = sels[0].obj;
				this.renderDetailPanel(robj);
			}
		},
		"保存" : {
			text : '保存列表',
			cls : 'x-btn-text-icon',
			icon : '../bat/icons/disk.png',
			handler : function(b, e) {
				//this.grid.stopEditing();
				this.save();
			}
		}
	},
	save : function(action,func_save) {
		this.evt.fireEvent("before_save", this, true); // wjh
		var me = this;
		// compare with obj loaded & get difference
		//兼容直接打开单条数据详细页
	
		var objs = me.objs || [me.obj_detail];
		var pobjs = [];
		
		for (var i = 0; i < objs.length; i++) {
			var pobj = Bat.getIts(objs[i],me);
			if (pobj){ // ='U'
				pobjs[pobjs.length] =pobj;
			}
		}
		if (pobjs.length == 0) {
			//保存按钮无论是否向后台传数据,一律刷新
			me.evt.fireEvent("after_put", me, true);
			if(!func_save)
				return false;
			else{
				delete this.view.req.map.objs;
				if(!func_save(me))
					return false;
			}
		}else{
			this.evt.fireEvent("before_put", this, pobjs); // wjh
			// Ext.MessageBox.wait('正在保存，请稍候...');
			this.view.req.map.objs = pobjs;
		}
		jsonrpc.BH.handle(function(data, ex) {
			if (Bat.outputEx(ex, '保存数据异常'))
				return;
			delete me.view.req.map.objs; // wjh
			me.evt.fireEvent("after_put", me, true); // wjh		
			//Ext.MessageBox.hide();
			}, action||'P', me.view.req, me.view.SN);
	
	},
	getPanel : function(name, title, btns) {
		var divId = "div_" + name + "_" + Ext.id();
		var panelName = "panel_" + name;
		var btnName = "btns_" + name;
		var panelDiv = "panel_div_" + name;
		if (this[panelDiv])
			return this[panelDiv];
		var vweb = btns, btns_detail = [];
		if (vweb) {
			var tb = layout.tbar;
			for (var i = 0; i < vweb.length; i++) {
				vweb[i].scope = this;
				var btn_detail = tb.insert(2, vweb[i]);
				btn_detail.only_detail = true;
				btns_detail[btns_detail.length] = btn_detail;
			}
			this.btns_detail = btns_detail;
			tb.doLayout();
		}
		var cd = this.tabPanel.add({
			title : "[" + this.gridTitle + "]" + title,
			iconCls : 'tabs',
			closable : true,
			autoDestroy : false,
			// autoScroll: true,
			html : "<div id='" + divId + "' style='width:100%;height:100%;'/>"
		});
		cd.on('activate', function(p) {
			if (btns_detail) {
				for (var i = 0; i < btns_detail.length; i++)
					btns_detail[i].show();
			}
		}, this);
		cd.on('deactivate', function(p) {
			layout.hideDetailBtns();
		});
		cd.show();
		this[panelName] = cd;
		this[btnName] = btns_detail;
		this[panelDiv] = divId;
		return divId;
	},
	getPanelDetail : function(cfg) {
		var cd;
		cfg.closable =true;
		cfg.autoDestroy=false;
		cfg.plain=true;
		//cfg.frame=true,
		cfg.title = "[" + this.gridTitle + "]" + this.titleDetail;
		//cfg.bodyStyle="background-image:url('../cm/icons/bk1.png');background-repeat: no-repeat;background-attachment: fixed;background-position: right bottom; ";

		cd = this.tabPanel.add(cfg);
		cd.on('activate', function(p) {
			if (this.btns_detail) {
				var btn_next;
				var tb = layout.tbar;
				var btn_ = tb.queryById("mi_next");
				for (var i = 0; i < this.btns_detail.length; i++){
					var btn = this.btns_detail[i];
					if(btn.bhide)
						continue;
					if(btn==btn_){
						btn_next=true;
					}
					btn.show();
				}
				//刷新游标按钮
				if(btn_next){
					this.refreshNextPrev();
				}
			}
		}, this);
		cd.on('deactivate', function(p) {
			layout.hideDetailBtns();
		});
		cd.show();
		cd.uiDetail = this;
		this.tabDetail = cd;
		return cd;
	},
	refreshNextPrev:function(obj){
		var tb = layout.tbar;
		var btn_prev =tb.queryById("mi_prev");
		if(!btn_prev || !btn_prev.isVisible()){
			return;
		}
		var obj=obj||this.obj_detail;
		var objs = this.objs;
		//c4w 5.13
		if(!objs || !objs.length){
			btn_prev.hide();
			tb.queryById("mi_next").hide();
			return;
		}
		var len = objs.length;
		for(var i=0; i<len; i++){
			if(obj==objs[i]){
				if(i>1){
					tb.queryById("mi_prev").enable();
				}else{
					tb.queryById("mi_prev").disable();
				}
				if(i<len-1){
					tb.queryById("mi_next").enable();
				}else{
					tb.queryById("mi_next").disable();
				}
			}
		}
	},
	validateForm:function(){
		var tabDetail = this.tabDetail;
		if(!tabDetail)
			return true;
		var its = tabDetail.query('form');
		if(tabDetail.is('form')){
			its[its.length]=tabDetail;
		}
		var fm_fail;
		for(var i=0; i<its.length; i++){
			var fm = its[i];
			if(fm && !fm.isValid()){
				fm_fail=fm;
				var invalidFields = [];
			    fm.form.getFields().filterBy(function(field) {
			        if (!field.validate()){
			        	invalidFields.push(field);
			        	console.log("invalidate field:"+field.getName());
			        }
			    });			    
				break;
			}
		}
		if(!fm_fail)
			return true;
		var par= fm_fail;
		for(var i=0; i<2; i++){
			var lastpar = par;
			par = par.up('tabpanel');
			if(!par){
				break;
			}
			par.setActiveTab(lastpar);
			if(par==tabDetail)
				break;
		}
		return false;
	},
	setObjDetail : function(obj) {
		this.obj_detail = obj;
		this.refreshNextPrev();
		var editors = this.editors;
		this.evt.fireEvent("before_setobj_detail", this, editors, obj);
		//如果新增,应当隐藏子表,关联表tab
		var tabDetail = this.tabDetail;
		if(tabDetail.xtype=='tabpanel'){
			if(obj.action!=this.last_obj_action){
				tabDetail.items.each(function(c){
					if(c.tab_mode){
						if(obj.action=='A'){
							c.tab.hide();
						}else{
							c.tab.show();
						}			
					}
				});
			}
			var vwd = this.view.cfg_detail || {};
			if(obj.action!='A' && vwd.default_active_tab){
				var tab = tabDetail.items.items[vwd.default_active_tab];
				tabDetail.setActiveTab(tab);
				//刷新子表
				if(tab.tab_mode){
					tab.fireEvent('activate',tab);					
				}
				
			}else{
				tabDetail.setActiveTab(0);
			}
		}
		this.last_obj_action = obj.action;
		var it = obj.items.map || {};
		for (var x in editors) {
			var fds = editors[x]; // 同一个属性可以在detail界面出现多次
			for (var j = 0, jlen = fds.length; j < jlen; j++) { 
				var fldval = fds[j]; 
				fldval.item = it; 
				fldval.obj = obj; 
				var v = it[x];
				if(fldval.edittype=='DisplayField'){
					v=fldval.valueToRaw(v);		
					fldval.setRawValue(v); 
					continue;
				}
				
				if((fldval.edittype=='ComboBox'&&v=="")||(fldval.edittype=='DateField'&&v==null)){
					if(fldval.reset){
						fldval.reset();
					}
				}
				if(fldval.setRawValue  && fldval.edittype=='DisplayField')
					fldval.setRawValue(v); 
				else{
					if(fldval.setValue)
						fldval.setValue(v); 
				}
			}
		}
		this.validateForm();
		this.evt.fireEvent("after_setobj_detail", this, editors, obj);
	},

	queryField:function(fn,oper){
		var pdf = this.view.cfg_prop[fn] || {
			editor : 'TextField'
		};
		var pdcfg = pdf.cfg || {};

		// layout cann't dowith TextArea
		var edr = pdf.editor;
		if (edr == 'TextArea' || edr == 'HtmlEditor'
				|| edr == 'FileField' || edr == 'TreeField'||edr=='DisplayField') {
				edr = 'TextField';							
			    pdcfg = {};
		}
		var oldreadOnly = pdcfg.readOnly;
		var oldwidth = pdcfg.width;
		pdcfg.readOnly = false;
		pdcfg.width = 300;
		pdcfg.name = fn;
		pdcfg.fieldLabel = (pdf.display || fn); // wjh
		var fldval = null;
		fldval = eval("new Ext.form." + edr + "(pdcfg)");
		fldval.edr = edr;
		fldval.fn=fn;
		if (this.co_oper.getValue() == "between") {
			var fldval2 = eval("new Ext.form." + edr
					+ "(pdcfg)");
			fldval2.edr = edr;
		}
		pdcfg.readOnly = oldreadOnly;
		pdcfg.width = oldwidth;
		return fldval;
	},
	translateField:function(p){
		var obj = this.obj_detail||{items:{map:{}}};
		var pval = obj.items.map[p];
		var vw = this.view;
		var vpd = vw.cfg_prop || {};
		var pdf = vpd[p] || {};
		//var editor = pdf.editor||'TextField';
		var editor = pdf.editor;
		if(!editor)
			return false;
		var pdcfg = pdf.cfg;
		if (!pdcfg) {
			pdcfg = {};
		}
		if(pdcfg.fieldLabel!=false){
			pdcfg.fieldLabel = (pdf.display || p); // wjh
			pdcfg.labelAlign='left';
			pdcfg.labelStyle ="color:#666;font-weight:800;font-family: helvetica,arial,verdana,sans-serif;";
		}
		pdcfg.name = p;
			
		var fldval = eval("new Ext.form." + editor + "(pdcfg)");
		fldval.item = obj.items.map;
		fldval.obj = obj;
		//.ui与extjs重名
		//fldval.ui = this;
		fldval.edittype = editor; // wjh
		if (editor == 'Checkbox'){
			fldval.on("check", function(ed, value) {
				ed.item[ed.name] = value;
				if (ed.obj.action != 'A') {
					ed.obj.action = 'U';
				}
				ed.setRawValue(value);
			}, this);
		}else{
			fldval.on("change", function(ed, value, startValue) {
				ed.item[ed.name] = value;
				if (ed.obj.action != 'A') {
					ed.obj.action = 'U';
				}
			}, this);
		}
	
		fldval.renderer = pdf.renderer;
		if (editor == 'FileField') {
			if (fldval.renderer) {
				var plink = pdf.renderer(pval, null, {
					'data' : obj.items.map
				});
				fldval.plink = plink;
			}else{
				fldval.setValue(pval);
			}
		}
		

		if(!this.editors)
			return fldval;
		if (!this.editors[p])
			this.editors[p] = [];
		var cilen = this.editors[p].length;
		this.editors[p][cilen] = fldval;
		return fldval;
	},
	queryDetail:function(cfg,po){
		if(cfg.tabs){
			var its=[];
			var len = cfg.tabs.length;
			for(var i=0; i<len; i++){
				var ci = cfg.tabs[i];
				var ti =this.queryDetail(ci);
				ti.title=ci.title;
				its[i]=ti;
			}
			var ro=po||{
				xtype: 'tabpanel',
				activeTab : 0,
				border : false,
				 plain: true,
			        defaults :{
			            autoScroll: true,
			            bodyPadding: 10
			        }
			};
			ro.items = its;
			return ro;
		}else if(cfg.fset){
			var its=[];
			var len = cfg.fset.length;
			for(var i=0; i<len; i++){
				var ci = cfg.fset[i];
				var ti={title:ci.legend,
					xtype:'fieldset',
					checkboxToggle: true,
                    collapsed:  !ci.collapsed,
                    sytle:ci.style,
					autoHeight:true};
				this.queryDetail(ci,ti);
				its[i]=ti;
			}
			var ro=po||{
				xtype : 'form'
			}
			ro.items=its;
			return ro;
		}else if(cfg.props){
			var its=[];
			var len = cfg.props.length;
			for(var i=0; i<len; i++){
				var ci = cfg.props[i];
				its[i]=this.queryField(ci);
			}
			var ro=po||{xtype : 'form'};
			ro.items=its;
			return ro;
		}else if(cfg.items){
			var len = cfg.items.length;
			for(var i=0; i<len; i++){
				var ci = cfg.items[i];
				this.queryDetail(ci,ci);
			}
			return cfg;
		}
	},
	translateDetail:function(cfg,po){
		if(!cfg)
			return;
		if(cfg.tabs){
			var its=[];
			var len = cfg.tabs.length;
			for(var i=0; i<len; i++){
				var ci = cfg.tabs[i];
				var ti =this.translateDetail(ci);
				ti.title=ci.title;
				its[i]=ti;
			}
			var ro=po||{
				xtype: 'tabpanel',
				activeTab : 0,
				border : false,
				 plain: true,
			        defaults :{
			            autoScroll: true,
			            bodyPadding: 10
			        }
			};
			ro.items = its;
			return ro;
		}else if(cfg.fset){
			var its=[];
			var len = cfg.fset.length;
			for(var i=0; i<len; i++){
				var ci = cfg.fset[i];
				var ti={title:ci.legend,
					xtype:'fieldset',
					checkboxToggle: true,
                    collapsed: !ci.collapsed,
                    style:ci.style,
					autoHeight:true};
				this.translateDetail(ci,ti);
				its[i]=ti;
			}
			var ro=po||{
				xtype : 'form'
			}
			ro.items=its;
			return ro;
		}else if(cfg.props){
			var its=[];
			var len = cfg.props.length;
			for(var i=0; i<len; i++){
				var ci = cfg.props[i];
				var ci_fld = this.translateField(ci);
				if(ci_fld)
					its[i]=ci_fld;
			}
			var ro=po||{xtype : 'form'};
			ro.items=its;
			return ro;
		}else if(cfg.items){
			var len = cfg.items.length;
			for(var i=0; i<len; i++){
				var ci = cfg.items[i];
				this.translateDetail(ci,ci);
			}
			return cfg;
		}else{
			console.log('ssss');
			cfg.pui = this;
		}
	},
	renderDetailPanel:function(robj,tn){
		var vw = this.view;
		var detailKey = this.getDetailKey(robj);
		if(robj.gridTitle){
			this.gridTitle=robj.gridTitle;
		}
		var title;
		if (vw.func_title)
			this.titleDetail = vw.func_title(robj);
		else
			this.titleDetail =robj.items.map["id"]||robj.items.map["A.id"];
		var tabDetail=this.tabDetails[detailKey];
		if (!tabDetail) {
			tabDetail=this.getPanelDetail(this.renderDetail(robj));	
			tabDetail.tn = tn;
			this.tabDetails[detailKey]=tabDetail;
			this.setObjDetail(robj);
		} else {
			tabDetail.bHide = false;
			tabDetail.tab.show();
			//this.tabPanel.unhideTabStripItem(tabDetail);
			this.setObjDetail(robj);
			this.tabPanel.setActiveTab(tabDetail);			
		}
		
        if(this.gridTitle.startsWith==undefined){
        	if(this.gridTitle.indexOf("dao")>=0){
    			tabDetail.setTitle("[" +this.titleDetail +"]"  );
    		}else{
    			tabDetail.setTitle("[" + this.gridTitle +this.titleDetail +"]"  );
    		}
        }else{
        	if(this.gridTitle.startsWith("dao")){
    			tabDetail.setTitle("[" +this.titleDetail +"]"  );
    		}else{
    			tabDetail.setTitle("[" + this.gridTitle +this.titleDetail +"]"  );
    		}
        }	
	},
	
	renderGridQuery:function(){
		var vwg = this.view.cfg_grid || {};
		if(!vwg.tabs)
			return false;
		var items_tab = this.queryDetail(vwg).items;
		var cfg;
		//如果只有一个tab并且没有关联视图和子表视图,直接用formpanel
		if(items_tab.length==1){
			var item0 = items_tab[0];
			item0.title = '';
			cfg=item0;
			cfg.xtype='form';
			cfg.padding='20 20 20 20',
			cfg.title = '';
			cfg.cls='query_form';
		}else{
			cfg = {
				cls:'query_tab',
				xtype: 'tabpanel',
				//id:obj.items.map["id"]||obj.items.map["A.id"],
				activeTab : 0,
				border : false,
				deferredRender : false,
				enableTabScroll : true,// wjh
				padding:'10 5 3 10',
				 plain: true,
			        defaults :{
			            autoScroll: true,
			            bodyPadding: 10
			        },
			        items : items_tab
			};
		}
		return cfg;
	},
	// 根据view画form界面
	renderDetail : function(obj) {
		this.obj_detail = obj;
		var vw = this.view;
		var vpd = vw.cfg_prop || {};
		var flds = [];
		var fldvals = [];

		this.editors = {};
		this.child_views=[];
		var tabobj;
		var odiv;
		var vwe = vw.cfg_detail || {};
		var items_tab = this.translateDetail(vwe).items;
		// explain and render child_views
		if (vw.child_views) {
			for (var j = 0; j < vw.child_views.length; j++) {
				var rw = vw.child_views[j];
				if(rw.func_limit && !rw.func_limit(obj))
					continue;
				var robj = obj;
				var id_tab = Ext.id();

				var vw1 = New(BView, [BView.viewMap[rw.vn]]);
				if(rw.func_view){
					rw.func_view(vw1,robj);
				}
				var ui1 = New(BatUI, [vw1, this.tabPanel,
						rw.title]);
				ui1.pui = this;
				if (rw.func_setdefault)
					ui1.func_setdefault = rw.func_setdefault;
				
				it = {
					tab_mode:'child_view',
					//autoScroll : false,
					bodyStyle : 'padding:0px;',
					id : id_tab,
					rw : rw,
					layout:'fit',
					items: {
					    title: 'Fit Panel',
					    header:false,
					    xtype:'bat_grid',
					    border: false,
					    ui:ui1
					},
					ui:ui1,
					title : rw.title,
					listeners : {
						activate : {
					fn : function(tab) {
						//alert("activate")
						var ui1 = tab.ui;
						var rw = tab.rw;
						ui1.robj = this.obj_detail;
						if (rw.func_filter)
							rw.func_filter(this.obj_detail, ui1);
						ui1.refreshGrid();
					},
					scope : this
				}
					}
				};
				if(Ext.isIE)
				   it.hideMode="offsets";
				items_tab[items_tab.length] = it;
			}
		}
		if(vw.rel_views){
			for(var j=0; j<vw.rel_views.length; j++){
				var lw = vw.rel_views[j];
				var robj=obj;
				var id_tab = Ext.id();

				var vw1 = New(BView, [BView.viewMap[lw.vt.vn]]);
				if(lw.vt.func_view){
				  lw.vt.func_view(vw1);
				 }
				var ui1 = New(BatUI, [vw1, this.tabPanel,lw.vt.title]);
				ui1.pui = this;
				ui1.gridHeight = 300;
				if (lw.vt.func_setdefault)
					ui1.func_setdefault = lw.vt.func_setdefault;

				var vw2 = New(BView, [BView.viewMap[lw.vs.vn]]);
				if(lw.vs.func_view){
				  lw.vs.func_view(vw2);
				 }
				var ui2 = New(BatUI, [vw2, this.tabPanel,lw.vs.title]);
				ui2.pui = this;
				ui2.ui1=ui1;
				ui1.ui2=ui2;
				ui2.gridHeight = 300;
				if (lw.vs.func_setdefault)
					ui2.func_setdefault = lw.vs.func_setdefault;

					it = {
					tab_mode:'rel_view',
					ui1:ui1,
					ui2:ui2,
					id : id_tab,
					lw : lw,
					title : lw.title,
					layout:'border',	
					defaults: {
					    collapsible: false,
					    split: true
					},					
					items: [{
						id:id_tab+"_vs",
					    title: lw.vs.title||'Main Content',
					    region:'north',
					    xtype:'bat_grid',
					    collapsible: true,
					    ui:ui2
					 },{
						id:id_tab+"_vt",
					    title: lw.vt.title||'Footer',
					    region: 'center',
					    layout:'fit',
					    xtype:'bat_grid',
					    ui:ui1
					  }],				
			listeners : {
				activate : {
		fn : function(tab) {
			var lw = tab.lw;
			var ui1 = tab.ui1;
			ui1.robj = this.obj_detail;
			if (lw.vt.func_filter)
				lw.vt.func_filter(this.obj_detail, ui1);
			ui1.refreshGrid();

			var ui2 = tab.ui2;
			ui2.robj = this.obj_detail;
			if (lw.vs.func_filter)
				lw.vs.func_filter(this.obj_detail, ui2);
			ui2.refreshGrid();
		},
		scope : this
				}
			}
		};
		if(Ext.isIE)
				   it.hideMode="offsets";
		items_tab[items_tab.length] = it;
	}
		}
		var cfg;
		//如果只有一个tab并且没有关联视图和子表视图,直接用formpanel
		if(items_tab.length==1){
			var item0 = items_tab[0];
			item0.title = '';
			cfg=item0;
			cfg.xtype='form';
			cfg.padding='20 20 20 20',
			cfg.title = '';
		}else{
			cfg = {
				xtype: 'tabpanel',
				//id:obj.items.map["id"]||obj.items.map["A.id"],
				activeTab : 0,
				border : false,
				autoScroll: false,
				deferredRender : false,
				enableTabScroll : true,// wjh
				padding:'10 5 3 10',
				 plain: true,
			        defaults :{
			            autoScroll: true,
			            bodyPadding: 10
			        },
			        items : items_tab
			};
		}
		var vweb = vwe.btns;
		if (vweb) {
			var tb = layout.tbar;
			var btns_detail = [];
			for (var i = 0; i < vweb.length; i++) {
				if (vweb[i] == "保存") {
					var btn_ = tb.queryById("mi_save");
					btn_.only_detail=true;
					btns_detail[btns_detail.length] = btn_;
					btn_.show();
					continue;
				}else if (vweb[i] == "游标") {
					//单条打开
					if(!this.objs){
						continue;
					}
					var btn_ = tb.queryById("mi_prev");
					btn_.only_detail=true;
					btns_detail[btns_detail.length] = btn_;
					btn_.show();
					btn_ = tb.queryById("mi_next");
					btn_.only_detail=true;
					btns_detail[btns_detail.length] = btn_;
					btn_.show();
					continue;
				}
				
				vweb[i].scope = this;
				//5 顶部工具条位置
				var btn_detail = tb.insert(2, vweb[i]);
				btns_detail[btns_detail.length] = btn_detail;
				btn_detail.only_detail=true;
			}
			this.btns_detail = btns_detail;
			tb.doLayout();
		}
		return cfg;
	},
	//get grid config by UI
	getConfigCrid:function(applyTo){
		var me = this;
		this.evt.on("after_put", function(ui) {
			if (!ui.grid.bbar) {
				this.ds.load();
				return;
			} else {
				// load current page
				var t = ui.grid.getBottomToolbar();
				var activePage = Math
						.ceil((t.cursor + t.pageSize) / t.pageSize);
				t.changePage(activePage);
			}
		}, this);
		var props = [];
		var cm = [];
		var v = this.view;
		var pd = v.cfg_prop || {};
		var pg = v.cfg_grid || {};
		var pgc = pg.ColumnModel || {};
		var req = v.req;
		var reqprops = v.propNames;
		var fields=[];
		for (var i = 0, len = reqprops.length; i < len; i++) {
			var curp = reqprops[i];
			fields[fields.length]=curp;
			var pdp = pd[curp] || {};
			var pgcp = pgc[curp] || {};
			var pde = pdp.editor;
			var pdcfg = pdp.cfg || {};
			props[props.length] = {
				name : curp
			};
			var fd;
			if (pde) {
				fd = eval("new Ext.form." + pde + "(pdcfg)");
				fd.fn = curp;
			}
			var c = {
					header : pdp.display || curp,
					width : pdp.width,
					dataIndex : curp,
					sortable : true
				};
			//14.01.10 to copy
			if(!c.width){
				c.width=c.header.length*25;
			}
			
			if (pde != "ComboBox")
				c.renderer=pdp.renderer;
			else{
				if(pdp.renderer)
					c.renderer=pdp.renderer;
				else
					c.renderer=Ext.util.Format.comboRenderer(fd);
			}
			if(pg.selType =='cellmodel' && pde && !pdcfg.readOnly){
				c.editor=fd;
			}
			for (x in pgcp) {
				c[x] = pgcp[x];
			}
			c.hidden=pdp.hidden;
			if(!c.hidden)
				cm[cm.length] = c;
		}
		
		var reader = new Bat.ExtObjReader(props);
		var pr = new Bat.ExtProxy({ui:this,reader:reader});
		//c4w 2015 默认每页100行
		//var prf = {proxy:pr};
		var prf = {proxy:pr,pageSize: 100};
		
		if(pg.store){
			for(var x in pg.store){
				prf[x]=pg.store[x];
			}
		}
		var ds = new Ext.data.Store(prf);
		this.ds = ds;
		// create toolbar
		var bpermitAdd = false;
		// var vg=v.grid||{};
		var vgb = pg.btns;
		if (!vgb)
			vgb = ['录入', '提示删除', '检索', '刷新', '排序'];
		var tbar = [];
		if (vgb.length > 0) {
			//c4w10.26
			var ths = [];
			var ps = v.propNames;
			// grid列名
			for (i = 0, len = ps.length; i < len; i++) {
				var pn = ps[i];
				var pdv = pd[pn] || {};
				var pt = pdv.display || pn;
				if(!pdv.hidden)
					ths[ths.length] = [pn, pt];
			}

			for (i = 0, len = vgb.length; i < len; i++) {
				var btn_txt = vgb[i];
				if (!btn_txt)
					continue;
				if (typeof(btn_txt) == 'object') {
					tbar[tbar.length] = btn_txt;
					continue;
				}
				var btn_default = this.toolbarItem[btn_txt];
				if (btn_default) {
					if (pg.btn_revise) {						
						btn_default = New(btn_default);
						pg.btn_revise(btn_default);
					}
					tbar[tbar.length] = btn_default;
					btn_default.scope = this;
					if(btn_txt=="增加")
					  bpermitAdd=true;
					continue;
				}
				if (btn_txt == '查询') {
					tbar[tbar.length]=new Ext.Button({
	                	text:'查询',
	                	handler:function(b,e){
	                		this.query();
	                		this.refreshGrid();
	                	},scope:this
					})
				}
				if (btn_txt == '刷新') {
					//tbar[tbar.length]=new Ext.button.Split({
					tbar[tbar.length]=new Ext.Button({
	                	text:'检索',
	                	handler:function(b,e){
	                		this.search('or');
	                	},scope:this/*,
						menu : {
			                items: [{
			                	text:'全部',
			                	handler:function(b,e){
			                		this.clearFilter();
			                	},scope:this
			                },{
			                	text:'或者',
			                	handler:function(b,e){
			                		this.search('or');
			                	},scope:this
			                },{
			                	text:'并且',
			                	handler:function(b,e){
			                		this.search('and');
			                	},scope:this
			                }  
			                ]
						}*/
					})
				}
				if (btn_txt == '检索') {
					var store = new Ext.data.SimpleStore({
						fields : ['prop', 'display'],
						data : ths
					});
					var cofld = new Ext.form.ComboBox({
						editable:false,
						store : store,
						displayField : 'display',
						valueField : 'prop',
						typeAhead : true,
						mode : 'local',
						triggerAction : 'all',
						emptyText : '请选择字段...',
						selectOnFocus : true,
						width : 130
					});
					this.cofld = cofld;
					tbar[tbar.length] = cofld;
					//tbar[tbar.length] = '&nbsp;&nbsp;检索条件：';
					var ops = [['=', '等于'], ['>', '大于'],
							['<', '小于'], ['<>', '不等于'], ['like', '包含'],
							['is null', '为空'], ['is not null', '非空']/**,
							['between', '区间'], ['in', '枚举']**/
							];
					var store = new Ext.data.SimpleStore({
						fields : ['prop', 'display'],
						data : ops
					});
					var co_oper = new Ext.form.ComboBox({
						editable:false,
						store : store,
						hidden:true,
						value:'like',
						displayField : 'display',
						valueField : 'prop',
						typeAhead : true,
						mode : 'local',
						triggerAction : 'all',
						emptyText : '请选择操作...',
						selectOnFocus : true,
						width : 60
					});
					this.co_oper = co_oper;
					cofld.addListener("select", function(co, rec, index) {
						var fn = co.getValue();//co为查询条件
						var lf = Ext.ComponentMgr.get(co.ipt_id); // wjh
						if(!fn){
							lf.removeAll(true);
							return;
						}
						var pdf = this.view.cfg_prop[fn] || {
							editor : 'TextField'
						};
						var pdcfg = pdf.cfg || {};

						// layout cann't dowith TextArea
						var edr = pdf.editor;
						if(edr=='ComboBox'){
							this.co_oper.setValue('=');
						}else {
							this.co_oper.setValue('like');
						}
						if (edr == 'TextArea' || edr == 'HtmlEditor'
								|| edr == 'FileField' || edr == 'TreeField'||edr=='DisplayField') {
								edr = 'TextField';							
							    pdcfg = {};
						}
						
						var oldreadOnly = pdcfg.readOnly;
						var oldwidth = pdcfg.width;
						var oldfieldLabel = pdcfg.fieldLabel;
						pdcfg.readOnly = false;
						pdcfg.width = 100;
						pdcfg.fieldLabel=false;
						lf.removeAll(true);
						var fldval = null;
						fldval = eval("new Ext.form." + edr + "(pdcfg)");
						fldval.edr = edr;
						lf.add(fldval);
						if(edr == 'DateField'){
							fldval2 = eval("new Ext.form." + edr + "(pdcfg)");
							fldval2.edr = edr;
							lf.add(fldval2);
						}
						/**   2014.07.18  hide 新项目不需要区间
						 if (this.co_oper.getValue() == "between") {
							var fldval2 = eval("new Ext.form." + edr
									+ "(pdcfg)");
							fldval2.edr = edr;
							lf.add(fldval2);
						}
						**/
						lf.doLayout();
						pdcfg.readOnly = oldreadOnly;
						pdcfg.width = oldwidth;
						pdcfg.fieldLabel=oldfieldLabel;
					}, this);
					tbar[tbar.length] = co_oper;
					var ipt_id = Ext.id();
					cofld.ipt_id = ipt_id;
					tbar[tbar.length] = new Ext.Container({
						id : ipt_id,
						layout : 'column',
						border : false//,
						//height : 20,
						//width : 110
					});
				}
				if(btn_txt=="日期"){
					var firstDate = new Ext.form.DateField({
						
					});
                    var secondDate = new Ext.form.DateField({
						
					});
					this.firstDate = firstDate;
					tbar[tbar.length]="起始日期";
					tbar[tbar.length] = firstDate;
					this.secondDate = secondDate;
					tbar[tbar.length]="结束日期";
					tbar[tbar.length] = secondDate;
				}

				if (btn_txt == '排序') {
					var ordItems = [];
					var descItems = [];
					for (var h = 0, hlen = ths.length; h < hlen; h++) {
						//将复合字段剔除出排序菜单
						if(ths[h][0].indexOf('.variable.')!=-1)
							continue;
						ordItems[ordItems.length] = new Ext.menu.CheckItem({
							text : ths[h][1],
							checked : false,
							group : 'ordby',
							prop : ths[h][0],
							prop_txt:ths[h][1],
							handler : function(item, evtObj) {
								ordbtn.setText(item.prop_txt+' ↑');
								ordbtn.orderby=item.prop;
								this.refreshGrid();
							},scope:this
						});
						descItems[descItems.length] = new Ext.menu.CheckItem({
							text : ths[h][1],
							checked : false,
							group : 'ordby',
							prop : ths[h][0],
							prop_txt:ths[h][1],
							checkHandler : function(item, checked) {
								if (!checked)
									return;
								ordbtn.setText(item.prop_txt+' ↓');
								ordbtn.orderby=item.prop + ' desc';
								this.refreshGrid();
							},scope:this
						});
					}
					var ordbtn = new Ext.button.Split({
						text : Bat.ORDBYDIS,
						cls : 'x-btn-text-icon',
						handler:function(b,e){
							var ord_txt = ordbtn.getText();
							if(ord_txt== Bat.ORDBYDIS){
								return;
							}
							var pos = ordbtn.orderby.indexOf('desc');
							if(pos==-1){
								ordbtn.orderby +=' desc';
								ordbtn.setText(ord_txt.replace('↑','↓'));
							}else{
								ordbtn.orderby = ordbtn.orderby.substring(0,pos-1);
								ordbtn.setText(ord_txt.replace('↓','↑'));
							}
							this.refreshGrid();
						},
						scope:this,
						// Menus can be built/referenced by using nested menu
						// config objects
						menu : new Ext.menu.Menu({
							items : [new Ext.menu.CheckItem({
								text : '升序',
								group : 'ordby',
								menu : {
									items : ordItems
								}
							}), new Ext.menu.CheckItem({
								text : '降序',
								group : 'ordby',
								menu : {
									items : descItems
								}
							})
//							new Ext.menu.CheckItem({
//								text : '无',
//								group : 'ordby',
//								checkHandler : function(item, checked) {
//									if (!checked)
//										return;
//									ordbtn.setText(Bat.ORDBYDIS);
//									this.refreshGrid();
//								}
//							})
							]
						})
					});
					this.ordbtn = ordbtn;
					tbar[tbar.length] = ordbtn;
				}
				
			if (btn_txt == '图表') {
        var grpfield = ths;
			  if(pg.grphide){
			    for(var gh = 0 ; gh < pg.grphide.length ; gh++){
			          grpfield.splice(pg.grphide[gh],1);
			    }
			  }		    
				var grpbtn=BView.getBtnChart(me,grpfield);
				this.grpbtn = grpbtn;
				tbar[tbar.length]=grpbtn;			
			}
			
			if (btn_txt == '导出') {	    
				var grpbtn=BView.getBtnForExcel(me);
				this.grpbtn = grpbtn;
				tbar[tbar.length]=grpbtn;			
			}
			
		 }
	  }

		var paging;
		if (!v.req.map.rownum)
			v.req.map.rownum = gridPagePer || 20;
		if (pg.page) {
			var paging = new Ext.PagingToolbar({
				autoWidth : true,
				store : ds,
				pageSize : v.req.map.rownum,
				displayInfo : true,
				displayMsg : '当前行: {0} - {1} 总行数 {2}',
				emptyMsg : "无数据",
				items:[{
				hidden :true,
	            text: '显示全部',
	            //tooltip: {text:'This is a an example QuickTip for a toolbar item', title:'Tip Title'},
	            handler : function(b, e) {
	            	//this.co_oper.setValue('');
	            	this.cofld.setValue('');
					var lf = Ext.ComponentMgr.get(this.cofld.ipt_id); // wjh
					lf.removeAll(true);
	            	this.refreshGrid();
	            },scope:this
	            
				},{
		            text: '',
		            //tooltip: {text:'description for search', title:'Tip Title'},
		            handler : function(b, e) {
		            	//this.co_oper.setValue('');
		            	//this.refreshGrid();
		            },scope:this
					}
				]
			});
		}
		
		var ntbar = Ext.create('Ext.toolbar.Toolbar', {
			style:'background:#F7F7F7;',
			enableOverflow: true ,
            items:tbar});
        var tbar_items;
        var qtab = this.renderGridQuery();
        if(qtab){
        	tbar_items=[qtab];
        }else{
        	tbar_items =[];
        }
        tbar_items[tbar_items.length]=ntbar;
        if(this.cofld)
        	tbar_items[tbar_items.length]={	
	            xtype: 'container',
	            layout:'column',
	            cls:'jd_tip',
	            items: [{
	            	cls:'f-list',
	                html: '<div class="a-key">已选条件：</div>'
	            }]
			};
        if(!this.gridHeight && this.tabPanel) {
        	this.gridHeight=this.tabPanel.getHeight();
        }
		var gridcfg = {
			viewConfig:{loadMask:false},
			selType: pg.selType|| 'checkboxmodel',
            trackMouseOver : true,
			applyTo : applyTo||this.applyTo,
			store : ds,
			tbar :{
	            xtype    : 'container',
	            layout   : {
	                type  : 'vbox',
	                pack  : 'start',
	                align : 'stretch'
	            },
	            //defaults : { width : 700 },
	            bodyStyle: 'background-color:transparent',
				items:tbar_items
			},
			bbar : paging,
			columns:cm,
			height : this.gridHeight
		};
		if (pg.cfg) {
			for (x in pg.cfg) {
				gridcfg[x] = pg.cfg[x];
			}
			if (pg.cfg.enableDragDrop) {
				gridcfg['ddGroup'] = 'GridDD';
				gridcfg['autoHeight'] = false;
				gridcfg['height'] = document.body.clientHeight - 300;
				gridcfg['autoScroll'] = true;
			}
		}
		if (bpermitAdd) {
			var contextMenu = new Ext.menu.Menu();
			contextMenu.add({
				text : "复制",
				handler : function() {
					this.grid.clip = this.grid.getSelectionModel().getSelection();
				},scope:this
			});
			contextMenu.add({
				text : "粘贴",
				handler : function() {
					var clip = this.grid.clip;
					if (!clip || clip.length == 0)
						return false;
					var objs = this.objs;
					for (var i = 0; i < clip.length; i++) {
						var o = Bat.clone(clip[i].obj);
						o.action = "A";
						objs[objs.length] = o;
					}
					this.reload = false;
					this.ds.load();
					//this.grid.getSelectionModel().selectLastRow();
					//var row = this.grid.store.getCount() - 1;
				},scope:this
			});
			// TODO send ui to button
			var lsn = gridcfg.listeners||{};
			lsn["containercontextmenu"]=function(grid, e) {
				contextMenu.showAt(e.xy);
			};
			gridcfg.listeners=lsn;
		}
		if(pg.features){
			gridcfg.features=pg.features;
		}
		return gridcfg;
	},
	// 根据view画grid界面
	renderGridTab : function() {
		if(!this.grid)
		  return;
		if (this.tabGrid) {
			this.tabGrid.bHide = false;
			//this.tabPanel.unhideTabStripItem(this.tabGrid.show());
			this.tabGrid.tab.show();
			this.tabPanel.setActiveTab(this.tabGrid);
		}
		this.grid.getStore().load();
	},
	// 用新的数据更新界面
	tojson:function(o){
		var out=[];
    	for(var i=0,len=o.length; i<len; i++){
    		if(o[i].action=='R' || o[i].action=='D'	)
    		  continue;
    		 out[out.length]=o[i].items.map;
    	}
    	return out;		
	},
	refreshData : function(params, reader, callback, scope, arg) {
		this.reload = true;
		var result;
		try {
			//var out=this.tojson(this.data);
			//result = reader.readRecords(out);
			result = reader.readRecords(this.objs);
			//result.total = this.total || this.data.length;
			Ext.apply(params, {
				//response : response,
				resultSet : new Ext.data.ResultSet({
		            total  : this.total || this.objs.length,
		            count  : this.objs.length,
		            records: result.records,
		            success: true,
		            message: null
		        })
			});

			params.commitRecords(result.records);
			params.setCompleted();
			params.setSuccessful();
			
		} catch (e) {
			this.evt.fireEvent("loadexception", this, arg, null, e);
			callback.call(scope, null, arg, false);
			return;
		}
		this.grid.getSelectionModel().deselectAll();
		callback.call(scope, params);
		this.evt.fireEvent("refresh_data", this, true);
	},

	// 暂存获得的数据
	setData : function(data) {
		this.evt.fireEvent("before_setdata", this, data, true);
		this.objs = data;
		if (this.func_setdefault && this.robj)
			this.func_setdefault(this.robj, this.objs);
		//keep the origal data
		var len = this.objs.length;
		for(var i=0; i<len; i++){
			Bat.setIts(this.objs[i]);
		}
		this.evt.fireEvent("after_setdata", this, true);
	},

	// 根据view画关联界面
	load : function(params, reader, callback, scope, arg) {
		var me = this;
		if (params && params.start != undefined) {
			this.view.req.map.start = params.start || 0;
			this.view.req.map.rownum = params.limit;
		} else {
			if (!this.view.req.map.start)
				this.view.req.map.start = 0;
			else
				params = {
					start : this.view.req.map.start,
					limit : this.view.req.map.rownum
				};
		}
		if (this.reload) {
			var mask = new Ext.LoadMask(Ext.getBody(), {                           
				msg : '正在载入... ',                            
				removeMask : true  
			});  
			mask.show();
			// Ext.MessageBox.wait('请求数据，请稍候...');
			jsonrpc.BH.handle(function(data, ex) {
				if (Bat.outputEx(ex, '请求记录数异常'))
					return;
				me.total = data;
				me.view.req.map.method = 'G';
				
				jsonrpc.BH.handle(function(data, ex) {
					if (Bat.outputEx(ex, '请求记录数据异常'))
						return;
					//me.data = data;
					me.setData(data);
					me.refreshData(params, reader, callback, scope, arg);
					mask.hide();
						// Ext.MessageBox.hide();
					}, 'G', me.view.req, me.view.SN);
			}, 'C', me.view.req,  me.view.SN);
		} else {
			params.start = me.view.req.map.start;
			params.limit=me.view.req.map.rownum;
			me.refreshData(params, reader, callback, scope, arg);
		}
	}

});
